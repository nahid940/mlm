<?php
include_once 'vendor/autoload.php';
\App\Session::init();
date_default_timezone_set("Asia/Dhaka");
$user=new \App\user\User();

include("simple-php-captcha.php");

$_SESSION['captcha'] = simple_php_captcha();


if (isset($_POST['contact']))
{
    
    if($_POST['username']=='')
    {
        $errors[]='Please provide your username.';
    }
    if($_POST['name']=='')
    {
        $errors[]='Please provide your full name.';
    }
    if($_POST['ic_no']=='')
    {
        $errors[]='Please provide your ic number.';
    }
    if($_POST['email']=='')
    {
        $errors[]='Please provide your email address.';
    }
    
    if(empty($errors)){
        
        if ( strcasecmp($_POST['captcha'],\App\Session::get('code'))==0 ) {
        if (!empty($user->validateUserByUsername($_POST['username'])))
        {

           $sql="insert into messages (username,full_name,ic_no,email,message,message_date,message_time) values(:username,:full_name,:ic_no,:email,:message,:message_date,:message_time)";
            $stmt=\App\DBConnection::myQuery($sql);
            $stmt->bindValue(':username',$_POST['username']);
            $stmt->bindValue(':full_name',$_POST['name']);
            $stmt->bindValue(':ic_no',$_POST['ic_no']);
            $stmt->bindValue(':email',$_POST['email']);
            $stmt->bindValue(':message',$_POST['message']);
            $stmt->bindValue(':message_date',date('Y-m-d'));
            $stmt->bindValue(':message_time',date('h:i A'));
            $stmt->execute();
            $x=1;
        }else
        {
            \App\Session::set('error', "Username '".$_POST['username']."' could not be found.");
        }
    }
        
    }
    
    
    
}



\App\Session::set('code',$_SESSION['captcha']['code']);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Monspacea &raquo; Member's login</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="html_template/default/login_assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="html_template/default/login_assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <link href="html_template/default/assets/img/logo/favicon.ico" rel="icon" type="image/ico">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login" style="">

<!-- BEGIN LOGIN -->

<div class="content">
    <div class="logo">
        <img src="./html_template/default/login_assets/img/logo.png" border="0">
    </div>
    <div class="languageSize"><a href="?__lang=en" target="_self">English</a>  | <a href="?__lang=cn" target="_self">简体中文</a> | <a href="?__lang=kr" target="_self">Korean (조선말)</a> | <a href="?__lang=id" target="_self">Indonesian</a> | <a href="?__lang=th" target="_self">Thai</a> | <a href="?__lang=vn" target="_self">Vietnamese</a> | <a href="?__lang=jp" target="_self">Japanese</a></div>


    <?php if($x==1) { ?>
    
        <div class="alert alert-block alert-success fade in">			
			<h4>Thank you!</h4>
			Your message has been successfully sent. We will contact you very soon!
		</div>
        <center><a href="login.php" class="btn blue">Continue</a></center>
    
    <?php } else {?>
    <form class="form-vertical " action="" method="post" novalidate="novalidate">
        <input type="hidden" name="__req" value="1">
        <h3>CONTACT INFORMATION</h3>

        <?php
        
        if(\App\Session::get('error')) {?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <span><?php echo \App\Session::get('error')?></span>
            </div>
            <?php
            \App\Session::UnsetKeySession('error');
        }
        
        ?>
        
        <?php if(!empty($errors)){?>
        
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <?php foreach($errors as $err) {?>
                <span><?php echo $err?></span><br>
                <?php }?>
            </div>
        
        <?php }?>
        <div class="control-group">
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-user"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" autocomplete="off" name="username" value="">
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-pencil"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="Full Name" autocomplete="off" name="name" value="">
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-eye-open"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="IC No." autocomplete="off" name="ic_no" value="">
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-envelope"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="Contact Email" autocomplete="off" name="email" value="">
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-comment"></i>
                    <textarea class="m-wrap placeholder-no-fix" type="text" placeholder="Message" autocomplete="off" name="message" style="padding-left: 33px !important;" rows="4"></textarea>
                </div>
            </div>
        </div>
        <div class='control-group'>
            <label class='control-label visible-ie8 visible-ie9'>Password</label>
            <div class='controls'>
                <div class='input-icon left'>
                    <i class='fa fa-check-square'></i>
                    <?php
                    echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code" style="background-repeat: repeat-x;width:100%;height:55px;margin-bottom: 20px" >';
                    ?>
                    <input type='text' class='m-wrap placeholder-no-fix' autocomplete='off' name='captcha' placeholder='Captcha' >
                </div>
            </div>
        </div>
        <div class="form-actions">
            <a href="login.php" class="btn btn-light-grey">
                <i class="m-icon-swapleft"></i> Back
            </a>
            <button type="submit" class="btn blue pull-right" name="contact">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>
    <?php }?>
</div>

<!-- END LOGIN -->
<script src="html_template/default/login_assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="html_template/default/login_assets/scripts/app.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        App.init();
        Login.init();
    });
</script>


<!-- END BODY -->
<div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 265px; width: 1349px; z-index: -999999; position: fixed;"><img src="html_template/default/login_assets/img/bg/2.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1349px; height: 899.333px; max-width: none; z-index: -999999; left: 0px; top: -317.167px;"></div></body></html>