<?php
/**
 * Created by PhpStorm.
 * User: PC-1 Agami Group
 * Date: 1/3/2019
 * Time: 1:49 PM
 */

namespace App\nominee;


use App\DBConnection;
use App\Session;

class Nominee
{

    private $nominee_name;
    private $relationship_with_nominee;
    private $nominee_ic_no;
    private $userID;

    public function set($data)
    {
        if (array_key_exists('nominee_name', $data)) {
            $this->nominee_name = $data['nominee_name'];
        }
        if (array_key_exists('relationship_with_nominee', $data)) {
            $this->relationship_with_nominee = $data['relationship_with_nominee'];
        }
        if (array_key_exists('nominee_ic_no', $data)) {
            $this->nominee_ic_no = $data['nominee_ic_no'];
        }
    }

    public function modifyNominee()
    {
        if($this->checkNominee(Session::get('userID')))
        {
            $sql="insert into user_nominees (nominee_name,relationship_with_nominee,nominee_ic_no,userID)
        values(:nominee_name,:relationship_with_nominee,:nominee_ic_no,:userID)
        ";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':nominee_name',$this->nominee_name);
            $stmt->bindValue(':relationship_with_nominee',$this->relationship_with_nominee);
            $stmt->bindValue(':nominee_ic_no',$this->nominee_ic_no);
            $stmt->bindValue(':userID',Session::get('userID'));
            $stmt->execute();
        }else
        {
            $this->updateNominee(Session::get('userID'));
        }
    }



    public function updateNominee($userID)
    {
        $sql="update user_nominees set nominee_name=:nominee_name,
        relationship_with_nominee=:relationship_with_nominee,nominee_ic_no=:nominee_ic_no where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':nominee_name',$this->nominee_name);
        $stmt->bindValue(':relationship_with_nominee',$this->relationship_with_nominee);
        $stmt->bindValue(':nominee_ic_no',$this->nominee_ic_no);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }


    public function checkNominee($userID)
    {
        $sql="select userID from user_nominees where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return true;
        }else
        {
            return false;
        }
    }


    public function getNomineeInfo()
    {
        $sql='select * from user_nominees where userID=:userID';
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',Session::get('userID'));
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }



}