<?php
namespace App;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
\App\Session::init();

/**
 * Class EmailHelper
 *
 * Email Helper Class For PHP Mailer
 *
 * First setupEmail(), then sendEmail()
 */
class EmailHelper{

    private $hostName;
    private $userName;
    private $password;
    private $port;
    private $secure;
    private $fromName;
    private $fromEmail;
    private $smtpAuth;


    /**
     * setupEmail
     *
     * Setup Email Credentials
     *
     * @param $hostName
     * @param $userName
     * @param $password
     * @param $port
     * @param string $secure
     * @param $fromEmail
     * @param $fromName
     * @param bool $smtpAuth
     */
    public function setupEmail($hostName, $userName, $password, $port, $secure='tls', $fromEmail, $fromName,
                               $smtpAuth=true){
        $this->hostName = $hostName;
        $this->userName = $userName;
        $this->password = $password;
        $this->port = $port;
        $this->secure = $secure;
        $this->fromEmail = $fromEmail;
        $this->fromName = $fromName;
        $this->smtpAuth = $smtpAuth;
    }

    /**
     * sendMail
     *
     * Send Email to the User
     * @param $to
     * @param $name
     * @param $subject
     * @param $message
     * @param $header
     * @param array $attachment
     * @param bool $isHTML
     * @return bool
     */
    public function sendMail($to, $name, $subject, $message, $header=null, $isHTML=true){
        $mail = new PHPMailer(true);
        try {

            //Server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = $this->hostName;
            $mail->SMTPAuth = $this->smtpAuth;
            $mail->Username = $this->userName;
            $mail->Password = $this->password;
            $mail->SMTPSecure = $this->secure;
            $mail->Port = $this->port;

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->setFrom($this->fromEmail, $this->fromName);
            if (!is_null($header)){
                foreach ($header as $head){
                    $mail->addCustomHeader($head);
                }
            }

            // Add Recipients
            $mail->addAddress($to, $name);     // Add a recipient

//            // Multiple Attachments
//            if (count($attachment) > 0){
//                for ($i=0; $i < count($attachment); $i++){
//                    $mail->addAttachment($attachment[$i]);
//                }
//            }

            //Content
            $mail->isHTML($isHTML);// Set email format to HTML

//            $mail->From = 'mail.agami-group.com';
//            $mail->Sender = 'mail.agami-group.com';

            $mail->Subject = $subject;
            $mail->Body    = $message;


            if ($mail->send()){
               // \App\Session::set('success', 'Message has been sent');
                return true;
            }else{
                //\App\Session::set('error', 'Message has not been sent');
                return false;
            }

        } catch (Exception $e) {
            \App\Session::set('error', $mail->ErrorInfo);
        }
    }
}