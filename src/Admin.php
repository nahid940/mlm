<?php
/**
 * Created by PhpStorm.
 * User: PC-1 Agami Group
 * Date: 1/19/2019
 * Time: 1:46 PM
 */

namespace App;

use App\DBConnection;
use PDO;
use App\Session;
class Admin
{


    /**
     * ------------------------------new user Login-----------------------------------------------------------------------
     **/
    public function userLogin($user_name,$Password, $companyID, $branchID, $applicationID){
        // Set Time Zone to Asia/Dhaka
        date_default_timezone_set("Asia/Dhaka");
        // Get User IP
        $ipAddress = $this->getClientIPAddress();
        // Check IP in permanent_block table
        $sql="SELECT blockedIP FROM permanent_block WHERE blockedIP=? LIMIT 1";
        $statementBlockedIP = DBConnection::myQuery($sql);
        $statementBlockedIP->execute(array($ipAddress));
        $numBlockedIP = $statementBlockedIP->rowCount();
        // If IP is in permanentBlock IP list
        if ($numBlockedIP > 0){
            // Throw Error
            throw new \Exception("Your IP address is in permanent blocked ip address list. Please contact with administrator to remove your IP.");
        }else{
            // Check the IP in login_lockdown table
            $sql="SELECT lockedIP, releaseDate FROM login_lockdown WHERE lockedIP=?  ORDER BY SL DESC LIMIT 1";
            $statementLockedIP = DBConnection::myQuery($sql);
            $statementLockedIP->execute(array($ipAddress));
            $result = $statementLockedIP->fetch(PDO::FETCH_ASSOC);
            $numLockedIP = $statementLockedIP->rowCount();
            // If IP is in login_lockdown IP list
            if ($numLockedIP > 0){
                // Check time difference from now to releaseDate
                $releaseDate = $result['releaseDate'];
                $currentDateTime =  date("Y-m-d h:i:s"); // 2018-12-21 08:17:22
                if ($currentDateTime > $releaseDate) {
                    // Login with data
                    $this->doLoginFunction($user_name,$Password, $ipAddress, $companyID, $branchID, $applicationID);
                }else{
                    // Throw Error
                    //throw new Exception("Your IP address is locked for 30 minutes. Try after $releaseDate");
                }
            }else{
                // Login with data
                $this->doLoginFunction($user_name,$Password, $ipAddress, $companyID, $branchID, $applicationID);
            }
        }
        return true;
    }

    /**
     * doLoginFunction
     *
     * @param $user_name
     * @param $Password
     * @param $ipAddress
     * @param $companyID
     * @param $branchID
     * @param $applicationID
     * @return bool
     * @throws Exception
     */
    public function  doLoginFunction($user_name,$Password, $ipAddress, $companyID, $branchID, $applicationID){

        $sql="SELECT userID,username,full_name FROM users WHERE username=? AND password=? and user_type=1";

        $statementLoginAttempt=DBConnection::myQuery($sql);
        $statementLoginAttempt->execute(array($user_name,$Password));
        $result = $statementLoginAttempt->fetch(PDO::FETCH_ASSOC);
        $numSuccessfulLoginAttempt = $statementLoginAttempt->rowCount();
        if($numSuccessfulLoginAttempt > 0){
            // Store in login_activity table about the login information
            $loginDate = date("Y-m-d h:i:s");

            if ($ipAddress == "::1"){
                $loginCountry = "Bangladesh";
            }else{
                $loginCountry = $this->ipInformation("Visitor", "Country");
            }

            $loginActivityId = $this->getNextLoginActivityId();
            $sql="INSERT INTO login_activity (userID, loginDate, logoutDate, loginIP, loginCountry, companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $statementAddLoginAttemptCount=DBConnection::myQuery($sql);
            $statementAddLoginAttemptCount->execute(array($result['userID'], $loginDate, null, $ipAddress, $loginCountry, $companyID, $branchID, $applicationID));
            // Remove old login_attempts for this IP
            $this->removeAllLoginAttemptForIP($ipAddress);
            // Remove old login_lockdown for this IP
            $this->removeAllLoginLockDownForIP($ipAddress);

            /**
             * ------------------store data in session-----------------------------
             **/
            $sql="select permissionID from user_permission where user_id=:user_id";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':user_id',$result['userID']);
            $stmt->execute();
            $res=$stmt->fetchAll(\PDO::FETCH_ASSOC);
            $permissionidarray=[];
            if(count($res)>=1)
            {
                foreach ($res as $permissionid)
                {
                    $permissionidarray[]=$permissionid['permissionID'];
                }
            }

            Session::init();
            Session::set('adminlogin',true);
            Session::set('userID',$result['userID']);
            Session::set('username',$result['username']);
            Session::set('full_name',$result['full_name']);
            Session::set('permissions',$permissionidarray);
            header('location:index.php');
            /**
             * -----------------------------------------------------------------------
             **/
        }else{

            // Check how many times this IP in login_attempt_count table
            $sql="SELECT attemptIP FROM login_attempt_count WHERE attemptIP=?";
            $statementLoginAttemptCount=DBConnection::myQuery($sql);
            $statementLoginAttemptCount->execute(array($ipAddress));
            $loginAttemptCount = $statementLoginAttemptCount->rowCount();

            // Get next ID in login_attempt_count table
            $loginAttemptID = $this->getNextLoginAttemptCountId();

            // store in login_attempt_count table
            $attemptTime = date("Y-m-d h:i:s");
            $sql="INSERT INTO login_attempt_count (userLogin, attemptTime, attemptIP, companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?)";
            $statementAddLoginAttemptCount = DBConnection::myQuery($sql);
            $statementAddLoginAttemptCount->execute(array($user_name, $attemptTime, $ipAddress, $companyID, $branchID, $applicationID));


            // store in failed_login table
            $failedLoginDate = date("Y-m-d h:i:s");
            $sql="INSERT INTO failed_logins (userID, userLoginID, failedLoginDate, loginAttemptID, companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $statementAddLoginFailed=DBConnection::myQuery($sql);
            $statementAddLoginFailed->execute(array(1, $user_name, $failedLoginDate, $loginAttemptID, $companyID, $branchID, $applicationID));


            // If login attempt count == 3 that means now 3 times he tries to wrong access
            if ($loginAttemptCount == 3){
                /**
                Add his data to login_lockdown table, remove all data of this IP from
                login_attempt_count table and throw a message
                 **/

                // Add data to login_lockdown table
                $lockDownDateTime = date("Y-m-d h:i:s");
                $releaseDateTime = date("Y-m-d H:i:s", strtotime($lockDownDateTime . "+30 minutes"));
                $lockType = 1;
                $sql="INSERT INTO login_lockdown (userID, userLogin, lockedIP, lockDownDate, releaseDate,lockType,companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $statementAddLoginLockDown=DBConnection::myQuery($sql);
                $statementAddLoginLockDown->execute(array(0, $user_name, $ipAddress, $lockDownDateTime, $releaseDateTime, $lockType, $companyID, $branchID, $applicationID));

                // remove all data of this IP from login_attempt_count table
                $this->removeAllLoginAttemptForIP($ipAddress);
                // Throw an error
                //throw new Exception("Your IP address is locked for 30 minutes. Try after $releaseDateTime");
            }
            // Throw an error
//            throw new Exception("Your Username and Password combination is not correct ! Please give correct one");
            Session::set('login-failure',"<div class='alert alert-danger'>Invalid login Credentials !!</div>");
        }
    }

    /**
     * removeAllLoginAttemptForIP
     *
     * @param $ipAddress
     */
    public function removeAllLoginAttemptForIP($ipAddress) {
        $sql = "DELETE FROM login_attempt_count where attemptIP=?";
        $stmt = DBConnection::myQuery($sql);
        $stmt->execute(array($ipAddress));
    }

    /**
     * removeAllLoginLockDownForIP
     *
     * @param $ipAddress
     */
    public function removeAllLoginLockDownForIP($ipAddress) {
        $sql = "DELETE FROM login_lockdown where lockedIP=?";
        $stmt = DBConnection::myQuery($sql);
        $stmt->execute(array($ipAddress));
    }

    /**
     * getClientIPAddress
     *
     * @return string
     */
    public function getClientIPAddress() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


    /**
     * ipInformation
     *
     * Get any information for an IP address
     *  Like,
     *              echo ipInformation("Visitor", "Country"); // India
    echo ipInformation("Visitor", "Country Code"); // IN
    echo ipInformation("Visitor", "State"); // Andhra Pradesh
    echo ipInformation("Visitor", "City"); // Proddatur
    echo ipInformation("Visitor", "Address"); // Proddatur, Andhra Pradesh, India
     * @param null $ip
     * @param string $purpose
     * @param bool $deep_detect
     * @return array|null|string
     */
    public function ipInformation ($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            if (is_null($ip)){
                $ip = $_SERVER["REMOTE_ADDR"];
            }

            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    /**
     * getNextLoginAttemptCountId
     *
     * @return mixed
     */
    public function getNextLoginAttemptCountId(){
//        $statement = $this->db->prepare("SHOW TABLE STATUS LIKE 'login_attempt_count'");
        $sql="SHOW TABLE STATUS LIKE 'login_attempt_count'";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $row) {
            return $row[10];
        }
    }

    /**
     * getNextLoginActivityId
     *
     * @return mixed
     */
    public function getNextLoginActivityId(){
//        $statement = $this->db->prepare("SHOW TABLE STATUS LIKE 'login_activity'");
        $sql="SHOW TABLE STATUS LIKE 'login_activity'";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $row) {
            return $row[10];
        }
    }

    /**
     * updateLoginActivity
     *
     * @return bool
     */
    public function updateLoginActivity(){
        // Set Time Zone to Asia/Dhaka
        date_default_timezone_set("Asia/Dhaka");

        $logoutDate = date("Y-m-d h:i:s");
        $activityID = $_SESSION['login_activity_id'];

        $sql = "UPDATE login_activity SET logoutDate=? WHERE activityID=?";
//        $stmt = $this->db->prepare($sql);
        $stmt = DBConnection::myQuery($sql);
        $update_or_not = $stmt->execute(array($logoutDate, $activityID));

        if ($update_or_not)
            return true;
        else
            return false;
    }

    /**
     * permanentBlockedIpLists
     *
     * @return mixed
     */
    public function permanentBlockedIpLists(){
//        $statement = $this->db->prepare("SELECT * FROM permanent_block ORDER BY ID DESC");
        $sql="SELECT * FROM permanent_block ORDER BY ID DESC";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    /**
     * addBlockIP
     *
     * @param $blockedIP
     * @param $blockReason
     * @param $blockDate
     * @param $status
     * @param $companyID
     * @param $branchID
     * @param $applicationID
     * @return bool
     */
    public function addBlockIP($blockedIP, $blockReason, $blockDate, $status, $companyID, $branchID,
                               $applicationID){
//        $statementAddLoginLockDown = $this->db->prepare("INSERT INTO permanent_block (blockedIP, blockReason, blockedDate, status,companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $sql="INSERT INTO permanent_block (blockedIP, blockReason, blockedDate, status,companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $statementAddLoginLockDown=DBConnection::myQuery($sql);
        $insert_or_not = $statementAddLoginLockDown->execute(array($blockedIP, $blockReason, $blockDate, $status,
            $companyID, $branchID, $applicationID));
        if ($insert_or_not)
            return true;
        else
            return false;

    }

    /**
     * updateBlockIP
     *
     * @param $blockedIP
     * @param $blockReason
     * @param $blockDate
     * @param $status
     * @param $companyID
     * @param $branchID
     * @param $applicationID
     * @param $ID
     * @return bool
     */
    public function updateBlockIP($blockedIP, $blockReason, $blockDate, $status, $companyID, $branchID,
                                  $applicationID, $ID){
        $sql = "UPDATE permanent_block SET blockedIP=?, blockReason=?, blockedDate=?, status=?, companyID=?, branchID=?,
                               applicationID=? WHERE ID=?";
//        $stmt = $this->db->prepare($sql);
        $stmt=DBConnection::myQuery($sql);
        $update_or_not = $stmt->execute(array($blockedIP, $blockReason, $blockDate, $status, $companyID, $branchID,
            $applicationID, $ID));

        if ($update_or_not)
            return true;
        else
            return false;

    }

    /**
     * deletePermanentBlockIP
     *
     * @param $ID
     * @return bool
     */
    public function deletePermanentBlockIP($ID){
        $sql = "DELETE FROM permanent_block where ID=?";
//        $stmt = $this->db->prepare($sql);
        $stmt = DBConnection::myQuery($sql);
        $delete_or_not = $stmt->execute(array($ID));

        if ($delete_or_not)
            return true;
        else
            return false;
    }

    /**
     * lockedDownIPLists
     *
     * @return mixed
     */
    public function lockedDownIPLists(){
//        $statement = $this->db->prepare("SELECT * FROM login_lockdown ORDER BY SL DESC");
        $sql="SELECT * FROM login_lockdown ORDER BY SL DESC";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * deleteLockDownIP
     *
     * @param $SL
     * @return bool
     */
    public function deleteLockDownIP($SL){
        $sql = "DELETE FROM login_lockdown where SL=?";
//        $stmt = $this->db->prepare($sql);
        $stmt = DBConnection::myQuery($sql);
        $delete_or_not = $stmt->execute(array($SL));

        if ($delete_or_not)
            return true;
        else
            return false;
    }

    /**
     * isIpInPermanentBlockList
     *
     * @return bool
     */
    public function isIpInPermanentBlockList(){
        $ipAddress = $this->getClientIPAddress();

        // Check IP in permanent_block table
        $sql="SELECT blockedIP FROM permanent_block WHERE blockedIP=? LIMIT 1";
        $statementBlockedIP=DBConnection::myQuery($sql);
//        $statementBlockedIP = $this->db->prepare("SELECT blockedIP FROM permanent_block WHERE blockedIP=? LIMIT 1");
        $statementBlockedIP->execute(array($ipAddress));
        $numBlockedIP = $statementBlockedIP->rowCount();

        if ($numBlockedIP > 0)
            return true;

        return false;
    }

    /**
     * isIPInLockedDownList
     *
     * @return bool
     */
    public function isIPInLockedDownList(){
        $ipAddress = $this->getClientIPAddress();

        $sql="SELECT lockedIP, releaseDate FROM login_lockdown WHERE lockedIP=?  ORDER BY SL DESC LIMIT 1";
//        $statementLockedIP = $this->db->prepare("SELECT lockedIP, releaseDate FROM login_lockdown WHERE lockedIP=?  ORDER BY SL DESC LIMIT 1");
        $statementLockedIP=DBConnection::myQuery($sql);
        $statementLockedIP->execute(array($ipAddress));
        $result = $statementLockedIP->fetch(PDO::FETCH_ASSOC);
        $numLockedIP = $statementLockedIP->rowCount();

        if ($numLockedIP > 0){
            // Check time difference from now to releaseDate
            $releaseDate = $result['releaseDate'];

            $currentDateTime =  date("Y-m-d h:i:s"); // 2018-12-21 08:17:22
            if ($currentDateTime > $releaseDate)
            {
                return false;
            }else
            {
                return true;
            }
        }
        return false;
    }
    /**
     * --------------------------------------------------------------------------------------------------------
     **/



    public function getUserPermissionInfo($user_id)
    {
        $sql="select * from user_permission where user_id=:user_id and user_id!=:logged_inuser";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':user_id',$user_id);
        $stmt->bindValue(':logged_inuser',Session::get('user_id'));
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function removeUserPermission($user_id)
    {
        $sql="delete from user_permission where user_id=:user_id";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':user_id',$user_id);
        $stmt->execute();
    }
    
    
     public function getUserAddress($user_id)
    {
        $sql="select districtID,upazilaID from admin_address where adminID=:user_id";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':user_id',Session::get('userID'));
        $stmt->execute();
        $res=$stmt->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }

}