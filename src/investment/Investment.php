<?php

namespace App\investment;
use App\DBConnection;
use App\Session;
use PDO;
class Investment
{

    public function newInvestment($userID,$investment_typeID,$investment_packageID,
    $invest_amount,$initial_rate,$final_rate,$initial_date,$invest_start_date,$invest_closing_date,$invest_status,$approval)
    {
        $sql="insert into invests(userID,invest_typeID,invest_packageID,
        invest_amount,initial_rate,final_rate,invest_initial_date,invest_start_date,invest_closing_date,invest_status,approved_userID)
        
        values(:userID,:invest_typeID,:invest_packageID,
        :invest_amount,:initial_rate,:final_rate,:invest_initial_date,:invest_start_date,:invest_closing_date,:invest_status,:approved_userID)
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':invest_typeID',$investment_typeID);
        $stmt->bindValue(':invest_packageID',$investment_packageID);
        $stmt->bindValue(':invest_amount',$invest_amount);
        $stmt->bindValue(':initial_rate',$initial_rate);
        $stmt->bindValue(':final_rate',$final_rate);
        $stmt->bindValue(':invest_initial_date',$initial_date);
        $stmt->bindValue(':invest_start_date',$invest_start_date);
        $stmt->bindValue(':invest_closing_date',$invest_closing_date);
        $stmt->bindValue(':invest_status',$invest_status);
        $stmt->bindValue(':approved_userID',$approval);
        $stmt->execute();
    }
    
    public function getAllInvestTypes()
    {
        $sql="select investment_typeID,investment_type_name from investment_types";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    } 
    
    public function getAllInvestmentPackages()
    {
        $sql="select packageID,package_name,investment_typeID from investment_packages where package_status=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getInvestmentAmount($id)
    {
        $sql="select investment_amount,investment_typeID,return_rate,package_duration from investment_packages where packageID=:packageID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':packageID',$id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getTC()
    {
        $sql="select tc from user_amounts where userID=:userID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',Session::get('userID'));
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    /**-----------------------------------new--------------------------------------**/
    public function getinvestmentReturnRate($id)
    {
        $sql="select return_rate from investment_packages where packageID=:packageID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':packageID',$id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    /**------------------------payout plan-----------------------------------------**/
    
    public function getAllPayoutPlan()
    {
        $sql="select planID,planName from payout_plans where status=1";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getPayoutPlan($id)
    {
        $sql="select * from payout_plans where status=1 and planID=:planID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':planID',$id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getLimitedPayoutPlan($id)
    {
        $sql="select planCash,planMSP,planPP from payout_plans where status=1 and planID=:planID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':planID',$id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    public function getUserPayoutBalance()
    {
        $sql="select userID,full_name,bdt from user_amounts
        join users on users.userID=user_amount.userID
        ";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    
    public function dateCountOfFirstInvest($userID)
    {
        $sql="select DATEDIFF(CURDATE(),invest_start_date) as total from invests where userID=:userID order by investID asc limit 1";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if($res['total']>30)
        {
            return true;
        }else
        {
            return false;
        }
    }
    
    public function dateCountOfFirstReg($userID)
    {
        $sql="select DATEDIFF(CURDATE(),user_registered_date) as total from users where userID=:userID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if($res['total']>30)
        {
            return true;
        }else
        {
            return false;
        }
    }
    
    public function getTypeWisePackage($typeID)
    {
        $sql="select packageID,package_name from investment_packages where investment_typeID=:investment_typeID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':investment_typeID',$typeID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function updateInvestStatus($id)
    {
        $sql="update invests set invest_status=6 where investID=:investID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':investID',$id);
        $stmt->execute();
    }
    
    public function returnTC($tc,$userID)
    {  
        $sql="update user_amounts set tc=:tc where userID=:userID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':tc',$tc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function getTransferMultiples()
    {
        $sql="select * from transfer_multiples";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getTransferMultiplesByColumn($column)
    {
        $sql="select $column from transfer_multiples";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
}