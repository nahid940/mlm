<?php
/**
 * Created by PhpStorm.
 * User: PC-1 Agami Group
 * Date: 12/26/2018
 * Time: 12:54 PM
 */

namespace App\client;


use App\DBConnection;
use App\Session;
class Client
{

    private $userID;
    private $username;
    private $parentid;
    private $sponsorid;
    private $root_parent;
    private $hand;
    private $client_level;
    private $packageID;

    private $security_password;

    public function set($data)
    {
        if (array_key_exists('userID', $data)) {
            $this->userID = $data['userID'];
        }
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('parentid', $data)) {
            $this->parentid = $data['parentid'];
        }
        if (array_key_exists('sponsorid', $data)) {
            $this->sponsorid = $data['sponsorid'];
        }
        if (array_key_exists('root_parent', $data)) {
            $this->root_parent = $data['root_parent'];
        }
        if (array_key_exists('hand', $data)) {
            $this->hand = $data['hand'];
        }
        if (array_key_exists('client_level', $data)) {
            $this->client_level = $data['client_level'];
        }
        if (array_key_exists('packageID', $data)) {
            $this->packageID = $data['packageID'];
        }
    }
    
    
    public function getUserIDbyUserName($username,$loggedinUserID)
    {
        $sql="select userID from clients where username=:username limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(!empty($res))
        {
            $sql="select user_positionID from client_positions where parent_id=:parent_id and user_id=:user_id limit 1";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':parent_id',$loggedinUserID);
            $stmt->bindValue(':user_id',$res['userID']);
            $stmt->execute();
            $res1=$stmt->fetch(\PDO::FETCH_ASSOC);
            if(!empty($res1))
            {
                return true;
            }else
            {
                return false;
            }
        }else
        {
            return false;
        }
    }
    
    

    public function addNewClient($rf,$dc,$pc)
    {
        $sql="insert into clients (userID,username,parent_id,sponsor_id,root_parent,hand,client_level,packageID,RF,DC,PC)
        values(:userID,:username,:parent_id,:sponsor_id,:root_parent,:hand,:client_level,:packageID,:RF,:DC,:PC)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$this->userID);
        $stmt->bindValue(':username',$this->username);
        $stmt->bindValue(':parent_id',$this->parentid);
        $stmt->bindValue(':sponsor_id',$this->sponsorid);
        $stmt->bindValue(':root_parent',$this->root_parent);
        $stmt->bindValue(':hand',$this->hand);
        $stmt->bindValue(':client_level',$this->client_level);
        $stmt->bindValue(':packageID',$this->packageID);
        $stmt->bindValue(':RF',$rf);
        $stmt->bindValue(':DC',$dc);
        $stmt->bindValue(':PC',$pc);
        $stmt->execute();
        return true;
    }

    /**----------------------- ------- store client and their parent position----------------------------*/
    public function storePosition($parentid,$userid,$hand,$client_level)
    {
        $sql="insert into client_positions (parent_id,user_id,hand,client_level,action_date,action_time)
        values(:parent_id,:user_id,:hand,:client_level,:action_date,:action_time)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id',$parentid);
        $stmt->bindValue(':user_id',$userid);
        $stmt->bindValue(':hand',$hand);
        $stmt->bindValue(':client_level',$client_level);
        $stmt->bindValue(':action_date',date('Y-m-d'));
        $stmt->bindValue(':action_time',date('H:i:s A'));
        $stmt->execute();
    }

     /**
     * -------------------------------------------users genealogy------------------------------------------------
    **/
    public function getUserInfo($username)
    {
        $sql="select clients.username,RF,DC,PC,full_name,nick_name,grade_id,user_registered_date,expire_date ,clients.userID,client_level,clients.packageID,package_type,packageName,hand from clients
        join users on users.userID=clients.userID
        join packages on packages.packageID=clients.packageID
        where clients.username=:username limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('username',$username);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function usersGenealogy($id)
    {
        $sql="select clients.username,RF,DC,PC,full_name,nick_name,user_registered_date,expire_date ,clients.userID,client_level,clients.packageID,packageName,hand
        from clients
        join users on users.userID=clients.userID
        join packages on packages.packageID=clients.packageID
        where parent_id=:parent_id and hand!=-1 limit 2";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('parent_id',$id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**-------------------left and right user genealogy--------------------------------------**/
    public function leftUsersGenealogy($id)
    {
        $sql="select clients.username,RF,DC,PC,full_name,nick_name,grade_id,user_registered_date,expire_date ,clients.userID,client_level,clients.packageID,packageName,hand
        from clients
        join users on users.userID=clients.userID
        join packages on packages.packageID=clients.packageID
        where parent_id=:parent_id and hand=0 and hand!=-1 limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('parent_id',$id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function rightUsersGenealogy($id)
    {
        $sql="select clients.username,RF,DC,PC,full_name,nick_name,grade_id,user_registered_date,expire_date ,clients.userID,client_level,clients.packageID,packageName,hand
        from clients
        join users on users.userID=clients.userID
        join packages on packages.packageID=clients.packageID
        where parent_id=:parent_id and hand=1 and hand!=-1 limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('parent_id',$id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }



    public function leftUsersLimitedInfo($id)
    {
        $sql="select clients.username
        from clients
        where parent_id=:parent_id and hand=0 and hand!=-1 limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('parent_id',$id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function rightUsersLimitedInfo($id)
    {
        $sql="select clients.username
        from clients
        where parent_id=:parent_id and hand=1 and hand!=-1 limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('parent_id',$id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------------------------**/


    /**-------------------------------------------- get nearest parent of a user----------------------------------**/
    public function getNearestParent($username)
    {
        $sql="select users.username from clients
          join users on users.userID=clients.parent_id
          where clients.username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['username'];
    }
    /**----------------------------------------------------------------------------------------------------------**/


    /**------------------------------count total left users------------------------------------------------------**/
    public function countTotalLeftUser($parentId)
    {
        $sql="select count(hand) as total from client_positions where parent_id=:parent_id and hand=0";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id',$parentId);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**-------------------------------------------------------------------------------------*------------*/

    /**--------------------------------count total right users--------------------------------------------**/
    public function countTotalRightUser($parentId)
    {
        $sql="select count(hand) as total from client_positions where parent_id=:parent_id and hand=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id',$parentId);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**------------------------------------------------------------------------------------------------------**/


    /**-------------------------- check if user can transfer with one another---------------------------------**/
    // public function checkUser($parentID,$userID)
    // {
    //     $sql="select user_positionID from client_positions 
    //     where (parent_id=:parent_id and user_id=:user_id) or (parent_id=:user_id and user_id=:parent_id)";
    //     $stmt=DBConnection::myQuery($sql);
    //     $stmt->bindValue(':parent_id',$parentID);
    //     $stmt->bindValue(':user_id',$userID);
    //     $stmt->execute();
    //     $res=$stmt->fetch(\PDO::FETCH_ASSOC);
    //     if(!empty($res))
    //     {
    //         return true;
    //     }else
    //     {
    //         return false;
    //     }
    // }
    
    public function checkUser($parentID,$userID)
    {
        
        if($parentID==1)
        {
            return true;
        }else if($parentID==$userID)
        {
            return true;
        }
        
        else{
           $sql="select user_positionID from client_positions 
            where (parent_id=:parent_id and user_id=:user_id) or (parent_id=:user_id and user_id=:parent_id)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':parent_id',$parentID);
            $stmt->bindValue(':user_id',$userID);
            $stmt->execute();
            $res=$stmt->fetch(\PDO::FETCH_ASSOC);
            if(!empty($res))
            {
                return true;
            }else
            {
                return false;
            }  
        }
    }
    /**--------------------------------------------------------------------------------------------------------**/
    
    //get user id of parent user
    public function getParentID($username)
    {
        $sql="select userID from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['userID'];
        
    }


    /**-----------------------------------store client transaction history-------------------------------------**/
    public function storeTransactionHistory($uid,$receiverID,$amount,$currency_type,$transaction_type,$remark)
    {
        $sql="insert into user_transactions
        (userID,receiverID,amount,currency_type,transaction_type,transaction_date,transaction_time,remark)
        values(:userID,:receiverID,:amount,:currency_type,:transaction_type,:transaction_date,:transaction_time,:remark)
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$uid);
        $stmt->bindValue(':receiverID',$receiverID);
        $stmt->bindValue(':amount',$amount);
        $stmt->bindValue(':currency_type',$currency_type);
        $stmt->bindValue(':transaction_type',$transaction_type);
        $stmt->bindValue(':transaction_date',date('Y-m-d'));
        $stmt->bindValue(':transaction_time',date('h:i:s A'));
        $stmt->bindValue(':remark',$remark);
        $stmt->execute();
    }
    /**---------------------------------------------------------------------------------------------------------**/


    /**---------------------------increase registration fee for receiver-----------------------------------**/
    public function increaseRegistrationFeeCurrency($userID,$amount)
    {
        $sql="select userID,rf from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,rf) values(:userID,:rf)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':rf',$amount);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set rf=:rf where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':rf',$amount+$res['rf']);
            $stmt->execute();
        }
    }

    /**----------------------------------------------------------------------------------------------------**/




    /**------------------------decrease registration fee from user-----------------------------**/
    public function decreaseRegistrationFee($userID,$amount)
    {
        $sql="select rf from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(\PDO::FETCH_ASSOC);
            $sql="update user_amounts set rf=:rf where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':rf',$res['rf']-$amount);
            $stmt->execute();
        }
    }
    /**-----------------------------------------------------------------------------------------**/






    /**------------------------decrease registration fee from user-----------------------------**/
    public function decreaseDeclarationFee($userID,$amount)
    {
        $sql="select dc from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(\PDO::FETCH_ASSOC);
            $sql="update user_amounts set dc=:dc where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':dc',$res['dc']-$amount);
            $stmt->execute();
        }
    }
    /**-----------------------------------------------------------------------------------------**/


    /**------------------------decrease product currrency fee from user-----------------------------**/
    public function decreaseProductCurrencyFee($userID,$amount)
    {
        $sql="select pc from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(\PDO::FETCH_ASSOC);
            $sql="update user_amounts set pc=:pc where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':pc',$res['pc']-$amount);
            $stmt->execute();
        }
    }
    /**-----------------------------------------------------------------------------------------**/


    public function decreaseMSPCurrencyFee($userID,$amount)
    {
        $sql="select msp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(\PDO::FETCH_ASSOC);
            $sql="update user_amounts set msp=:msp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':msp',$res['msp']-$amount);
            $stmt->execute();
        }
    }


    public function decreasePPCurrencyFee($userID,$amount)
    {
        $sql="select pp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(\PDO::FETCH_ASSOC);
            $sql="update user_amounts set pp=:pp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':pp',$res['pp']-$amount);
            $stmt->execute();
        }
    }



    /**---------------------------increase registration fee for receiver-----------------------------------**/
    public function increaseDeclarationCurrency($userID,$amount)
    {
        $sql="select userID,dc from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,dc) values(:userID,:dc)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':dc',$amount);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set dc=:dc where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':dc',$amount+$res['dc']);
            $stmt->execute();
        }
    }

    /**--------------------------------------------------------------------------------------------------------**/



    /**---------------------------increase product currency fee for receiver-----------------------------------**/
    public function increaseProductCurrency($userID,$amount)
    {
        $sql="select userID,pc from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,pc) values(:userID,:pc)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':pc',$amount);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set pc=:pc where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':pc',$amount+$res['pc']);
            $stmt->execute();
        }
    }

    /**----------------------------------------------------------------------------------------------------**/



    /**---------------------------increase registration fee for receiver-----------------------------------**/
    public function increaseMSPCurrency($userID,$amount)
    {
        $sql="select userID,msp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,msp) values(:userID,:msp)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':msp',$amount);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set msp=:msp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':msp',$amount+$res['msp']);
            $stmt->execute();
        }
    }

    /**----------------------------------------------------------------------------------------------------**/



    public function increasePPCurrency($userID,$amount)
    {
        $sql="select userID,pp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,pp) values(:userID,:pp)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':msp',$amount);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set pp=:pp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':pp',$amount+$res['pp']);
            $stmt->execute();
        }
    }
    
    
    public function getAllSpondoredClients($loggedin_user_id)
    {
        $sql="select clients.username,full_name,packageID,RF,DC,PC,user_registered_date from clients 
        join users on users.userID=clients.userID where sponsor_id=:sponsor_id order by clients.userID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':sponsor_id',$loggedin_user_id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
     public function getSpondoredClient($username)
    {
        $sql="select clients.username,full_name,packageID,RF,DC,PC,user_registered_date from clients 
        join users on users.userID=clients.userID where clients.username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


}