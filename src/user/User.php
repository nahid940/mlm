<?php
/**
 * Created by PhpStorm.
 * User: PC-1 Agami Group
 * Date: 12/26/2018
 * Time: 10:09 AM
 */

namespace App\user;


use App\DBConnection;
use App\Session;
use PDO;

class User
{

    private $username;
    private $password;
    private $full_name;
    private $nick_name;
    private $IC_no;
    private $wechat_ID;
    private $email;
    private $address;
    private $country;
    private $contact_no;
    private $security_password;

    public function set($data)
    {
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('full_name', $data)) {
            $this->full_name = $data['full_name'];
        }
        if (array_key_exists('nick_name', $data)) {
            $this->nick_name = $data['nick_name'];
        }
        if (array_key_exists('IC_no', $data)) {
            $this->IC_no = $data['IC_no'];
        }
        if (array_key_exists('wechat_ID', $data)) {
            $this->wechat_ID = $data['wechat_ID'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('address', $data)) {
            $this->address = $data['address'];
        }
        if (array_key_exists('country', $data)) {
            $this->country = $data['country'];
        }
        if (array_key_exists('contact_no', $data)) {
            $this->contact_no = $data['contact_no'];
        }
        if (array_key_exists('security_password', $data)) {
            $this->security_password = $data['security_password'];
        }
    }


    /**-----------------------Check user validity----------------**/
    public function checkUserValidity($userid)
    {
        $sql="select user_status from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userid);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
//        if($res['user_status']==0 || $res['user_status']==6 || $res['user_status']==5)
        if($res['user_status']==0 || $res['user_status']==2 )
        {
            header('location:profile-password.php');
        }else if($res['user_status']==3)
        {
            header('location:renew.php');
        }
    }


    /**----------------------------------------------------------**/


    /**----------------update user password----------------------**/
    public function updateUserPss($userid)
    {
        $sql="select pss_update from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userid);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);

        if($res['pss_update']==0)
        {
            $sql="update users set pss_update=:pss_update where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':pss_update',1);
            $stmt->bindValue(':userID',Session::get('userID'));
            $stmt->execute();
        }

    } 
    
    
    public function countChild($userID)
    {
        $sql="select count(root_parent) as total from clients where root_parent=:root_parent";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':root_parent',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }
    
    public function updateUserSecpss($userid)
    {
        $sql="select sec_p_update from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userid);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);

        if($res['sec_p_update']==0  )
        {
            $sql="update users set sec_p_update=:sec_p_update where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':sec_p_update',1);
            $stmt->bindValue(':userID',Session::get('userID'));
            $stmt->execute();
        }

    }
    /**----------------------------------------------------------**/


    public function registerUserOnEcommerce($username,$password,$email,$full_name,$contact)
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "http://ecommerce.agamisoft.net/api/msm-user-reg.php");
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, "username=$username&password=$password&email=$email&FullName=$full_name&mobile=$contact");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $x = curl_exec($handle);
        curl_close($handle);
    }


    public function updateStatus()
    {
        $sql="select sec_p_update,pss_update from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',Session::get('userID'));
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        
        if($res['sec_p_update']==1 && $res['pss_update']==1)
        {
            $sql="update users set user_status=:user_status where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':user_status',1);
            $stmt->bindValue(':userID',Session::get('userID'));
            if($stmt->execute())
            {
                $sql="select username,password,full_name,email,contact_no from users where userID=:userID";
                $stmt=DBConnection::myQuery($sql);
                $stmt->bindValue(':userID',Session::get('userID'));
                $stmt->execute();
                $res=$stmt->fetch(PDO::FETCH_ASSOC);
                $this->registerUserOnEcommerce($res['username'],md5(base64_decode($res['password'])),$res['email'],$res['full_name'],$res['contact_no']);
            }
        }
    }



    /**----------------get all users----------------------**/
    public function getTotalUser()
    {
        $sql="select count(userID) as total from users where  user_type!=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**----------------------------------------------------------**/


    /**----------------get all users for today----------------------**/
    public function getTotalForToadyUser()
    {
        $sql="select count(userID) as total from users where  user_type!=1 and user_registered_date=:user_registered_date";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':user_registered_date',date('Y-m-d'));
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**----------------------------------------------------------**/


    /**----------------get all new MSP withdraw request----------------------**/
    public function getNewMSPWithdrawRequestToday()
    {
        $sql="select count(purchaseID) as total from purchase_history where  admin_approval=0 and action_date=:action_date";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':action_date',date('Y-m-d'));
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**----------------------------------------------------------**/

    /**----------------get total package----------------------**/
    public function getTotalPackage()
    {
        $sql="select count(productPackageID) as total from productpackages";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**----------------------------------------------------------**/


    /**
     * --------validate input -----------------------------------
    **/
    
    public function validateInput($data)
    {
        $inputarray=array();

        if($data['full_name']=='')
        {
            $inputarray[]="'Full name' is a required field.";
        }
        if($data['nick_name']=='')
        {
            $inputarray[]="'Nick Name' is a required field.";
        }
        if($data['IC_no']=='')
        {
            $inputarray[]="'IC No' is a required field.";
        }
        if($data['contact_no']=='')
        {
           
           $inputarray[]="'Contact No' is a required field.";
        }
        if($data['email']=='')
        {
            $inputarray[]="'Email' is a required field.";
        }
        
         if($data['address']=='')
        {
            $inputarray[]="'Address' is a required field.";
        }
        
        
        if($data['country']=='')
        {
            $inputarray[]="'Country' is a required field.";
        }if($data['ref']=='')
        {
            $inputarray[]="'Upline username' is a required field.";
        }
        if($data['parent']=='')
        {
            $inputarray[]="'Placement username' is a required field.";
        }
        if($data['hand']=='')
        {
            $inputarray[]="'Client position' is a required field.";
        }
        
        if($data['username']=='')
        {
            $inputarray[]="'Username' is a required field.";
        }
        if($data['package']=='')
        {
            $inputarray[]="You must select one of the package.";
        }
        
        if(!preg_match('/^[a-zA-Z,]+$/i', $data['username']) )
        {
            $inputarray[]="'Username' must be a combination of alphabets";
        }
        
        if($data['sec_pwd']=='')
        {
            $inputarray[]="'Security Password' is a required field.";
        }
        Session::set('inputarray',$inputarray);
        if(empty($inputarray))
        {
            return true;
        }else
        {
            return false;
        }
    }
    
    
    // public function validateInput($data)
    // {
    //     if (
    //         //check pattern for username
    //         preg_match('/^[a-zA-Z,]+$/i', $data['username']) &&
    //         $data['full_name']!='' &&
    //         $data['nick_name']!=''
    //         && $data['IC_no']!=''
    //         && $data['email']!=''
    //         && $data['contact_no']!=''
    //         && $data['address']!=''
    //         && $data['country']!='' ) {
    //         return true;
    //     }else
    //     {
    //         $inputarray=["'Full Name' is a required field.", "'Nick Name' is a required field.","'IC NO' is a required field.","'Email' is a required field.","'Contact No' is a required field.","'Address' is a required field.","'Country' is a required field."];
    //         Session::set('inputarray',$inputarray);
    //         return false;
    //     }
    // }

    /**----------------------------get parent and all sponsor----------------------**/
    public function getAllSponsorUser($sponsorid)
    {
        $sql="select users.userID as userID,users.username,full_name,nick_name,clients.packageID,package_type,user_registered_date,RF,DC,PC,grade_id from clients 
        join users on users.userID=clients.userID
        join packages on packages.packageID=clients.packageID
        where sponsor_id=:sponsor_id
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':sponsor_id',$sponsorid);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**----------------------------------------------------------------------------**/


    /**
     * ------------ add new user data into users table---------------
    **/
    public function addNewUser($refuser)
    {
        if($this->checkExistingUser($this->username) && $this->checkUplineUserOnformSubmit($refuser)) {
            $sql = "insert into users(username,password,full_name,nick_name,IC_no,wechat_ID,email,address,country,contact_no,user_registered_date,expire_date,security_password,grade_id)
        values(:username,:password,:full_name,:nick_name,:IC_no,:wechat_ID,:email,:address,:country,:contact_no,:user_registered_date,:expire_date,:security_password,:grade_id)";
            $stmt = DBConnection::myQuery($sql);
            $stmt->bindValue(':username', $this->username);
            $stmt->bindValue(':password', $this->password);
            $stmt->bindValue(':full_name', $this->full_name);
            $stmt->bindValue(':nick_name', $this->nick_name);
            $stmt->bindValue(':IC_no', $this->IC_no);
            $stmt->bindValue(':wechat_ID', $this->wechat_ID);
            $stmt->bindValue(':email', $this->email);
            $stmt->bindValue(':address', $this->address);
            $stmt->bindValue(':country', $this->country);
            $stmt->bindValue(':contact_no', $this->contact_no);
            $stmt->bindValue(':user_registered_date', date('Y-m-d'));
            $stmt->bindValue(':expire_date', date('Y-m-d',strtotime('+300 days',strtotime(date('Y-m-d'))))); // expire after 300 days
            $stmt->bindValue(':security_password', $this->security_password);
            $stmt->bindValue(':grade_id', 1);
            $stmt->execute();
            return DBConnection::myDb()->lastInsertId();
        }
    }
    /**
     *------------------------------------------------------------------------
     **/


    /**--------------------get all user id-------------------------------**/

    public function getAlluserIDAndName()
    {
        $sql="SELECT bonus.userID,username from bonus
            join users on users.userID=bonus.userID
            where bonus_type=1 GROUP BY bonus.userID  having count(bonus.userID)=5
        ";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**----------------------------------------------------------------**/


    /**--------------------get each user's username and fullname-------------------------------**/
    public function getUserUsernameAndFullname($userID)
    {
        $sql="select username,full_name from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID', $userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**----------------------------------------------------------------**/


    /**-----------------------update user info---------------------------------**/
    public function updateUserInfo()
    {
        $sql="update users set nick_name=:nick_name,wechat_ID=:wechat_ID,
        email=:email,address=:address,country=:country,contact_no=:contact_no where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':nick_name', $this->nick_name);
        $stmt->bindValue(':wechat_ID', $this->wechat_ID);
        $stmt->bindValue(':email', $this->email);
        $stmt->bindValue(':address', $this->address);
        $stmt->bindValue(':country', $this->country);
        $stmt->bindValue(':contact_no', $this->contact_no);
        $stmt->bindValue(':userID', Session::get('userID'));
        $stmt->execute();
    }
    /**------------------------------------------------------------------------**/


    /**
     *---function to generate random password----------------------------
    **/
    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return (implode($pass)); //turn the array into a string
    }


    function randomSecPassword() {
        $alphabet = "abcdefghijkl0123fghijk456789mnopqrstuwxyz";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return (implode($pass)); //turn the array into a string
    }

    /**
     *-----------------------------------------------------------------------------
     **/


    /**
     * --------------------check existing upline user-------------------------------
     **/
    public function checkUplineUserOnformSubmit($username)
    {
        $sql="select username,full_name from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(!empty($res))
        {
            return true;
        }
    }
    /**
     * -----------------------------------------------------------------------------
    **/

    /**
     * --------------------check existing user-------------------------------
    **/
    public function checkExistingUser($username)
    {
        $sql="select username,full_name from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return true;
        }else
        {
            return false;
        }
    }


    /**--------------- check existing user on ecommerce-----------------**/
    public function checkUserOnEcommerce($username)
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "http://ecommerce.agamisoft.net/api/user-signup.php");
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, "username=$username");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $x = curl_exec($handle);
        curl_close($handle);
        if($x==1)
        {
            return true;
        }else
        {
            return false;
        }
    }


    /**
     * --------------------check existing upline user on ajax call-------------------------------
     **/
    public function checkUplineUser($username)
    {
        $sql="select username,full_name,nick_name from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return false;
        }else
        {
            return $res;
        }
    }
    /**
     * -----------------------------------------------------------------------------------------------
     **/
     
     
     /**------------------check upline user---------------------**/
     
     
     
    public function uplineUserDetails($username)
    {
        $sql="select userID,username from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return false;
        }else
        {
            return $res;
        }
    }
     
     
     /**-----------------------------------------------------------**/

    /**
     * ----------------------------- get placement user details----------------------------------------
    **/
    public function placementUserDetails($username)
    {
        $sql="select userID,username,full_name from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return false;
        }else
        {
            return $res;
        }
    }


    /**
     * ------------------------ get information by user id---------------------
    **/
    public function placementUserDetailsByID($id)
    {
        $sql="select userID,username from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$id);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return false;
        }else
        {
            return $res;
        }
    }
    /**
     * -------------------------------------------------------------------------------------
    **/


    /**-------------------------update user password------------------------------------**/
    public function checkExistingPassword($password)
    {
        $sql="select userID from users where userID=:userID and password=:password";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('userID',Session::get('userID'));
        $stmt->bindValue('password',$password);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return false;
        }else{
            return true;
        }
    }

    public function checkExistingSecurityPassword($password)
    {
        $sql="select userID from users where userID=:userID and security_password=:security_password";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('userID',Session::get('userID'));
        $stmt->bindValue('security_password',$password);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return false;
        }else{
            return true;
        }
    }


    // public function updatePassword($password,$userID)
    // {
    //     $sql="update users set password=:password where userID=:userID";
    //     $stmt=DBConnection::myQuery($sql);
    //     $stmt->bindValue(':userID',$userID);
    //     $stmt->bindValue(':password',$password);
    //     if($stmt->execute())
    //     {
    //         return true;
    //     }
    // }
    
    
     public function updatePassword($password,$userID)
    {
        $sql="select password from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $pass=$stmt->fetch(PDO::FETCH_ASSOC);

        if($pass['password']==$password)
        {
            Session::set('error','Existing Password and Password cannot set the same password');

        }else
        {
            $sql="update users set password=:password where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':password',$password);
            if($stmt->execute())
            {
                return true;
            }
        }
    }
    
    public function updateSts($userID)
    {
        $sql="update users set user_status=:user_status,sec_p_update=:sec_p_update, pss_update=:pss_update where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':user_status',1);
        $stmt->bindValue(':sec_p_update',1);
        $stmt->bindValue(':pss_update',1);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }


    public function updateSecurityPassword($password,$userID)
    {
        $sql="update users set security_password=:security_password where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':security_password',$password);
        if($stmt->execute())
        {
            return true;
        }
    }


    public function getAllSubAccounts($root_parent)
    {
        $sql="select userID from clients where root_parent=:root_parent and userID!=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$root_parent);
        $stmt->bindValue(':root_parent',$root_parent);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**----------------------------------------------------------------------------------**/

    /**
     * ------------------------Check parents hand----------------------------------
     **/
    public function parentUserHand($parentid)
    {
        $sql="select count(system_userID) as total from clients where (hand=0 or hand=1) and userID!=parent_id and parent_id=:parent_id";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id',$parentid);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if($res['total']==2)
        {
            return false;
        }else
        {
            return true;
        }
    }
    /**
     * --------------------------------------------------------------------------------
    **/
    
    
     public function getParentLevel($id)
    {
        $sql="select client_level from clients where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$id);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['client_level'];
    }
    

    /**
     * ------------------------Check parents hand------------------------------------------------------------------------
     **/
    public function parentHandPositionForNewUser($hand,$parentid)
    {
        $sql="select count(system_userID) as total from clients where hand=:hand and userID!=parent_id and parent_id=:parent_id";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':hand',$hand);
        $stmt->bindValue(':parent_id',$parentid);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        //if hand found
        if($res['total']==1)
        {
            return false;
        }else
        {
            return true;
        }
    }
    /**
     * ---------------------------------------------------------------------------------------------
     **/

    /**
     * ----------------------------- get placement user level---------------------------------------
    **/
    public function placementUserLevel($username)
    {
        $sql="select client_level from clients where username=:username limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if(empty($res))
        {
            return false;
        }else
        {
            return $res;
        }
    }
    /**
     * -------------------------------------------------------------------------------------
    **/

    /**
     * ---------------- get user's parents -----------------------------------------------------
    @------------------param $id is the id of last added users id----------------------------
    **/
    public function getUserParent($id)
    {
        $root=1;
        $userID=$id;
        $data=[];
        while($root != $userID)
        {
            $sql="select parent_id,hand,client_level from clients where userID=$userID";
            $stmt=\App\DBConnection::myQuery($sql);
            $stmt->execute();
            $res= $stmt->fetch(\PDO::FETCH_ASSOC);
            if($res['parent_id']==$root)
            {
                $data[]=$res;
                break;
            }else
            {
                $data[]=$res;
                $userID=$res['parent_id'];
            }
        }
        return $data;
    }
    /**
     * ------------------------------------------------------------------------------------------------
    **/






    /** ------------------------------- get parent level----------------------------------------------**/
    public function getParentLevelInfo($id)
    {
        $root=1;
        $userID=$id;
        $data=[];
        while($root != $userID)
        {
            $sql="select parent_id,client_level from clients where userID=$userID";
            $stmt=\App\DBConnection::myQuery($sql);
            $stmt->execute();
            $res= $stmt->fetch(\PDO::FETCH_ASSOC);
            if($res['parent_id']==$root)
            {
                $data[]=$res;
                break;
            }else
            {
                $data[]=$res;
                $userID=$res['parent_id'];
            }
        }
        return $data;
    }


    /**-------------------------------------------------------------------------------------------------**/

    /**----- Check if user is eligible for getting bonus and data not exist in bonus table-------------**/
//    public function checkDataExistance( $id,$client_level,$left_user,$right_user,$bonus_type)
    public function checkDataExistance( $id,$client_level,$bonus_type)
    {
//        $sql="select count(bonusID) as total from bonus where
//        userID=:userID and
//        bonus_level=:bonus_level and
//        left_userID=:left_userID and
//        right_userID=:right_userID and
//        bonus_type=:bonus_type";

        $sql="select count(bonusID) as total from bonus where
        userID=:userID and
        bonus_level=:bonus_level and 
        bonus_type=:bonus_type";

        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$id);
//        $stmt->bindValue(':left_userID',$left_user);
//        $stmt->bindValue(':right_userID',$right_user);
        $stmt->bindValue(':bonus_level',$client_level);
        $stmt->bindValue(':bonus_type',$bonus_type);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        if($res['total']>=1)
        {
            return false;
        }
        else{
            return true;
        }
    }
    /**------------------------------------------------------------------------------------------------*/

    /** ----------------------------- count total user on parents left and right--------------------  **/
    public function checkTotalUserOnLeftAndRight($id)
    {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $left_user='';
        $right_user='';
        $bonus_type='';

//        $sql="select count(user_id) total,client_level from client_positions where parent_id=:parent_id and (hand=0 or hand=1)";
        $sql="select user_id,client_level from client_positions where parent_id=:parent_id and (hand=0 or hand=1)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id',$id);
        $stmt->execute();
        $res=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        if(count($res)>=2)
//        if($res['total']>=2)
        {
            $data=$this->getParentInfo($id);
            if($data[0]['hand']==0)
            {
//                $left_user=$data[0]['userID'];
                $left_user=$data[0]['user_id'];
            }
            else if($data[0]['hand']==1)
            {
                $right_user=$data[0]['user_id'];
            }

            if($data[1]['hand']==0)
            {
                $left_user=$data[1]['user_id'];
            }
            else if($data[1]['hand']==1)
            {
                $right_user=$data[1]['user_id'];
            }

            foreach ($res as $rs) {
                if($rs['client_level']<=5)
                {
                    $bonus_type=1; /** ------------ 1 means level bonus ------------------- **/
                }
                /**---------------------------check if data exist the return false else return true----------**/
                if ($this->checkDataExistance($id, $rs['client_level'], $left_user, $right_user, $bonus_type))
                {
                    /** ------------------------------give bonus to parent----------------------------------**/
                    $this->storeParentBonus($id, $rs['client_level'], $left_user, $right_user, $bonus_type);
                    /**-------------------------------------------------------------------------------------**/
                }
            }

//            if($this->checkDataExistance($id, $res['client_level'], $left_user, $right_user, $bonus_type)) {
//                /** ------------------------------give bonus to parent----------------------------------**/
//                $this->storeParentBonus($id, $res['client_level'], $left_user, $right_user, $bonus_type);
//                /**-------------------------------------------------------------------------------------**/
//            }else
//            {
//                return false;
//            }
        }else
        {
            return false;
        }
    }
    /**-----------------------------------------------------------------------------------**/


    /**------------------------ get parents info (for those parent who will get bonus)---------**/
    public function getParentInfo($id,$level)
    {
//        $sql="select userID,hand from clients where parent_id=:parent_id and hand!=-1"; /** -1 means the root user's main hand so it will not be used **/
//        $sql="select user_id,hand,client_level from client_positions where parent_id=:parent_id"; /** -1 means the root user's main hand so it will not be used **/
        $sql="select user_id,hand,client_level from client_positions where parent_id=:parent_id and client_level=:client_level"; /** -1 means the root user's main hand so it will not be used **/

        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id',$id);
        $stmt->bindValue(':client_level',$level);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/

    /** ------------------ store data in bonus table--------------------------**/
    public function storeParentBonus($id,$bonus_point,$level,$left_userID,$right_userID,$bonus_type)
    {
         date_default_timezone_set('Asia/Kuala_Lumpur');
        $sql="insert into bonus(userID,bonus_point,bonus_date,bonus_time,bonus_level,left_userID,right_userID,bonus_type)
        values(:userID,:bonus_point,:bonus_date,:bonus_time,:bonus_level,:left_userID,:right_userID,:bonus_type)
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$id);
        $stmt->bindValue(':bonus_point',$bonus_point);
        $stmt->bindValue(':bonus_date',date('Y-m-d'));
        $stmt->bindValue(':bonus_time',date('h:i:s A'));
        $stmt->bindValue(':bonus_level',$level);
        $stmt->bindValue(':left_userID',$left_userID);
        $stmt->bindValue(':right_userID',$right_userID);
        $stmt->bindValue(':bonus_type',$bonus_type);
        $stmt->execute();
    }
    /**------------------------------------------------------------------------------------**/


    /**--------------------------------get sponsor user ID----------------------------------**/
    public function getSponsorUserID($username)
    {
        $sql="select userID from users where username=:username limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('username',$username);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
    
    public function getSponsorUserIDByID($userid)
    {
        $sql="select sponsor_id from clients where userID=:userID limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue('userID',$userid);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
    /**--------------------------------------------------------------------------------------**/


    /**----------count parent bonus to determine the type of bonus for each parent-----------**/
    public function countLevelBonus($userID)
    {
        $sql="select count(userID) as total from bonus where userID=:userID and bonus_type=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**-----------------------------------------------------------------------------------------**/


    /**--------------------------------get logged in user profile info-------------------------**/
    public function getUserInfo()
    {
        $sql="select users.username,sponsor_id,full_name,nick_name,IC_no,wechat_ID,email,RF,DC,PC,grade_id,
        address,country,contact_no,user_registered_date,user_status,expire_date,packageID from users
        join clients on clients.userID=users.userID
        where users.userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',Session::get('userID'));
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/
    
    
    
    public function getGradeAmount($gradeID)
    {
        $sql="select requiredDC,requiredPC from usergrades where userGradeID=:userGradeID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userGradeID',$gradeID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    

    public function getPasswordInfo()
    {   
        $sql='select sec_p_update,pss_update from users where userID=:userID';
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',Session::get('userID'));
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }


    


    /**--------------------------------get logged in user profile info-------------------------**/
    public function getUserAccountDateInfo()
    {
        $sql="select user_registered_date,expire_date from users
        where users.userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',Session::get('userID'));
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/





    public function getUserInfoex($userID)
    {
        $sql="select users.username,full_name,nick_name,IC_no,wechat_ID,email,
        address,country,contact_no,user_registered_date,user_status,expire_date,packageID from users
        join clients on clients.userID=users.userID
        where users.userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }




    /**--------------------------------get logged in user sponsor details-------------------------**/
    public function getSponsorDetails($id)
    {
        // $sql="select users.username,full_name,nick_name from users
        // join clients on clients.sponsor_id=users.userID
        // where users.userID=:userID";
        
        $sql="select username from users where userID=:userID";
        
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/

    
    



    /**-------------------------------verify user-----------------------------------------------**/
    public function verifyUser($security_password)
    {
        $sql="select userID from users where security_password=:security_password and userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':security_password',$security_password);
        $stmt->bindValue(':userID',Session::get('userID'));
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if(!empty($res))
        {
            return true;
        }
    }


    /**-----------------------------get user name---------------------------------------------**/
    public function getUserName($userID)
    {
        $sql="select username from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['username'];
    }
    /**---------------------------------------------------------------------------------------**/
    
     public function getUserNickName($userID)
    {
        //$sql="select nick_name,username from users where userID=:userID";
        $sql="select username from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['username'];
    }


    /**-----------------------------get user name---------------------------------------------**/
    public function validateUserByUsername($username)
    {
        $sql="select username from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['username'];
    }
    /**---------------------------------------------------------------------------------------**/

    public function getEmailAndID($username)
    {
        $sql="select username,email,userID,full_name,password,security_password from users where username=:username";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':username',$username);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**----------------------------get level bonus of each user-------------------------------**/
    public function allLevelBonus($userID,$limit)
    {
        $sql="select userID,bonus_point,bonus_date,bonus_time,bonus_level,left_userID,right_userID
        from bonus
        where userID=:userID and bonus_type=1 order by bonus_date desc, bonusID desc limit $limit, 10";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**---------------------------------------------------------------------------------------**/


    /**-----------------filter level bonus by date---------------------------------------------------*/
    public function dateWiseAllLevelBonus($userID,$start,$end,$limit)
    {
        $sql="select userID,bonus_point,bonus_date,bonus_time,bonus_level,left_userID,right_userID
        from bonus
        where userID=:userID and ( bonus_date between :bonus_date and :bonus_date1) and bonus_type=1 order by bonus_date desc, bonusID desc limit $limit,10";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':bonus_date',$start);
        $stmt->bindValue(':bonus_date1',$end);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**--------------------------------------------------------------------------------------------**/


    /**-----------------filter level bonus by date---------------------------------------------------*/
    public function dateWiseAllIntroduceBonus($userID,$start,$end,$limit)
    {
        $sql="select bonus_point,bonus_date,left_userID,right_userID from bonus 
        where bonus.userID=:userID and bonus_type=3 and ( bonus_date between :bonus_date and :bonus_date1)  order by bonus_date desc limit $limit, 10
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':bonus_date',$start);
        $stmt->bindValue(':bonus_date1',$end);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**--------------------------------------------------------------------------------------------**/


    /**----------------------------total level bonus of each user--------------------------------**/
    public function totalLevelBonus($userID)
    {
        $sql="select sum(bonus_point) as total_level_bonus from bonus where userID=:userID and bonus_type=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total_level_bonus'];
    }
    /**----------------------------------------------------------------------------------------**/



    /**
     * --------------------get previous carry amount----------------------------------
    **/
    public function previousCarryAmount($userID,$date)
    {
        $sql = "select left_carry,right_carry from pairing_bonus
        where userID=:userID and bonus_date=:bonus_date";
        $stmt = \App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':bonus_date',$date);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**------------------------------------------------------------------------------**/



    /**----------------------------total pair bonus of each user--------------------------------**/
    public function totalPairBonus($userID)
    {
        $sql="select sum(bonus) as total_pair_bonus from pairing_bonus where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total_pair_bonus'];
    }
    /**----------------------------------------------------------------------------------------**/




    /**----------------------------todays pair bonus sell of each user--------------------------------**/
    public function todaysPairBonusSales($userID)
    {
        $sql="select total_user_left,total_user_right,left_carry,right_carry
        from pairing_bonus where userID=:userID and bonus>0 and bonus_date=:bonus_date";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':bonus_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day')));
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**----------------------------------------------------------------------------------------**/



    /**----------------------------total pair bonus of each user--------------------------------**/
    public function pairBonusList($userID,$limit)
    {
        $sql="select * from pairing_bonus where userID=:userID and bonus>0 order by bonus_date desc limit $limit,10 ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**----------------------------------------------------------------------------------------**/
    
    
     public function pairList($userID)
    {
        $sql="select * from pairing_bonus where userID=:userID and bonus>0 order by bonus_date desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    /**----------------------------total pair bonus of each user--------------------------------**/
    public function dateWisePairBonusList($userID,$start,$enddate,$limit)
    {
        $sql="select * from pairing_bonus where userID=:userID and bonus>0
        and (bonus_date between :start and :enddate) order by bonus_date desc limit $limit,10";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**----------------------------------------------------------------------------------------**/



    /**----------------------------total introducer bonus of each user--------------------------------**/
    public function totalIntroducerBonus($userID)
    {
        $sql="select sum(bonus_point) as total_introducer_bonus from bonus where userID=:userID and bonus_type=3";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total_introducer_bonus'];
    }
    /**----------------------------------------------------------------------------------------**/

    /**------------------ calculate total bonus except current date---------------------------**/
    public function totalBonusExceptToday($userID)
    {
        $sql="select sum(bonus_point) as total_previous_bonus from bonus where userID=:userID and bonus_date!=:this_date";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':this_date',date('Y-m-d'));
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total_previous_bonus'];
    }
    /**----------------------------------------------------------------------------------------**/

    /**------------------ calculate total leveling bonus except current date-------------------**/
    public function todaysLevelingEc($userID)
    {
        $sql="select sum(bonus_point) 
        as total_level_bonus from bonus 
        where userID=:userID and bonus_date=:this_date and (bonus_type=1 or bonus_type=3)"; /**--- 1 means leveling bonus---**/
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':this_date',date('Y-m-d'));
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total_level_bonus'];
    }

    public function totalEc($userID)
    {
        $sql="select sum(bonus_point) 
        as total_level_bonus from bonus 
        where userID=:userID and (bonus_type=1 or bonus_type=3)"; /**--- 1 means leveling bonus---**/
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total_level_bonus'];
    }


    /**-----------------------------------------------------------------------------------------**/

    /**---------------------------calculate total times of leveling bonus and total amount------**/
    public function levelingBonusSummery($userID)
    {
        $sql="select count(bonusID) as totalTimes, sum(bonus_point) as totalBonus
        from bonus where userID=:userID and bonus_type=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/


    /**--------------------------- Introducer bonus list------**/
    public function getIntroducerBonusList($userID,$limit)
    {
        $sql="select bonus_point,bonus_date,left_userID,right_userID from bonus 
        where bonus.userID=:userID and bonus_type=3 order by bonus_date desc, bonusID desc limit $limit,10
        ";

        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function getTotalIntroducerBonusList($userID)
    {
        $sql="select sum(bonus_point) as total from bonus where userID=:userID and bonus_type=3
        ";

        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }

    /**-----------------------------------------------------------------------------------------**/


    /**---------------------------calculate total times of Introducer bonus and total amount------**/
    public function introducerBonusSummery($userID)
    {
        $sql="select count(bonusID) as totalTimes, sum(bonus_point) as totalBonus
        from bonus where userID=:userID and bonus_type=3";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/


    /**---------------------------------Total pairing bonus time----------------------------------**/
    public function getTotalTimeOfPairingBonus($userID)
    {
        $sql="select count(tableID) as total
        from pairing_bonus where userID=:userID and bonus>0";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }
    /**-----------------------------------------------------------------------------------------**/


    /**---------------------------------last bonus time record----------------------------------**/
    public function getLastTimeRecord($userID,$bonusType)
    {
        $sql="select bonus_date,bonus_time
        from bonus where userID=:userID and bonus_type=:bonus_type order by bonusID desc limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':bonus_type',$bonusType);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/



    public function getLastTimePairBonusRecord($userID)
    {
        $sql="select bonus_date,bonus_time
        from pairing_bonus where userID=:userID and bonus>0 order by tableID desc limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-------------------------------date wise level and pairing bonus summer------------------**/


    /**---------------------------calculate total times of leveling bonus and total amount between this date range------**/
    public function levelingBonusSummeryOfThisDate($userID,$start,$enddate)
    {
        $sql="select count(bonusID) as totalTimes, sum(bonus_point) as totalBonus
        from bonus where userID=:userID and bonus_type=1 and (bonus_date between :start and :enddate)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/


    /**---------------------------calculate total times of leveling bonus and total amount between this date range------**/
    public function pairingBonusSummeryOfThisDate($userID,$start,$enddate)
    {
        $sql="select count(tableID) as totalTimes, sum(bonus) as totalBonus
        from pairing_bonus where userID=:userID and bonus>0 and (bonus_date between :start and :enddate)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/

    /**---------------------------------last bonus time record between this date range----------------------------------**/
    public function getLastTimePairbonusRecordOfThisDate($userID,$start,$enddate)
    {
        $sql="select bonus_date,bonus_time
        from pairing_bonus where userID=:userID 
        and bonus>0 and (bonus_date between :start and :enddate) order by tableID desc limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/



    /**---------------------------------last bonus time record between this date range----------------------------------**/
    public function getLastTimeRecordOfThisDate($userID,$bonusType,$start,$enddate)
    {
        $sql="select bonus_date,bonus_time
        from bonus where userID=:userID and bonus_type=:bonus_type 
        and (bonus_date between :start and :enddate) order by bonusID desc limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':bonus_type',$bonusType);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/



    /**------------------ calculate total paring bonus of current date---------------------------**/
    public function previousPairingEc($userID)
    {
        $sql="select sum(bonus_point)
        as total_pairing_bonus,count(bonusID) as totalTimes from bonus 
        where userID=:userID and bonus_date!=:this_date and bonus_type=2"; /**--- 1 means leveling bonus---**/
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':this_date',date('Y-m-d'));
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $res=$stmt->fetch(PDO::FETCH_ASSOC);

    }
    /**------------------------------------------------------------------------------------------**/

    /**------------------ calculate total paring bonus of current date---------------------------**/
    public function todaysPairingEc($userID)
    {
        $sql="select sum(bonus_point)
        as total_pairing_bonus,count(bonusID) as totalTimes from bonus 
        where userID=:userID and bonus_date=:this_date and bonus_type=2"; /**--- 2 means pairing bonus---**/
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':this_date',date('Y-m-d'));
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $res=$stmt->fetch(PDO::FETCH_ASSOC);

    }
    /**------------------------------------------------------------------------------------------**/


    /**-------------------------------------pairing bonus between date range---------------------**/

    /**------------------ calculate total paring bonus of current date---------------------------**/
    public function PairingEcBetweenThisDateRangeBeforeFivePM($userID,$start,$enddate)
    {
        $sql="select sum(bonus_point)
        as total_pairing_bonus,count(bonusID) as totalTimes from bonus 
        where userID=:userID and (bonus_date between :start and :enddate) and bonus_type=2 and bonus_time<:bonus_time"; /**--- 2 means pairing bonus---**/
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->bindValue(':bonus_time',date('h:i:s A'));
        $stmt->execute();
        return $res=$stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function PairingEcBetweenThisDateRangeAfterFivePM($userID,$start,$enddate)
    {
        $sql="select sum(bonus_point)
        as total_pairing_bonus,count(bonusID) as totalTimes from bonus 
        where userID=:userID and (bonus_date between :start and :enddate)
        and bonus_type=2 and bonus_time>=:bonus_time"; /**--- 2 means pairing bonus---**/
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->bindValue(':bonus_time',date('h:i:s A'));
        $stmt->execute();
        return $res=$stmt->fetch(PDO::FETCH_ASSOC);
    }


    /**---------------------------calculate total times of Introducer bonus and total amount------**/
    public function introducerBonusSummeryBwteenDateRange($userID,$start,$enddate)
    {
        $sql="select count(bonusID) as totalTimes, sum(bonus_point) as totalBonus
        from bonus where userID=:userID and bonus_type=3 and (bonus_date between :start and :enddate) ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**-----------------------------------------------------------------------------------------**/

    /**------------------------------------------------------------------------------------------**/

    /**------------------------------------------------------------------------------------------**/

    /***--------------------------- get user rf,dc,pc currency amounts----------------------------*/
    public function userCurrencies($userID)
    {
        $sql="select ec,rf,dc,pc,msp,pp,tc,bc,bdt,tp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    /**--------------------------------------------------------------------------------------------------------**/


    /***--------------------------Product Package-------------------------------------------------------------*/
    public function getAllProductPackages()
    {
        $sql="select productPackageID,packageName,packagePrice from productpackages where packageStatus=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**--------------------------------------------------------------------------------------------------------**/


    /**-------------------------------------store user amounts in user_amounts table--------------------------**/
    public function storeElectronicCurrency($userID,$ec)
    {
        $sql="select userID,ec from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,ec) values(:userID,:ec)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':ec',$ec);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set ec=:ec where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':ec',$ec+$res['ec']);
            $stmt->execute();
        }
    }
    /***---------------------------------------------------------------------------------------------------------*/



    /**-------------------------------------store user amounts in user_amounts table--------------------------**/
    public function storeMSPCurrency($userID,$msp)
    {
        $sql="select userID,msp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,msp) values(:userID,:msp)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':msp',$msp);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set msp=:msp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':msp',$msp+$res['msp']);
            $stmt->execute();
        }
    }
    /***---------------------------------------------------------------------------------------------------------*/


    public function storePPCurrency($userID,$pp)
    {
        $sql="select userID,pp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if(empty($res))
        {
            $sql="insert into user_amounts (userID,pp) values(:userID,:pp)";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':pp',$pp);
            $stmt->execute();
        }else
        {
            $sql="update user_amounts set pp=:pp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$userID);
            $stmt->bindValue(':pp',$pp+$res['pp']);
            $stmt->execute();
        }
    }






    /**------------------------------- decrement parent user's FR,DC,PC-----------------------------------------**/
    public function checkCurrency($userID,$rf,$dc,$pc)
    {
        $sql="select rf,dc,pc from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        if($res['rf']>=$rf && $res['dc']>=$dc && $res['pc']>=$pc)
        {
            return true;
        }else
        {
            return false;
        }
    }
    /**--------------------------------------------------------------------------------------------------------**/

    /**----------------------------------- decrement user amount------------------------------------------------**/

    public function decrementCurrency($userID,$rf,$dc,$pc)
    {
        $sql="select rf,dc,pc from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(PDO::FETCH_ASSOC);
            $sql="update user_amounts set rf=:rf,dc=:dc,pc=:pc where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':rf',$res['rf']-$rf);
            $stmt->bindValue(':dc',$res['dc']-$dc);
            $stmt->bindValue(':pc',$res['pc']-$pc);
            $stmt->bindValue(':userID',$userID);
            $stmt->execute();
        }
    }
    /**---------------------------------------------------------------------------------------------------------**/


    /**----------------------------------- decrement user amount------------------------------------------------**/

    public function decrementMSPCurrency($userID,$msp)
    {
        $sql="select msp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(PDO::FETCH_ASSOC);
            $sql="update user_amounts set msp=:msp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':msp',$res['msp']-$msp);
            $stmt->bindValue(':userID',$userID);
            $stmt->execute();
        }
    }
    /**---------------------------------------------------------------------------------------------------------**/

    public function decrementPPCurrency($userID,$pp)
    {
        $sql="select pp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        if($stmt->execute())
        {
            $res=$stmt->fetch(PDO::FETCH_ASSOC);
            $sql="update user_amounts set pp=:pp where userID=:userID";
            $stmt=DBConnection::myQuery($sql);
            $stmt->bindValue(':pp',$res['pp']-$pp);
            $stmt->bindValue(':userID',$userID);
            $stmt->execute();
        }
    }






    /**-----------------------------currency purchase------------------------------------------------------**/
    public function updateDC($dc,$userID)
    {
        $sql="update user_amounts set dc=:dc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':dc',$dc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }


    public function updateMSP($msp,$userID)
    {
        $sql="update user_amounts set msp=:msp where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':msp',$msp);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }

    public function decrementEC($ec,$userID)
    {
        $sql="update user_amounts set ec=:ec where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':ec',$ec);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }


    public function decrementDC($dc,$userID)
    {
        $sql="update user_amounts set dc=:dc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':dc',$dc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }





    public function recordCurrencyConversion($userID,$currency_type,$convert_to,$amount)
    {
        $sql="insert into currency_convert(userID,currency_type,convert_to,amount,convert_date,convert_time)
        values(:userID,:currency_type,:convert_to,:amount,:convert_date,:convert_time)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':currency_type',$currency_type);
        $stmt->bindValue(':convert_to',$convert_to);
        $stmt->bindValue(':amount',$amount);
        $stmt->bindValue(':convert_date',date('Y-m-d'));
        $stmt->bindValue(':convert_time',date('H:i:s A'));
        $stmt->execute();
    }

    public function totalECConvert($userID)
    {
        $sql="select sum(amount) as totalConvert from currency_convert where userID=:userID and currency_type=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['totalConvert'];
    }

    /**-----------------------------------------------------------------------------------------------------**/
    public function userCurrencyHistory($userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark,$total,$receiver_total)
    {
        $sql="insert into currency_history (userID,receiverID,amount,description,currency_type,reason,debit,credit,action_date,remark,user_total,receiver_total)
    values(:userID,:receiverID,:amount,:description,:currency_type,:reason,:debit,:credit,:action_date,:remark,:user_total,:receiver_total)
    ";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':receiverID',$receiverID);
        $stmt->bindValue(':amount',$amount);
        $stmt->bindValue(':description',$description);
        $stmt->bindValue(':currency_type',$currency_type);//2 means DC
        $stmt->bindValue(':reason',$reason);//2 means DC transfer
        $stmt->bindValue(':debit',$debit);
        $stmt->bindValue(':credit',$credit);//2 means DC transfer
        $stmt->bindValue(':action_date',date('Y-m-d'));//2 means DC transfer
        $stmt->bindValue(':remark',$remark);//2 means DC transfer
        $stmt->bindValue(':user_total',$total);
        $stmt->bindValue(':receiver_total',$receiver_total);
        $stmt->execute();
    }

    /**--------------------DC history---------------------------------------------------------**/
    public function getDCHistory($userID)
    {
        $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history 
        where (currency_type=2 or currency_type=8 or currency_type=53) and userID=:userID order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**--------------------msp history---------------------------------------------------------**/
    public function getClientMSPHistory($userID)
    {
        $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from
        currency_history where (currency_type=11 or currency_type=15 or currency_type=20 or currency_type=62) and userID=:userID and action_date>:action_date  order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**--------------------pp history---------------------------------------------------------**/
    public function getClientPPHistory($userID)
    {
        $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from
        currency_history where (currency_type=16 or currency_type=14  or currency_type=57  or currency_type=64 ) and userID=:userID and action_date>:action_date order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

//    /**--------------------MSP history---------------------------------------------------------**/
//    public function getMSPHistory($userID)
//    {
//        $sql="select * from currency_history where currency_type=5 and userID=:userID order by action_date desc limit 300";
//        $stmt=DBConnection::myQuery($sql);
//        $stmt->bindValue(':userID',$userID);
//        $stmt->execute();
//        return $stmt->fetchAll(PDO::FETCH_ASSOC);
//    }


    public function getPCHistory($userID)
    {
        $sql="select  historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history
        where (currency_type=12 or currency_type=9 or currency_type=55 ) and (userID=:userID) and action_date>:action_date order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function getECHistory($userID)
    {
        $sql="select * from currency_history where (currency_type=1 or currency_type=4 or currency_type=13 or currency_type=17 or currency_type=61)
        and (userID=:userID or receiverID=:userID) and action_date>:action_date order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getRFHistory($userID)
    {
        $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total
        from currency_history where (currency_type=3 or currency_type=7) and userID=:userID and action_date>:action_date order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**----------------------------------------------------------------------------------------**/


    public function getAdminTransferHistory()
    {
        $sql="select full_name,amount,description,action_date,remark,currency_type
          from currency_history
          join users on users.userID=currency_history.receiverID
         where currency_type=7 or currency_type=8 or currency_type=11 or currency_type=12 order by action_date desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getAdminTransferHistoryType($type)
    {
        $sql="select full_name,amount,description,action_date,remark,currency_type
          from currency_history
          join users on users.userID=currency_history.receiverID
         where currency_type=:c_type order by action_date desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':c_type',$type);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    /**-----------------------------------------user grade-------------------------------------**/
    public function gradeGradeDetails($id)
    {
        $sql="select * from usergrades where userGradeId=:userGradeId";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userGradeId',$id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function userGrade($userID)
    {
        $sql="select grade_id from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['grade_id'];
    }



    /**-----------------------------get bonus amounts------------------------------------------**/


    public function getBonusInfo($type)
    {
        $sql="select * from bonus_amounts where bonus_type=$type";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**----------------------------------------------------------------------------------------**/
    
    
    public function getPsrd($userID)
    {
        $sql="select password from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    

    /**----------------------------------------------------------------------------------------**/



    /**
     * ==================================================================================================================
     * ==================================================================================================================
    **/
    /**------------------------------user login functions-----------------------------------------------**/
    /**
     * ------------------------------new user Login-----------------------------------------------------------------------
     **/
    public function userLogin($user_name,$Password, $companyID, $branchID, $applicationID){
        // Set Time Zone to Asia/Dhaka
        date_default_timezone_set('Asia/Kuala_Lumpur');
        // Get User IP
        $ipAddress = $this->getClientIPAddress();
        // Check IP in permanent_block table
        $sql="SELECT blockedIP FROM permanent_block WHERE blockedIP=? LIMIT 1";
        $statementBlockedIP = DBConnection::myQuery($sql);
        $statementBlockedIP->execute(array($ipAddress));
        $numBlockedIP = $statementBlockedIP->rowCount();
        // If IP is in permanentBlock IP list
        if ($numBlockedIP > 0){
            // Throw Error
            throw new \Exception("Your IP address is in permanent blocked ip address list. Please contact with administrator to remove your IP.");
        }else{
            // Check the IP in login_lockdown table
            $sql="SELECT lockedIP, releaseDate FROM login_lockdown WHERE lockedIP=?  ORDER BY SL DESC LIMIT 1";
            $statementLockedIP = DBConnection::myQuery($sql);
            $statementLockedIP->execute(array($ipAddress));
            $result = $statementLockedIP->fetch(PDO::FETCH_ASSOC);
            $numLockedIP = $statementLockedIP->rowCount();
            // If IP is in login_lockdown IP list
            if ($numLockedIP > 0){
                // Check time difference from now to releaseDate
                $releaseDate = $result['releaseDate'];
                $currentDateTime =  date("Y-m-d h:i:s"); // 2018-12-21 08:17:22
                if ($currentDateTime > $releaseDate) {
                    // Login with data
                    $this->doLoginFunction($user_name,$Password, $ipAddress, $companyID, $branchID, $applicationID);
                }else{
                    // Throw Error
                    //throw new Exception("Your IP address is locked for 30 minutes. Try after $releaseDate");
                }
            }else{
                // Login with data
                $this->doLoginFunction($user_name,$Password, $ipAddress, $companyID, $branchID, $applicationID);
            }
        }
        return true;
    }

    /**
     * doLoginFunction
     *
     * @param $user_name
     * @param $Password
     * @param $ipAddress
     * @param $companyID
     * @param $branchID
     * @param $applicationID
     * @return bool
     * @throws Exception
     */
    public function  doLoginFunction($user_name,$Password, $ipAddress, $companyID, $branchID, $applicationID){

        $sql="SELECT userID,username,full_name,nick_name,email FROM users WHERE username=? AND password=? and user_type=0";

        $statementLoginAttempt=DBConnection::myQuery($sql);
        $statementLoginAttempt->execute(array($user_name,$Password));
        $result = $statementLoginAttempt->fetch(PDO::FETCH_ASSOC);
        $numSuccessfulLoginAttempt = $statementLoginAttempt->rowCount();
        if($numSuccessfulLoginAttempt > 0){
            // Store in login_activity table about the login information
            $loginDate = date("Y-m-d h:i:s");

            if ($ipAddress == "::1"){
                $loginCountry = "Bangladesh";
            }else{
                $loginCountry = $this->ipInformation("Visitor", "Country");
            }

            $loginActivityId = $this->getNextLoginActivityId();
            $sql="INSERT INTO login_activity (userID, loginDate, logoutDate, loginIP, loginCountry, companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $statementAddLoginAttemptCount=DBConnection::myQuery($sql);
            $statementAddLoginAttemptCount->execute(array($result['userID'], $loginDate, null, $ipAddress, $loginCountry, $companyID, $branchID, $applicationID));
            // Remove old login_attempts for this IP
            $this->removeAllLoginAttemptForIP($ipAddress);
            // Remove old login_lockdown for this IP
            $this->removeAllLoginLockDownForIP($ipAddress);

            /**
             * ------------------store data in session-----------------------------
             **/
            Session::init();
            Session::set('login',true);
            Session::set('email',$result['email']);
            Session::set('userID',$result['userID']);
            Session::set('username',$result['username']);
            Session::set('full_name',$result['full_name']);
            Session::set('nick_name',$result['nick_name']);
            header('location:members/index.php');
            /**
             * -----------------------------------------------------------------------
             **/
        }else{

            // Check how many times this IP in login_attempt_count table
            $sql="SELECT attemptIP FROM login_attempt_count WHERE attemptIP=?";
            $statementLoginAttemptCount=DBConnection::myQuery($sql);
            $statementLoginAttemptCount->execute(array($ipAddress));
            $loginAttemptCount = $statementLoginAttemptCount->rowCount();

            // Get next ID in login_attempt_count table
            $loginAttemptID = $this->getNextLoginAttemptCountId();

            // store in login_attempt_count table
            $attemptTime = date("Y-m-d h:i:s");
            $sql="INSERT INTO login_attempt_count (userLogin, attemptTime, attemptIP, companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?)";
            $statementAddLoginAttemptCount = DBConnection::myQuery($sql);
            $statementAddLoginAttemptCount->execute(array($user_name, $attemptTime, $ipAddress, $companyID, $branchID, $applicationID));


            // store in failed_login table
            $failedLoginDate = date("Y-m-d h:i:s");
            $sql="INSERT INTO failed_logins (userID, userLoginID, failedLoginDate, loginAttemptID, companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $statementAddLoginFailed=DBConnection::myQuery($sql);
            $statementAddLoginFailed->execute(array(1, $user_name, $failedLoginDate, $loginAttemptID, $companyID, $branchID, $applicationID));


            // If login attempt count == 3 that means now 3 times he tries to wrong access
            if ($loginAttemptCount == 3){
                /**
                Add his data to login_lockdown table, remove all data of this IP from
                login_attempt_count table and throw a message
                 **/

                // Add data to login_lockdown table
                $lockDownDateTime = date("Y-m-d h:i:s");
                $releaseDateTime = date("Y-m-d H:i:s", strtotime($lockDownDateTime . "+30 minutes"));
                $lockType = 1;
                $sql="INSERT INTO login_lockdown (userID, userLogin, lockedIP, lockDownDate, releaseDate,lockType,companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $statementAddLoginLockDown=DBConnection::myQuery($sql);
                $statementAddLoginLockDown->execute(array(0, $user_name, $ipAddress, $lockDownDateTime, $releaseDateTime, $lockType, $companyID, $branchID, $applicationID));

                // remove all data of this IP from login_attempt_count table
                $this->removeAllLoginAttemptForIP($ipAddress);
                // Throw an error
                //throw new Exception("Your IP address is locked for 30 minutes. Try after $releaseDateTime");
            }
            // Throw an error
            throw new \Exception("Invalid username or password.");
//            Session::set('login-failure',"Invalid username or password.");
        }
    }

    /**
     * removeAllLoginAttemptForIP
     *
     * @param $ipAddress
     */
    public function removeAllLoginAttemptForIP($ipAddress) {
        $sql = "DELETE FROM login_attempt_count where attemptIP=?";
        $stmt = DBConnection::myQuery($sql);
        $stmt->execute(array($ipAddress));
    }

    /**
     * removeAllLoginLockDownForIP
     *
     * @param $ipAddress
     */
    public function removeAllLoginLockDownForIP($ipAddress) {
        $sql = "DELETE FROM login_lockdown where lockedIP=?";
        $stmt = DBConnection::myQuery($sql);
        $stmt->execute(array($ipAddress));
    }

    /**
     * getClientIPAddress
     *
     * @return string
     */
    public function getClientIPAddress() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


    /**
     * ipInformation
     *
     * Get any information for an IP address
     *  Like,
     *              echo ipInformation("Visitor", "Country"); // India
    echo ipInformation("Visitor", "Country Code"); // IN
    echo ipInformation("Visitor", "State"); // Andhra Pradesh
    echo ipInformation("Visitor", "City"); // Proddatur
    echo ipInformation("Visitor", "Address"); // Proddatur, Andhra Pradesh, India
     * @param null $ip
     * @param string $purpose
     * @param bool $deep_detect
     * @return array|null|string
     */
    public function ipInformation ($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            if (is_null($ip)){
                $ip = $_SERVER["REMOTE_ADDR"];
            }

            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    /**
     * getNextLoginAttemptCountId
     *
     * @return mixed
     */
    public function getNextLoginAttemptCountId(){
//        $statement = $this->db->prepare("SHOW TABLE STATUS LIKE 'login_attempt_count'");
        $sql="SHOW TABLE STATUS LIKE 'login_attempt_count'";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $row) {
            return $row[10];
        }
    }

    /**
     * getNextLoginActivityId
     *
     * @return mixed
     */
    public function getNextLoginActivityId(){
//        $statement = $this->db->prepare("SHOW TABLE STATUS LIKE 'login_activity'");
        $sql="SHOW TABLE STATUS LIKE 'login_activity'";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $row) {
            return $row[10];
        }
    }

    /**
     * updateLoginActivity
     *
     * @return bool
     */
    public function updateLoginActivity(){
        // Set Time Zone to Asia/Dhaka
         date_default_timezone_set('Asia/Kuala_Lumpur');

        $logoutDate = date("Y-m-d h:i:s");
        $activityID = $_SESSION['login_activity_id'];

        $sql = "UPDATE login_activity SET logoutDate=? WHERE activityID=?";
//        $stmt = $this->db->prepare($sql);
        $stmt = DBConnection::myQuery($sql);
        $update_or_not = $stmt->execute(array($logoutDate, $activityID));

        if ($update_or_not)
            return true;
        else
            return false;
    }

    /**
     * permanentBlockedIpLists
     *
     * @return mixed
     */
    public function permanentBlockedIpLists(){
//        $statement = $this->db->prepare("SELECT * FROM permanent_block ORDER BY ID DESC");
        $sql="SELECT * FROM permanent_block ORDER BY ID DESC";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    /**
     * addBlockIP
     *
     * @param $blockedIP
     * @param $blockReason
     * @param $blockDate
     * @param $status
     * @param $companyID
     * @param $branchID
     * @param $applicationID
     * @return bool
     */
    public function addBlockIP($blockedIP, $blockReason, $blockDate, $status, $companyID, $branchID,
                               $applicationID){
//        $statementAddLoginLockDown = $this->db->prepare("INSERT INTO permanent_block (blockedIP, blockReason, blockedDate, status,companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $sql="INSERT INTO permanent_block (blockedIP, blockReason, blockedDate, status,companyID, branchID, applicationID) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $statementAddLoginLockDown=DBConnection::myQuery($sql);
        $insert_or_not = $statementAddLoginLockDown->execute(array($blockedIP, $blockReason, $blockDate, $status,
            $companyID, $branchID, $applicationID));
        if ($insert_or_not)
            return true;
        else
            return false;

    }

    /**
     * updateBlockIP
     *
     * @param $blockedIP
     * @param $blockReason
     * @param $blockDate
     * @param $status
     * @param $companyID
     * @param $branchID
     * @param $applicationID
     * @param $ID
     * @return bool
     */
    public function updateBlockIP($blockedIP, $blockReason, $blockDate, $status, $companyID, $branchID,
                                  $applicationID, $ID){
        $sql = "UPDATE permanent_block SET blockedIP=?, blockReason=?, blockedDate=?, status=?, companyID=?, branchID=?,
                               applicationID=? WHERE ID=?";
//        $stmt = $this->db->prepare($sql);
        $stmt=DBConnection::myQuery($sql);
        $update_or_not = $stmt->execute(array($blockedIP, $blockReason, $blockDate, $status, $companyID, $branchID,
            $applicationID, $ID));

        if ($update_or_not)
            return true;
        else
            return false;

    }

    /**
     * deletePermanentBlockIP
     *
     * @param $ID
     * @return bool
     */
    public function deletePermanentBlockIP($ID){
        $sql = "DELETE FROM permanent_block where ID=?";
//        $stmt = $this->db->prepare($sql);
        $stmt = DBConnection::myQuery($sql);
        $delete_or_not = $stmt->execute(array($ID));

        if ($delete_or_not)
            return true;
        else
            return false;
    }

    /**
     * lockedDownIPLists
     *
     * @return mixed
     */
    public function lockedDownIPLists(){
//        $statement = $this->db->prepare("SELECT * FROM login_lockdown ORDER BY SL DESC");
        $sql="SELECT * FROM login_lockdown ORDER BY SL DESC";
        $statement=DBConnection::myQuery($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * deleteLockDownIP
     *
     * @param $SL
     * @return bool
     */
    public function deleteLockDownIP($SL){
        $sql = "DELETE FROM login_lockdown where SL=?";
//        $stmt = $this->db->prepare($sql);
        $stmt = DBConnection::myQuery($sql);
        $delete_or_not = $stmt->execute(array($SL));

        if ($delete_or_not)
            return true;
        else
            return false;
    }

    /**
     * isIpInPermanentBlockList
     *
     * @return bool
     */
    public function isIpInPermanentBlockList(){
        $ipAddress = $this->getClientIPAddress();

        // Check IP in permanent_block table
        $sql="SELECT blockedIP FROM permanent_block WHERE blockedIP=? LIMIT 1";
        $statementBlockedIP=DBConnection::myQuery($sql);
//        $statementBlockedIP = $this->db->prepare("SELECT blockedIP FROM permanent_block WHERE blockedIP=? LIMIT 1");
        $statementBlockedIP->execute(array($ipAddress));
        $numBlockedIP = $statementBlockedIP->rowCount();

        if ($numBlockedIP > 0)
            return true;

        return false;
    }

    /**
     * isIPInLockedDownList
     *
     * @return bool
     */
    public function isIPInLockedDownList(){
        $ipAddress = $this->getClientIPAddress();

        $sql="SELECT lockedIP, releaseDate FROM login_lockdown WHERE lockedIP=?  ORDER BY SL DESC LIMIT 1";
//        $statementLockedIP = $this->db->prepare("SELECT lockedIP, releaseDate FROM login_lockdown WHERE lockedIP=?  ORDER BY SL DESC LIMIT 1");
        $statementLockedIP=DBConnection::myQuery($sql);
        $statementLockedIP->execute(array($ipAddress));
        $result = $statementLockedIP->fetch(PDO::FETCH_ASSOC);
        $numLockedIP = $statementLockedIP->rowCount();

        if ($numLockedIP > 0){
            // Check time difference from now to releaseDate
            $releaseDate = $result['releaseDate'];

            $currentDateTime =  date("Y-m-d h:i:s"); // 2018-12-21 08:17:22
            if ($currentDateTime > $releaseDate)
            {
                return false;
            }else
            {
                return true;
            }
        }
        return false;
    }
    /**
     * --------------------------------------------------------------------------------------------------------
     **/


    public function getUserMSPandPP($userID)
    {
        $sql="select msp,pp from user_amounts where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $amount=$stmt->fetch(PDO::FETCH_ASSOC);
        return $amount;
    }


    public function getUserInfoForEcommerceUser($userID)
    {
        $sql="select username,password,full_name,nick_name,email from users where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    
    /**---------------------------new ------------------------------------------------------------------------**/
    public function decrementTC($tc,$userID)
    {
        $sql="update user_amounts set tc=:tc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':tc',$tc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    } 
    
    public function incrementTC($tc,$userID)
    {
        $sql="update user_amounts set tc=:tc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':tc',$tc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function incrementBC($bc,$userID)
    {
        $sql="update user_amounts set bc=:bc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':bc',$bc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function incrementPC($pc,$userID)
    {
        $sql="update user_amounts set pc=:pc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':pc',$pc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function incrementPP($pp,$userID)
    {
        $sql="update user_amounts set pp=:pp where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':pp',$pp);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function incrementDC($dc,$userID)
    {
        $sql="update user_amounts set dc=:dc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':dc',$dc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function decrementBC($bc,$userID)
    {
        $sql="update user_amounts set bc=:bc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':bc',$bc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
     public function decreaseTradeCurrency($tc,$userID)
    {
        $sql="update user_amounts set tc=:tc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':tc',$tc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function updateBC($bc,$userID)
    {
        $sql="update user_amounts set bc=:bc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':bc',$bc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function incrementBDT($bdt,$userID)
    {
        $sql="update user_amounts set bdt=:bdt where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':bdt',$bdt);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function increaseTradeCurrency($tc,$userID)
    {
        $sql="update user_amounts set tc=:tc where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':tc',$tc);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function decrementBDT($bdt,$userID)
    {
        $sql="update user_amounts set bdt=:bdt where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':bdt',$bdt);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    /**--------------------BC history---------------------------------------------------------**/
    public function getBCHistory($userID)
    {
        $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history 
        where (currency_type=45 or currency_type=50 or currency_type=52 or currency_type=54 or currency_type=56 or currency_type=58 or currency_type=91 or currency_type=93) and userID=:userID order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getTCHistory($userID)
    {
        $sql="select  historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history
        where (currency_type=20 or currency_type=31 or currency_type=51) and (userID=:userID) and action_date>:action_date order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    
    // public function getInvestHistory($userID)
    // {
    //     $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history 
    //     where (currency_type=35) and userID=:userID order by historyID desc";
    //     $stmt=DBConnection::myQuery($sql);
    //     $stmt->bindValue(':userID',$userID);
    //     $stmt->execute();
    //     return $stmt->fetchAll(PDO::FETCH_ASSOC);
    // }
    
    
    public function getInvestHistory($userID)
    {
        $sql="select investID,invest_amount,invest_initial_date,investment_type_name,package_name,investment_amount
        from invests
        join investment_packages on investment_packages.packageID=invests.invest_packageID
        join investment_types on investment_types.investment_typeID=invests.invest_typeID
        where userID=:userID order by investID desc
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
     public function getBDTHistory($userID)
    {
        $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history 
        where (currency_type=59 or currency_type=73 or currency_type=63) and userID=:userID order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    } 
    
    public function getBCPayoutHistory($userID)
    {
        $sql="select historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history 
        where (currency_type=59) and userID=:userID order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getECPayoutHistory($userID)
    {
        $sql="select * from currency_history where (currency_type=61)
        and (userID=:userID or receiverID=:userID) and action_date>:action_date order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function decrementTP($tp,$userID)
    {
        $sql="update user_amounts set tp=:tp where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':tp',$tp);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function incrementTP($tp,$userID)
    {
        $sql="update user_amounts set tp=:tp where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':tp',$tp);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
    }
    
    public function getTPHistory($userID)
    {
        $sql="select  historyID,userID,receiverID,description,debit,credit,action_date,remark,user_total from currency_history
        where ( currency_type=90) and (userID=:userID) and action_date>:action_date order by historyID desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(date('Y-m-d') . ' -90 day')));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    
    
    
    /**=======================================================investment commission part=========================================================**/
    
    
     public function userInvestmentCurrencyHistory($userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark,$total,$receiver_total)
    {
        $sql="insert into currency_history (userID,receiverID,amount,description,currency_type,reason,debit,credit,action_date,remark,user_total,receiver_total)
    values(:userID,:receiverID,:amount,:description,:currency_type,:reason,:debit,:credit,:action_date,:remark,:user_total,:receiver_total)
    ";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':receiverID',$receiverID);
        $stmt->bindValue(':amount',$amount);
        $stmt->bindValue(':description',$description);
        $stmt->bindValue(':currency_type',$currency_type);
        $stmt->bindValue(':reason',$reason);
        $stmt->bindValue(':debit',$debit);
        $stmt->bindValue(':credit',$credit);
        $stmt->bindValue(':action_date',date('d/m/Y h:i:s a'));
        $stmt->bindValue(':remark',$remark);
        $stmt->bindValue(':user_total',$total);
        $stmt->bindValue(':receiver_total',$receiver_total);
        $stmt->execute();
    }
    
    
    public function totalInvestmentIntroducerBonus($userID)
    {
        $sql="select sum(bonus_point) as totalTimes, sum(bonus_point) as totalBonus
        from bonus where userID=:userID and bonus_type=10";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['totalBonus'];
    }
    
    
    public function introducerInvestmentBonusSummery($userID)
    {
        // $sql="select count(historyID) as totalTimes, sum(amount) as totalBonus
        // from currency_history where userID=:userID and currency_type=45";
        
        $sql="select count(bonusID) as totalTimes, sum(bonus_point) as totalBonus
        from bonus where userID=:userID and bonus_type=10";
        
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getLastTimeInvestmentPairBonusRecord($userID)
    {
        $sql="select bonus_date,bonus_time
        from invest_pairing_bonus where userID=:userID and bonus>0 order by tableID desc limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getTotalTimeOfInvestmentPairingBonus($userID)
    {
        $sql="select count(tableID) as total
        from invest_pairing_bonus where userID=:userID and bonus>0";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total'];
    }
    
    

    
    public function totalInvestmentPairBonus($userID)
    {
        $sql="select sum(bonus) as total_pair_bonus from invest_pairing_bonus where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        return $res['total_pair_bonus'];
    }
    
    public function investmentIntroducerBonusSummeryByDateRange($userID,$start,$enddate)
    {
        // $sql="select count(historyID) as totalTimes, sum(amount) as totalBonus
        // from currency_history where userID=:userID and currency_type=45 and (action_date between :start and :enddate)";
        
        $sql="select count(bonusID) as totalTimes, sum(bonus_point) as totalBonus
        from bonus where userID=:userID and bonus_type=10 and (bonus_date between :start and :enddate) ";
        
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    public function getLastTimeInvestPairBonusRecordByDate($userID,$start,$enddate)
    {
        $sql="select action_date
        from currency_history where userID=:userID and currency_type=45 and (action_date between :start and :enddate)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    public function getLastTimeInvestmentBonusRecord($userID,$bonusType)
    {
        $sql="select action_date from currency_history where userID=:userID and currency_type=:currency_type order by historyID desc limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':currency_type',$bonusType);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    public function investmentPairingBonusSummeryOfThisDate($userID,$start,$enddate)
    {
        $sql="select count(historyID) as totalTimes, sum(amount) as totalBonus
        from currency_history where userID=:userID and currency_type=100 and (action_date between :start and :enddate)";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    
    public function getInvestmentIntroducerBonusList($userID,$limit)
    {
        $sql="select bonus_point,bonus_date,left_userID,right_userID from bonus 
        where bonus.userID=:userID and bonus_type=10 order by bonus_date desc, bonusID desc limit $limit,10
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    
    public function dateWiseAllInvestmentIntroduceBonus($userID,$start,$end,$limit)
    {
        $sql="select bonus_point,bonus_date,left_userID,right_userID from bonus 
        where bonus.userID=:userID and bonus_type=10 and ( bonus_date between :bonus_date and :bonus_date1)  order by bonus_date desc limit $limit, 10
        ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':bonus_date',$start);
        $stmt->bindValue(':bonus_date1',$end);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function investmentPairBonusList($userID,$limit)
    {
        $sql="select * from invest_pairing_bonus where userID=:userID and bonus>0 order by bonus_date desc limit $limit,10 ";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function dateWiseInvestmentPairBonusList($userID,$start,$enddate,$limit)
    {
        $sql="select * from invest_pairing_bonus where userID=:userID and bonus>0
        and (bonus_date between :start and :enddate) order by bonus_date desc limit $limit,10";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->bindValue(':start',$start);
        $stmt->bindValue(':enddate',$enddate);

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function investPairList($userID)
    {
        $sql="select * from invest_pairing_bonus where userID=:userID and bonus>0 order by bonus_date desc";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$userID);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**==========================================================================================================================================**/
    
    
}