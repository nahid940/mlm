<?php
/**
 * Created by PhpStorm.
 * User: PC-1 Agami Group
 * Date: 12/26/2018
 * Time: 12:46 PM
 */

namespace App\package;


use App\DBConnection;
use App\Session;

class Package
{
    /**
     * --------------------- get all packages -----------------------------------------------
    **/
    public function getAllPackages()
    {
        $sql="select * from packages where start_date<=:cur_date and end_date>=:cur_date and status=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':cur_date',date('Y-m-d'));
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function getPackages()
    {
        $sql="select * from packages where start_date<=:cur_date and end_date>=:cur_date";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':cur_date',date('Y-m-d'));
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    /**
     * --------------------- ---------------------------------------------------------------
     **/
     
       public function getCurrencyForPackage($id)
        {
        $sql="select packageRF,packageDC,packagePC,PP,MSP,package_type from packages where packageID=:id limit 1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':id',$id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
        }


     public function getCurrencyForPackage1()
    {
        $sql="select packageRF,packageDC,packagePC,PP,MSP from packages where packageID=1";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getCurrencyForPackage2()
    {
        $sql="select packageRF,packageDC,packagePC,PP,MSP from packages where packageID=2";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function getCurrencyForPackage3()
    {
        $sql="select packageRF,packageDC,packagePC,PP,MSP from packages where packageID=3";
        $stmt=DBConnection::myQuery($sql);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
    
    
    public function getPackageAmount($id)
    {
        $sql="select (sum(packageRF)+sum(packageDC)+sum(packagePC)) as total from packages where packageID=:id";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':id',$id);
        $stmt->execute();
        $res= $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['total'];
    }


    // public function getPackageAmount($id)
    // {
    //     $sql="select (sum(packageRF)+sum(packageDC)+sum(packagePC)) as total from packages where package_type=:id";
    //     $stmt=DBConnection::myQuery($sql);
    //     $stmt->bindValue(':id',$id);
    //     $stmt->execute();
    //     $res= $stmt->fetch(\PDO::FETCH_ASSOC);
    //     return $res['total'];
    // }

    public function getTotalUserOfEachPackage($packageID)
    {
        $sql="select count(packageID) as total from clients where packageID=:packageID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':packageID',$packageID);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $res['total'];
    }

    public function getPackageAmountforEachUser()
    {

        $sql="select packageID from clients where userID=:userID";
        $stmt=DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',Session::get('userID'));
        if($stmt->execute())
        {
            $res=$stmt->fetch(\PDO::FETCH_ASSOC);

            $sql="select packageDC,packagePC from packages where packageID=:packageID";
            $stmt1=DBConnection::myQuery($sql);
            $stmt1->bindValue(':packageID',$res['packageID']);
            $stmt1->execute();
            return $stmt1->fetch(\PDO::FETCH_ASSOC);

        }

    }
}