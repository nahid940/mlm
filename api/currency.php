<?php
//header("Access-Control-Allow-Origin: ");
//header("Content-Type: application/json; charset=UTF-8");

include_once '../vendor/autoload.php';
\App\Session::init();

$user=new \App\user\User();
$client=new \App\client\Client();

$id=$_GET['id'];

$userinfo=$user->getUserInfoForEcommerceUser($id);

extract($userinfo);

$msp=0;
$pp=10;
$base_url= $_SERVER['REQUEST_URI'];

if($pp==0)
{
    $amount=$msp;
    $type=1;
    $handle = curl_init();
    curl_setopt($handle, CURLOPT_URL, "http://ecommerce.agamisoft.net/api/transaction.php");
    curl_setopt($handle, CURLOPT_POST, 1);
    curl_setopt($handle, CURLOPT_POSTFIELDS,"userID=$id&amount=$amount&type=$type&uri=$base_url");
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $x=curl_exec($handle);
    curl_close($handle);

    if($x==200)
    {
        $client->decreaseMSPCurrencyFee($id,$msp);
    }else
    {
        echo "error";
    }
}else
{
    $amount=$pp;
    $type=2;
    $handle = curl_init();
    curl_setopt($handle, CURLOPT_URL, "http://ecommerce.agamisoft.net/api/transaction.php");
    curl_setopt($handle, CURLOPT_POST, 1);
    curl_setopt($handle, CURLOPT_POSTFIELDS,"userID=$id&amount=$amount&type=$type&uri=$base_url");
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $x=curl_exec($handle);
    curl_close($handle);

    if($x==200)
    {
        $client->decreasePPCurrencyFee($id,$pp);
    }else
    {
        echo "error";
    }
}

