<?php
include_once 'vendor/autoload.php';
\App\Session::init();
date_default_timezone_set("Asia/Dhaka");
$user=new \App\user\User();
$emailHelper=new \App\EmailHelper();
$helper=new \App\Helper();
include("simple-php-captcha.php");


$_SESSION['captcha'] = simple_php_captcha();
$submit='';
if(isset($_POST['forgot_pw']))
{

if ( strcasecmp($_POST['captcha'],\App\Session::get('code'))==0 ) {
    if (!empty($user->validateUserByUsername($_POST['emailid']))) {
        
        $info = $user->getEmailAndID($_POST['emailid']);
    
       // $user->updatePassword(md5($val), $info['userID']);

        /**---------------------send mail---------------------------------------**/
        // $body = "";
        // $body .= "<div style='padding: 20px;'>";
        // $body .= "<h1 style='text-align: center'>Dear " . $info['full_name'] . ",</h1>";
        // $body .= "<p style='text-align: center'> <strong>Username : " . $info['username'] . "</strong></p>";
        // $body .= "<p style='text-align: center'> <strong>Your new password is : " . base64_decode($info['password']) . "</strong></p>";
        // $body .= "</div>";
        
        
        $body.="
                <tbody><tr><td>
                    <table cellspacing='0' border='0' id='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432email-content' cellpadding='0' width='624'>
                        <tbody>
                        <tr>
                            <td>
                                <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                    <tbody>
                                    <tr>
                                        <td valign='top' align='center'>
                                            <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                                <tbody><tr>
                                                    <td valign='bottom'></td>
                                                </tr>
                                                </tbody>
                                            </table>";
                                            $body.="<h1>Retrieve Password</h1>
                                            <br>
                                            <br>
                                            <h1> Dear ".$info['full_name'].",</h1>
                                            <p> You requested to retrieve your password. <br> Your account details and password as follows:</p>
                                            <br/>
                                            <table border='0' cellpadding='2' cellspacing='0'>
                                                <tbody><tr>
                                                    <td><b>Username</b></td>
                                                    <td>:</td>
                                                    <td><b>".$info['username']."</b></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Account Password</b></td>
                                                    <td>:</td>
                                                    <td><b>".base64_decode($info['password'])."</b></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Security Password</b></td>
                                                    <td>:</td>
                                                    <td><b>".base64_decode($info['security_password'])."</b></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                            <p class='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432text'>
                                                Please ignore this email if you have already retrieved the account password. <br>
                                                Only you have receive this email. For more information, please contact us. 
                                            </p>
                                            <br>
                                            <br>
                                            <p>Mon Space Client Area URL : http://client.bmsm4all.com/secure/</p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                    <tbody>
                                    <tr>
                                        <td height='72'></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        ";
        

        $headers = array(
            'From' => 'mail.bmsm4all.com'
        );

        $emailHelper->setupEmail('mail.bmsm4all.com', 'support@bmsm4all.com', '##support123##', '587', 'tls', 'support@client.bmsm4all.com', 'Support', true);

        //$to, $name, $subject, $message, $header=null, $isHTML=true
        $emailHelper->sendMail($info['email'], 'Support', 'Monspacea - Account Password Retrieval', $body, null, true);
        /**--------------------------------------------------------------------------**/
       \App\Session::set('success', "Your request to retrieve your account password has been successful and an email including your account details has been sent to your email address of  ".$helper->obfuscate_email($info['email']).".");
       
       
       $submit=1;

    } else {
        \App\Session::set('error', 'The username that you have provided does not exist in the database.');
    }
}else
{
    \App\Session::set('error','Invalid Captcha.');
}
}
\App\Session::set('code',$_SESSION['captcha']['code']);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Monspacea &raquo; Member's login</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="html_template/default/login_assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="html_template/default/login_assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="html_template/default/login_assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <link href="html_template/default/assets/img/logo/favicon.ico" rel="icon" type="image/ico">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login" style="">

<!-- BEGIN LOGIN -->

<div class="content">
    <div class="logo">
        <img src="./html_template/default/login_assets/img/logo.png" border="0">
    </div>
    <div class="languageSize"><a href="?__lang=en" target="_self">English</a>  | <a href="?__lang=cn" target="_self">简体中文</a> | <a href="?__lang=kr" target="_self">Korean (조선말)</a> | <a href="?__lang=id" target="_self">Indonesian</a> | <a href="?__lang=th" target="_self">Thai</a> | <a href="?__lang=vn" target="_self">Vietnamese</a> | <a href="?__lang=jp" target="_self">Japanese</a></div>
    <form class="form-vertical " action="" method="post" novalidate="novalidate">
        <input type="hidden" name="__req" value="1">
        <h3>RETRIEVE FORGOTTEN PASSWORD</h3>

        <?php if(\App\Session::get('error')) {?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert"></button>
                <span><?php echo \App\Session::get('error')?></span>
            </div>
            <?php
            \App\Session::UnsetKeySession('error');
        }
        ?>

        <?php if(\App\Session::get('success')) {?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert"></button>
                <span><?php echo \App\Session::get('success')?></span>
            </div>
            <?php
            \App\Session::UnsetKeySession('success');
        }
        ?>
        
        <?php if($submit=='') {?>
        
        <p>Please fill in your Username in the field below.</p>
        <div class="control-group">
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-envelope"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" autocomplete="off" name="emailid">
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class='controls'>
                <div class='input-icon left'>
                    <i class='fa fa-check-square'></i>
                    <?php
                    echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code" style="background-repeat: repeat-x;width:100%;height:55px;margin-bottom: 20px" >';
                    ?>
                    <input type='text' class='m-wrap placeholder-no-fix' autocomplete='off' name='captcha' placeholder='Captcha' >
                </div>
            </div>
        </div>
        <div class="form-actions">
            <a href="login.php" class="btn btn-light-grey">
                <i class="m-icon-swapleft"></i> Back
            </a>
            <button type="submit" class="btn blue pull-right" name="forgot_pw">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        
        
        
        <?php } else {?>
            <div style="text-align:center">
                <a href="login.php" class='btn'>Sign in</a>
            </div>
        <?php }?>
    </form>
</div>

<!-- END LOGIN -->

<script src="html_template/default/login_assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="html_template/default/login_assets/scripts/app.js" type="text/javascript"></script>
<script src="html_template/default/login_assets/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        App.init();
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->


<!-- END BODY -->
<div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 265px; width: 1349px; z-index: -999999; position: fixed;"><img src="html_template/default/login_assets/img/bg/2.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1349px; height: 899.333px; max-width: none; z-index: -999999; left: 0px; top: -317.167px;"></div></body></html>