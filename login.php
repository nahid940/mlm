<?php
include_once 'vendor/autoload.php';
\App\Session::init();
date_default_timezone_set('Asia/Kuala_Lumpur');
$user=new \App\user\User();
\App\Session::UnsetKeySession('passwrodupdate');
include("simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();
if(\App\Session::get('login')==true)
{
    header("Location: members/index.php");
    exit;
}

if(isset($_POST['login'])) {
    if ( strcasecmp($_POST['captcha'],\App\Session::get('code'))==0 ) {
        try {
            $user->userLogin($_POST['username'], base64_encode($_POST['password']), 1, 1, 1);
        } catch (Exception $e) {
            \App\Session::set('login-failure',$e->getMessage());
        }
    }else
    {
        \App\Session::set('error','Invalid Captcha.');
    }
}

\App\Session::set('code',$_SESSION['captcha']['code']);
?>


<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Monspacea &raquo; Member's login</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
   
	<link href="http://localhost/mlm/html_template/default/login_assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="http://localhost/mlm/html_template/default/login_assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="http://localhost/mlm/html_template/default/login_assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="http://localhost/mlm/html_template/default/login_assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="http://localhost/mlm/html_template/default/login_assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="http://localhost/mlm/html_template/default/login_assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link href="http://localhost/mlm/html_template/default/assets/img/logo/favicon.ico" rel="icon" type="image/ico">
	
	<style>
	    @media only screen and (max-width: 768px) {
          .col-md-5{
            width:400px;
          }
        }
	</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
	
	<!-- BEGIN LOGIN -->
	
	<!-- BEGIN LOGIN -->
    <?php if(date('H:i A')>='24:00 PM' || date('H:i A')<='06:00 AM'){?>

    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div style="text-align: center;margin-top: 8%" class="img">
                    <img  src='off.png' border='0' style="width:500px;">
                </div>
            </div>
        </div>
    </div>
        
    <?php  //else if(date('H:i A')<='04:00 AM'){ ?>

<!--        <div style="text-align: center;margin-top: 3%">-->
<!--            <img  src='off.png' border='0'>-->
<!--        </div>-->

    <?php } else {?>
	
			<div class='content'>
				<div class='logo'>
					<img src='http://client.bmsm4all.com/secure/html_template/default/login_assets/img/logo.png' border='0' >
				</div>
				
				<div class='languageSize'><a href='?__lang=en' target='_self'>English</a>  | <a href='?__lang=cn' target='_self'>简体中文</a> | <a href='?__lang=kr' target='_self'>Korean (조선말)</a> | <a href='?__lang=id' target='_self'>Indonesian</a> | <a href='?__lang=th' target='_self'>Thai</a> | <a href='?__lang=vn' target='_self'>Vietnamese</a> | <a href='?__lang=jp' target='_self'>Japanese</a></div>
				<form class='form-vertical login-form' action='login.php' method='post' novalidate='novalidate' style='display: block;'>
					<input type='hidden' name='__log' value='1'>
					<h3 class='form-title'>LOGIN TO YOUR ACCOUNT</h3>
					
					<?php if(\App\Session::get('error')) {?>
                        <div class="alert alert-error">
                            <button class="close" data-dismiss="alert"></button>
                            <span><?php echo \App\Session::get('error')?></span>
                        </div>
                    <?php
                        \App\Session::UnsetKeySession('error');
                    }
                    ?>

                    <?php if(\App\Session::get('login-failure')) {?>
                        <div class="alert alert-error">
                            <button class="close" data-dismiss="alert"></button>
                            <span><?php echo \App\Session::get('login-failure')?></span>
                        </div>
                    <?php
                        \App\Session::UnsetKeySession('login-failure');
                    }
                    ?>
					
					<div class='control-group'>
						<label class='control-label visible-ie8 visible-ie9'>Username</label>
						<div class='controls'>
							<div class='input-icon left'>
								<i class='icon-user'></i>
								<input class='m-wrap placeholder-no-fix' type='text' autocomplete='off' placeholder='Username' name='username'>
							</div>
						</div>
					</div>
				  <div class='control-group'>
						<label class='control-label visible-ie8 visible-ie9'>Password</label>
						<div class='controls'>
							<div class='input-icon left'>
								<i class='icon-lock'></i>
								<input class='m-wrap placeholder-no-fix' type='password' autocomplete='off' placeholder='Password' name='password'>
							</div>
						</div>
					</div>
					<div class='control-group'>
						<label class='control-label visible-ie8 visible-ie9'>Password</label>
						<div class='controls'>
							<div class='input-icon left'>
								<i class='fa fa-check-square'></i>
                                <?php
                                echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code" style="background-repeat: repeat-x;width:100%;height:55px;margin-bottom: 20px" >';
                                ?>
								<input type='text' class='m-wrap placeholder-no-fix' autocomplete='off' name='captcha' placeholder='Captcha' >
							</div>
						</div>
					</div>
					<div class='form-actions'>
						<button type='submit' class='btn blue pull-right' name="login">
							Login <i class='m-icon-swapright m-icon-white'></i>
						</button>
					</div>
					<div class='forget-password'>
						<h4>FORGOT YOUR PASSWORD</h4>
						<p>
							Click <a href='forgot_password.php' >here</a>
							to reset your password.
						</p>
						<p>Please <a href='contact_us.php'>contact us</a> if your email address is incorrect</p>
					</div>
					<br/><br/>
				</form>     
			</div>
	<!-- END LOGIN -->

	<script src="http://localhost/mlm/html_template/default/login_assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="http://localhost/mlm/html_template/default/login_assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="http://localhost/mlm/html_template/default/login_assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="http://localhost/mlm/html_template/default/login_assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>

	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="http://localhost/mlm/html_template/default/login_assets/scripts/app.js" type="text/javascript"></script>
	<script src="http://localhost/mlm/html_template/default/login_assets/scripts/login-soft.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script>
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
<?php } ?>
</body>
<!-- END BODY -->
</html>