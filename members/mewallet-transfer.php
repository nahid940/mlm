<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
<!-- PAGE HEADER-->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <!-- STYLER -->

            <!-- /STYLER -->
            <!-- BREADCRUMBS -->
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="members/index.php">Home</a>
                </li>
                <li> Transfer</li>
                <li>GODC Transfer</li>
            </ul>
            <!-- /BREADCRUMBS -->
            <div class="clearfix">
                <h3 class="content-title pull-left">GODC Transfer</h3>
            </div>
        </div>
    </div>
</div>
<!-- /PAGE HEADER -->


<div class='row'>
    <div class='col-md-3'>
        <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
            <div style='font-size:20px;'>0</div>
            <div>GODC</div>
        </a>
    </div>
</div>


<div class='row'>
    <div class='col-md-12'>
        <div class='box border'>
            <div class='box-title'>
                <h4 style='height:15px;'></h4>
            </div>
            <div class='box-body'>
                <div class='tabbable header-tabs user-profile'>
                    <ul class='nav nav-tabs'>
                        <li ><a href='members/cwallet-transfer.php'>MSP</a></li>
                        <li ><a href='members/amwallet-transfer.php'>SEL</a></li>
                        <li class='active'><a href='members/iwallet-transfer.php'>HTC</a></li>

                        <li ><a href='members/cpoint-transfer.php'>CMC</a></li>
                        <li ><a href='members/aiwallet-transfer.php'>AIDC</a></li>
                        <li class='active'><a href='members/mewallet-transfer.php'>GODC</a></li>
                        <li ><a href='members/rpoint-transfer.php'>Declaration Currency</a></li>
                        <li ><a href='members/epoint-transfer.php'>WC</a></li>
                        <li ><a href='members/rwallet-transfer.php'>Registration Fee</a></li>
                    </ul>
                    <div class='tab-content'>
                        <div class='tab-pane fade in active'>
                            <div class='row'>

                                <div class='col-md-12'>

                                    <br/>
                                    <form name='transfer_form' method='post' action='mewallet-transfer.php' class='form-horizontal form-bordered form-row-stripped'>
                                        <input type='hidden' name='__req' value='1'>
                                        <div class='form-body'>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Transfer Point Type: <span class='require'>*</span></label>
                                                <div class='col-md-5'>
                                                    GODC Transfer
                                                </div>
                                            </div>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Transfer Amount: <span class='require'>*</span></label>
                                                <div class='col-md-5'>
                                                    <div class='row'>
                                                        <div class='col-md-3' style='margin-top:0px;'>
                                                            1 x
                                                        </div>
                                                        <div class='col-md-9' style='margin-top:0px;'>
                                                            <input type='number' name='_amount' class='form-control' value='' min='0' step='1'>
                                                        </div>
                                                    </div>
                                                    <p class='help-block'>Transfer GODC is 1 times.(E.g : 1 unit equal to 1)</p>
                                                </div>
                                            </div>
                                            <div class='form-group' id='transfer'>
                                                <label class='col-md-3 control-label'>Transfer To: <span class='require'>*</span></label>
                                                <div class='col-md-5'>
                                                    <input type='text' name='uid' class='form-control' value=''>
                                                    <span class='help-block' id='transferee_text'>Please provide a Username.</span>
                                                    <input type='button' id='check_user_btn' value='Check User' class='btn btn-info'>
                                                    <div id='search_user_result'></div>
                                                </div>
                                            </div>
                                            <div class='form-group' id='mewallet'>
                                                <label class='col-md-3 control-label'>GODC: <span class='require'>*</span></label>
                                                <div class='col-md-5' >
                                                    0
                                                </div>
                                            </div>
                                            <div class='form-group' id='remark'>
                                                <label class='col-md-3 control-label'>Remark: <span class='require'>*</span></label>
                                                <div class='col-md-5'>
                                                    <textarea type='text' name='remark' class='form-control'></textarea>
                                                </div>
                                            </div>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Security Password:</label>
                                                <div class='col-md-5'>
                                                    <input type='password' name='password' class='form-control' value=''>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'></label>
                                                <div class='col-md-5'>
                                                    <input type='submit' name='submit_btn' value='Submit' class='btn btn-primary' />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once "includes/footer.php";?>


<script type='text/javascript'>
    jQuery(document).ready(function(j){
        j('form[name=transfer_form]').submit(function(){
            j('input[name=submit_btn]').attr('disabled','disabled');
        });

        j('input#check_user_btn').click(function(){
            //alert(1);
            j('span#transferee_text').html('').removeClass('red');
            if(j('input[name=uid]').val()==''){
                j('span#transferee_text').html('Please provide a Username.').addClass('red');
            }
            else{
                j(this).attr('disabled','disabled');
                j.ajax({
                    url: 'mewallet-transfer.php?aj=1&check_user=1',
                    data: {'uid': j('input[name=uid]').val()},
                    type: 'post',
                    dataType: 'html',
                    error: function(){
                        j('span#transferee_text').html('Error checking...').addClass('red');
                    },
                    success: function(data){
                        var mesg='';
                        if(j('loggedout', data).text()=='loggedout'){
                            mesg='You have been logged out.';
                        }else{
                            j(data).find('msg').each(function(){
                                mesg+=j(this).text()+' ';
                            });
                        }
                        var status=j(data).find('status').text();
                        j('span#transferee_text').html(mesg).addClass(status==1? 'bold' : 'red');
                    },
                    complete: function(){
                        j('input#check_user_btn').removeAttr('disabled');
                    }
                });
            }
        });

        j('input[name=back_btn]').click(function(){
            q('__edit').value = 1;
            q('__confirm').value = '';
            j('input[name=back_btn],input[name=submit_btn]').attr('disabled','disabled');
            document.transfer_form.submit();
        });
    });
</script>
<?php }else {
    header('location:../login.php');
}?>