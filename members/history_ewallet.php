<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
$user=new \App\user\User();
$currency=$user->userCurrencies(\App\Session::get('userID'));
$histories=$user->getECHistory(\App\Session::get('userID'));
$helper=new \App\Helper();


?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>Electronic Currency</li>
                    <li>Electronic Currency History</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Electronic Currency History</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->

    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo (empty($currency['ec'])?'0.00':number_format($currency['ec'],2))?></div>
                <div>Electronic Currency</div>
            </a>
        </div>
    </div>
    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li class='active'><a href='members/history_ewallet.php'>Electronic Currency History</a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <div class='col-md-12'>

                                        <table id='example-paper' class='table table-paper table-striped'>
                                            <thead>
                                                <tr>
                                                    <th style='width: 8%;text-align:center;'>No.</th>
                                                    <th style='width: 8%;text-align:center;'>Ref #</th>
                                                    <th style='text-align:center;'>Date</th>
                                                    <th style='text-align:center;'>Description</th>
                                                    <th style='width: 12%;text-align:center;'>Debit</th>
                                                    <th style='width: 12%;text-align:center;'>Credit</th>
                                                    <th style='width: 12%;text-align:center;'>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            <?php $i=0; foreach ($histories as $history){ $i++?>
                                                <?php if($history['debit']+$history['credit'] !=0) {?>
                                                    <tr class='row0'>
                                                        <td style='text-align:center;'><?php echo $i?></td>
                                                        <td style='text-align:center;'>#<?php echo $helper->generateReference($history['historyID'],$history['historyID'])?></td>
                                                        <td style='text-align:center;width:110px'><?php echo date('d-m-Y',strtotime($history['action_date']))?></td>
                                                        <td style='text-align:center;width:250px;'><?php echo $history['description']?><br/></td>
                                                        <td class='number' style='text-align:center;'><span class='red'><?php echo number_format($history['debit'],2)?></td>
                                                        <td class='number' style='text-align:center;'><span class='green'><?php echo number_format( $history['credit'],2) ?></td>
                                                        <td class='number' style='text-align:center;'><span class='green'>
                                                        <?php 
                                                        
                                                        if($history['receiverID']==0)
                                                        {
                                                            echo number_format($history['user_total'],2);
                                                        }
                                                        
                                                        ?></td>
                                                    </tr>
                                                <?php }?>
                                               
                                            <?php }?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>
