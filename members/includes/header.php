<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <title>Monspacea &raquo; Member's Home</title>
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">-->
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="http://localhost/mlm/">


    <link rel="stylesheet" type="text/css" href="html_template/default/assets/css/cloud-admin.css" >
    <link rel="stylesheet" type="text/css"  href="html_template/default/assets/css/themes/default.css" id="skin-switcher" >
    <link rel="stylesheet" type="text/css"  href="html_template/default/assets/css/responsive.css" >

    <!-- added -->
    <link rel="stylesheet" type="text/css" href="html_template/default/assets/js/datepicker/themes/default.min.css" />
    <link rel="stylesheet" type="text/css" href="html_template/default/assets/js/datepicker/themes/default.date.min.css" />
    <link rel="stylesheet" type="text/css" href="html_template/default/assets/js/datepicker/themes/default.time.min.css" />
    <link href="html_template/default/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- DATE RANGE PICKER -->
    <link rel="stylesheet" type="text/css" href="html_template/default/assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" media="all" rel="stylesheet" type="text/css" />

    <link href="html_template/default/assets/css/style.css?ver=1.5" rel="stylesheet">
    <!-- FONTS -->
    <link href="html_template/default/assets/img/logo/favicon.ico" rel="icon" type="image/ico">

    <link rel="stylesheet" href="tree/zTreeStyle.css" type="text/css">

    <link href="tree/dtree.css" rel="stylesheet" />

</head>
<body>
<!-- HEADER -->
<header class="navbar clearfix" id="header">
    <div class="container">
        <div class="navbar-brand">
            <!-- COMPANY LOGO -->
            <a href="members/index.php">
                <img src="html_template/default/assets/img/logo/logo.png" class="img-responsive" height="30">
            </a>
            <!-- /COMPANY LOGO -->
            <!-- TEAM STATUS FOR MOBILE -->
            <div class="visible-xs">
                <a href="#" class="team-status-toggle switcher btn dropdown-toggle">
                    <i class="fa fa-users"></i>
                </a>
            </div>
            <!-- /TEAM STATUS FOR MOBILE -->
            <!-- SIDEBAR COLLAPSE -->
            <div id="sidebar-collapse" class="sidebar-collapse btn">
                <i class="fa fa-bars"
                   data-icon1="fa fa-bars"
                   data-icon2="fa fa-bars" ></i>
            </div>
            <!-- /SIDEBAR COLLAPSE -->
        </div>
        <!-- NAVBAR LEFT -->

        <!-- /NAVBAR LEFT -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">

            <!-- BEGIN TODO DROPDOWN -->
            <li class='dropdown' style='margin-left: 100px;'>
                <a href='javascript:void(0)' class='dropdown-toggle'>
                    <i class='fa fa-envelope'></i>
                    <span class='badge'></span>
                </a>
            </li>
            <li class="dropdown" id="header-tasks" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-tasks"></i>
                    <span class="badge"></span>
                </a>
                <ul class="dropdown-menu tasks">
                    <li class="dropdown-title">
                        <span> Language</span>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
									<span class="header clearfix">
										<span class="pull-left">EN</span>
									</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
									<span class="header clearfix">
										<span class="pull-left">中文</span>
									</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
									<span class="header clearfix">
										<span class="pull-left">Korean</span>
									</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
									<span class="header clearfix">
										<span class="pull-left">Indonesian</span>
									</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
									<span class="header clearfix">
										<span class="pull-left">Thai</span>
									</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
									<span class="header clearfix">
										<span class="pull-left">Vietnamese</span>
									</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
									<span class="header clearfix">
										<span class="pull-left">Japanese</span>
									</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END TODO DROPDOWN -->
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user" id="header-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php
                        $email = \App\Session::get('email');
                        $hash = base64_encode( strtolower( trim( $email ) ) );
                        $default = "http://client.msm4all.com/secure/html_template/default/assets/img/avatars/avatar3.jpg";
                        $size = 40;
                        $grav_url = $grav_url = "https://www.gravatar.com/avatar/" . base64_encode( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
                    ?>
                    <img alt="" src="<?php echo $grav_url?>" />

                    <span class="username"><?php echo \App\Session::get('username')?></span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="members/profile.php"><i class="fa fa-user"></i> Profile</a></li>
                    <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>

</header>
<!--/HEADER --><!-- PAGE -->

<section id="page">
    <!-- SIDEBAR -->
    <div id="sidebar" class="sidebar">
        <div class="sidebar-menu nav-collapse">

            <!-- SIDEBAR MENU -->
            <ul>

                <li class='active'>
                    <a href='members/index.php'>
                        <i class='fa fa-home fa-fw'></i> <span class='menu-text'>Home</span>
                    </a>
                </li>
                <li class='has-sub '>
                    <a class=''>
                        <i class='fa fa-user fa-fw'></i> <span class='menu-text'>Profile</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li><a href='members/profile.php'>Personal Profile</a></li>
                        <li><a href='members/profile-password.php'>Password</a></li>
                    </ul>
                </li>
                
                 <li class='has-sub '>
                    
                    <a class=''>
                        <i class='fa fa-gift fa-fw'></i> <span class='menu-text'>Commission</span>
                        <span class='arrow'></span>
                    </a>
                    
                    <ul class='sub'>
                        <li><a href='members/rebates.php'>Electronic Currency</a></li>
                        <li><a href='members/bc-rebates.php'>Business Currency</a></li>
                    </ul>
                </li>
                
                <li class='has-sub '>
                    <a class=''>
                        <i class='fa fa-stack-exchange fa-fw'></i> <span class='menu-text'> Transfer</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li><a href='members/rwallet-transfer.php'>Registration Fee</a></li>
                        <li><a href='members/rpoint-transfer.php'>Declaration Currency</a></li>
                        <li><a href='members/pc_transfer.php'>Product Currency</a></li>
                        <li>
                            <a href='members/trade-currency-transfer.php'>
                                <span class='menu-text'>Trade Currency</span>
                            </a>
                        </li>
                        <li>
                            <a href='members/bc-transfer.php'>
                                <span class='menu-text'>Business Currency</span>
                            </a>
                        </li>
                        <li><a href='members/pp_transfer.php'>Product Point</a></li>
                        <li><a href='members/msp_transfer.php'>MSP</a></li>
                        
                    </ul>
                </li>
              
                
                <li class='has-sub '>
                    <a class=''>
                        <i class='fa fa-stack-exchange fa-fw'></i> <span class='menu-text'>Purchase</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li>
                            <a href='members/r_purchase.php'>
                                 <span class='menu-text'>Declaration Currency</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href='members/bc_pc.php'>
                                 <span class='menu-text'>Product Currency</span>
                            </a>
                        </li>
                        <li>
                            <a href='members/bc_tc.php'>
                                 <span class='menu-text'>Trade Currency</span>
                            </a>
                        </li>
                        <li>
                            <a href='members/tp-bc.php'>
                                 <span class='menu-text'>Business Currency</span>
                            </a>
                        </li>
                        <li>
                            <a href='members/pp_purchase.php'>
                                 <span class='menu-text'>Product Point</span>
                            </a>
                        </li>
                        <li>
                            <a href='members/bc_bdt.php'>
                                 <span class='menu-text'>MSM</span>
                            </a>
                        </li>
                        <!--<li>-->
                        <!--    <a href='members/r_purchase.php'>-->
                        <!--        <i class='fa fa-arrows-h fa-fw'></i> <span class='menu-text'>Purchase Points</span>-->
                        <!--    </a>-->
                        <!--</li>-->
                        <!--<li>-->
                        <!--    <a href='members/invest_history.php'>-->
                        <!--        <span class='menu-text'>Purchase History</span>-->
                        <!--    </a>-->
                        <!--</li>-->
                    </ul>
                </li>

                <li class='has-sub '>
                    <a class=''>
                        <i class='fa fa-long-arrow-right fa-fw'></i> <span class='menu-text'> Transfer To MSP</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li><a href='members/ec_purchase.php'>Electronic Currency</a></li>
                        <li><a href='members/dc_purchase.php'>Declaration Currency</a></li>
                        <li><a href='members/history_cwallet.php'>MSP History</a></li>
                        <li><a href='members/mc_purchase.php'>GPmall Transfer</a></li>
                    </ul>
                </li>
                <li class=''>
                    <a href='members/history_ewallet.php'>
                        <i class='fa fa-money fa-fw'></i> <span class='menu-text'>Electronic Currency</span>
                    </a>
                </li>
                <li class=''>
                    <a href='members/history_rwallet.php'>
                        <i class='fa fa-inr fa-fw'></i> <span class='menu-text'>Registration Fee</span>
                    </a>
                </li>
<!--                <li class=''>-->
<!--                    <a href='members/history_epoint.php'>-->
<!--                        <i class='fa fa-btc fa-fw'></i> <span class='menu-text'>WC</span>-->
<!--                    </a>-->
<!--                </li>-->

                <li class=''>
                    <a href='members/history_rpoint.php'>
                        <i class='fa fa-try fa-fw'></i> <span class='menu-text'>Declaration Currency</span>
                    </a>
                </li>
                <li class=''>
                    <a href='members/pc-history.php'>
                        <i class='fa fa-jpy fa-fw'></i> <span class='menu-text'>Product Currency</span>
                    </a>
                </li>
                <li class='has-sub'>
                    <a class=''>
                        <i class="fa fa-briefcase fa-fw" aria-hidden="true"></i> <span class='menu-text'>Product Point</span>
                        <span class='arrow'></span>
                    </a>
                     <ul class='sub'>
                        <li>
                            <a href ="members/pp-withdraw.php" class=''>
                                GPMall Transfer
                            </a>
                        </li>
                        <li>
                            <a href='members/pp-history.php'>
                                Product Point History
                            </a>
                        </li>
                    </ul>
                </li>
                
                <li class='has-sub '>
                    <a class=''>
                        <i class='fa fa-stack-exchange fa-fw'></i> <span class='menu-text'>Trading</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li>
                            <a href='members/client_invest.php'>
                                <span class='menu-text'>Trade Policy</span>
                            </a>
                        </li>
                        <li>
                            <a href='members/invest_history.php'>
                                <span class='menu-text'>History</span>
                            </a>
                        </li>
                    </ul>
                </li>
                
                <li>
                    <li>
                        <a href='members/trade-currency-history.php'>
                           <i class="fa fa-align-center" aria-hidden="true"></i>  <span class='menu-text'>Trade Currency</span>
                        </a>
                    </li>
                    <!--<a class=''>-->
                    <!--    <i class="fa fa-align-center" aria-hidden="true"></i> <span class='menu-text'>Trade Currency</span>-->
                    <!--    <span class='arrow'></span>-->
                    <!--</a>-->
                    <!--<ul class='sub'>-->
                        
                        <!--<li>-->
                        <!--    <a href='members/bc_tc.php'>-->
                        <!--        <span class='menu-text'>Purchase Trade Currency</span>-->
                        <!--    </a>-->
                        <!--</li>-->
                    <!--</ul>-->
                </li>
                
                <!--<li class='has-sub '>-->
                    
                <!--    <ul class='sub'>-->
                <!--        <li>-->
                <!--            <a href='members/trade-profit-history.php'>-->
                <!--                <span class='menu-text'>Trade Point History</span>-->
                <!--            </a>-->
                <!--        </li>-->
                <!--        <li>-->
                <!--            <a href='members/tp-bc.php'>-->
                <!--                <span class='menu-text'>Transfer to Business Currency</span>-->
                <!--            </a>-->
                <!--        </li>-->
                <!--    </ul>-->
                <!--</li>-->
                
                <li class=''>
                    <a href='members/trade-profit-history.php'>
                        <i class='fa fa-stack-exchange fa-fw'></i> <span class='menu-text'>Trade Point</span>
                    </a>
                </li>
                
                
                <li class=''>
                    <a href='members/bc_history.php'>
                        <i class="fa fa-th" aria-hidden="true"></i> <span class='menu-text'>Business Currency</span>
                    </a>
                </li>
                
                <li class='has-sub '>
                    <a class=''>
                        <i class='fa fa-stack-exchange fa-fw'></i> <span class='menu-text'>MSM</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li>
                            <a href='members/cash_withdraw.php'>
                                <span class='menu-text'>MSM Payout</span>
                            </a>
                        </li>
                        <li>
                            <a href='members/cash_history.php'>
                                <span class='menu-text'>MSM History</span>
                            </a>
                        </li>
                    </ul>
                </li>
                
                <li class='has-sub '>
                    <a class=''>
                        <i class='fa fa-plus-circle fa-fw'></i> <span class='menu-text'>Membership</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li><a href='members/register.php'>Registration</a></li>
                        <li><a href='members/renew.php'>Account Renewal</a></li>
                        <li><a href='members/upgrade.php'>Account Upgrade</a></li>
                    </ul>
                </li>
                
                
                <?php if(\App\Session::get('page_approved')==true){?>
                <li class='has-sub'>
                    <a class=''>
                        <i class='fa fa-cogs fa-fw'></i> <span class='menu-text'>Genealogy</span>
                        <span class='arrow'></span>
                    </a>
                    <ul class='sub'>
                        <li><a href='members/genealogy.php'>Client List</a></li>
                        <li><a href='members/genealogy2.php?view=network'>Network Genealogy</a></li>
                    </ul>
                </li>
                <?php }else{?>
                <li class='has-sub'>
                    <a href="members/clientlist.php" class=''>
                        <i class='fa fa-cogs fa-fw'></i> <span class='menu-text'>Client List</span>
                    </a>
                </li>
                <?php }?>
                
                <!--<li class='has-sub'>-->
                <!--    <a class=''>-->
                <!--        <i class='fa fa-cogs fa-fw'></i> <span class='menu-text'>Trade Center</span>-->
                <!--        <span class='arrow'></span>-->
                <!--    </a>-->
                <!--    <ul class='sub'>-->
                <!--        <li><a href='members/genealogy.php'>Trade Currency</a></li>-->
                <!--        <li><a href='members/genealogy2.php?view=network'>Trade Currency History</a></li>-->
                <!--    </ul>-->
                <!--</li>-->
                
                <li class=''>
                    <a href='members/news.php'>
                        <i class='fa fa-list-alt fa-fw'></i> <span class='menu-text'>News<span class='tooltip-error pull-right' title='' data-original-title=''></span> </span>
                    </a>
                </li>
                <li class=''>
                    <a href='members/videos.php'>
                        <i class='fa fa-video-camera fa-fw'></i> <span class='menu-text'>Videos</span>
                    </a>
                </li>


                <li class=''>
                    <a href='logout.php'>
                        <i class='fa fa-power-off fa-fw'></i> <span class='menu-text'>Logout</span>
                    </a>
                </li>


            </ul>
            <!-- /SIDEBAR MENU -->
        </div>
    </div>
    <!-- /SIDEBAR -->
    <div id="main-content">
        <div class="container">
            <div class="row">

