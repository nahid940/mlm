
</div>
</div>
</div>
</section>
<!--/PAGE -->
<!-- JAVASCRIPTS -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<script src="html_template/default/assets/js/jquery/jquery-2.0.3.min.js"></script>
<!-- JQUERY UI-->
<script src="html_template/default/assets/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="html_template/default/assets/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
<!-- BOOTSTRAP -->
<script src="html_template/default/assets/bootstrap-dist/js/bootstrap.min.js"></script>


<!-- DATE RANGE PICKER -->
<script src="html_template/default/assets/js/bootstrap-daterangepicker/moment.min.js"></script>

<!-- <script src="html_template/default/assets/js/bootstrap-daterangepicker/daterangepicker.min.js"></script> -->
<!-- SLIMSCROLL -->
<!-- <script type="text/javascript" src="html_template/default/assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script><script type="text/javascript" src="html_template/default/assets/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script> -->
<!-- COOKIE -->
<script type="text/javascript" src="html_template/default/assets/js/jQuery-Cookie/jquery.cookie.min.js"></script>
<!-- CUSTOM SCRIPT -->
<script src="html_template/default/assets/js/script.js"></script>

<!-- added -->
<script type="text/javascript" src="html_template/default/assets/js/datepicker/picker.js"></script>
<script type="text/javascript" src="html_template/default/assets/js/datepicker/picker.date.js"></script>
<script type="text/javascript" src="html_template/default/assets/js/datepicker/picker.time.js"></script>

<script>
    jQuery(document).ready(function() {
        App.setPage("widgets_box");  //Set current page
        App.setPage("elements");
        App.init(); //Initialise plugins and elements
    });
</script>

<link rel="stylesheet" href="res/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="res/fancybox/jquery.fancybox.js"></script>

<script type='text/javascript' src='html_template/defaults/assets/js/common.js'></script>
<script type='text/javascript' src='html_template/defaults/assets/js/jquery/jquery.livequery.js'></script>
<script src="tree/dtree.js"></script>
<script>
    $(document).ready(function(){
        $(".dTree").dTree();
    });
</script>

<script>
    var _idepth = 10000;

    $(document).ready(function() {

        $.fancybox($('#content0').html(), //fancybox works perfect with hidden divs
            {
                'closeClick'  		 : false, // prevents closing when clicking INSIDE fancybox
                'helpers'     		 : { overlay : {closeClick: false} }

            }
        );
    });

    function pop(id,obj,ok){
        var overlay_name = "overlay-" + id;
        _idepth++;
        $(document.body).prepend('<div class="modalOverLay '+overlay_name+'" style="background-color:black;position:absolute;left:0px;top:0px;z-index:'+_idepth+'"></div>');
        var w = $(document).width();
        var h = $(document).height();
        var _top = (h - $("#"+id).height()) / 3;
        /*
         if(_top > 500){
         _top = 300;
         }
         */

        $('.modalOverLay').css({ opacity: 0.3, width:w, height: $(document).height()});


        var defaultcss = {
            position: 'absolute',
            'z-index': _idepth,
            left: (w - $("#"+id).width()) / 2,
            top : _top
        }
        for(k in obj)defaultcss[k] = obj[k];
        $("#"+id).css(defaultcss);
        $("#"+id).show();
        $("."+overlay_name).show();
        if(typeof ok == "function")ok.call(this);
    }

    function hide(id){
        var overlay_name = "overlay-" + id;
        $("."+overlay_name).remove();
        $("#"+id).hide();
    }
    
    
    
    
    
//     if (document.layers) {
//     //Capture the MouseDown event.
//     document.captureEvents(Event.MOUSEDOWN);
 
//     //Disable the OnMouseDown event handler.
//     document.onmousedown = function () {
//         return false;
//     };
//     }
//     else {
//         //Disable the OnMouseUp event handler.
//         document.onmouseup = function (e) {
//             if (e != null && e.type == "mouseup") {
//                 //Check the Mouse Button which is clicked.
//                 if (e.which == 2 || e.which == 3) {
//                     //If the Button is middle or right then disable.
//                     return false;
//                 }
//             }
//         };
//     }
 
// //Disable the Context Menu event.
// document.oncontextmenu = function () {
//     return false;
// };
    
    
    
    
</script>


<!-- /JAVASCRIPTS -->
</body>
</html>