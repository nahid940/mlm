<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();

$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
$nominee=new \App\nominee\Nominee();
$currency=$user->userCurrencies(\App\Session::get('userID'));

$total_pairingEC=$user->totalPairBonus(\App\Session::get('userID'));

$levelBonusSummery=$user->levelingBonusSummery(\App\Session::get('userID'))['totalBonus'];

$introducerBonusSummery=$user->introducerBonusSummery(\App\Session::get('userID'))['totalBonus'];

date_default_timezone_set('Asia/Dhaka');
if(\App\Session::get('login')==true) {

    if(isset($_POST['sec_password']) ) {
        
        if($_POST['sec_password']!=''){
            if (isset($_POST['modify'])) {
            if ($user->verifyUser(base64_encode($_POST['sec_password']))) {

                $user->set($_POST);
                $user->updateUserInfo();

                $nominee->set($_POST);
                $nominee->modifyNominee();
                \App\Session::set('success','You have successfully updated your account information.');
            } else {
                \App\Session::set('error','You have entered an invalid Security Password.');
            }
        }
        }else{
            \App\Session::set('error','Please provide your Security Password.');
        }
        
    }
    
    $nomineeinfo=$nominee->getNomineeInfo();
    $info=$user->getUserInfo();
    $sponsor=$user->getSponsorDetails($info['sponsor_id']);
?>
<?php include_once "includes/header.php";?>

    <div id="content" class="col-lg-12">
                    <!-- PAGE HEADER-->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                <!-- STYLER -->

                                <!-- /STYLER -->
                                <!-- BREADCRUMBS -->
                                <ul class="breadcrumb">
                                    <li>
                                        <i class="fa fa-home"></i>
                                        <a href="members/index.php">Home</a>
                                    </li>

                                    <li>Profile</li>

                                </ul>
                                <!-- /BREADCRUMBS -->
                                <div class="clearfix">
                                    <h3 class="content-title pull-left">Profile</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /PAGE HEADER -->

                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='box border'>
                                <div class='box-title'>
                                    <h4><i class='fa fa-user'></i><span class='hidden-inline-mobile'>Hello, <?php echo $info['full_name']?> !</span></h4>
                                </div>
                                <div class='box-body'>
                                    <div class='tabbable header-tabs user-profile'>
                                        <ul class='nav nav-tabs'>
                                            <li class=''><a href='members/profile-password.php'><i class='fa fa-edit'></i> <span class='hidden-inline-mobile'>Password</span></a></li>
                                            <li class='active'><a href='members/profile.php'><i class='fa fa-user'></i> <span class='hidden-inline-mobile'>Profile</span></a></li>
                                        </ul>
                                        <div class='tab-content'>
                                            <div class='tab-pane fade in active'>
                                                <div class='row'>
                                                    <div class='col-md-3'>
                                                        <div class='list-group'>
                                                            <div class='list-group-item profile-details'>
                                                                <h2 style='margin-top:0px;'><?php echo $info['full_name']?> </h2>
                                                                <p><i class='fa fa-circle text-green'></i> Online</p>
                                                                <?php echo $info['email']?> </p>
                                                            </div>

                                                            <a href='members/history_ewallet.php' class='list-group-item'>
                                                                <?php
                                                                    echo $currency['ec'] ?>.00<br/>
                                                                <strong>Electronic Currency</strong>
                                                            </a>

                                                            <a href='members/history_rwallet.php' class='list-group-item'><?php echo $currency['rf']?>.00<br/><strong>Registration Fee</strong></a>
                                                            <a href='members/history_rpoint.php' class='list-group-item'><?php echo $currency['dc']?>.00<br/><strong>Declaration Currency</strong></a>
                                                            <a href='members/pc-history.php' class='list-group-item'><?php echo $currency['pc']?>.00<br/><strong>Product Currency</strong></a>
                                                            <a href='members/pp-history.php' class='list-group-item'><?php echo $currency['pp']?>.00<br/><strong>Product Point</strong></a>
                                                            <a href='members/history_cwallet.php' class='list-group-item'><?php echo $currency['msp']?>.00<br/><strong>MSP</strong></a>
                                                            <a href='members/rebates-pairing-bonus.php' class='list-group-item'><?php echo ($total_pairingEC==0?'0':$total_pairingEC)?>.00<br/><strong>Pairing EC Income</strong></a>
                                                            <a href='members/rebates.php' class='list-group-item'><?php echo $total_pairingEC+$levelBonusSummery+$introducerBonusSummery?>.00<br/><strong>Total Income</strong></a>
                                                        </div>
                                                    </div>

                                                    <div class='col-md-8'>
                                                        <div class='row'>

                                                            <?php if(\App\Session::get('error')) {?>
                                                                <div class="row" style="margin-left:0px;margin-right:0px">
                                                                    <div class="col-md-12 profile-details">
                                                                        <div class="alert alert-block alert-danger fade in">
                                                                            <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                            <p><?php echo \App\Session::get('error')?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                \App\Session::UnsetKeySession('error');
                                                            }?>

                                                            <?php if(\App\Session::get('success')) {?>
                                                                <div class="row" style="margin-left:0px;margin-right:0px">
                                                                    <div class="col-md-12 profile-details">

                                                                        <div class="alert alert-block alert-success fade in">
                                                                            <h4><i class="fa fa-heart" aria-hidden="true"></i> Successful!</h4>
                                                                            <p> <?php echo \App\Session::get('success')?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                \App\Session::UnsetKeySession('success');
                                                            }?>

                                                            <div class='col-md-12 profile-details'>

                                                                <form name='profile_info_form' method='post' action='' class='form-horizontal'>
                                                                    <input type='hidden' name='__req' value='1' />
                                                                    <div class='box border green'>
                                                                        <div class='box-body big'>
                                                                            <div class='row'>
                                                                                <div class='col-md-12'>
                                                                                    <h4>Account profile</h4>
                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Sponsor Username:</label>
                                                                                        <div class='col-md-9'>
                                                                                            
                                                                                            <?php
                                                                                            if($info['sponsor_id']==0)
                                                                                            {
                                                                                                echo $info['username'];
                                                                                            }else
                                                                                            {
                                                                                                echo $sponsor['username'];
                                                                                            }
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Username</label>
                                                                                        <div class='col-md-9' style='margin-top: 7px;'><?php echo $info['username']?> </div>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Full Name</label>
                                                                                        <div class='col-md-9' style='margin-top: 7px;'><?php echo $info['full_name']?> </div>
                                                                                    </div>

                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Nickname: <span class='require'>*</span></label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='nick_name' class='form-control' value="<?php echo $info['nick_name']?> "  />
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>IC No.</label>
                                                                                        <div class='col-md-9' style='margin-top: 7px;'><?php echo $info['IC_no']?> </div>
                                                                                    </div>

                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Wechat ID:</label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='wechat_ID' class='form-control' value="<?php echo $info['wechat_ID']?> "  />
                                                                                        </div>
                                                                                    </div>


                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Email: <span class='require'>*</span></label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='email' class='form-control' value="<?php echo $info['email']?> "  />
                                                                                        </div>
                                                                                    </div>


                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Address: <span class='require'>*</span></label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='address' class='form-control' value="<?php echo $info['address']?> "/>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Country: *</label>
                                                                                        <div class='col-md-9'><select class='form-control' name="country"  disabled = 'disabled'><option value="AF" >Afghanistan</option><option value="AX" >&#197;land Islands</option><option value="AL" >Albania</option><option value="DZ" >Algeria</option><option value="AS" >American Samoa</option><option value="AD" >Andorra</option><option value="AO" >Angola</option><option value="AI" >Anguilla</option><option value="AQ" >Antarctica</option><option value="AG" >Antigua and Barbuda</option><option value="AR" >Argentina</option><option value="AM" >Armenia</option><option value="AW" >Aruba</option><option value="AU" >Australia</option><option value="AT" >Austria</option><option value="AZ" >Azerbaijan</option><option value="BS" >Bahamas</option><option value="BH" >Bahrain</option><option value="BD" selected="selected">Bangladesh</option><option value="BB" >Barbados</option><option value="BY" >Belarus</option><option value="BE" >Belgium</option><option value="BZ" >Belize</option><option value="BJ" >Benin</option><option value="BM" >Bermuda</option><option value="BT" >Bhutan</option><option value="BO" >Bolivia</option><option value="BA" >Bosnia and Herzegovina</option><option value="BW" >Botswana</option><option value="BV" >Bouvet Island</option><option value="BR" >Brazil</option><option value="IO" >British Indian Ocean Territory</option><option value="BN" >Brunei Darussalam</option><option value="BG" >Bulgaria</option><option value="BF" >Burkina Faso</option><option value="BI" >Burundi</option><option value="KH" >Cambodia</option><option value="CM" >Cameroon</option><option value="CA" >Canada</option><option value="CV" >Cape Verde</option><option value="KY" >Cayman Islands</option><option value="CF" >Central African Republic</option><option value="TD" >Chad</option><option value="CL" >Chile</option><option value="CN" >China</option><option value="CX" >Christmas Island</option><option value="CC" >Cocos (Keeling) Islands</option><option value="CO" >Colombia</option><option value="KM" >Comoros</option><option value="CG" >Congo</option><option value="CD" >Congo, The Democratic Republic of The</option><option value="CK" >Cook Islands</option><option value="CR" >Costa Rica</option><option value="CI" >C&#244;te D'ivoire</option><option value="HR" >Croatia</option><option value="CU" >Cuba</option><option value="CY" >Cyprus</option><option value="CZ" >Czech Republic</option><option value="DK" >Denmark</option><option value="DJ" >Djibouti</option><option value="DM" >Dominica</option><option value="DO" >Dominican Republic</option><option value="EC" >Ecuador</option><option value="EG" >Egypt</option><option value="SV" >El Salvador</option><option value="GQ" >Equatorial Guinea</option><option value="ER" >Eritrea</option><option value="EE" >Estonia</option><option value="ET" >Ethiopia</option><option value="FK" >Falkland Islands (Malvinas)</option><option value="FO" >Faroe Islands</option><option value="FJ" >Fiji</option><option value="FI" >Finland</option><option value="FR" >France</option><option value="GF" >French Guiana</option><option value="PF" >French Polynesia</option><option value="TF" >French Southern Territories</option><option value="GA" >Gabon</option><option value="GM" >Gambia</option><option value="GE" >Georgia</option><option value="DE" >Germany</option><option value="GH" >Ghana</option><option value="GI" >Gibraltar</option><option value="GR" >Greece</option><option value="GL" >Greenland</option><option value="GD" >Grenada</option><option value="GP" >Guadeloupe</option><option value="GU" >Guam</option><option value="GT" >Guatemala</option><option value="GG" >Guernsey</option><option value="GN" >Guinea</option><option value="GW" >Guinea-bissau</option><option value="GY" >Guyana</option><option value="HT" >Haiti</option><option value="HM" >Heard Island and Mcdonald Islands</option><option value="VA" >Holy See (Vatican City State)</option><option value="HN" >Honduras</option><option value="HK" >Hong Kong</option><option value="HU" >Hungary</option><option value="IS" >Iceland</option><option value="IN" >India</option><option value="ID" >Indonesia</option><option value="IR" >Iran, Islamic Republic Of</option><option value="IQ" >Iraq</option><option value="IE" >Ireland</option><option value="IM" >Isle of Man</option><option value="IL" >Israel</option><option value="IT" >Italy</option><option value="JM" >Jamaica</option><option value="JP" >Japan</option><option value="JE" >Jersey</option><option value="JO" >Jordan</option><option value="KZ" >Kazakhstan</option><option value="KE" >Kenya</option><option value="KI" >Kiribati</option><option value="KP" >Korea, Democratic People's Republic Of</option><option value="KR" >Korea, Republic Of</option><option value="KW" >Kuwait</option><option value="KG" >Kyrgyzstan</option><option value="LA" >Lao People's Democratic Republic</option><option value="LV" >Latvia</option><option value="LB" >Lebanon</option><option value="LS" >Lesotho</option><option value="LR" >Liberia</option><option value="LY" >Libyan Arab Jamahiriya</option><option value="LI" >Liechtenstein</option><option value="LT" >Lithuania</option><option value="LU" >Luxembourg</option><option value="MO" >Macao</option><option value="MK" >Macedonia, The Former Yugoslav Republic Of</option><option value="MG" >Madagascar</option><option value="MW" >Malawi</option><option value="MY" >Malaysia</option><option value="MV" >Maldives</option><option value="ML" >Mali</option><option value="MT" >Malta</option><option value="MH" >Marshall Islands</option><option value="MQ" >Martinique</option><option value="MR" >Mauritania</option><option value="MU" >Mauritius</option><option value="YT" >Mayotte</option><option value="MX" >Mexico</option><option value="FM" >Micronesia, Federated States Of</option><option value="MD" >Moldova, Republic Of</option><option value="MC" >Monaco</option><option value="MN" >Mongolia</option><option value="ME" >Montenegro</option><option value="MS" >Montserrat</option><option value="MA" >Morocco</option><option value="MZ" >Mozambique</option><option value="MM" >Myanmar</option><option value="NA" >Namibia</option><option value="NR" >Nauru</option><option value="NP" >Nepal</option><option value="NL" >Netherlands</option><option value="AN" >Netherlands Antilles</option><option value="NC" >New Caledonia</option><option value="NZ" >New Zealand</option><option value="NI" >Nicaragua</option><option value="NE" >Niger</option><option value="NG" >Nigeria</option><option value="NU" >Niue</option><option value="NF" >Norfolk Island</option><option value="MP" >Northern Mariana Islands</option><option value="NO" >Norway</option><option value="OM" >Oman</option><option value="PK" >Pakistan</option><option value="PW" >Palau</option><option value="PS" >Palestinian Territory, Occupied</option><option value="PA" >Panama</option><option value="PG" >Papua New Guinea</option><option value="PY" >Paraguay</option><option value="PE" >Peru</option><option value="PH" >Philippines</option><option value="PN" >Pitcairn</option><option value="PL" >Poland</option><option value="PT" >Portugal</option><option value="PR" >Puerto Rico</option><option value="QA" >Qatar</option><option value="RE" >R&#233;union</option><option value="RO" >Romania</option><option value="RU" >Russian Federation</option><option value="RW" >Rwanda</option><option value="BL" >Saint Barth&#233;lemy</option><option value="SH" >Saint Helena</option><option value="KN" >Saint Kitts and Nevis</option><option value="LC" >Saint Lucia</option><option value="MF" >Saint Martin</option><option value="PM" >Saint Pierre and Miquelon</option><option value="VC" >Saint Vincent and The Grenadines</option><option value="WS" >Samoa</option><option value="SM" >San Marino</option><option value="ST" >Sao Tome and Principe</option><option value="SA" >Saudi Arabia</option><option value="SN" >Senegal</option><option value="RS" >Serbia</option><option value="SC" >Seychelles</option><option value="SL" >Sierra Leone</option><option value="SG" >Singapore</option><option value="SK" >Slovakia</option><option value="SI" >Slovenia</option><option value="SB" >Solomon Islands</option><option value="SO" >Somalia</option><option value="ZA" >South Africa</option><option value="GS" >South Georgia and The South Sandwich Islands</option><option value="ES" >Spain</option><option value="LK" >Sri Lanka</option><option value="SD" >Sudan</option><option value="SR" >Suriname</option><option value="SJ" >Svalbard and Jan Mayen</option><option value="SZ" >Swaziland</option><option value="SE" >Sweden</option><option value="CH" >Switzerland</option><option value="SY" >Syrian Arab Republic</option><option value="TW" >Taiwan</option><option value="TJ" >Tajikistan</option><option value="TZ" >Tanzania, United Republic Of</option><option value="TH" >Thailand</option><option value="TL" >Timor-leste</option><option value="TG" >Togo</option><option value="TK" >Tokelau</option><option value="TO" >Tonga</option><option value="TT" >Trinidad and Tobago</option><option value="TN" >Tunisia</option><option value="TR" >Turkey</option><option value="TM" >Turkmenistan</option><option value="TC" >Turks and Caicos Islands</option><option value="TV" >Tuvalu</option><option value="UG" >Uganda</option><option value="UA" >Ukraine</option><option value="AE" >United Arab Emirates</option><option value="GB" >United Kingdom</option><option value="US" >United States</option><option value="UM" >United States Minor Outlying Islands</option><option value="UY" >Uruguay</option><option value="UZ" >Uzbekistan</option><option value="VU" >Vanuatu</option><option value="VE" >Venezuela</option><option value="VN" >Viet Nam</option><option value="VG" >Virgin Islands, British</option><option value="VI" >Virgin Islands, U.s.</option><option value="WF" >Wallis and Futuna</option><option value="EH" >Western Sahara</option><option value="YE" >Yemen</option><option value="ZM" >Zambia</option><option value="ZW" >Zimbabwe</option></select></div>
                                                                                    </div>

                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Contact Number: <span class='require'>*</span></label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='contact_no' class='form-control' value="<?php echo $info['contact_no']?> "  />
                                                                                        </div>
                                                                                    </div>

                                                                                    <br/>
                                                                                    <h4>Nominee</h4>

                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Nominee:</label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='nominee_name' class='form-control' value="<?php echo $nomineeinfo['nominee_name']?>"  />
                                                                                        </div>
                                                                                    </div>


                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Relationship with Nominee:</label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='relationship_with_nominee' class='form-control' value="<?php echo $nomineeinfo['relationship_with_nominee']?>"  />
                                                                                        </div>
                                                                                    </div>


                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>IC No. of Nominee:</label>
                                                                                        <div class='col-md-9'>
                                                                                            <input type='text' name='nominee_ic_no' class='form-control' value="<?php echo $nomineeinfo['nominee_ic_no']?>"  />
                                                                                        </div>
                                                                                    </div>

                                                                                    <br/>
                                                                                    <h4>Security Verification</h4>
                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3 control-label'>Security Password:</label>
                                                                                        <div class='col-md-9'><input type='password' name='sec_password' class='form-control'/></div>
                                                                                    </div>
                                                                                    <br/>
                                                                                    <div class='form-group'>
                                                                                        <label class='col-md-3'><input type='submit' class='btn btn-success' value='Submit' name="modify"/></label>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

<?php include_once "includes/footer.php";?>

<?php }else { 
    
    header('location:../login.php');

}?>
