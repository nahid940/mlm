<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));

$client=new \App\client\Client();
$bc_currency=$user->userCurrencies(\App\Session::get('userID'))['bc'];
date_default_timezone_set('Asia/Dhaka');
\App\Session::UnsetKeySession('confirm');

$invest=new \App\investment\Investment();
$multiples=$invest->getTransferMultiples()['bc_dc'];

if(isset($_POST['submit_btn']))
{
    if ($user->verifyUser(base64_encode($_POST['password']))) {

    if($_POST['_amount']!='') {

        if (($_POST['_amount']*$multiples ) <= $bc_currency) {
    
            $_POST['amount'] =  $_POST['_amount']*$multiples;
            
            \App\Session::set('confirm', 'confirm');
    
        } else {
            \App\Session::set('error', "You do not have sufficient Business Currency for this purchase.");
        }
    }
    else
    {
        \App\Session::set('error', 'Please select an amount.');
    }

    }else
    {
        \App\Session::set('error',"'Security Password' is a required field.");
    }
}
    
    if(isset($_POST['submit_btn_confirm']))
    {
        
        
        try{
            \App\DBConnection::myDb()->beginTransaction();
            
            $currency = $user->userCurrencies(\App\Session::get('userID'));
        
            $user->decrementBC(($currency['bc'] - $_POST['amount']), \App\Session::get('userID'));
            $user->incrementDC(($currency['dc'] + $_POST['amount']), \App\Session::get('userID'));
            // //for dc
            
            $currency = $user->userCurrencies(\App\Session::get('userID'));
            $user->userCurrencyHistory(\App\Session::get('userID'), 0, $_POST['amount'], 'Transfer to DC', 52, 52, $_POST['amount'], 0,"",$currency['bc'],0);
    
            $user->userCurrencyHistory(\App\Session::get('userID'), 0, $_POST['amount'], 'Transfer From Business Currency', 53, 53, 0, $_POST['amount'],"",$currency['dc'],0);
    
            $comlpete=1;
            \App\Session::set('success', 'Your transfer has just been completed.');
            
            $bc_currency=$user->userCurrencies(\App\Session::get('userID'))['bc'];
            
            \App\DBConnection::myDb()->commit();
        }catch (PDOException $exp)
        {
            \App\DBConnection::myDb()->rollBack();
        }
             
        
    }

?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>Transfer</li>
                    <li>Business Currency To Declaration Currency</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Business Currency To Declaration Currency</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->

    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'> <?php echo ($bc_currency==0?0.00:$bc_currency)?></div>
                <div>Business Currency</div>
            </a>
        </div>
    </div>


    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li><a href="members/msp_transfer.php">Business Currency To Declaration Currency</a></li>
                        </ul>
                        <?php if(isset($comlpete)) { ?>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <?php if (\App\Session::get('success')) { ?>
                                        <div class="row"
                                             style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                <div class="alert alert-block alert-success fade in">
                                                    <h4><i class="fa fa-heart" aria-hidden="true"></i>
                                                        Successful!</h4>
                                                    <?php echo \App\Session::get('success') ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        \App\Session::UnsetKeySession('success');
                                    } ?>

                                    <div class="text-center">
                                        <a href="members/bc_dc.php" name='submit_btn_confirm' class='btn btn-primary '>Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php  \App\Session::UnsetKeySession('complete');?>

                        <?php } else { ?>
                        

                        <?php if(\App\Session::get('confirm')) {?>

                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>

                                    <div class='col-md-12'>
                                        <div class="alert alert-info">
                                            Please review your transfer information and click the 'Confirm' button to proceed with the transfer.
                                        </div>
                                        <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                            <input type='hidden' name='__req' value='1'>
                                            <div class='form-body'>
                                                
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Type: <span class='require'>*</span></label>
                                                    <div class='col-md-5'>
                                                        Business Currency To Declaration Currency Transfer
                                                    </div>
                                                </div>

                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Transfer Amount: <span class='require'>*</span></label>
                                                    <div class='col-md-5'>
                                                        <?php echo $_POST['amount']?>.00
                                                        <input type="hidden" value="<?php echo $_POST['amount']?>" name="amount">
                                                    </div>
                                                </div>

                                                <br/>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'></label>
                                                    <div class='col-md-5'>
                                                        <a href="javascript:history.go(-1)" class='btn btn-default'>Back</a>
                                                        <input type='submit' name='submit_btn_confirm' value='Confirm' class='btn btn-primary' />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php } else {?>

                            <div class='tab-content'>
                                <div class='tab-pane fade in active'>
                                    <div class='row'>
                                        <?php if(\App\Session::get('error')) {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    <div class="alert alert-block alert-danger fade in">
                                                        <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                        <p><?php echo \App\Session::get('error')?></p>
                                                        <p><?php //echo \App\Session::get('usererror')?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            \App\Session::UnsetKeySession('error');
                                            //\App\Session::UnsetKeySession('usererror');
                                        }?>
                                        
                                        
                                        <?php if(\App\Session::get('amounterror')) {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    <div class="alert alert-block alert-danger fade in">
                                                        <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                        <p><?php echo \App\Session::get('amounterror')?>.</p>
                                                        <p>Registration Fee required: <?php echo $req_amount?>.00</p>
                                                        <p>Registration Fee available: <?php echo $avaialbe_amount?>.00</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            \App\Session::UnsetKeySession('amounterror');
                                        }?>

                                        <?php if(\App\Session::get('success')) {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    
                                                    <div class="alert alert-block alert-success fade in">
                                                        <p><?php echo \App\Session::get('success')?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            \App\Session::UnsetKeySession('success');
                                        }?>

                                        <div class='col-md-12'>
                                            <br/>
                                            <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                <input type='hidden' name='__req' value='1'>
                                                <div class='form-body'>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Transfer Point Type: <span class='require'>*</span></label>
                                                        <div class='col-md-5'>
                                                            Business Currency To Declaration Currency
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Amount: *</label>
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <div class="col-md-3" style="margin-top:0px;">
                                                                    <?php echo $multiples?> x
                                                                </div>
                                                                <div class="col-md-9" style="margin-top:0px;">
                                                                    <input type="number" name="_amount" class="form-control" value="" min="0" step="1">
                                                                </div>
                                                            </div>
                                                            <p class="help-block">Business Currency Transfer is <?php echo $multiples ?>  times.(E.g : 1 unit equal to <?php echo $multiples ?> )</p>
                                                        </div>
                                                    </div>

                                                    <!--<div class='form-group'>-->
                                                    <!--    <label class='col-md-3 control-label'>Transfer Amount: <span class='require'>*</span></label>-->
                                                    <!--    <div class='col-md-5'>-->
                                                    <!--        <div class='row'>-->
                                                    <!--            <div class='col-md-12' style='margin-top:0px;'>-->
                                                    <!--                <input type='number' name='_amount' class='form-control' value='' min='0' step='1'>-->
                                                    <!--            </div>-->
                                                    <!--        </div>-->
                                                    <!--    </div>-->
                                                    <!--</div>-->
                                                    <div class='form-group' id='rwallet'>
                                                        <label class='col-md-3 control-label'>Business Currency : <span class='require'>*</span></label>
                                                        <div class='col-md-5' >
                                                            <?php echo ($bc_currency==0?0.00:$bc_currency)?>
                                                        </div>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Security Password:</label>
                                                        <div class='col-md-5'>
                                                            <input type='password' name='password' class='form-control' value='' required>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'></label>
                                                        <div class='col-md-5'>
                                                            <input type='submit' name='submit_btn' value='Submit' class='btn btn-primary' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        
                        }
                        
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>


<script type='text/javascript'>
    jQuery(document).ready(function(j){
//        j('form[name=transfer_form]').submit(function(){
//            j('input[name=submit_btn]').attr('disabled','disabled');
//        });

//        j('input#check_user_btn').click(function(){
//            //alert(1);
//            j('span#transferee_text').html('').removeClass('red');
//            if(j('input[name=uid]').val()==''){
//                j('span#transferee_text').html('Please provide a Username.').addClass('red');
//            }
//            else{
//                j(this).attr('disabled','disabled');
//                j.ajax({
//                    url: 'rwallet-transfer.php?aj=1&check_user=1',
//                    data: {'uid': j('input[name=uid]').val()},
//                    type: 'post',
//                    dataType: 'html',
//                    error: function(){
//                        j('span#transferee_text').html('Error checking...').addClass('red');
//                    },
//                    success: function(data){
//                        var mesg='';
//                        if(j('loggedout', data).text()=='loggedout'){
//                            mesg='You have been logged out.';
//                        }else{
//                            j(data).find('msg').each(function(){
//                                mesg+=j(this).text()+' ';
//                            });
//                        }
//                        var status=j(data).find('status').text();
//                        j('span#transferee_text').html(mesg).addClass(status==1? 'bold' : 'red');
//                    },
//                    complete: function(){
//                        j('input#check_user_btn').removeAttr('disabled');
//                    }
//                });
//            }
//        });

//        j('input[name=back_btn]').click(function(){
//            q('__edit').value = 1;
//            q('__confirm').value = '';
//            j('input[name=back_btn],input[name=submit_btn]').attr('disabled','disabled');
//            document.transfer_form.submit();
//        });
    });

    $('#check_user_btn').on('click',function () {
        var user= $('#uid').val();

        if(user!='')
        {
            $.ajax({
                type:'post',
                url:'ajax_files/check_upline_user.php',
                data:{
                    'userfind':1,
                    'username':user
                },
                success:function (data) {
                    $('#transferee_text').text(data);
                }
            })
        }
    })
</script>

<?php }else {
    header('location:../login.php');
}?>