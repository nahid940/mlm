<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$invest=new \App\investment\Investment();
$user->checkUserValidity(\App\Session::get('userID'));

$client=new \App\client\Client();
$ec_currency=$user->userCurrencies(\App\Session::get('userID'))['ec'];
$bdt_currency=$user->userCurrencies(\App\Session::get('userID'))['bdt'];
date_default_timezone_set('Asia/Dhaka');
\App\Session::UnsetKeySession('confirm');

$payout_plans=$invest->getAllPayoutPlan();

$percentage=5;
$multiples=250;

    if(isset($_POST['submit_btn']))
    {
        $plan_info=$invest->getPayoutPlan($_POST['planID']);
        if ($user->verifyUser(base64_encode($_POST['password']))) {
            
            if($_POST['_amount']!='') {
                
                if (($_POST['_amount']*$multiples ) <= $ec_currency) {
                    
                    $amount=$_POST['_amount']*$multiples;
        
                    if($plan_info['planCash']!='')
                    {
                        $planCash=$amount*$plan_info['planCash']/100;
                    }
                    
                    if($plan_info['planMSP']!='')
                    {
                        $planMSP=$amount*$plan_info['planMSP']/100;
                    }
                    
                    if($plan_info['planPP']!='')
                    {
                        $planPP=$amount*$plan_info['planPP']/100;
                    }
                    
                    \App\Session::set('confirm', 'confirm');
            
                } else {
                    \App\Session::set('error', "You do not have sufficient Electronic Currency for this payout.");
                }
            }
            else
            {
                \App\Session::set('error', 'Please select an amount.');
            }
        }else
        {
            \App\Session::set('error',"'Security Password' is a required field.");
        }
    }

  
    
    if(isset($_POST['submit_btn_confirm']))
    { 
        try{
        \App\DBConnection::myDb()->beginTransaction();
        
        $currency = $user->userCurrencies(\App\Session::get('userID'));
        
        $plan_info=$invest->getLimitedPayoutPlan($_POST['planID']);
        
        $amount=$_POST['amount'];
        
        if($plan_info['planCash']!='')
        {
            $planCash=$amount*$plan_info['planCash']/100;
        }
        
        if($plan_info['planMSP']!='')
        {
            $planMSP=$amount*$plan_info['planMSP']/100;
        }
        
        if($plan_info['planPP']!='')
        {
            $planPP=$amount*$plan_info['planPP']/100;
        }
        
        $user->decrementEC(($currency['ec'] - $_POST['amount']), \App\Session::get('userID'));
        
        $user->updateMSP(($currency['msp'] + $planMSP), \App\Session::get('userID'));
        $user->incrementBDT(($currency['bdt'] + $planCash), \App\Session::get('userID'));
        $client->increasePPCurrency(\App\Session::get('userID'),$planPP);
        
        $currency = $user->userCurrencies(\App\Session::get('userID'));
        
        $user->userCurrencyHistory(\App\Session::get('userID'), 0, $_POST['amount'], 'Payout', 61, 61, $_POST['amount'], 0,"",$currency['ec'],0);
        
        $user->userCurrencyHistory(\App\Session::get('userID'), 0, $planMSP, 'Payout From Electronic Currency', 62, 62, 0, $planMSP,"",$currency['msp'],0);//ec->msp
        
        $user->userCurrencyHistory(\App\Session::get('userID'), 0, $planCash, 'Payout From Electronic Currency', 63,63, 0, $planCash,"",$currency['bdt'],0); //ec->bdt
        
        $user->userCurrencyHistory(\App\Session::get('userID'),0,$planPP,'Payout From Electronic Currency',64,64,$planPP,0,"",$currency['pp'],0);//ec->pp
        
        $ec_currency=number_format($ec_currency-$_POST['amount'],2);
        $comlpete=1;
        $success="Payout successful.";
        
        \App\DBConnection::myDb()->commit();
        
        }catch(PDOException $exp)
        {
            \App\DBConnection::myDb()->rollBack();
        }
        
    }

?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>Purchase</li>
                    <li>MSM Purchase</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">MSM Purchase</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->

    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'> <?php echo ($ec_currency==0?0.00:$ec_currency)?></div>
                <div>Electronic Currency</div>
            </a>
        </div>
        <div class="col-md-3">
            <a class="btn btn-info btn-icon input-block-level" href="javascript:void(0);" style="margin-top: -20px; margin-bottom: 25px; cursor: default;">
                <div style="font-size:20px;"><?php echo ($bdt_currency==0?'00.0':$bdt_currency)?></div>
                <div>MSM</div>
            </a>
        </div>
    </div>


    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li ><a href='members/bc_bdt.php'>Business Currency</a></li>
                            <li class='active'><a href="members/ec_payout.php">Electronic Currency</a></li>
                        </ul>
                        <?php if(isset($comlpete)) { ?>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <?php if (isset($success)) { ?>
                                        <div class="row"
                                             style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                <div class="alert alert-block alert-success fade in">
                                                    <h4><i class="fa fa-heart" aria-hidden="true"></i>
                                                        Successful!</h4>
                                                    <?php echo $success; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>

                                    <div class="text-center">
                                        <a href="members/ec_payout.php" name='submit_btn_confirm' class='btn btn-primary '>Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php  \App\Session::UnsetKeySession('complete');?>

                        <?php } else { ?>
                        

                        <?php if(\App\Session::get('confirm')) {?>

                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>

                                    <div class='col-md-12'>
                                        <div class="alert alert-info">
                                            Please review your transfer information and click the 'Confirm' button to proceed with the transfer.
                                        </div>
                                        <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                            <input type='hidden' name='__req' value='1'>
                                            <input type="hidden" value="<?php echo $_POST['planID']?>" name="planID">
                                            <div class='form-body'>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Type: <span class='require'>*</span></label>
                                                    <div class='col-md-5'>
                                                        Electronic Currency Purchase
                                                    </div>
                                                </div>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Electronic Currency Amount: <span class='require'>*</span></label>
                                                    <div class='col-md-5'>
                                                        <?php echo $amount?>.00
                                                        <input type="hidden" value="<?php echo $amount?>" name="amount">
                                                    </div>
                                                </div>
                                                <!--<div class='form-group'>-->
                                                <!--    <label class='col-md-3 control-label'>MSP Amount: <span class='require'>*</span></label>-->
                                                <!--    <div class='col-md-5'>-->
                                                <!--        <?php echo $planMSP?>.00-->
                                                <!--    </div>-->
                                                <!--</div>-->
                                                <!--<div class='form-group'>-->
                                                <!--    <label class='col-md-3 control-label'>Product Point Amount: <span class='require'>*</span></label>-->
                                                <!--    <div class='col-md-5'>-->
                                                <!--        <?php echo $planPP?>.00-->
                                                <!--    </div>-->
                                                <!--</div>-->
                                                <br/>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'></label>
                                                    <div class='col-md-5'>
                                                        <a href="javascript:history.go(-1)" class='btn btn-default'>Back</a>
                                                        <input type='submit' name='submit_btn_confirm' value='Confirm' class='btn btn-primary' />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php } else {?>

                            <div class='tab-content'>
                                <div class='tab-pane fade in active'>
                                    <div class='row'>
                                        <?php if(\App\Session::get('error')) {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    <div class="alert alert-block alert-danger fade in">
                                                        <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                        <p><?php echo \App\Session::get('error')?></p>
                                                        <p><?php //echo \App\Session::get('usererror')?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            \App\Session::UnsetKeySession('error');
                                            //\App\Session::UnsetKeySession('usererror');
                                        }?>
                                        
                                        <?php if(\App\Session::get('amounterror')) {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    <div class="alert alert-block alert-danger fade in">
                                                        <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                        <p><?php echo \App\Session::get('amounterror')?>.</p>
                                                        <p>Registration Fee required: <?php echo $req_amount?>.00</p>
                                                        <p>Registration Fee available: <?php echo $avaialbe_amount?>.00</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            \App\Session::UnsetKeySession('amounterror');
                                        }?>
                                        <?php if(\App\Session::get('success')) {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    <div class="alert alert-block alert-success fade in">
                                                        <p><?php echo \App\Session::get('success')?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            \App\Session::UnsetKeySession('success');
                                        }?>

                                        <div class='col-md-12'>
                                            <br/>
                                            <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                <input type='hidden' name='__req' value='1'>
                                                <div class='form-body'>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Transfer Type: <span class='require'>*</span></label>
                                                        <div class='col-md-5'>
                                                            From Electronic Currency To MSM
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Method: *</label>
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <div class="col-md-12" style="margin-top:0px;">
                                                                    <select name="planID" class="form-control">
                                                                            <option>-- choose one --</option>
                                                                        <?php foreach($payout_plans as $payout_plan){?>
                                                                            <option value="<?php echo $payout_plan['planID']?>"><?php echo $payout_plan['planName']?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--<p class="help-block">Business Currency Transfer is 20  times.(E.g : 1 unit equal to 20 )</p>-->
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Amount: *</label>
                                                        <!--<div class="col-md-5">-->
                                                        <!--    <div class="row">-->
                                                        <!--        <div class="col-md-3" style="margin-top:0px;">-->
                                                        <!--            20 x-->
                                                        <!--        </div>-->
                                                        <!--        <div class="col-md-12" style="margin-top:0px;">-->
                                                        <!--            <input type="number" name="_amount" class="form-control" value="" min="0" step="1">-->
                                                        <!--        </div>-->
                                                        <!--    </div>-->
                                                        <!--    <p class="help-block">Business Currency Transfer is 20  times.(E.g : 1 unit equal to 20 )</p>-->
                                                        <!--</div>-->
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <div class="col-md-3" style="margin-top:0px;">
                                                                    <?php echo $multiples?> x
                                                                </div>
                                                                <div class="col-md-9" style="margin-top:0px;">
                                                                    <input type="number" name="_amount" class="form-control" value="" min="0" step="1">
                                                                </div>
                                                            </div>
                                                            <p class="help-block">Business Currency Transfer is <?php echo $multiples?>  times.(E.g : 1 unit equal to <?php echo $multiples?> )</p>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class='form-group' id='rwallet'>
                                                        <label class='col-md-3 control-label'>Electronic Currency : <span class='require'>*</span></label>
                                                        <div class='col-md-5' >
                                                            <?php echo ($ec_currency==0?0.00:$ec_currency)?>
                                                        </div>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Security Password:</label>
                                                        <div class='col-md-5'>
                                                            <input type='password' name='password' class='form-control' value='' required>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'></label>
                                                        <div class='col-md-5'>
                                                            <input type='submit' name='submit_btn' value='Submit' class='btn btn-primary' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        
                        }
                        
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>


<script type='text/javascript'>
    jQuery(document).ready(function(j){
//        j('form[name=transfer_form]').submit(function(){
//            j('input[name=submit_btn]').attr('disabled','disabled');
//        });

//        j('input#check_user_btn').click(function(){
//            //alert(1);
//            j('span#transferee_text').html('').removeClass('red');
//            if(j('input[name=uid]').val()==''){
//                j('span#transferee_text').html('Please provide a Username.').addClass('red');
//            }
//            else{
//                j(this).attr('disabled','disabled');
//                j.ajax({
//                    url: 'rwallet-transfer.php?aj=1&check_user=1',
//                    data: {'uid': j('input[name=uid]').val()},
//                    type: 'post',
//                    dataType: 'html',
//                    error: function(){
//                        j('span#transferee_text').html('Error checking...').addClass('red');
//                    },
//                    success: function(data){
//                        var mesg='';
//                        if(j('loggedout', data).text()=='loggedout'){
//                            mesg='You have been logged out.';
//                        }else{
//                            j(data).find('msg').each(function(){
//                                mesg+=j(this).text()+' ';
//                            });
//                        }
//                        var status=j(data).find('status').text();
//                        j('span#transferee_text').html(mesg).addClass(status==1? 'bold' : 'red');
//                    },
//                    complete: function(){
//                        j('input#check_user_btn').removeAttr('disabled');
//                    }
//                });
//            }
//        });

//        j('input[name=back_btn]').click(function(){
//            q('__edit').value = 1;
//            q('__confirm').value = '';
//            j('input[name=back_btn],input[name=submit_btn]').attr('disabled','disabled');
//            document.transfer_form.submit();
//        });
    });

    $('#check_user_btn').on('click',function () {
        var user= $('#uid').val();

        if(user!='')
        {
            $.ajax({
                type:'post',
                url:'ajax_files/check_upline_user.php',
                data:{
                    'userfind':1,
                    'username':user
                },
                success:function (data) {
                    $('#transferee_text').text(data);
                }
            })
        }
    })
</script>

<?php }else {
    header('location:../login.php');
}?>