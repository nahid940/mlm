<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));

$msp_currency=$user->userCurrencies(\App\Session::get('userID'))['msp'];

//$histories=$user->getMSPHistory(\App\Session::get('userID'));
$histories=$user->getClientMSPHistory(\App\Session::get('userID'));
$helper=new \App\Helper();
?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>MSP</li>
                    <li>MSP History</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Payout Balance History</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->


   
    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>

                        <ul class='nav nav-tabs'>
                            <li ><a href='members/mc_purchase.php'>GPmall Transfer</a></li>
                            <li  class='active'><a href='members/history_cwallet.php'>MSP History</a></li>
                            <li ><a href='members/dc_purchase.php'>Declaration Currency</a></li>
                            <li><a href='members/ec_purchase.php'>Electronic Currency</a></li>
                        </ul>

                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='row'>
                                            <div class='col-md-12'>

                                                <table id='example-paper' class='table table-paper table-striped'>
                                                    <thead>
                                                    <tr>
                                                        <th style='width: 8%;text-align:center;'>No.</th>
                                                        <th style='width: 8%;text-align:center;'>Ref #</th>
                                                        <th style='text-align:center;'>Date</th>
                                                        <th style='text-align:center;'>Description</th>
                                                        <th style='text-align:center;'>User</th>
                                                        <th style='width: 12%;text-align:center;'>Debit</th>
                                                        <th style='width: 12%;text-align:center;'>Credit</th>
                                                        <th style='width: 12%;text-align:center;'>Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i=0; foreach ($histories as $history){ $i++?>
                                                            <tr class='row0'>
                                                                <td style='text-align:center;'><?php echo $i?></td>
                                                                <td style='text-align:center;'>#<?php echo $helper->generateReference($history['historyID'],$history['historyID'])?></td>
                                                                 <td style='text-align:center;width:110px'><?php echo date('d-m-Y',strtotime($history['action_date']))?></td>
                                                                
                                                             
                                                                <?php if($history['description']=='Transfer') {?>
                                                                    <td style='text-align:center;width:250px;'><?php echo $history['description'];echo ($history['debit']==0?'(Credit)':'(Debit)') ?><br/>
                                                                <?} else {?>
                                                                    <td style='text-align:center;width:250px;'><?php echo $history['description']; ?><br/>
                                                                <?php } ?>
                                                                
                                                                <?php if(($history['remark']!='')) echo "(".$history['remark'].")"  ?>
                                                                
                                                                </td>
                                                                <td style='text-align:center;'>
                                                                    
                                                                    <?php
                                                                    
                                                                    if(empty($history['receiverID']))
                                                                    {
                                                                        echo "-";
                                                                    }else 
                                                                    {
                                                                        if(\App\Session::get('userID')==$history['userID'])
                                                                        {
                                                                            echo $user->getUserNickName($history['receiverID']);
                                                                        }else 
                                                                        {
                                                                            echo $user->getUserNickName($history['userID']);
                                                                        }
                                                                    }
                                                                     
                                                                    ?>
                                                                    
                                                                </td>
                                                                <td class='number' style='text-align:center;'><span class='red'>
                                                                        <?php

                                                                        echo $history['debit'];
                                                                        ?>.00

                                                                </td>
                                                                <td class='number' style='text-align:center;'><span class='green'>
                                                                    <?php
                                                                    echo $history['credit'];
                                                                    ?>.00
                                                                </td>
                                                             
                                                                
                                                                
                                                                <td class='number' style='text-align:center;'><span class='green'>
                                                                <?php 
                                                                
                                                                    echo $history['user_total'];
                                                                        
                                                                ?>.00</td>
                                                            </tr>
                                                        <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>
<?php }else {
    header('location:../login.php');
}?>