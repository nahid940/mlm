<?php
include_once '../vendor/autoload.php';
\App\Session::init();

$helper=new \App\Helper();
$helper->checkTime();

if(\App\Session::get('login')==true) {

$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));

$currency=$user->userCurrencies(\App\Session::get('userID'));


$sql="select dc from conversion_rate";
$stmt=\App\DBConnection::myQuery($sql);
$stmt->execute();
$ec_rate=$stmt->fetch(PDO::FETCH_ASSOC);
\App\Session::UnsetKeySession('confirm');
//$ec=$currency['ec']+(empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')));

if(isset($_POST['submit_btn']))
{
    if ($user->verifyUser(base64_encode($_POST['password']))) {

        if($_POST['_amount']!='') {
            if (($_POST['_amount'] * $ec_rate['dc']) <= $currency['dc']) {

                $_POST['amount'] = $ec_rate['dc'] * $_POST['_amount'];

                \App\Session::set('confirm', 'confirm');

            } else {
                \App\Session::set('error', "You do not have sufficient Declaration Currency for this purchase.");
            }
        }else
        {
            \App\Session::set('error', "Please select an amount.");
        }


    }else
    {
        \App\Session::set('error',"'Security Password' is a required field.");
    }

}

if(isset($_POST['submit_btn_confirm']))
{

    $user->decrementDC(($currency['dc'] - $_POST['amount']), \App\Session::get('userID'));
    $user->updateMSP(($currency['msp'] + $_POST['amount']), \App\Session::get('userID'));

    $currency = $user->userCurrencies(\App\Session::get('userID'));
    //for dc

    $user->userCurrencyHistory(\App\Session::get('userID'), 0, $_POST['amount'], 'Transfer to MSP', 2, 1, $_POST['amount'], 0,'',$currency['dc'],0);

    $user->userCurrencyHistory(\App\Session::get('userID'), 0, $_POST['amount'], 'Transfer From Declaration Currency', 15, 1, 0, $_POST['amount'],'',$currency['msp'],0);

    //$user->recordCurrencyConversion(\App\Session::get('userID'), 1, 5, $_POST['amount']);
    /** 1 means ec, 5 means msp (ec to dc convert)**/

    $complete=1;
    \App\Session::set('success', 'Your transfer has just been completed.');
}

?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>MSP</li>
                    <li>Declaration Currency Transfer</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Declaration Currency Transfer</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->


    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'>
                    <?php echo number_format($currency['dc'],2) ?>
                    </div>
                <div>Declaration Currency</div>
            </a>
        </div>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo ($currency['msp']==0?0.00:number_format($currency['msp'],2))?></div>
                <div>MSP</div>
            </a>
        </div>
    </div>
    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>

                        <ul class='nav nav-tabs'>
                            <li ><a href='members/mc_purchase.php'>GPmall Transfer</a></li>
                            <li ><a href='members/history_cwallet.php'>MSP History</a></li>
                            <li class='active'><a href='members/dc_purchase.php'>Declaration Currency</a></li>
                            <li ><a href='members/ec_purchase.php'>Electronic Currency</a></li>
                        </ul>

                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>


                                    <?php if($complete==1) {?>
                                    
                                    <?php if(\App\Session::get('success')) {?>
                                        <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                <div class="alert alert-block alert-success fade in">
                                                     <h4><i class="fa fa-heart" aria-hidden="true"></i> Successful!</h4>
                                                    <p><?php echo \App\Session::get('success')?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        \App\Session::UnsetKeySession('success');
                                    }?>
                                    
                                    <div style="text-align:center">
                                        <a href="members/dc_purchase.php" class="btn btn-success">Continue</a>
                                    </div>
                                    
                                    <?php }else {?>
                                    
                                    
                                    
                                    <?php if(\App\Session::get('error')) {?>
                                        <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                
                                                <div class="alert alert-block alert-danger fade in">
                                                    <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                    <p><?php echo \App\Session::get('error')?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        \App\Session::UnsetKeySession('error');
                                    }?>
                                    

                                    <?php if(\App\Session::get('confirm')) {?>

                                        <div class='col-md-12'>
                                            <div class="alert alert-info">
                                                Please review your transfer information and click the 'Confirm' button to proceed with the transfer.
                                            </div>
                                            <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                <input type='hidden' name='__req' value='1'>
                                                <div class='form-body'>


                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Transfer Amount: <span class='require'>*</span></label>
                                                        <div class='col-md-5'>
                                                            <?php echo $_POST['amount']?>.00
                                                            <input type="hidden" value="<?php echo $_POST['amount']?>" name="amount">
                                                        </div>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Type: <span class='require'>*</span></label>
                                                        <div class='col-md-5'>
                                                           From Declaration Currency To MSP
                                                        </div>
                                                    </div>

                                                    <br/>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'></label>
                                                        <div class='col-md-5'>
                                                            <a href="javascript:history.go(-1)" class='btn btn-default'>Back</a>
                                                            <input type='submit' name='submit_btn_confirm' value='Confirm' class='btn btn-primary' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!--                                                </div>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->

                                    <?php } else {?>

                                    <div class='col-md-12'>

                                        <div class='alert alert-block alert-info fade in'>
                                            Convert Rate 1:0.8
                                        </div>
                                        <br/>
                                        <form name='withdraw_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                            <input type='hidden' name='__req' value='1' />
                                            <input type='hidden' name='__preview' value='1' />
                                            <div class='form-body'>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Amount: *</label>
                                                    <div class='col-md-5'>
                                                        <div class='row'>
                                                            <div class='col-md-3' style='margin-top:0px;'>
                                                                <?php echo $ec_rate['dc']?> x
                                                            </div>
                                                            <div class='col-md-9' style='margin-top:0px;'>
                                                                <input type='number' name='_amount' class='form-control' value='' min='0' step='1'>
                                                            </div>
                                                        </div>
                                                        <p class='help-block'>Declaration Currency Transfer is <?php echo $ec_rate['dc']?>  times.(E.g : 1 unit equal to <?php echo $ec_rate['dc']?> )</p>
                                                    </div>
                                                </div>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Type: *</label>
                                                    <div class='col-md-5'>
                                                        From Declaration Currency to MSP
                                                    </div>
                                                </div>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Declaration Currency: *</label>
                                                    <div class='col-md-5'>
                                                        <?php echo $currency['dc']?>.00
                                                    </div>
                                                </div>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>MSP: *</label>
                                                    <div class='col-md-5'>
                                                        <?php echo ($currency['msp']==0?0:$currency['msp'])?>.00
                                                    </div>
                                                </div>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'>Security Password:</label>
                                                    <div class='col-md-5'>
                                                        <input type='password' name='password' class='form-control' value='' >
                                                    </div>
                                                </div>
                                                <br/>
                                                <div class='form-group'>
                                                    <label class='col-md-3 control-label'></label>
                                                    <div class='col-md-5'>
                                                        <input type='submit' name='submit_btn' value='Submit' class='btn btn-primary' />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <?php }
                                    
                                    }
                                    
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>

<script type='text/javascript'>

</script>

<?php }else {
    header('location:../login.php');
}?>