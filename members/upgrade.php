<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));

$client=new \App\client\Client();
$currency=$user->userCurrencies(\App\Session::get('userID'));

$package=new \App\package\Package();
$packagePrice=$package->getPackageAmountforEachUser();


$date=$user->getUserAccountDateInfo();
$usergrade=$user->userGrade(\App\Session::get('userID'));
$gradeInfo=$user->gradeGradeDetails($usergrade+1);



if(isset($_POST['submit_btn']))
{
    
    if ($user->verifyUser((base64_encode($_POST['sec_pwd'])))) 
    {
        $sql = "select  requiredDC,requiredPC  from usergrades where userGradeID=:userGradeID";
        $stmt = \App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userGradeID',$usergrade+1);
        $stmt->execute();
        $rate = $stmt->fetch(PDO::FETCH_ASSOC);
    
        if($currency['dc']>=$rate['requiredDC'] && $currency['pc']>=$rate['requiredPC'])
        {
            $confirm=1;
        }else
        {
            \App\Session::set('error','Insufficient Funds.');
        }
    
    }else 
    {
         \App\Session::set('error','You have entered an invalid User Security Password.');
    }

}


if(isset($_POST['submit_btn_confirm']))
{
    

    // if($usergrade==1)
    // {
    //     $sql = "select  requiredDC,requiredPC  from usergrades where userGradeID=1";
    //     $stmt = \App\DBConnection::myQuery($sql);
    //     $stmt->execute();
    //     $rate = $stmt->fetch(PDO::FETCH_ASSOC);

    //     if($currency['dc']>=$rate['requiredDC'] && $currency['pc']>=$rate['requiredPC'])
    //     {
    //         try{
    //             \App\DBConnection::myDb()->beginTransaction();

    //             $sql = "update users set expire_date=:expire_date, grade_id=1 where userID=:userID";
    //             $stmt = \App\DBConnection::myQuery($sql);
    //             $stmt->bindValue(':userID', \App\Session::get('userID'));
    //             $stmt->bindValue(':expire_date', date('Y-m-d', strtotime($date['expire_date'].'+300 days')));
    //             $stmt->execute();

    //             $client->decreaseDeclarationFee(\App\Session::get('userID'), $rate['requiredDC']);
    //             $client->decreaseProductCurrencyFee(\App\Session::get('userID'), $rate['requiredPC']);
                
    //             $currency = $user->userCurrencies(\App\Session::get('userID'));
    //             $user->userCurrencyHistory(\App\Session::get('userID'), 0, $rate['requiredDC'], 'Account upgrade', 2, 6, $rate['requiredDC'], 0, '',$currency['dc'],0);
    //             $user->userCurrencyHistory(\App\Session::get('userID'), 0, $rate['requiredPC'], 'Account upgrade ', 9, 6, $rate['requiredPC'], 0, '',$currency['pc'],0);

    //             \App\DBConnection::myDb()->commit();

    //             \App\Session::set('success','Account Upgraded Successfully !!');
                
    //             $usergrade=$user->userGrade(\App\Session::get('userID'));
    //             $gradeInfo=$user->gradeGradeDetails($usergrade+1);

    //         }catch (PDOException $e){
    //             \App\DBConnection::myDb()->rollBack();
    //             \App\Session::set('error',$e->getMessage());
    //         }

    //     }else
    //     {
    //         \App\Session::set('error','Insufficient Funds.');
    //     }

    // }
    
    // else
    if($usergrade==1)
    {

        $sql = "select  requiredDC,requiredPC  from usergrades where userGradeID=2";
        $stmt = \App\DBConnection::myQuery($sql);
        $stmt->execute();
        $rate = $stmt->fetch(PDO::FETCH_ASSOC);
        if($currency['dc']>=$rate['requiredDC'] && $currency['pc']>=$rate['requiredPC']) {

            try {
                \App\DBConnection::myDb()->beginTransaction();

                $sql = "update users set  expire_date=:expire_date, grade_id=2 where userID=:userID";
                $stmt = \App\DBConnection::myQuery($sql);
                $stmt->bindValue(':userID', \App\Session::get('userID'));
                $stmt->bindValue(':expire_date', date('Y-m-d', strtotime($date['expire_date'].'+300 days')));
                $stmt->execute();

                $client->decreaseDeclarationFee(\App\Session::get('userID'), $rate['requiredDC']);
                $client->decreaseProductCurrencyFee(\App\Session::get('userID'), $rate['requiredPC']);
                
                $currency = $user->userCurrencies(\App\Session::get('userID'));
                $user->userCurrencyHistory(\App\Session::get('userID'), 0, $rate['requiredDC'], 'Account upgrade', 2, 6, $rate['requiredDC'], 0, '',$currency['dc'],0);
                $user->userCurrencyHistory(\App\Session::get('userID'), 0, $rate['requiredPC'], 'Account upgrade ', 9, 6, $rate['requiredPC'], 0, '',$currency['pc'],0);


                \App\DBConnection::myDb()->commit();
                
                $usergrade=$user->userGrade(\App\Session::get('userID'));
                $gradeInfo=$user->gradeGradeDetails($usergrade+1);

                \App\Session::set('success', 'Account Upgraded Successfully !!');

            } catch (PDOException $e) {
                \App\DBConnection::myDb()->rollBack();
                \App\Session::set('error', $e->getMessage());
            }
        } else
        {
            \App\Session::set('error','Insufficient Funds.');
        }



    }
    else if($usergrade==2)
    {

        $sql = "select  requiredDC,requiredPC  from usergrades where userGradeID=3";
        $stmt = \App\DBConnection::myQuery($sql);
        $stmt->execute();
        $rate = $stmt->fetch(PDO::FETCH_ASSOC);
        

        if($currency['dc']>=$rate['requiredDC'] && $currency['pc']>=$rate['requiredPC'])
        {
            try{
                \App\DBConnection::myDb()->beginTransaction();

               $sql = "update users set expire_date=:expire_date, grade_id=3 where userID=:userID";
                $stmt = \App\DBConnection::myQuery($sql);
                $stmt->bindValue(':userID', \App\Session::get('userID'));
                $stmt->bindValue(':expire_date', date('Y-m-d', strtotime($date['expire_date'].'+300 days')));
                $stmt->execute();

                $client->decreaseDeclarationFee(\App\Session::get('userID'), $rate['requiredDC']);
                $client->decreaseProductCurrencyFee(\App\Session::get('userID'), $rate['requiredPC']);
                
                $currency = $user->userCurrencies(\App\Session::get('userID'));
                $user->userCurrencyHistory(\App\Session::get('userID'), 0, $rate['requiredDC'], 'Account upgrade', 2, 6, $rate['requiredDC'], 0, '',$currency['dc'],0);
                $user->userCurrencyHistory(\App\Session::get('userID'), 0, $rate['requiredPC'], 'Account upgrade ', 9, 6, $rate['requiredPC'], 0, '',$currency['pc'],0);

                
            

                \App\DBConnection::myDb()->commit();
                $usergrade=$user->userGrade(\App\Session::get('userID'));
                $gradeInfo=$user->gradeGradeDetails($usergrade+1);

                \App\Session::set('success','Account Upgraded Successfully !!');

            }catch (PDOException $e){
                \App\DBConnection::myDb()->rollBack();
                \App\Session::set('error',$e->getMessage());
            }

        }else
        {
            \App\Session::set('error','Insufficient Funds.');
        }

    }
    else
    {
        \App\Session::set('error','Your account been already upgraded.');
    }
    
    
    
}


?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
<!-- PAGE HEADER-->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <!-- STYLER -->

            <!-- /STYLER -->
            <!-- BREADCRUMBS -->
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="members/index.php">Home</a>
                </li>
                <li>Account Upgrade</li>
            </ul>
            <!-- /BREADCRUMBS -->
            <div class="clearfix">
                <h3 class="content-title pull-left">Account Upgrade</h3>
            </div>
        </div>
    </div>
</div>
<!-- /PAGE HEADER -->


<div class='row'>
    <div class='col-md-3'>
        <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
            <div style='font-size:20px;'><?php echo $currency['dc']==0?0.00:number_format($currency['dc'],2)?></div>
            <div>Declaration Currency</div>
        </a>
    </div>
    <div class='col-md-3'>
        <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
            <div style='font-size:20px;'>
               <?php $ec=(empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')));
                echo ($ec==0?0.00:number_format($ec,2)) ?></div>
            <div>Pairing EC Income</div>
        </a>
    </div>
</div>
<div class='row'>
    <div class='col-md-12'>
        <div class='box border'>
            <div class='box-title'>
                <h4 style='height:15px;'></h4>
            </div>
            <div class='box-body'>
                <div class='tabbable header-tabs user-profile'>
                    <ul class='nav nav-tabs'>
                        <li class='active'><a href='members/upgrade.php'>Account Upgrade</a></li>

                        <li ><a href='members/renew.php'>Account Renewal</a></li>
                        <li ><a href='members/register.php'>Registration</a></li>
                    </ul>
                    <div class='tab-content'>
                        <div class='tab-pane fade in active'>
                            <div class='row'>
                                <?php if(\App\Session::get('success')) {?>
                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                        <div class="col-md-12 profile-details">
                                            <br>
                                            <div style="width:1000px;margin-left:-100px;height:0px;">
                                                <a style="float:left;" name="left">&nbsp;</a>
                                                <a style="float:right;" name="right">&nbsp;</a>
                                            </div>
                                            <br>
                                            <br>
                                            
                                            <div class="alert alert-block alert-success fade in">
                                                <h4><i class="fa fa-heart" aria-hidden="true"></i> Successful!</h4>
                                                <?php echo \App\Session::get('success')?>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <?php
                                    \App\Session::UnsetKeySession('success');
                                }?>

                                <?php if(\App\Session::get('error')) {?>
                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                        <div class="col-md-12 profile-details">
                                            <br>
                                            <div style="width:1000px;margin-left:-100px;height:0px;">
                                                <a style="float:left;" name="left">&nbsp;</a>
                                                <a style="float:right;" name="right">&nbsp;</a>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="alert alert-block alert-danger fade in">
                                                <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                <p><?php echo \App\Session::get('error')?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    \App\Session::UnsetKeySession('error');
                                }?>

                                <div class='col-md-12'>
                                    
                                    
                                    <?php if($confirm==1) {?>
                                    
                                        <div class='tab-content'>
                                            <div class='tab-pane fade in active'>
                                                <div class='row'>
        
                                                    <div class='col-md-12'>
                                                        <!--<div class="alert alert-info">-->
                                                        <!--    Please review your transfer information and click the 'Confirm' button to proceed with the transfer.-->
                                                        <!--</div>-->
                                                        <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                            <input type='hidden' name='__req' value='1'>
                                                            <div class='form-body'>
                                                                <div class='form-group'>
                                                                   
                                                                </div>
        
                                                                <div class='form-group'>
                                                                    <label class='col-md-3 control-label'>Required DC: <span class='require'>*</span></label>
                                                                    <div class='col-md-5'>
                                                                        <?php echo $rate['requiredDC']?>.00
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class='form-group'>
                                                                    <label class='col-md-3 control-label'>Required PC: <span class='require'>*</span></label>
                                                                    <div class='col-md-5'>
                                                                        <?php echo $rate['requiredPC']?>.00
                                                                    </div>
                                                                </div>
        
                                                                <div class='form-group'>
                                                                    <label class='col-md-3 control-label'>Type: <span class='require'>*</span></label>
                                                                    <div class='col-md-5'>
                                                                        Account Upgrade
                                                                    </div>
                                                                </div>
                                                            
                                                                <br/>
                                                                <div class='form-group'>
                                                                    <label class='col-md-3 control-label'></label>
                                                                    <div class='col-md-5'>
                                                                        <a href="javascript:history.go(-1)" class='btn btn-default'>Back</a>
                                                                        <input type='submit' name='submit_btn_confirm' value='Confirm' class='btn btn-primary' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } else {?>
                                    
                                    <?php if($usergrade==3){?>
                                        <div class='alert alert-block alert-danger fade in'>
                                            <h4><i class='fa fa-times'></i> There is / are some error(s), please correct them before continuing</h4>
                                            <p>You have already upgraded your account to <?php echo $user->gradeGradeDetails(3)['gradeName']?></p>
                                        </div>
                                    <?php } else {?>

                                    <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                        <input type='hidden' name='__req' value='1'>
                                        <input type='hidden' name='__confirm' value='1'>
                                        <input type='hidden' name='__key' value='8185664'>

                                        <div class='form-body'>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Upgrade To:</span></label>
                                                <div class='col-md-4' ><?php echo  $gradeInfo['requiredDC']. " + ".$gradeInfo['requiredPC']?></div>
                                            </div>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Pairing EC Income:</span></label>
                                                <div class='col-md-4' >
                                                    <?php $ec=(empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')));
                                                    echo ($ec==0?0:$ec) ?>
                                                    .00</div>
                                            </div>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Declaration Currency:</span></label>
                                                <div class='col-md-4' ><?php echo $currency['dc']==0?0:$currency['dc']?>.00</div>
                                            </div>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Expired Date:</span></label>
                                                <div class='col-md-4'> <?php echo date('d-M-Y',strtotime($date['expire_date']))?></div>
                                            </div>
<!--                                            13-Oct-2019-->
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>New Expired Date:</span></label>
                                                <div class='col-md-4' > <?php echo  date('d-M-Y', strtotime($date['expire_date'].'+300 days'))?></div>
                                            </div>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'>Security Password: <span class='require'>*</span></label>
                                                <div class='col-md-4' ><input type='password' name='sec_pwd' class='form-control' value=''></div>
                                            </div>
                                            <div class='form-group'>
                                                <label class='col-md-3 control-label'></label>
                                                <div class='col-md-4'>
                                                    <input type='submit' name='submit_btn' value="Confirm" class='btn btn-primary btn-outlined' />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <?php }
                                
                                    }
                                
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>