<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
\App\Session::checkLoginSession();
    
    $user=new \App\user\User();
    $user->checkUserValidity(\App\Session::get('userID'));

    $client=new \App\client\Client();
    $currency=$user->userCurrencies(\App\Session::get('userID'));
    date_default_timezone_set('Asia/Dhaka');
    \App\Session::UnsetKeySession('confirm');

//    $sql="select dc from conversion_rate";
//    $stmt=\App\DBConnection::myQuery($sql);
//    $stmt->execute();
    $bc_rate=150;

    if(isset($_POST['submit_btn']))
    {
        if($user->verifyUser(base64_encode($_POST['password']))) {

            if(isset($_POST['_amount']) && $_POST['_amount']!='') {

                if (isset($_POST['remark']) && $_POST['remark'] != '') {

                    $_POST['amount'] = $bc_rate * $_POST['_amount'];
                    $res = $user->placementUserDetails($_POST['uid']);
                    
                    if($res['userID']!=\App\Session::get('userID')){
                        if ($_POST['amount'] <= $currency['bc'] && $currency['bc'] >=1) {

                            if ($client->checkUser(\App\Session::get('userID'), $res['userID'])) {
                                    /**
                                     * 2 for register currency
                                     * 1 for transfer type transaction
                                     **/
                                    \App\Session::set('confirm','confirm');
                                } else {
                                    \App\Session::set('error', 'You are not allowed to transfer to this client');
                                }
                            } else {
                                \App\Session::set('amounterror', ' You do not have sufficient Business Currency for this transfer');
                                $req_amount=$tc_rate * $_POST['_amount'];
                                //$avaialbe_amount=$tc_currency;
                        }
                    }else
                    {
                         \App\Session::set('error', 'You are not allowed to transfer to this client.');
                    }
                    
                }else
                {
                    \App\Session::set('error','Remark is a required field');
                }
            }else
            {
                \App\Session::set('error','Please select an amount.');
            }
        }else
        {
            \App\Session::set('error','You have entered an invalid Security Password.');
        }
    }
    
    if(isset($_POST['submit_btn_confirm']))
    {
        try{

            \App\DBConnection::myDb()->beginTransaction();
            
            //$client->storeTransactionHistory(\App\Session::get('userID'), $_POST['uid'], $_POST['amount'], 2, 1, $_POST['remark']);
            
            $bc_currency=$user->userCurrencies(\App\Session::get('userID'))['bc'];
            
            $user->decrementBC($bc_currency-$_POST['amount'],\App\Session::get('userID'));
            
            $user->userCurrencyHistory(\App\Session::get('userID'),$_POST['uid'],$_POST['amount'],'Transfer',93,93,$_POST['amount'],0,$_POST['remark'],$bc_currency-$_POST['amount'],0); //93 for bc transfer
    
    
            $receiver_bc_currency=$user->userCurrencies($_POST['uid'])['bc'];
            $user->updateBC($receiver_bc_currency+$_POST['amount'],$_POST['uid']);
            
            $user->userCurrencyHistory($_POST['uid'],\App\Session::get('userID'),$_POST['amount'],'Transfer',93,93,0,$_POST['amount'],$_POST['remark'],$receiver_bc_currency+$_POST['amount'],0); //93 for bc transfer
    
            // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit
            \App\Session::UnsetKeySession('confirm');
    
            \App\Session::set('complete','yes');
    
            \App\Session::set('success',"You have successfully transferred ".$_POST['amount'].".00 Business Currency to ".$_POST['username']."(".$_POST['full_name'].")");
            
            $currency['bc']=$bc_currency-$_POST['amount'];
            $new_bc=$bc_currency;
            
            \App\DBConnection::myDb()->commit();
            
        }catch(PDOException $exp){
            \App\DBConnection::myDb()->rollBack();
        }

    }

    ?>
    <?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
        <!-- PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <!-- STYLER -->

                    <!-- /STYLER -->
                    <!-- BREADCRUMBS -->
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="members/index.php">Home</a>
                        </li>
                        <li>Transfer</li>
                        <li>Business Currency Transfer</li>
                    </ul>
                    <!-- /BREADCRUMBS -->
                    <div class="clearfix">
                        <h3 class="content-title pull-left">Business Currency Transfer</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PAGE HEADER -->


        <div class='row'>
            <div class='col-md-3'>
                <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                    
                    <div style='font-size:20px;'><?php echo (($currency['bc']==0)?0.00:$currency['bc'])?></div>
                    <div>Business Currency</div>
                </a>
            </div>
        </div>


        <div class='row'>
            <div class='col-md-12'>
                <div class='box border'>
                    <div class='box-title'>
                        <h4 style='height:15px;'></h4>
                    </div>
                    <div class='box-body'>
                        <div class='tabbable header-tabs user-profile'>
                            
                            <ul class='nav nav-tabs'>
                            <li><a href="members/msp_transfer.php">MSP</a></li>
                            <li><a href="members/pp_transfer.php">Product Point</a></li>
                            <li class='active'><a href="members/bc-transfer.php">Business Currency</a></li>
                            <li><a href="members/trade-currency-transfer.php">Trade Currency</a></li>
                            <li><a href="members/pc_transfer.php">Product Currency</a></li>
                            <li><a href='members/rpoint-transfer.php'>Declaration Currency</a></li>
                            <li ><a href='members/rwallet-transfer.php'>Registration Fee</a></li>
                        </ul>

                            <?php if(\App\Session::get('complete')) { ?>

                                <div class='tab-content'>
                                    <div class='tab-pane fade in active'>
                                        <div class='row'>
                                            <?php if (\App\Session::get('success')) { ?>
                                                <div class="row"
                                                     style="overflow-x:auto;margin-left:0px;margin-right:0px;margin-top:25px">
                                                    <div class="col-md-12 profile-details">

                                                        <div class="alert alert-block alert-success fade in">
                                                            <h4><i class="fa fa-heart" aria-hidden="true"></i>
                                                                Successful!</h4>
                                                            <?php echo \App\Session::get('success') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                \App\Session::UnsetKeySession('success');
                                            } ?>

                                            <div class="text-center">
                                                <a href="members/bc-transfer.php" name='submit_btn_confirm' class='btn btn-primary '>Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php  \App\Session::UnsetKeySession('complete');?>

                            <?php } else { ?>

                                <?php if(\App\Session::get('confirm')) {?>

                                    <div class='tab-content'>
                                        <div class='tab-pane fade in active'>
                                            <div class='row'>
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                <div class='col-md-12'>
                                                    <div class="alert alert-info">
                                                        Please review your transfer information and click the 'Confirm' button to proceed with the transfer.
                                                    </div>
                                                    <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                        <input type='hidden' name='__req' value='1'>
                                                        <div class='form-body'>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Transfer To: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    <?php echo $_POST['uid']."(".$res['full_name'].")"?>
                                                                    <input type="hidden" value="<?php echo $res['userID']?>" name="uid">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" value="<?php echo $res['full_name'] ?>"
                                                                   name="full_name">
                                                            <input type="hidden" value="<?php echo $res['username'] ?>"
                                                                   name="username">

                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Transfer Amount: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    <?php echo $_POST['amount']?>.00
                                                                    <input type="hidden" value="<?php echo $_POST['amount']?>" name="amount">
                                                                </div>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Type: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    Business Currency Transfer
                                                                </div>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Remark: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    <?php echo $_POST['remark']?>
                                                                    <input type="hidden" value="<?php echo $_POST['remark']?>" name="remark">
                                                                </div>
                                                            </div>

                                                            <br/>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'></label>
                                                                <div class='col-md-5'>
                                                                    <a href="javascript:history.go(-1)" class='btn btn-default'>Back</a>
                                                                    <input type='submit' name='submit_btn_confirm' value='Confirm' class='btn btn-primary' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } else {?>

                                    <div class='tab-content'>
                                        <div class='tab-pane fade in active'>
                                            <div class='row'>
                                                &nbsp;
                                                &nbsp;
                                                &nbsp;
                                                <?php if(\App\Session::get('error')) {?>
                                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px;margin-top:50px">
                                                        <div class="col-md-12 profile-details">

                                                            <div class="alert alert-block alert-danger fade in">
                                                                <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                <p><?php echo \App\Session::get('error')?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    \App\Session::UnsetKeySession('error');
                                                }?>

                                                <?php if(\App\Session::get('success')) {?>
                                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                        <div class="col-md-12 profile-details">

                                                            <div class="alert alert-block alert-success fade in">
                                                                <p><?php echo \App\Session::get('success')?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    \App\Session::UnsetKeySession('success');
                                                }?>

                                                <?php if(\App\Session::get('amounterror')) {?>
                                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                        <div class="col-md-12 profile-details">

                                                            <div class="alert alert-block alert-danger fade in">
                                                                <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                <p><?php echo \App\Session::get('amounterror')?>.</p>
                                                                <p>Business Currency required: <?php echo $req_amount?>.00</p>
                                                                <p>Business Currency available: <?php echo (($currency['bc']==0)?0:$currency['bc'])?>.00</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    \App\Session::UnsetKeySession('amounterror');
                                                }?>

                                                <div class='col-md-12'>
                                                    <br/>
                                                    <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                        <input type='hidden' name='__req' value='1'>
                                                        <div class='form-body'>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Transfer Point Type: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    Business Currency Transfer
                                                                </div>
                                                            </div>
                                                            <!--$dc_rate-->
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Transfer Amount: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    <div class='row'>
                                                                        <div class='col-md-3' style='margin-top:0px;'>
                                                                            <?php echo $bc_rate?> x
                                                                        </div>
                                                                        <div class='col-md-9' style='margin-top:0px;'>
                                                                            <input type='number' name='_amount' class='form-control' value='' min='0' step='1'>
                                                                        </div>
                                                                    </div>
                                                                    <p class='help-block'>Business Currency Fee is <?php echo $bc_rate?> times.(E.g : 1 unit equal to <?php echo $bc_rate?>)</p>
                                                                </div>
                                                            </div>
                                                            <div class='form-group' id='transfer'>
                                                                <label class='col-md-3 control-label'>Transfer To: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    <input type='text' name='uid' id="uid" class='form-control' value='' required>
                                                                    <span class='help-block' id='search_user_result'>Please provide a Username.</span>
                                                                    <!--<div id='search_user_result'></div>-->
                                                                    <input type='button' id='check_user_btn1' value='Check User' class='btn btn-info'>
                                                                </div>
                                                            </div>
                                                            <div class='form-group' id='rpoint'>
                                                                <label class='col-md-3 control-label'>Business Currency: <span class='require'>*</span></label>
                                                                <div class='col-md-5' >
                                                                    <?php echo (($currency['bc']==0)?0:$currency['bc'])?>.00
                                                                </div>
                                                            </div>
                                                            <div class='form-group' id='remark'>
                                                                <label class='col-md-3 control-label'>Remark: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    <textarea name='remark' class='form-control' required></textarea>
                                                                </div>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Security Password:</label>
                                                                <div class='col-md-5'>
                                                                    <input type='password' name='password' class='form-control' value='' required>
                                                                </div>
                                                            </div>
                                                            <br/>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'></label>
                                                                <div class='col-md-5'>
                                                                    <input type='submit' name='submit_btn' value='Submit' class='btn btn-primary' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once "includes/footer.php";?>

    <script type='text/javascript'>
        jQuery(document).ready(function(j){
//        j('form[name=transfer_form]').submit(function(){
//            j('input[name=submit_btn]').attr('disabled','disabled');
//        });

//        j('input#check_user_btn').click(function(){
//            //alert(1);
//            j('span#transferee_text').html('').removeClass('red');
//            if(j('input[name=uid]').val()==''){
//                j('span#transferee_text').html('Please provide a Username.').addClass('red');
//            }
//            else{
//                j(this).attr('disabled','disabled');
//                j.ajax({
//                    url: 'rpoint-transfer.php?aj=1&check_user=1',
//                    data: {'uid': j('input[name=uid]').val()},
//                    type: 'post',
//                    dataType: 'html',
//                    error: function(){
//                        j('span#transferee_text').html('Error checking...').addClass('red');
//                    },
//                    success: function(data){
//                        var mesg='';
//                        if(j('loggedout', data).text()=='loggedout'){
//                            mesg='You have been logged out.';
//                        }else{
//                            j(data).find('msg').each(function(){
//                                mesg+=j(this).text()+' ';
//                            });
//                        }
//                        var status=j(data).find('status').text();
//                        j('span#transferee_text').html(mesg).addClass(status==1? 'bold' : 'red');
//                    },
//                    complete: function(){
//                        j('input#check_user_btn').removeAttr('disabled');
//                    }
//                });
//            }
//        });

            j('input[name=back_btn]').click(function(){
                q('__edit').value = 1;
                q('__confirm').value = '';
                j('input[name=back_btn],input[name=submit_btn]').attr('disabled','disabled');
                document.transfer_form.submit();
            });

            j('select[name=_type]').change(function(){
                update_amount();
            });

            update_amount();

            function update_amount(){
                var sel = j('select[name=_type] > option:selected').val();

                if(sel == '500' ){
                    j('p#50').hide();
                    j('p#250').hide();
                    j('p#500').show();
                    j('p#400').hide();
                }
                else if(sel == '400' ){
                    j('p#50').hide();
                    j('p#250').hide();
                    j('p#400').show();
                    j('p#500').hide();
                }
                else if(sel == '250' ){
                    j('p#50').hide();
                    j('p#250').show();
                    j('p#400').hide();
                    j('p#500').hide();
                }
                else if(sel == '50' ){
                    j('p#50').show();
                    j('p#250').hide();
                    j('p#400').hide();
                    j('p#500').hide();
                }
            }
        });

        $('#check_user_btn1').on('click',function () {
            var user= $('#uid').val();

            if(user!='')
            {
                $.ajax({
                    type:'post',
                    url:'ajax_files/check_upline_user.php',
                    data:{
                        'userfind':1,
                        'username':user
                    },
                    success:function (data) {
                        $('#search_user_result').text(data);
                    }
                })
            }
        })
    </script>
