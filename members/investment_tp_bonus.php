<?php 
include_once '../vendor/autoload.php';
\App\Session::init();
use \App\DBConnection;
$user=new \App\user\User();
$invest=new \App\investment\Investment();

$sql="select investID,userID,invest_packageID,invest_amount,investment_amount,return_rate

from invests

join investment_packages on investment_packages.packageID=invests.invest_packageID

join investment_types on investment_types.investment_typeID=invests.invest_typeID

where invest_closing_date=CURDATE() 

and invest_status=1 and invest_typeID=2 order by investID desc";

$stmt=DBConnection::myQuery($sql);
$stmt->execute();
$res=$stmt->fetchAll(PDO::FETCH_ASSOC);


try{
    \App\DBConnection::myDb()->beginTransaction();
    
    
    foreach($res as $rs)
    {
        $currency = $user->userCurrencies($rs['userID']);
        
        $tp_bonus=$rs['invest_amount']+($rs['invest_amount']*$rs['return_rate']/100);
        
        $user->incrementTP(($currency['tp'] + $tp_bonus), $rs['userID']);
       // 
        $user->userCurrencyHistory($rs['userID'], 0, $tp_bonus, 'TP Bonus', 92, 92, 0, $tp_bonus,"",$currency['tp']+$tp_bonus,0); //92 tp bonus on invest for without investment
        
        $invest->updateInvestStatus($rs['investID']);
        
        $invest->returnTC($currency['tc']+$rs['invest_amount'],$rs['userID']);
        
        $user->userCurrencyHistory(\App\Session::get('userID'), 0, $rs['invest_amount'], 'Return', 80, 80, 0, $rs['invest_amount'],"",$currency['tc']+$rs['invest_amount'],0);
        //80 for TC return on investment
        // echo $rs['investID'];
    }
    
    \App\DBConnection::myDb()->commit();
}catch (PDOException $exp)
{
    \App\DBConnection::myDb()->rollBack();
}



?>