<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
date_default_timezone_set('Asia/Dhaka');
$user=new \App\user\User();
$info=$user->getUserInfo();



$currency=$user->userCurrencies(\App\Session::get('userID'));
$total_pairingEC=$user->totalPairBonus(\App\Session::get('userID'));

$levelBonusSummery=$user->levelingBonusSummery(\App\Session::get('userID'))['totalBonus'];

$introducerBonusSummery=$user->introducerBonusSummery(\App\Session::get('userID'))['totalBonus'];
if(isset($_POST['submit_btn']))
{
    if(isset($_POST['sec_password']) && isset($_POST['_password']) && isset($_POST['password2']) ) {

        if($_POST['_password']!='' && $_POST['sec_password'] !=''){

            if($_POST['_password'] != $_POST['sec_password']) {

                      if(strlen($_POST['_password'])>=8)  {
                              if(preg_match('/(?=.*\d)(?=.*[A-Za-z])/', $_POST['_password']))
                              {
                                    if ($user->verifyUser((base64_encode($_POST['sec_password'])))) {
                                        if ($user->checkExistingPassword(base64_encode($_POST['old_password']))) {
                                            if (base64_encode($_POST['_password']) == base64_encode($_POST['password2'])) {
                        
                                                if($user->updatePassword(base64_encode($_POST['_password']), \App\Session::get('userID')))
                                                {
                                                    if(isset($_POST['all']) && $_POST['all']!='' && $info['packageID']!=1)
                                                    {
                                                        foreach ($user->getAllSubAccounts(\App\Session::get('userID')) as $subusers)
                                                        {
                                                            $user->updatePassword(base64_encode($_POST['_password']), $subusers['userID']);
                                                            $user->updateSts($subusers['userID']);
                                                        }
                                                    }
                                                    $info=$user->getUserInfo();
                                                    \App\Session::UnsetKeySession('login',false);
                                                    \App\Session::set('passwrodupdate','yes');
                                                    \App\Session::set('success', "<p>You have successfully updated your account password.</p><p>You will need to re-login to your account. Please click on the bellow button to login.</p>");
                                                }
                      
                                                $user->updateUserPss(\App\Session::get('userID'));
                                                $user->updateStatus();

                                            } else {
                                                \App\Session::set('error', "You have entered an invalid old Password.");
                                            }
                                        }else
                                        {
                                            \App\Session::set('error', 'You have entered an invalid old password.');
                                        }
                        
                                    } else {
                                        \App\Session::set('error', 'You have entered an invalid security password.');
                                    }
                            }else 
                            {
                                \App\Session::set('error', 'Password must be a combination of numbers and alphabets.');
                            }
                    }else
                    
                    {
                         \App\Session::set('error', "'Password' must between 8 to 32 characters only. You have entered ".strlen($_POST['_password'])." character(s).");
                    }

            }else
            {
                 \App\Session::set('error', 'Password and security password can not be the same.');
            }
                    
                    
            }else 
            {
               \App\Session::set('error', "<p>'Password' is a required field.</p><p>Please provide your Security Password.</p>"); 
            }
            
            
               
        }else
            {
                \App\Session::set('error', "'Password' is a required field.");
            }
            
    }
    


if(isset($_POST['submit_btn1']))
{

    if(isset($_POST['old_sec_password']) && ($_POST['old_sec_password']!='')  ) {
        
                      if(strlen($_POST['_sec_password'])>=8)  {
                          
                          
                        if($_POST['_sec_password']!=base64_decode($user->getPsrd(\App\Session::get('userID'))['password'])){
                    
                        if(preg_match('/(?=.*\d)(?=.*[A-Za-z])/', $_POST['_sec_password'])){
                        
                                    if ($user->checkExistingSecurityPassword(base64_encode($_POST['old_sec_password']))) {
                        
                                        if (base64_encode($_POST['_sec_password']) == base64_encode($_POST['sec_password2'])) {
                        
                                            $user->updateSecurityPassword(base64_encode($_POST['_sec_password']), \App\Session::get('userID'));
                        //
                        //                    $user->updateUserAccountStatus(1);
                        
                                            $user->updateUserSecpss(\App\Session::get('userID'));
                                            $user->updateStatus();
                        //
                                            \App\Session::set('success', "Security Password updated Successfully!!");
                        //
                                            if(isset($_POST['all']) && $_POST['all']!='' && $info['packageID']!=1)
                                            {
                                                foreach ($user->getAllSubAccounts(\App\Session::get('userID')) as $subusers)
                                                {
                                                    $user->updateSecurityPassword(base64_encode($_POST['_sec_password']), $subusers['userID']);
                                                }
                                            }
                                            $info=$user->getUserInfo();
                        
                                        } else {
                                            \App\Session::set('error', "You have entered an invalid Security Password.");
                                        }
                                    }else
                                    {
                                        \App\Session::set('error', 'You have entered an invalid Old Security Password');
                                    }
                              }
                              else
                                {
                                    \App\Session::set('error','Security Password must be a combination of numbers and alphabets.');
                                }
                                
                                
                        }else
                        {
                               \App\Session::set('error','Security Password and password can not be the same.');
                        }
                                
                                
                      }else
                      {
                             \App\Session::set('error',"Security Password' must between 8 to 32 characters only. You have entered ".strlen($_POST['_sec_password'])." character(s).");
                      }
                      
                      
                      
                      
                    
        }
        else {
            \App\Session::set('error', "'Security Password' is a required field.");
    }
}

$pinfo=$user->getPasswordInfo();

?>
<?php include_once "includes/header.php";?>

    <div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>

                    <li>Password</li>

                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Profile</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->


    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4><i class='fa fa-user'></i><span class='hidden-inline-mobile'>Hello, <?php echo $info['full_name']?>  !</span></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li class='active'><a href='members/profile-password.php'><i class='fa fa-edit'></i> <span class='hidden-inline-mobile'>Password</span></a></li>
                            <li class=''><a href='members/profile.php'><i class='fa fa-user'></i> <span class='hidden-inline-mobile'>Profile</span></a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>

                                <div class='row'>

                                    <div class='col-md-3'>
                                        <div class='list-group'>
                                            <div class='list-group-item profile-details'>
                                                <h2 style='margin-top:0px;'><?php echo $info['full_name']?> </h2>
                                                <p><i class='fa fa-circle text-green'></i> Online</p>
                                                <?php echo $info['email']?></p>
                                            </div>

                                            <a href='members/history_ewallet.php' class='list-group-item'>
                                                <?php
//                                                $ec=$user->totalEc((\App\Session::get('userID')))+(empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')));
                                                echo number_format($currency['ec'] ,2) ?><br/>
                                                <strong>Electronic Currency</strong>
                                            </a>

                                            
                                            <a href='members/history_rwallet.php' class='list-group-item'><?php echo  number_format($currency['rf'],2)?><br/><strong>Registration Fee</strong></a>
                                            <a href='members/history_rpoint.php' class='list-group-item'><?php echo  number_format($currency['dc'],2)?><br/><strong>Declaration Currency</strong></a>
                                            <a href='members/pc-history.php' class='list-group-item'><?php echo number_format($currency['pc'],2) ?><br/><strong>Product Currency</strong></a>
                                            <a href='members/pp-history.php' class='list-group-item'><?php echo  number_format($currency['pp'],2)?><br/><strong>Product Point</strong></a>
                                            <a href='members/history_cwallet.php' class='list-group-item'><?php echo  number_format($currency['msp'],2)?><br/><strong>MSP</strong></a>
                                            <a href='members/rebates-pairing-bonus.php' class='list-group-item'><?php echo ($total_pairingEC==0?'0.00':$total_pairingEC)?><br/><strong>Pairing EC Income</strong></a>
                                            <a href='members/rebates.php' class='list-group-item'><?php echo $total_pairingEC+$levelBonusSummery+$introducerBonusSummery?>.00<br/><strong>Total Income</strong></a>
                                        </div>
                                    </div>

                                    <div class='col-md-8'>
                                        <div class='row'>
                                            <div class='col-md-12 profile-details'>
                                                <div class='box border green'>
                                                    <div class='box-body big'>
                                                        <div class='row'>
                                                            <?php if(\App\Session::get('error')) {?>
                                                                <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                                    <div class="col-md-12 profile-details">
                                                                        <div class="alert alert-block alert-danger fade in">
                                                                            <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                            <p><?php echo \App\Session::get('error')?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                \App\Session::UnsetKeySession('error');
                                                            }?>
                                                            
                                                            <?php
                                                                if($pinfo['sec_p_update']==0 || $pinfo['pss_update']==0)
                                                                {
                                                            ?>
                                                                <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                                    <div class="col-md-12 profile-details">
                                                                        <div class="alert alert-block alert-danger fade in">
                                                                            <?php
                                                                                if($pinfo['sec_p_update']==0)
                                                                                {
                                                                            ?>
                                                                            <p>Please change your security password.</p>
                                                                            <p>Please make sure your new security password is different from the existing security password.</p>
                                                                            <?php }?>
                                                                            
                                                                            <?php
                                                                                if( $pinfo['pss_update']==0)
                                                                                {
                                                                            ?>
                                                                            
                                                                             <p>Please change your password.</p>
                                                                            <p>Please make sure your new password is different from the existing password.</p>
                                                                            <?php }?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            
                                                            <?php }?>
                                                            

                                                            <?php if(\App\Session::get('success')) {?>
                                                                <div class="row" style="margin-left:0px;margin-right:0px">
                                                                    <div class="col-md-12 profile-details">
                                                                        <div class="alert alert-block alert-success fade in">
                                                                            <h4><i class="fa fa-heart" aria-hidden="true"></i> Successful!</h4>
                                                                            <?php echo \App\Session::get('success')?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                \App\Session::UnsetKeySession('success');
                                                            }?>


                                                            <?php if(\App\Session::get('passwrodupdate')=='yes') {?>

                                                                <div class='form-group'>
                                                                    <label class='col-md-12'><a href="login.php" class="btn btn-success" style="text-align: center;margin: 0 auto;display: table">Login</a></label>
                                                                </div>

                                                            <?php \App\Session::UnsetKeySession('passwrodupdate'); }?>

                                                                <div class='col-md-12'>
                                                                    <h4>Account Password</h4>

                                                                    <form name='profile_pass_form' method='post' action='' class='form-horizontal'>
                                                                        <input type='hidden' name='__req' value='1' />

                                                                        <div class='form-group'>
                                                                            <label class='col-md-4 control-label'>Existing Password:</label>
                                                                            <div class='col-md-8'><input type='password' name='old_password' class='form-control' required/></div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class='col-md-4 control-label'>New Password:</label>
                                                                            <div class='col-md-8'><input type='password' name='_password' class='form-control' required/><span class='help-block'>Password must be at least 8 characters long, and must be a combination of numbers and alphabet.</span></div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class='col-md-4 control-label'>Confirm New Password:</label>
                                                                            <div class='col-md-8'><input type='password' name='password2' class='form-control' required/></div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class='col-md-4 control-label'>Security Password:</label>
                                                                            <div class='col-md-8'><input type='password' name='sec_password' class='form-control' required /></div>
                                                                        </div>

                                                                        <?php if($user->countChild(\App\Session::get('userID'))>1){?>
                                                                            <div class='form-group'>
                                                                                <label class='col-md-4 control-label'></label>
                                                                                <div class='col-md-8'><input type='checkbox' name='all' /> Apply to all sub account</div>
                                                                            </div>
                                                                        <?php }?>

                                                                        <br/>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3'><input type='submit' class='btn btn-success' name='submit_btn' value="Update" /></label>
                                                                        </div>
                                                                        <br/>
                                                                    </form>


                                                                    <h4>Security Password</h4>


                                                                    <form name='profile_secpass_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                                        <input type='hidden' name='__req' value='2' />
                                                                        <div class='form-group'>
                                                                            <label class='col-md-4 control-label'>Existing Security Password:</label>
                                                                            <div class='col-md-8'><input type='password' name='old_sec_password' class='form-control' required /></div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class='col-md-4 control-label'>New Security Password:</label>
                                                                            <div class='col-md-8'><input type='password' name='_sec_password' class='form-control' required/></div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class='col-md-4 control-label'>Confirm New Security Password:</label>
                                                                            <div class='col-md-8'><input type='password' name='sec_password2' class='form-control' required /></div>
                                                                        </div>

                                                                        <?php if($user->countChild(\App\Session::get('userID'))>1){?>
                                                                            <div class='form-group'>
                                                                                <label class='col-md-4 control-label'></label>
                                                                                <div class='col-md-8'><input type='checkbox' name='all' /> Apply to all sub account</div>
                                                                            </div>
                                                                        <?php }?>

                                                                        <br/>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3'><input type='submit' class='btn btn-success' name='submit_btn1' value="Update" /></label>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>