<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
date_default_timezone_set('Asia/Dhaka');
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
$levelbonus=$user->totalLevelBonus(\App\Session::get('userID'));

$ToatlintroducerBonus=$user->totalIntroducerBonus(\App\Session::get('userID'));

$previousPairingEc=$user->previousPairingEc(\App\Session::get('userID'));

$todaysPairingbonus=$user->todaysPairingEc(\App\Session::get('userID'));

$levelBonusSummery=$user->levelingBonusSummery(\App\Session::get('userID'));

$introducerBonusSummery=$user->introducerBonusSummery(\App\Session::get('userID'));

?>
<?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>Bonus Summary</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Bonus Summary</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->



    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-danger btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo (empty($ToatlintroducerBonus)?0:$ToatlintroducerBonus)?>.00</div>
                <div>Introducer Bonus</div>
            </a>
        </div>
        <div class='col-md-3'>

            <a class='btn btn-blueBox btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo (empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')))?>.00</div>
                <div>Pairing Bonus</div>
            </a>

        </div>
        <div class='col-md-3'>
            <a class='btn btn-greenBox btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo (empty($levelbonus)?0:$levelbonus)?>.00</div>
                <div>Leveling Bonus</div>
            </a>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li class=''><a href="members/rebates-pairing-bonus-reports.php"><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus Reports</span></a></li>
                            <li class=''><a href='members/rebates-leveling-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Leveling Bonus</span></a></li>
                            <li class=''><a href='members/rebates-pairing-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus</span></a></li>
                            <li class=''><a href='members/rebates-introducer.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Introducer Bonus</span></a></li>
                            <li class='active'><a href='members/rebates.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Bonus Summary</span></a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <div class='col-md-12'>

                                        <div class='box border blue'>
                                            <div class='box-body' style='margin-top:10px;'>
                                                <form class='form-horizontal' action='' method='get'>
                                                    <input type='hidden' name='__req' value='1' />
                                                    <input type='hidden' name='type' value='' />
                                                    <input type='hidden' name='nav' value='' />
                                                    <div class='form-group'>
                                                        <label class='col-md-2 control-label'>From:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='start' value=''>
                                                        </div>

                                                        <label class='col-md-1 control-label'>To:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='end' value=''>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <input type='submit' class='btn btn-success' value="Generate"/>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>


                                        <table id='example-paper' class='table table-paper table-striped'>
                                            <thead>
                                                <tr>
                                                    <th width='30%'>Bonus</th>
                                                    <th class='center'>Last Bonus</th>
                                                    <th class='center'>No. of Times</th>
                                                    <th class='center'>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                            <?php if(isset($_GET['start']) && $_GET['start']!='' && isset($_GET['end']) && $_GET['end']!=''){?>

                                                <?php
                                                    $introducerBonus=$user->introducerBonusSummeryBwteenDateRange(\App\Session::get('userID'),$_GET['start'],$_GET['end']);
                                                ?>

                                                <tr class='row0'>
                                                    <td>Introducer Bonus</td>
                                                    <?php if(!empty($introducerBonus['totalBonus'])){?>
                                                        <td class='center'>
                                                            <?php
                                                            $datetime=$user->getLastTimeRecord(\App\Session::get('userID'),3);
                                                            echo (empty($datetime)?'-':date('d/m/Y',strtotime($datetime['bonus_date']))." ".$datetime['bonus_time'])
                                                            ?>
                                                        </td>
                                                    <?php } else {?>
                                                        <td class='center'>
                                                            -
                                                        </td>
                                                    <?php }?>
                                                    <td class='center'><?php echo (empty($introducerBonus['totalTimes'])?0:$introducerBonus['totalTimes'])?></td>
                                                    <td class='center'><?php echo (empty($introducerBonus['totalBonus'])?0:$introducerBonus['totalBonus'])?>.00</td>
                                                </tr>


                                                <tr class='row1'>
                                                    <?php
                                                    $pairbonusSummery=$user->pairingBonusSummeryOfThisDate(\App\Session::get('userID'),$_GET['start'],$_GET['end']);
                                                    ?>

                                                    <td>Pairing Bonus</td>
                                                    <td class="center">
                                                        <?php
                                                        $datetime=$user->getLastTimePairbonusRecordOfThisDate(\App\Session::get('userID'),$_GET['start'],$_GET['end']);
                                                        echo (empty($datetime)?'-':date('d/m/Y',strtotime($datetime['bonus_date']))." ".$datetime['bonus_time'])
                                                        ?>
                                                    </td>

                                                    <td class="center">
                                                        <?php echo (empty($pairbonusSummery['totalTimes'])?0:$pairbonusSummery['totalTimes'])?>
                                                    </td>

                                                    <td class='center'>
                                                        <?php echo (empty($pairbonusSummery['totalBonus'])?'-':$pairbonusSummery['totalBonus'])?>
                                                    </td>

                                                </tr>

                                                <tr class='row0'>
                                                    <?php
                                                        $levelBonusSummery=$user->levelingBonusSummeryOfThisDate(\App\Session::get('userID'),$_GET['start'],$_GET['end']);
                                                    ?>
                                                    <td>Leveling Bonus</td>
                                                    <td class='center'>
                                                        <?php
                                                        $datetime=$user->getLastTimeRecordOfThisDate(\App\Session::get('userID'),1,$_GET['start'],$_GET['end']);
                                                        echo (empty($datetime)?'-':date('d/m/Y',strtotime($datetime['bonus_date']))." ".$datetime['bonus_time'])
                                                        ?></td>
                                                    <td class='center'><?php echo (empty($levelBonusSummery['totalTimes'])?0:$levelBonusSummery['totalTimes'])?></td>
                                                    <td class='center'><?php echo (empty($levelBonusSummery['totalBonus'])?0:$levelBonusSummery['totalBonus'])?>.00</td>
                                                </tr>

                                            <?php }

                                            /**----------------------------------------------------------------------------------------------------------------**/

                                            else {?>

                                                <tr class='row0'>
                                                    <td>Introducer Bonus</td>

                                                    <?php if(!empty($introducerBonusSummery['totalBonus'])){?>
                                                        <td class='center'>
                                                            <?php
                                                            $datetime=$user->getLastTimeRecord(\App\Session::get('userID'),3);
                                                            echo (empty($datetime)?'-':date('d/m/Y',strtotime($datetime['bonus_date']))." ".$datetime['bonus_time'])
                                                            ?>
                                                        </td>
                                                    <?php } else {?>
                                                        <td class='center'>
                                                            -
                                                        </td>
                                                    <?php }?>

                                                    <td class='center'><?php echo (empty($introducerBonusSummery['totalTimes'])?0:$introducerBonusSummery['totalTimes'])?></td>
                                                    <td class='center'><?php echo (empty($introducerBonusSummery['totalBonus'])?0:$introducerBonusSummery['totalBonus'])?>.00</td>
                                                </tr>

                                                <tr class='row1'>
                                                    <td>Pairing Bonus</td>
                                                    <td class='center'>
                                                        <?php $datetime=$user->getLastTimePairBonusRecord(\App\Session::get('userID'));
                                                        echo (empty($datetime)?'-':date('d/m/Y',strtotime($datetime['bonus_date']))." ".$datetime['bonus_time']);
                                                        ?>
                                                    </td>
                                                    <td class='center'>
                                                        <?php echo (empty($user->getTotalTimeOfPairingBonus(\App\Session::get('userID')))?0:$user->getTotalTimeOfPairingBonus(\App\Session::get('userID')))?>
                                                    </td>
                                                    <td class='center'>
                                                        <?php echo (empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')))?>.00
                                                    </td>
                                                </tr>

                                                <tr class='row0'>
                                                    <td>Leveling Bonus</td>
                                                    <td class='center'>
                                                        <?php
                                                        $datetime=$user->getLastTimeRecord(\App\Session::get('userID'),1);
                                                        echo (empty($datetime)?'-':date('d/m/Y',strtotime($datetime['bonus_date']))." ".$datetime['bonus_time'])
                                                        ?></td>
                                                    <td class='center'><?php echo (empty($levelBonusSummery['totalTimes'])?0:$levelBonusSummery['totalTimes'])?></td>
                                                    <td class='center'><?php echo (empty($levelBonusSummery['totalBonus'])?0:$levelBonusSummery['totalBonus'])?>.00</td>
                                                </tr>
                                            <?php }?>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>