<?php
include_once '../vendor/autoload.php';
\App\Session::init();
\App\Session::clientListSession();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
$client=new \App\client\Client();
//$info=$user->getUserInfoex(\App\Session::get('userID'));

if(isset($_GET['headUid']) && $_GET['headUid']!='')
{
    $info=$client->getUserInfo($_GET['headUid']);
    if(empty($info))
    {
        \App\Session::set('error','Invalid Username');
    }
}else
{
    $info=$client->getUserInfo(\App\Session::get('username'));
}

$package=new \App\package\Package();

$usergrade1=$user->getGradeAmount(1);
$usergrade2=$user->getGradeAmount(2);
$usergrade3=$user->getGradeAmount(3);

?>
<?php include_once "includes/header.php";?>

<div id="content" class="col-lg-12">
            <!-- PAGE HEADER-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header">
                        <!-- STYLER -->

                        <!-- /STYLER -->
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="members/index.php">Home</a>
                            </li>

                            <li>Genealogy</li>

                        </ul>
                        <!-- /BREADCRUMBS -->
                        <div class="clearfix">
                            <h3 class="content-title pull-left">Genealogy</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /PAGE HEADER -->


            <div class='row'>
                <div class='col-md-12'>
                    <div class='box border'>
                        <div class='box-title'>
                            <h4 style='height:15px;'></h4>
                        </div>
                        <div class='box-body'>
                            <div class='tabbable header-tabs user-profile'>
                                <ul class='nav nav-tabs'>
                                    <li class='active'><a href='members/genealogy.php'>Genealogy</a></li>
                                </ul>
                                <div class='tab-content'>
                                    <div class='tab-pane fade in active'>
                                        <div class='row'>
                                            <div class='col-md-12'>
                                                <div class='box border blue'>
                                                    <div class='box-body' style='margin-top:10px;'>
                                                        <form class='form-horizontal' action='' method='get'>
                                                            <input type='hidden' name='__req' value='1'>
                                                            <input type='hidden' name='type' value=''>
                                                            <input type='hidden' name='nav' value=''>
                                                            <div class='form-group'>
                                                                <label class='col-md-2 control-label'>Username</label>
                                                                <div class='col-md-3'>
                                                                    <?php if(isset($_GET['headUid']) && $_GET['headUid']!=''){?>
                                                                        <input class='form-control' type='text' name='headUid' value='<?php echo $_GET['headUid']?>'>
                                                                    <?php } else {?>
                                                                        <input class='form-control' type='text' name='headUid' value=''>
                                                                    <?php }?>
                                                                </div>
                                                                <div class='col-md-3'>
                                                                    <input type='submit' name='user_search' value='Generate' class='btn btn-success'>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class='row'>
                                                    <div class='col-md-12 profile-details'>
                                                        <br/>
                                                        <div class='genealogy-legend'>
                                                            <div class='legend-item' style="margin-bottom:20px">
                                                                <?php foreach($package->getPackages() as $pckgs){?>
                                                                
                                                                    <div class='legend-rank-text' style="margin-right:30px" >
                                                                        
                                                                        <div class='amount_image' style="width: 38px;height: 35px;top: 13px;font-size: 15px;padding-top: 6px;">
                                                                            <?php
                                                                                echo $pckgs['packageDC']+$pckgs['packagePC']; ?>
                                                                        </div>
                                                                        <?php echo $pckgs['packageName']?>
                                                                        
                                                                        </div>
                                                                    
                                                                    
                                                                    
                                                                <?php }?>
                                                            </div>


                                                        <div class='clear'></div>

                                                        <div class="tree-genealogy">

                                                            <div class="tree-controller-root-wrap">


                                                                <?php if(!empty($info)) {?>
                                                                <div class="dTree">
                                                                <ul>
                                                                    <li>
                                                                        <!--<img src="http://client.msm4all.com/secure/app_html/image/network/rank/2000.png" width="32" >-->
                                                                        <div class='amount_image'>
                                                                            <?php
                                                                            //     if($info['grade_id']==1)
                                                                            // {
                                                                            //     if($info['RF']==0)
                                                                            //     {
                                                                            //         echo  $info['DC'] +$info['PC'];
                                                                            //     }else
                                                                            //     {
                                                                            //         echo  $info['DC']+$info['PC'];
                                                                            //     }
                                                                            // }else
                                                                            
                                                                            if($info['grade_id']==1)
                                                                            {
                                                                                echo $usergrade1['requiredDC'] +$usergrade1['requiredPC'];
                                                                                
                                                                            }else if($info['grade_id']==2)
                                                                            {
                                                                                echo $usergrade2['requiredDC'] +$usergrade2['requiredPC'];
                                                                                
                                                                            }
                                                                            else if($info['grade_id']==3)
                                                                            {
                                                                                echo $usergrade3['requiredDC'] +$usergrade3['requiredPC'];
                                                                            }
                                                                    
                                                                            ?>
                                                                        </div>
                                                                        <a class="list" href="#" style="border: 1px solid orange;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo $info['username']?></a>
                                                                        <a class="list" href="#" style="border: 1px solid red;min-width: 70px;text-align: center;width:188px;font-size: 11px;height: 15px"><?php echo $info['full_name']?></a>
                                                                        <a class="list" href="#" style="border: 1px solid green;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px">
                                                                            
                                                                            <?php 
                                                                            //$package->getPackageAmount($info['packageID']) 
                                                                            
                                                                            if($info['grade_id']==1)
                                                                            {
                                                                                if($info['RF']==0)
                                                                                {
                                                                                    echo  "DC".$info['DC']." + PC".$info['PC'];
                                                                                }else
                                                                                {
                                                                                    echo  "DC".$info['DC']." + PC".$info['PC']." + RF".$info['RF'];
                                                                                }
                                                                            }else if($info['grade_id']==2)
                                                                            {
                                                                                echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                
                                                                            }else if($info['grade_id']==3)
                                                                            {
                                                                                echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                            }
                                                                        
                                                                            
                                                                            ?>
                                                                            
                                                                            </a>
                                                                        <a class="list" href="#" style="border: 1px solid blue;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo "Joined ".$info['user_registered_date']?></a>

                                                                        <ul>
                                                                            <?php

                                                                            if(isset($_GET['headUid']) && $_GET['headUid']!='')
                                                                            {
                                                                                $info=$client->getUserInfo($_GET['headUid']);
                                                                                $datas=$user->getAllSponsorUser($info['userID']);
                                                                            }else{
                                                                                $datas=$user->getAllSponsorUser(\App\Session::get('userID'));
                                                                            }

                                                                            foreach ($datas as $usr){?>
                                                                                <li>
                                                                                    <!--<img src="http://client.msm4all.com/secure/app_html/image/network/rank/1000.png" width="32" >-->
                                                                                    <div class='amount_image'>
                                                                                        <?php   
                                                                                            // if($usr['grade_id']==1)
                                                                                            // {
                                                                                            //     if($usr['RF']==0)
                                                                                            //     {
                                                                                            //         echo  $usr['DC']+ $usr['PC'];
                                                                                            //     }else
                                                                                            //     {
                                                                                            //         echo  $usr['DC'] + $usr['PC'];
                                                                                            //     }
                                                                                            // }else
                                                                                            
                                                                                            if($usr['grade_id']==1)
                                                                                            {
                                                                                                echo $usergrade1['requiredDC'] +$usergrade1['requiredPC'];
                                                                                            }
                                                                                            else if($usr['grade_id']==2)
                                                                                            {
                                                                                                echo $usergrade2['requiredDC']+ $usergrade2['requiredPC'];
                                                                                            }
                                                                                            else if($usr['grade_id']==3)
                                                                                            {
                                                                                                echo $usergrade3['requiredDC']+ $usergrade3['requiredPC'];
                                                                                            }
                                                                                        
                                                                                        ?>
                                                                                    </div>
                                                                                    <a class="list" href="#" style="border: 1px solid orange;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo $usr['username']?></a>
                                                                                    <a class="list" href="#" style="border: 1px solid red;min-width: 70px;text-align: center;width:188px;font-size: 11px;height: 15px"><?php echo $usr['full_name']?></a>
                                                                                    <a class="list" href="#" style="border: 1px solid green;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px">
                                                                                        <?php
                                                                                        
                                                                                        //echo $package->getPackageAmount($usr['packageID'])
                                                                                        
                                                                                            if($usr['grade_id']==1)
                                                                                            {
                                                                                                if($usr['RF']==0)
                                                                                                {
                                                                                                    echo  "DC".$usr['DC']." + PC".$usr['PC'];
                                                                                                }else
                                                                                                {
                                                                                                    echo  "DC".$usr['DC']." + PC".$usr['PC']." + RF".$usr['RF'];
                                                                                                }
                                                                                            }else if($usr['grade_id']==2)
                                                                                            {
                                                                                                echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                                
                                                                                            }else if($usr['grade_id']==3)
                                                                                            {
                                                                                                echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                            }
                                                                                        
                                                                                        ?>
                                                                                        
                                                                                        </a>
                                                                                    <a class="list" href="#" style="border: 1px solid blue;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo "Joined ".$usr['user_registered_date']?></a>

                                                                                    <?php
                                                                                        $moreusers=$user->getAllSponsorUser($usr['userID']);
                                                                                        if((count($moreusers))>=1) {
                                                                                                ?>
                                                                                                <ul style="">
                                                                                                    <?php foreach ($moreusers as $musr){?>
                                                                                                        <li>
                                                                                                            <!--<img src="http://client.msm4all.com/secure/app_html/image/network/rank/1000.png" width="32">-->
                                                                                                            
                                                                                                            <div class='amount_image'>
                                                                                                                <?php   
                                                                                                                    // if($musr['grade_id']==1)
                                                                                                                    // {
                                                                                                                    //     if($musr['RF']==0)
                                                                                                                    //     {
                                                                                                                    //         echo  $musr['DC']+ $musr['PC'];
                                                                                                                    //     }else
                                                                                                                    //     {
                                                                                                                    //         echo  $musr['DC'] + $musr['PC'];
                                                                                                                    //     }
                                                                                                                    // }else
                                                                                                                    
                                                                                                                    if($musr['grade_id']==1)
                                                                                                                    {
                                                                                                                        echo $usergrade1['requiredDC'] +$usergrade1['requiredPC'];
                                                                                                                        
                                                                                                                    }else if($musr['grade_id']==2)
                                                                                                                    {
                                                                                                                        echo $usergrade2['requiredDC']+ $usergrade2['requiredPC'];
                                                                                                                    }
                                                                                                                    else if($musr['grade_id']==3)
                                                                                                                    {
                                                                                                                        echo $usergrade3['requiredDC']+ $usergrade3['requiredPC'];
                                                                                                                    }
                                                                                                                ?>
                                                                                                            </div>
                                                                                                            
                                                                                                            <a class="list" href="#" style="border: 1px solid orange;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo $musr['username']?></a>
                                                                                                            <a class="list" href="#" style="border: 1px solid red;min-width: 70px;text-align: center;width:188px;font-size: 11px;height: 15px"><?php echo $musr['full_name']?></a>
                                                                                                            <a class="list" href="#" style="border: 1px solid green;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px">
                                                                                                                <?php
                                                                                                                
                                                                                                                    if($musr['grade_id']==1)
                                                                                                                    {
                                                                                                                        if($musr['RF']==0)
                                                                                                                        {
                                                                                                                            echo  "DC".$musr['DC']." + PC".$musr['PC'];
                                                                                                                        }else
                                                                                                                        {
                                                                                                                            echo  "DC".$musr['DC']." + PC".$musr['PC']." + RF".$musr['RF'];
                                                                                                                        }
                                                                                                                    }else
                                                                                                                    
                                                                                                                    if($musr['grade_id']==2)
                                                                                                                    {
                                                                                                                        echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                                                        
                                                                                                                    }else if($musr['grade_id']==3)
                                                                                                                    {
                                                                                                                        echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                                                    }
                                                                                                                    
                                                                                                                ?>
                                                                                                                
                                                                                                                </a>
                                                                                                            <a class="list" href="#" style="border: 1px solid blue;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo "Joined ".$musr['user_registered_date']?></a>
                                                                                                            <?php
                                                                                                            $moreuserss=$user->getAllSponsorUser($musr['userID']);
                                                                                                            if((count($moreuserss))>=1) {
                                                                                                                ?>
                                                                                                                <ul>
                                                                                                                    <?php foreach ($moreuserss as $msr){?>
                                                                                                                        <li>
                                                                                                                            <!--<img src="http://client.msm4all.com/secure/app_html/image/network/rank/1000.png" width="32">-->
                                                                                                                            
                                                                                                                            <div class='amount_image'>
                                                                                                                                <?php   
                                                                                                                                    // if($msr['grade_id']==1)
                                                                                                                                    // {
                                                                                                                                    //     if($msr['RF']==0)
                                                                                                                                    //     {
                                                                                                                                    //         echo  $msr['DC']+ $msr['PC'];
                                                                                                                                    //     }else
                                                                                                                                    //     {
                                                                                                                                    //         echo  $msr['DC'] + $msr['PC'];
                                                                                                                                    //     }
                                                                                                                                    // }else if($msr['grade_id']==2)
                                                                                                                                    // {
                                                                                                                                    //     echo $usergrade2['requiredDC'] +$usergrade2['requiredPC'];
                                                                                                                                        
                                                                                                                                    // }else if($msr['grade_id']==3)
                                                                                                                                    // {
                                                                                                                                    //     echo $usergrade3['requiredDC']+ $usergrade3['requiredPC'];
                                                                                                                                    // }
                                                                                                                                    
                                                                                                                                    
                                                                                                                                     if($msr['grade_id']==1)
                                                                                                                                    {
                                                                                                                                        echo $usergrade1['requiredDC'] +$usergrade1['requiredPC'];
                                                                                                                                        
                                                                                                                                    }
                                                                                                                                    else if($msr['grade_id']==2)
                                                                                                                                    {
                                                                                                                                        echo $usergrade2['requiredDC']+ $usergrade2['requiredPC'];
                                                                                                                                    }else if($msr['grade_id']==3)
                                                                                                                                    {
                                                                                                                                        echo $usergrade3['requiredDC']+ $usergrade3['requiredPC'];
                                                                                                                                    }
                                                                                                                                
                                                                                                                                ?>
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <a class="list" href="#" style="border: 1px solid orange;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo $msr['username']?></a>
                                                                                                                            <a class="list" href="#" style="border: 1px solid red;min-width: 70px;text-align: center;width:188px;font-size: 11px;height: 15px"><?php echo $msr['full_name']?></a>
                                                                                                                            <a class="list" href="#" style="border: 1px solid green;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px">
                                                                                                                                <?php 
                                                                                                                               
                                                                                                                                
                                                                                                                                 if($msr['grade_id']==1)
                                                                                                                                {
                                                                                                                                    if($msr['RF']==0)
                                                                                                                                    {
                                                                                                                                        echo  "DC".$msr['DC']." + PC".$msr['PC'];
                                                                                                                                    }else
                                                                                                                                    {
                                                                                                                                        echo  "DC".$msr['DC']." + PC".$msr['PC']." + RF".$msr['RF'];
                                                                                                                                    }
                                                                                                                                }else if($msr['grade_id']==2)
                                                                                                                                {
                                                                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                                                                    
                                                                                                                                }else if($msr['grade_id']==3)
                                                                                                                                {
                                                                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                                                                }
                                                                                                                                
                                                                                                                                ?>
                                                                                                                                </a>
                                                                                                                            <a class="list" href="#" style="border: 1px solid blue;min-width: 70px;text-align: center;width:150px;font-size: 11px;height: 15px"><?php echo "Joined ".$msr['user_registered_date']?></a>
                                                                                                                        </li>
                                                                                                                    <?php }?>
                                                                                                                </ul>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </li>
                                                                                                    <?php }?>
                                                                                                </ul>
                                                                                            <?php
                                                                                        }
                                                                                    ?>
                                                                                </li>
                                                                            <?php }?>
                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                            <?php } else {?>
                                                                    <?php if(\App\Session::get('error')) {?>
                                                                        <div class="alert alert-block alert-danger fade in" style="width: 1000px">
                                                                            <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                            <p>Invalid Username</p>
                                                                        </div>
                                                                        <?php
                                                                        (\App\Session::UnsetKeySession('error'));
                                                                    }?>

                                                                <?php }?>

<!--                                                                <div id="node-wrapper-13449776" class="">-->
<!---->
<!--                                                                    --><?php //foreach ($user->getAllSponsorUser(\App\Session::get('userID')) as $usr){?>
<!--                                                                        <div class="tree-controller-wrap">-->
<!--                                                                            <div class="controller-node-con">-->
<!--                                                                                <div class="tree-controller tree-controller-t-line">-->
<!--                                                                                    <div class="tree-controller-in tree-controller-t-right">&nbsp;</div>-->
<!--                                                                                </div><div class="node-info-raw" id="node-id-13449778"><div class="node-info">-->
<!--                                                                                        <span class="user-rank"><img src="http://client.msm4all.com/secure/app_html/image/network/rank/1000.png" width="32"></span>-->
<!--                                                                                        <span class="user-id" style="width:150px"><a style="color:#000000;" href="register.php">--><?php //echo $usr['username']?><!--</a></span>-->
<!--                                                                                        <span class="user-name" title="Md. Kafayet Hossain Tanvir" style="width:150px">--><?php //echo $usr['full_name']?><!--</span>-->
<!--                                                                                        <span class="user-activated">--><?php //echo $usr['packageID']?><!--</span>-->
<!--                                                                                        <span class="user-joined" style="width:150px">Joined --><?php //echo $usr['user_registered_date']?><!--</span>-->
<!--                                                                                    </div></div>-->
<!--                                                                            </div>-->
<!--                                                                            <div id="node-wrapper-13449778" class="">-->
<!---->
<!--                                                                            </div>-->
<!--                                                                        </div>-->
<!--                                                                        -->
<!--                                                                    --><?php //}?>

<!--                                                                    <div class="tree-controller-wrap">-->
<!--                                                                        <div class="controller-node-con">-->
<!--                                                                            <div class="tree-controller tree-controller-t-line">-->
<!--                                                                                <div class="tree-controller-in tree-controller-t-right">&nbsp;</div>-->
<!--                                                                            </div>-->
<!--                                                                            <div class="node-info-raw" id="node-id-13449777"><div class="node-info">-->
<!--                                                                                    <span class="user-rank"><img src="http://client.msm4all.com/secure/app_html/image/network/rank/1000.png" width="32"></span>-->
<!--                                                                                    <span class="user-id" style="width:150px"><a style="color:#000000;" href="register.php">Tanvir1</a></span>-->
<!--                                                                                    <span class="user-name" title="Md. Kafayet Hossain Tanvir" style="width:150px">Md. Kafayet Hossain Tanvir</span>-->
<!--                                                                                    <span class="user-activated">CMC5500</span>-->
<!--                                                                                    <span class="user-joined" style="width:150px">Joined 26/04/2017</span>-->
<!--                                                                                </div>-->
<!--                                                                            </div>-->
<!--                                                                        </div>-->
<!--                                                                        <div id="node-wrapper-13449777" class="">-->
<!---->
<!--                                                                        </div>-->
<!--                                                                    </div>-->
<!--                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

<script src="html_template/default/assets/js/jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
<?php include_once "includes/footer.php";?>

<style type="text/css">
    .clear{
        clear:both;
    }
    .tree-genealogy{

        height:700px;
        overflow:auto;
    }
    .controller-node-con{
        min-width:728px;
    }
    .tree-controller, .tree-controller-in{
        width:28px;
        height:37px;
    }
    .tree-controller-dash, .tree-controller-dashplus, .tree-controller-dashminus {
        margin-left:16px;
        width:12px;
        background:url(html_template/default/assets/img/network/gentree/images/leaf_h.png) repeat-x 0 18px;
        float:left;
    }
    .tree-controller-wrap, .tree-controller-l-wrap{
        margin-left:28px;
    }
    .tree-controller-wrap{
        background:url(html_template/default/assets/img/network/gentree/images/leaf.png) repeat-y 16px 0;
    }
    .tree-controller-tplus-line, .tree-controller-lplus-line, .tree-controller-tminus-line, .tree-controller-lminus-line, .tree-controller-t-line, .tree-controller-l-line{
        margin-left:16px;
        width:12px;
        float:left;
    }
    .tree-controller-lplus-line, .tree-controller-lminus-line, .tree-controller-l-line{
        background:url(html_template/default/assets/img/network/gentree/images/leaf.png) repeat-y 0 0;
        float:left;
        height:18px;
    }
    .tree-controller-lplus-right, .tree-controller-tplus-right, .tree-controller-tminus-right, .tree-controller-lminus-right, .tree-controller-l-right, .tree-controller-t-right{
        width:12px;
        background:url(html_template/default/assets/img/network/gentree/images/leaf_h.png) repeat-x 0 18px;
        float:left;
    }
    .tree-controller-dash img, .tree-controller-dashplus img, .tree-controller-dashminus img,
    .tree-controller-tminus-right img, .tree-controller-lminus-right img, .tree-controller-tplus-right img, .tree-controller-lplus-right img{
        margin-left:-4px;
    }
    img.tree-minus-button, img.tree-plus-button{
        margin-top:14px;
    }
    img.tree-minus-button:hover, img.tree-plus-button:hover{
        cursor:pointer;
    }
    .node-info-raw{
        height:37px;
        width:700px;
        overflow:hidden;
    }
</style>
<link rel="stylesheet" href="html_template/default/assets/img/network/gentree/gentree.css" type="text/css" />
<link rel="stylesheet" href="html_template/default/assets/img/network/legend.css" type="text/css" />


<script type="text/javascript">

//    $(document).ready(function(){
//        $('img.tree-minus-button').live('click', function(){
//            $(this).attr('class', 'tree-plus-button');
//            var nodeId = $(this).parent().parent().next().attr('id').replace(/^node\-id\-/, '');
//            $('#node-wrapper-'+nodeId).slideUp(200);
//            $(this).attr('src', 'http://client.msm4all.com/secure/app_html/image/network/gentree/images/plus.png');
//        });
//
//        $('img.tree-plus-button').live('click', function(){
//            $(this).attr('class', 'tree-minus-button');
//            var nodeId = $(this).parent().parent().next().attr('id').replace(/^node\-id\-/, '');
//            if($('#node-wrapper-'+nodeId).attr('class').match(/ajax\-more/)){
//                $('#node-wrapper-'+nodeId).removeClass('ajax-more');
//                ajaxLoadNode(nodeId);
//            }
//            $('#node-wrapper-'+nodeId).slideDown(200);
//            $(this).attr('src', 'http://client.msm4all.com/secure/app_html/image/network/gentree/images/minus.png');
//        });
//
//        function ajaxLoadNode(nodeId){
//            $('#node-wrapper-'+nodeId).html('<img src="http://client.msm4all.com/secure/app_html/image/network/gentree/images/spinner.gif">');
//            $.ajax({
//                url: '?ajTreeNodes=1&headUid='+nodeId,
//                type: 'post',
//                dataType: 'html',
//                error: function(){
//                    debug('error loading nodes for ' + nodeId);
//                },
//                success: function(data){
//                    $('#node-wrapper-'+nodeId).html(data);
//                },
//                complete: function(){
//                }
//            });
//        }
//
//        function debug(str){
//            alert(str);
//        }
//    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
//        $('input[name=headUid]').keypress(function(e){
//            if (e.which == 13) {
//                $('input[name=user_search]').click();
//                return false;
//            }
//        });
//        $('input[name=user_search]').click(function(){
//            $(this).attr('disabled', 'disabled');
//            window.location = '?headUid='+$('input[name=headUid]').val();
//        });
//        $('select[name=did]').change(function(){
//            var headUid = $(this).val();
//            window.location = '?headUid='+headUid;
//        });

      $('.sidebar').addClass('mini-menu');
       $('#main-content').addClass('margin-left-50');
    });
</script>
<?php }else {
    header('location:../login.php');
}?>