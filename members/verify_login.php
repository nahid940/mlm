<?php
include_once '../vendor/autoload.php';
\App\Session::init();
if(\App\Session::get('login')==true && \App\Session::get('page_approved')==false) {
    $user=new \App\user\User();
    $user->checkUserValidity(\App\Session::get('userID'));
    $client=new \App\client\Client();
    $lists=$client->getAllSpondoredClients(\App\Session::get('userID'));
    if(isset($_POST['sec_password']) && isset($_POST['submit_security']))
    {
        if($user->verifyUser(base64_encode($_POST['sec_password']))) {
            \App\Session::set('page_approved',true);
            header('location:/members/genealogy2.php');
        }
        
        
    }
?>

    <?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
        <!-- PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <!-- STYLER -->

                    <!-- /STYLER -->
                    <!-- BREADCRUMBS -->
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="members/index.php">Home</a>
                        </li>
                        <li>Security Verification</li>
                    </ul>
                    <!-- /BREADCRUMBS -->
                    <div class="clearfix">
                        <h3 class="content-title pull-left">Security Verification</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PAGE HEADER -->

              
        <div class='row'>
			<div class='col-md-12'>
				<div class='box border'>
					<div class='box-title'>
						<h4 style='height:15px;'></h4>
					</div>
				
    				<form method='post' action='' class='form-horizontal'>
    					<input type='hidden' name='__req' value='1' />
    					<!--<div class='box'>-->
    						<div class='box-body big'>
    							<div class='row' style='margin-bottom:10px;'>
    								<div class='col-md-12'>
    									<h4>Security Verification</h4>
    									<div class='form-group'>
    										<label class='col-md-3 control-label'>Security Password:</label> 
    										<div class='col-md-6'><input type='password' name='sec_password' class='form-control'/></div>
    									</div>
    									<br/>
    									<div class='form-group'>
    										<label class='col-md-3'></label> 
    										<div class='col-md-6'>
    											<input type='button' value='Back' onclick="window.location='members/clientlist.php';" class='btn btn-light_grey' />&nbsp;
    											<input type='submit' class='btn btn-success' value='Submit' name="submit_security"/>
    										</div>
    									</div>
    								</div>
    							</div>
    						</div>
    				</form>
            </div>
        </div>


    </div>

    <script src="html_template/default/assets/js/jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <?php include_once "includes/footer.php";?>

    <link rel="stylesheet" href="html_template/default/assets/img/network/gentree/gentree.css" type="text/css" />
    <link rel="stylesheet" href="html_template/default/assets/img/network/legend.css" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function(){
            $('.stats-node img').hover(function(){
                    var rel = $(this).attr('rel');

                    $('#tooltip_'+rel).show();
                },
                function(){
                    var rel = $(this).attr('rel');
                    $('#tooltip_'+rel).hide();
                });
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('input[name=headUid]').keypress(function(e){
                if (e.which == 13) {
                    $('input[name=user_search]').click();
                    return false;
                }
            });

            $('input[name=user_search]').click(function(){
                $(this).attr('disabled', 'disabled');
                window.location = 'members/genealogy2.php?view=network&headUid='+$('input[name=headUid]').val();
            });

            $('select[name=did]').change(function(){
                var headUid = $(this).val();
                window.location = '?headUid='+headUid;
            });

            // $('.sidebar').addClass('mini-menu');
            // $('#main-content').addClass('margin-left-50');
        });
    </script>

<?php }else {
    header('location:../login.php');
}?>