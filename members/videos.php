<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));


$sql="select count(videoID) as total from videos";
$stmt1=\App\DBConnection::myQuery($sql);
$stmt1->bindValue(':userID',\App\Session::get('userID'));
$stmt1->execute();
$totalRow=$stmt1->fetch(PDO::FETCH_ASSOC);
$perpage= $totalRow['total']/12;


if(isset($_GET['nav'])){
    $pageNumber=$_GET['nav'];
}else{
    $pageNumber=0;
}


if($pageNumber==0 || $pageNumber==1){
    $page1=0;
}else{
    $page1=($pageNumber*12)-12;
}


$sql="select * from videos order by videoID desc limit $page1,12";
$stmt=\App\DBConnection::myQuery($sql);
$stmt->execute();
$lists=$stmt->fetchAll(PDO::FETCH_ASSOC);

?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>Videos</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Videos</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->

    <div class='row'>
        <div class='col-md-12'>
            <div class='box'>
                <div class='box-title'>
                    <h4><a href='http://i.youku.com/u/UMzI2NzcxNjU3Mg==' target='_blank'>MONSPACE</a></h4>
                </div>
                <div class='box-body clearfix'>
<!--                    <div class='nav_link'><a href="videos.php?nav=1">First</a> <a href="videos.php?nav=1">1</a> <a href="videos.php?nav=2">2</a> <a href="videos.php?nav=3">3</a> <a href="videos.php?nav=4">4</a> <a href="videos.php?nav=5">5</a> <a href="videos.php?nav=6">6</a> <div class='nav_no_more' style='display:inline;'>...</div><a href="videos.php?nav=2">Next</a> <a href="videos.php?nav=19">Last</a> </div><div style="clear:both;"></div>-->

                    <?php if(!empty($lists)){?>
                        <div class='nav_link'>
                            <a href="members/videos.php?__req=1&type=s&nav=1">First</a>
                            <?php if(isset($_GET['nav']) && $_GET['nav']>1) { ?>
                                <a href="members/videos.php?__req=1&type=v&nav=<?php echo ($_GET['nav']-1)?>">Previous</a>
                            <?php }?>
                            <?php for($x=1;$x<=ceil($perpage);$x++){?>
                                <a href="members/videos.php?__req=1&type=v&nav=<?php echo $x?>"  style='color:red;'><?php echo $x?></a>
                            <?php }?>
                            <a href="members/videos.php?__req=1&type=v&nav=<?php echo (isset($_GET['nav'])?$_GET['nav']+1:1)?>">Next</a>
                            <a href="members/videos.php?__req=1&type=v&nav=<?php echo $totalRow['total']?>">Last</a>
                        </div>
                    <?php }?>
                    <div class='row'>
                        <?php foreach ($lists as $list){?>
                        <div class='col-md-4 item' style="height:265px">
                            <div class='filter-content'>
                                <a href='<?php echo $list['video_link']?>' target='_blank'>
                                    <img src='http://secure.bmsm4all.com/thumbnail/<?php echo $list['thumbnail_image']?>' alt='《彩云星意》你的满星云 由你开始 第219集 20181101' class='img-responsive' style="height: 200px" /></a>
                                <br/>
                                <p style='font-weight:bold; font-size:14px;'><a href='<?php echo $list['video_link']?>' target='_blank'><?php echo $list['video_title']?></a></p>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>
