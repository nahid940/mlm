<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
?>
<?php include_once "includes/header.php";?>

<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>MSDollar</li>
                    <li>MSD Trading</li>

                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">MSD Trading</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->


    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'>1.6452</div>
                <div>MSD Price</div>
            </a>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li ><a href='members/msd_trading_history.php'>MSD Trading History</a></li>
                            <li class='active'><a href='members/msd_trading_form.php'>MSD Trading</a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <br/><br/>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div id='chartdiv' style='height:200px;width:100%;'></div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <br/><br/>
                                        <div class='alert alert-block alert-info fade in'>
                                            <p>【Instructions for MSD internal trading of member and yxgd】<br/><br/><strong>First, transfer the amount of MSD to be traded to this wallet address using mymsdspace system: 0x702a806f9582c17514ca922672dfb09afb617a98</strong><br/><br/>Then login to your account to complete the transaction.<br/>1. Fill in the MSD Selling Price<br/>2. Fill in the amount of MSD (minimum : 100, maximum : 2000)<br/>3. Fill in your MSD wallet address<br/><br/>Special Reminder!<br/>★Please make sure that the correct wallet address is filled in before you complete the transaction as cancellations are not allowed.<br/>★The company will not be responsible for any wrong transactions on your end. Please be sure to double check all details before completing the sell order.
                                            </p>
                                        </div>

                                        <form method='post' action='msd_trading_form.php' class='form-horizontal'>
                                            <input type='hidden' name='__req' value='1' />
                                            <div class='row' style='margin-bottom:10px;'>
                                                <div class='col-lg-12'>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>MSD Selling Price:<span class='require'>*</span></label>
                                                        <div class='col-md-7'>
                                                            <input type='number' name='price' value='1.8097' class='form-control' min='0' step='0.0001'/>
                                                        </div>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Amount of MSD:<span class='require'>*</span></label>
                                                        <div class='col-md-7'>
                                                            <input type='number' name='amount' value='' class='form-control' min='0' step='1'>
                                                        </div>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label class='col-md-3 control-label'>Your MSD Wallet Address:<span class='require'>*</span></label>
                                                        <div class='col-md-7'>
                                                            <input type='text' name='trans_id' value='' class='form-control' />
                                                        </div>
                                                    </div>
                                                    <div class='form-group' style=''>
                                                        <label class='col-md-3 control-label'></label>
                                                        <div class='col-md-7'>
                                                            <input type='submit' name='add_btn' value='Submit' class='btn btn-success btn-outlined' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>

<?php include_once "includes/footer.php";?>

    <script>
        jQuery(document).ready(function() {
            App.setPage("widgets_box");  //Set current page
            App.setPage("elements");
            App.init(); //Initialise plugins and elements
        });
    </script>

    <link rel="stylesheet" href='http://client.msm4all.com/secure/app_html/js/amcharts/style.css' type="text/css">
    <script type='text/javascript' src='http://client.msm4all.com/secure/app_html/js/amcharts/amcharts.js'></script>
    <script type='text/javascript' src='http://client.msm4all.com/secure/app_html/js/amcharts/serial.js'></script>

    <script>
        $(document).ready(function() {
        });

        AmCharts.loadJSON = function(url) {
            // create the request
            if(window.XMLHttpRequest) {
                // IE7+, Firefox, Chrome, Opera, Safari
                var request = new XMLHttpRequest();
            }
            else{
                // code for IE6, IE5
                var request = new ActiveXObject('Microsoft.XMLHTTP');
            }

            request.open('GET', url, false);
            request.send();

            // parse adn return the output
            return eval(request.responseText);
        };

        var chart;

        AmCharts.ready(function() {
            var chartData = AmCharts.loadJSON('get_chart_price.php');

            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "http://yxgd.msm4all.com/secure/app_html/js/amcharts/images/";
            chart.dataProvider = chartData;
            chart.categoryField = "date";
            chart.dataDateFormat = "YYYY-MM-DD HH:NN:SS";

            // GRAPHS
            var graph1 = new AmCharts.AmGraph();
            graph1.valueField = "value1";

            graph1.bullet = "round";
            graph1.bulletColor = "#ec008b";
            graph1.bulletBorderColor = "#ec008b";
            graph1.bulletBorderThickness = 2;
            graph1.lineThickness = 2;
            graph1.lineAlpha = 0.5;
            graph1.balloonText = "<b>[[value]]</b>";
            chart.addGraph(graph1);

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.precision = 4;
            chart.addValueAxis(valueAxis);

            // CATEGORY AXIS
            chart.categoryAxis.parseDates = false;
            chart.precision = 4;

            // WRITE
            chart.write("chartdiv");


        });

    </script>


    <!-- /JAVASCRIPTS -->
<?php }else {
    header('location:../login.php');
}?>