<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$user=new \App\user\User();

$helper=new \App\Helper();
$helper->checkTime();

$sql="select coverImage,url_link,status from cover_images limit 1";
$stmt=\App\DBConnection::myQuery($sql);
$stmt->execute();
$image=$stmt->fetch(PDO::FETCH_ASSOC);


$sql="select title,news from news where type=1 order by newsID desc limit 1";
$stmt=\App\DBConnection::myQuery($sql);
$stmt->execute();
$news=$stmt->fetch(PDO::FETCH_ASSOC);


if(\App\Session::get('login')==true) {
   $user->checkUserValidity(\App\Session::get('userID'));
    $info=$user->getUserInfo();
    \App\Session::UnsetKeySession('code');
?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>

                    <li>Member's Home</li>

                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Member's Home</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->


    <?php if($image['status']==1){?>
        <div class='panel' >
            <div class='panel-body' style ='text-align: center;'>
                <a href='http://<?php echo $image['url_link']?>' target='_blank'><img src='http://secure.bmsm4all.com/cover/<?php  echo $image['coverImage']?>' alt='special notice' border='0' width='100%' /></a>
            </div>
        </div>
    <?php }?>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border inverse'>
                <div class='box-title'>
                    <h4><i class='fa fa-arrows-h'></i>Profile</h4>
                </div>
                <div class='box-body'>
                    <div class='form-horizontal '>

                        <div class='form-group'>
                            <label class='col-md-3 control-label'>
                                Username:
                            </label>
                            <div class='col-md-9'>
                                <?php echo $info['username']?>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-3 control-label'>
                                Nickname:
                            </label>
                            <div class='col-md-9'>
                                <?php echo $info['nick_name']?>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-3 control-label'>
                                Email:
                            </label>
                            <div class='col-md-9'>
                                <?php echo $info['email']?>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-3 control-label'>
                                Joined Date:
                            </label>
                            <div class='col-md-9'>
                                <?php echo date('d-M-Y',strtotime($info['user_registered_date']))?>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-3 control-label'>
                                Package:
                            </label>
                            <div class='col-md-9'>
                                 <?php
                                 
                                //  getGradeAmount
                                
                                if($info['grade_id']==1)
                                {
                                    if($info['RF']==0)
                                    {
                                        echo  "DC".$info['DC']." + PC".$info['PC'];
                                    }else
                                    {
                                        echo  "DC".$info['DC']." + PC".$info['PC']." + RF".$info['RF'];
                                    }
                                }else 
                                {
                                    $data=$user->getGradeAmount($info['grade_id']);
                                    echo "DC".$data['requiredDC']." + PC".$data['requiredPC'];
                                }
                                 
                                 
                                

                                ?>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='col-md-3 control-label'>
                                Expired Date:
                            </label>
                            <div class='col-md-9'>
                                <?php echo  date('d-M-Y',strtotime($info['expire_date']))  ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='box border'>
        <div class='box-title'>
            <h4>Latest Video</h4>
            <div class='tools'>
                <a href='members/videos.php' style='color:#ec008c;'>More Videos <i class='fa fa-plus-square'></i></a>
            </div>
        </div>
        <div class='box-body'>
            <?php
                $sql="select thumbnail_image,video_link,video_title from videos order by videoID desc limit 1";
                $stmt=\App\DBConnection::myQuery($sql);
                $stmt->execute();
                $res=$stmt->fetch(PDO::FETCH_ASSOC);
            ?>
            <p>
                <a href='<?php echo $res['video_link']?>' target='_blank'>
                    <img src='http://secure.bmsm4all.com/thumbnail/<?php echo $res['thumbnail_image']?>' alt='Thumbnail Image' width='500' />
                </a>
            </p>
            <p><a href="<?php echo $res['video_link']?>" target='_blank'><?php echo $res['video_title']?></a></p>
        </div>
    </div>
    <div class='panel'>
        <div class='panel-body'>
            <div class='tab-content'>
                <h4><?php
                    echo $news['title'];
                    ?></h4>
                <hr/>
                <div class='tab-pane fade active in indexpagenews'>
                    <?php
                        echo $news['news'];
                    ?>
                </div>
                <br/>
            </div>
        </div>
    </div>

</div>

<?php include_once "includes/footer.php";?>

<div id='content0' style='display: none;'>
    <img src='html_template/default/assets/img/logo/logo1.png' width='100%' alt='logo'/>

    <h2><?php echo $news['title']?></h2>
    <hr>
    <?php echo $news['news'];?>
</div>

<?php } else {
    header('location:../login.php');
}?>
