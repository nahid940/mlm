<?php
include_once '../vendor/autoload.php';
\App\Session::init();
\App\Session::clientListSession();
if(\App\Session::get('login')==true) {
    $user=new \App\user\User();
    $user->checkUserValidity(\App\Session::get('userID'));
    $client=new \App\client\Client();
    
    if(isset($_GET['headUid']))
    {  
      
    //   if($client->getUserIDbyUserName($_GET['headUid'],\App\Session::get('userID'))==true)
    //   {
            $userInfo=$client->getUserInfo($_GET['headUid']);
            
            if(empty($userInfo))
            {
                \App\Session::set('error','Invalid Username');
            }  
    //   }
    //   else
    //   {
    //       \App\Session::set('error','This username is not in your network');
    //   }
        
        
    }else
    {
        $userInfo=$client->getUserInfo(\App\Session::get('username'));
    }
    
    
    $usergrade2=$user->getGradeAmount(2);
    $usergrade3=$user->getGradeAmount(3);

    ?>

    <?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
        <!-- PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <!-- STYLER -->

                    <!-- /STYLER -->
                    <!-- BREADCRUMBS -->
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="members/index.php">Home</a>
                        </li>
                        <li>Placement View</li>
                    </ul>
                    <!-- /BREADCRUMBS -->
                    <div class="clearfix">
                        <h3 class="content-title pull-left">Placement View</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PAGE HEADER -->


        <div class='row'>
            <div class='col-md-12'>
                <div class='box border'>
                    <div class='box-title'>
                        <h4 style='height:15px;'></h4>
                    </div>
                    <div class='box-body'>
                        <div class='tabbable header-tabs user-profile'>
                            <ul class='nav nav-tabs'>
                                <li class='active'><a href='members/genealogy2.php'>Placement</a></li>

                            </ul>
                            <div class='tab-content' style='height: 1000px;'>
                                <div class='tab-pane fade in active'>
                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <div class='box border blue'>
                                                <div class='box-body' style='margin-top:10px;'>
                                                    <form class='form-horizontal' action="#" method='get'>
                                                        <input type='hidden' name='__req' value='1'>
                                                        <input type='hidden' name='type' value=''>
                                                        <input type='hidden' name='nav' value=''>
                                                        <input type='hidden' name='view' value=''>
                                                        <div class='form-group'>
                                                            <label class='col-md-2 control-label'>Username</label>
                                                            <div class='col-md-3'>
                                                                <?php if(isset($_GET['headUid']) && $_GET['headUid']!=''){?>
                                                                    <input class='form-control' type='text' name='headUid' value='<?php echo $_GET['headUid']?>'>
                                                                <?php } else {?>
                                                                    <input class='form-control' type='text' name='headUid' value=''>
                                                                <?php }?>
                                                            </div>

                                                            <div class='col-md-3'>
                                                                <input type='submit' name='user_search' value='Generate' class='btn btn-success'>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <p class='alert alert-info'>
                                                L / R Sales shown in the Placement View includes Leveling Bonus, hence it is not calculated in the Pairing Bonus
                                            </p>
                                            <center style='padding-left:200px;'>
                                                <table>
                                                    <tr>
                                                        <th width='200'>Most Left</th>
                                                        <th width='200'>Most Right</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <?php
                                                             $sql="select user_id,client_level from client_positions where parent_id=:parent_id and hand=0 order by client_level desc limit 1";
 
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':parent_id',$userInfo['userID']);
                                                            $stmt->execute();
															$userdetails=$stmt->fetch(PDO::FETCH_ASSOC);
															
															
															$sql="select username,full_name,nick_name from users where userID=:userID";
															$stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':userID',$userdetails['user_id']);
                                                            $stmt->execute();
															$left=$stmt->fetch(PDO::FETCH_ASSOC);
															
                                                            ?>

                                                            Username: <?php echo $left['username']?> <br/>
                                                            Name: <?php echo $left['nick_name']?> <br/>
                                                            Level: <?php
                                                            $level=($userdetails['client_level']==0?0:$userdetails['client_level'])-$userInfo['client_level'];
                                                            if($level<=0)
                                                            {
                                                                echo 0;
                                                            }else
                                                            {
                                                                echo $level;
                                                            }

                                                            ?>

                                                        </td>
                                                        <td>
                                                            <?php
                                                            
                                                            
                                                            $sql="select user_id,client_level from client_positions where parent_id=:parent_id and hand=1 order by client_level desc limit 1";
 
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':parent_id',$userInfo['userID']);
                                                            $stmt->execute();
															$userdetails=$stmt->fetch(PDO::FETCH_ASSOC);
															
															
															$sql="select username,full_name,nick_name from users where userID=:userID";
															$stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':userID',$userdetails['user_id']);
                                                            $stmt->execute();
															$right=$stmt->fetch(PDO::FETCH_ASSOC);
															
                                                            ?>

                                                            Username: <?php echo $right['username']?> <br/>
                                                            Name: <?php echo $right['nick_name']?> <br/>
                                                            Level: <?php
                                                            $level=($userdetails['client_level']==0?0:$userdetails['client_level'])-$userInfo['client_level'];
                                                            if($level<=0)
                                                            {
                                                                echo 0;
                                                            }else
                                                            {
                                                                echo $level;
                                                            }
                                                            
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </center>

                                            <?php if(\App\Session::get('error')) {?>
                                                <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px;margin-top:-20px;min-height:800px;">
                                                    <div class="col-md-12 profile-details" style="padding-left:100px;padding-bottom:20px;margin-left:140px;">
                                                       
                                                       <br/>
                                                        <div style='width:1000px;margin-left:-100px;height:0px;'>
                                                            <a style='float:left;' name='left'>&nbsp;</a>
                                                            <a style='float:right;' name='right'>&nbsp;</a>
                                                        </div>
                                                        <br/>
                                                        <br/>
                                                        <div class="alert alert-block alert-danger fade in">
                                                            <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                            <p>Invalid Username</p>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <?php
                                                \App\Session::UnsetKeySession('error');
                                            }?>

                                            <div class='row' style='overflow-x:auto;margin-left:0px;margin-right:0px;margin-top:-20px;min-height:800px;'>
                                                <div class='col-md-12 profile-details' style='padding-left:100px;padding-bottom:20px;margin-left:140px;'>
                                                    
                                                     <br/>
                                                    <div style='width:1000px;margin-left:-100px;height:0px;'>
                                                        <a style='float:left;' name='left'>&nbsp;</a>
                                                        <a style='float:right;' name='right'>&nbsp;</a>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div style="width: 1000px;">
                                                        <div class='stats-node' style='width: 75px; margin-left: 390px; text-align:center; float:left;'>
                                                            <?php if (isset($_GET['headUid']) && ($_GET['headUid']!='')  && ($_GET['headUid']!=\App\Session::get('username'))){?>
                                                                <div class="network-top-more-node"><a href="members/genealogy2.php?view=network&headUid=<?php echo $client->getNearestParent($_GET['headUid'])?>"></a></div>
                                                            <?php }?>
                                                            <a href='members/genealogy2.php?view=network&headUid=<?php echo $userInfo['username']?>'>
                                                                <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                    <table cellspacing='0' cellpadding='0' style='line-height:13px;margin-left:7px;'>
                                                                        <tr><td width='135'>Username: <?php echo $userInfo['username']?></td></tr>
                                                                        <tr><td width='135'>Name: <?php echo $userInfo['nick_name']?></td></tr>
                                                                        <tr><td width='135'>Joined: <?php echo $userInfo['user_registered_date']?></td></tr>
                                                                        <tr><td width='135'>Expired Date: <?php echo $userInfo['expire_date']?></td></tr>
                                                                        <tr><td width='135'>Package: <?php
                                                                        
                                                                                if($userInfo['grade_id']==1)
                                                                                {
                                                                                    if($userInfo['RF']==0)
                                                                                    {
                                                                                        echo  "DC".$userInfo['DC']." + PC".$userInfo['PC'];
                                                                                    }else
                                                                                    {
                                                                                        echo  "DC".$userInfo['DC']." + PC".$userInfo['PC']." + RF".$userInfo['RF'];
                                                                                    }
                                                                                }else if($userInfo['grade_id']==2)
                                                                                {
                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                    
                                                                                }else if($userInfo['grade_id']==3)
                                                                                {
                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                }
                                                                                
                                                                                ?></td></tr>
                                                                        <tr><td width='135'>L / R Sales: <?php echo $client->countTotalLeftUser($userInfo['userID'])?> / <?php echo $client->countTotalRightUser($userInfo['userID'])?></td></tr>
                                                                    </table>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div style='clear:both;'></div>
                                                        <div style='width: 535px; margin-left: 197px; float:left; height: 12px;'>
                                                            <div class='stats-node-line-up stats-node-line' style='width: 2px; overflow-x: hidden; margin-left: 267px; height: 10px;'></div>
                                                            <div class='stats-node-line-side stats-node-line' style='width: 537px; margin-left:-1px; overflow-y: hidden; height: 2px;'></div>
                                                        </div>
                                                        <div style='clear:both;'></div>
                                                        <div class='stats-node-line-up stats-node-line' style='width: 2px; overflow-x: hidden; margin-left: 196px; float:left; height: 10px;'></div>
                                                        <div class='stats-node-line-up stats-node-line' style='width: 2px; overflow-x: hidden; margin-left: 533px; float:left; height: 10px;'></div>
                                                        <div style='clear:both;'></div>

                                                        <?php
                                                        $leftGenealogy=$client->leftUsersGenealogy($userInfo['userID']);
                                                        $rightGenealogy=$client->rightUsersGenealogy($userInfo['userID']);
                                                        ?>

                                                        <!-----------------------left side------------------------------------------------------->
                                                        <?php if(!empty($leftGenealogy)){?>
                                                            <div class='stats-node' style='width: 75px; margin-left: 122px; text-align:center; float:left;'>
                                                                <a href='members/genealogy2.php?view=network&headUid=<?php echo $leftGenealogy['username']?>'>
                                                                    <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                        <table cellspacing='0' cellpadding='0' style='line-height:13px;margin-left:7px;'>
                                                                            <tr><td width='135'>Username: <?php echo $leftGenealogy['username']?></td></tr>
                                                                            <tr><td width='135'>Name: <?php echo $leftGenealogy['nick_name']?></td></tr>
                                                                            <tr><td width='135'>Joined: <?php echo $leftGenealogy['user_registered_date']?></td></tr>
                                                                            <tr><td width='135'>Expired Date: <?php echo $leftGenealogy['expire_date']?></td></tr>
                                                                            <tr><td width='135'>Package: <?php
                                                                            
                                                                            
                                                                                    // if($leftGenealogy['RF']==0)
                                                                                    // {
                                                                                    //     echo  "DC".$leftGenealogy['DC']." + PC".$leftGenealogy['PC'];
                                                                                    // }else
                                                                                    // {
                                                                                    //   echo "DC".$leftGenealogy['DC']." + PC".$leftGenealogy['PC']." + RF".$leftGenealogy['RF'];
                                                                                    // }
                                                                                    
                                                                                    
                                                                                    
                                                                                if($leftGenealogy['grade_id']==1)
                                                                                {
                                                                                    if($leftGenealogy['RF']==0)
                                                                                    {
                                                                                        echo  "DC".$leftGenealogy['DC']." + PC".$leftGenealogy['PC'];
                                                                                    }else
                                                                                    {
                                                                                        echo  "DC".$leftGenealogy['DC']." + PC".$leftGenealogy['PC']." + RF".$leftGenealogy['RF'];
                                                                                    }
                                                                                }else if($leftGenealogy['grade_id']==2)
                                                                                {
                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                    
                                                                                }else if($leftGenealogy['grade_id']==3)
                                                                                {
                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                }
                                                                                    //$leftGenealogy['packageName']?></td></tr>
                                                                            <tr><td width='135'>L / R Sales: <?php echo $client->countTotalLeftUser($leftGenealogy['userID'])?> / <?php echo $client->countTotalRightUser($leftGenealogy['userID'])?></td></tr>
                                                                        </table>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                            <div style="width: 0px; margin-left: -77px; float:left; height: 12px;margin-top: 95px">
                                                                <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: 75px; height: 10px;"></div>
                                                                <div class="stats-node-line-side stats-node-line" style="width: 350px; margin-left:-100px; overflow-y: hidden; height: 2px;"></div>
                                                            </div>
                                                            <?php
                                                            $leftGenealogy0=$client->leftUsersGenealogy($leftGenealogy['userID']);
                                                            $rightGenealogy0=$client->rightUsersGenealogy($leftGenealogy['userID']);
                                                            ?>
                                                            <?php if(!empty($leftGenealogy0)){?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: -100px; height: 14px;"></div>
                                                                </div>
                                                                <div class='stats-node' style='width: 75px; margin-left: -256px; text-align:center; float:left; margin-top: 120px'>
                                                                    <a href='members/genealogy2.php?view=network&headUid=<?php echo $leftGenealogy0['username']?>'>
                                                                        <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                            <table cellspacing='0' cellpadding='0' style='line-height:13px;margin-left:7px;'>
                                                                                <tr><td width='135'>Username: <?php echo $leftGenealogy0['username']?></td></tr>
                                                                                <tr><td width='135'>Name: <?php echo $leftGenealogy0['nick_name']?></td></tr>
                                                                                <tr><td width='135'>Joined: <?php echo $leftGenealogy0['user_registered_date']?></td></tr>
                                                                                <tr><td width='135'>Expired Date: <?php echo $leftGenealogy0['expire_date']?></td></tr>
                                                                                <tr><td width='135'>Package: <?php
                                                                                       
                                                                                        //$leftGenealogy0['packageName']
                                                                                        
                                                                                        
                                                                                if($leftGenealogy0['grade_id']==1)
                                                                                {
                                                                                    // if($userInfo['RF']==0)
                                                                                    // {
                                                                                    //     echo  "DC".$leftGenealogy['DC']." + PC".$leftGenealogy['PC'];
                                                                                    // }else
                                                                                    // {
                                                                                    //     echo  "DC".$leftGenealogy['DC']." + PC".$leftGenealogy['PC']." + RF".$leftGenealogy['RF'];
                                                                                    // }
                                                                                    
                                                                                 if($leftGenealogy0['RF']==0)
                                                                                    {
                                                                                        echo  "DC".$leftGenealogy0['DC']." + PC".$leftGenealogy0['PC'];
                                                                                    }else
                                                                                    {
                                                                                        echo "DC".$leftGenealogy['DC']." + PC".$leftGenealogy['PC']." + RF".$leftGenealogy['RF'];
                                                                                    }
                                                                                }else if($leftGenealogy0['grade_id']==2)
                                                                                {
                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                    
                                                                                }else if($leftGenealogy0['grade_id']==3)
                                                                                {
                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                }
                                                                                        
                                                                                        
                                                                                        
                                                                                        ?></td></tr>
                                                                                <tr><td width='135'>L / R Sales: <?php echo $client->countTotalLeftUser($leftGenealogy0['userID'])?> / <?php echo $client->countTotalRightUser($leftGenealogy0['userID'])?></td></tr>
                                                                            </table>
                                                                        </div>
                                                                    </a>
                                                                    <div class='network-bottom-more-node'><a href='members/genealogy2.php?view=network&headUid=<?php echo $leftGenealogy0['username']?>'></a></div>
                                                                </div>
                                                            <?php }else{ ?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: -100px; height: 14px;"></div>
                                                                </div>
                                                                <div  style='width: 75px; margin-left: -256px; text-align:center; float:left; margin-top: 120px'>
                                                                    <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                        <center>
                                                                            <br/><br/>
                                                                            <a href='members/register.php?placement_type=m&position1=0&upline1=<?php echo $leftGenealogy['username']?>'>Sign Up</a>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            <?php }?>

                                                            <?php if(!empty($rightGenealogy0)){?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: 248px; height: 14px;"></div>
                                                                </div>
                                                                <div class='stats-node' style='width: 75px; margin-left: 94px; text-align:center; float:left; margin-top: 120px'>
                                                                    <a href='members/genealogy2.php?view=network&headUid=<?php echo $rightGenealogy0['username']?>'>
                                                                        <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                            <table cellspacing='0' cellpadding='0' style='line-height:13px;margin-left:7px;'>
                                                                                <tr><td width='135'>Username: <?php echo $rightGenealogy0['username']?></td></tr>
                                                                                <tr><td width='135'>Name: <?php echo $rightGenealogy0['nick_name']?></td></tr>
                                                                                <tr><td width='135'>Joined: <?php echo $rightGenealogy0['user_registered_date']?></td></tr>
                                                                                <tr><td width='135'>Expired Date: <?php echo $rightGenealogy0['expire_date']?></td></tr>
                                                                                <tr><td width='135'>Package: <?php
                                                                                        
                                                                                        
                                                                                        
                                                                                if($rightGenealogy0['grade_id']==1)
                                                                                {
                                                                                    if($rightGenealogy0['RF']==0)
                                                                                    {
                                                                                        echo  "DC".$rightGenealogy0['DC']." + PC".$rightGenealogy0['PC'];
                                                                                    }else
                                                                                    {
                                                                                         echo "DC".$rightGenealogy0['DC']." + PC".$rightGenealogy0['PC']." + RF".$rightGenealogy0['RF'];
                                                                                    }
                                                                                }else if($rightGenealogy0['grade_id']==2)
                                                                                {
                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                    
                                                                                }else if($rightGenealogy0['grade_id']==3)
                                                                                {
                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                }
                                                                                       
                                                                                        
                                                                                        
                                                                                        //$rightGenealogy0['packageName']?></td></tr>
                                                                                <tr><td width='135'>L / R Sales: <?php echo $client->countTotalLeftUser($rightGenealogy0['userID'])?> / <?php echo $client->countTotalRightUser($rightGenealogy0['userID'])?></td></tr>
                                                                            </table>
                                                                        </div>
                                                                    </a>
                                                                    <div class='network-bottom-more-node'><a href='members/genealogy2.php?view=network&headUid=<?php echo $rightGenealogy0['username']?>'></a></div>
                                                                </div>
                                                            <?php }else {?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: 248px; height: 14px;"></div>
                                                                </div>
                                                                <div  style='width: 75px; margin-left: 95px; text-align:center; float:left; margin-top: 120px'>
                                                                    <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                        <center>
                                                                            <br/><br/>
                                                                            <a href='members/register.php?placement_type=m&position1=1&upline1=<?php echo $leftGenealogy['username']?>'>Sign Up</a>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            <?php }?>

                                                        <?php } else {?>
                                                            <div  style='width: 75px; margin-left: 118px; text-align:center; float:left; margin-right:174px ; margin-top: 0px'>
                                                                <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                    <center>
                                                                        <br/><br/>
                                                                        <a href='members/register.php?placement_type=m&position1=0&upline1=<?php echo $userInfo['username']?>'>Sign Up</a>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        <?php }?>

                                                        <!-------------------------------- Right side---------------------------------------------------------------------------->

                                                        <?php if(!empty($rightGenealogy)) {?>
                                                            <div class='stats-node' style='width: 75px; margin-left: 292px; text-align:center; float:left;'>
                                                                <a href='members/genealogy2.php?view=network&headUid=<?php echo $rightGenealogy['username']?>'>
                                                                    <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                        <table cellspacing='0' cellpadding='0' style='line-height:13px;margin-left:7px;'>
                                                                            <tr><td width='135'>Username: <?php echo $rightGenealogy['username']?></td></tr>
                                                                            <tr><td width='135'>Name: <?php echo $rightGenealogy['nick_name']?></td></tr>
                                                                            <tr><td width='135'>Joined: <?php echo $rightGenealogy['user_registered_date']?></td></tr>
                                                                            <tr><td width='135'>Expired Date: <?php echo $rightGenealogy['expire_date']?></td></tr>
                                                                            <tr><td width='135'>Package: <?php
                                                                                   
                                                                                             
                                                                                if($rightGenealogy['grade_id']==1)
                                                                                {
                                                                                     if($rightGenealogy['RF']==0)
                                                                                    {
                                                                                        echo  "DC".$rightGenealogy['DC']." + PC".$rightGenealogy['PC'];
                                                                                    }else
                                                                                    {
                                                                                        echo "DC".$rightGenealogy['DC']." + PC".$rightGenealogy['PC']." + RF".$rightGenealogy['RF'];
                                                                                    }
                                                                                }else if($rightGenealogy['grade_id']==2)
                                                                                {
                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                    
                                                                                }else if($rightGenealogy['grade_id']==3)
                                                                                {
                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                }
                                                                                    
                                                                                    

                                                                                    //$rightGenealogy['packageName']?></td></tr>
                                                                            <tr><td width='135'>L / R Sales: <?php echo $client->countTotalLeftUser($rightGenealogy['userID'])?> / <?php echo $client->countTotalRightUser($rightGenealogy['userID'])?></td></tr>
                                                                        </table>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                            <div style="width: 0px; margin-left: -77px; float:left; height: 12px;margin-top: 95px">
                                                                <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: 75px; height: 10px;"></div>
                                                                <div class="stats-node-line-side stats-node-line" style="width: 350px; margin-left:-100px; overflow-y: hidden; height: 2px;"></div>
                                                            </div>
                                                            <?php

                                                            $leftGenealogy1=$client->leftUsersGenealogy($rightGenealogy['userID']);
                                                            $rightGenealogy1=$client->rightUsersGenealogy($rightGenealogy['userID']);

                                                            ?>
                                                            <?php if(!empty($leftGenealogy1)){?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: -100px; height: 14px;"></div>
                                                                </div>
                                                                <div class='stats-node' style='width: 75px; margin-left: -256px; text-align:center; float:left; margin-top: 120px'>
                                                                    <a href='members/genealogy2.php?view=network&headUid=<?php echo $leftGenealogy1['username']?>'>
                                                                        <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                            <table cellspacing='0' cellpadding='0' style='line-height:13px;margin-left:7px;'>
                                                                                <tr><td width='135'>Username: <?php echo $leftGenealogy1['username']?></td></tr>
                                                                                <tr><td width='135'>Name: <?php echo $leftGenealogy1['nick_name']?></td></tr>
                                                                                <tr><td width='135'>Joined: <?php echo $leftGenealogy1['user_registered_date']?></td></tr>
                                                                                <tr><td width='135'>Expired Date: <?php echo $leftGenealogy1['expire_date']?></td></tr>
                                                                                <tr><td width='135'>Package: <?php
                                                                                        
                                                                               if($leftGenealogy1['grade_id']==1)
                                                                                {
                                                                                     if($leftGenealogy1['RF']==0)
                                                                                        {
                                                                                            echo  "DC".$leftGenealogy1['DC']." + PC".$leftGenealogy1['PC'];
                                                                                        }else
                                                                                        {
                                                                                            //echo  "RF".$leftGenealogy1['RF']." + DC".$leftGenealogy1['DC']." + PC".$leftGenealogy1['PC'];
                                                                                            echo "DC".$leftGenealogy1['DC']." + PC".$leftGenealogy1['PC']." + RF".$leftGenealogy1['RF'];
                                                                                            
                                                                                        }
                                                                                }else if($leftGenealogy1['grade_id']==2)
                                                                                {
                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                    
                                                                                }else if($leftGenealogy1['grade_id']==3)
                                                                                {
                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                }

                                                                                        //$leftGenealogy1['packageName']?></td></tr>
                                                                                <tr><td width='135'>L / R Sales: <?php echo $client->countTotalLeftUser($leftGenealogy1['userID'])?> / <?php echo $client->countTotalRightUser($leftGenealogy1['userID'])?></td></tr>
                                                                            </table>
                                                                        </div>
                                                                    </a>
                                                                    <div class='network-bottom-more-node'><a href='members/genealogy2.php?view=network&headUid=<?php echo $leftGenealogy1['username']?>'></a></div>
                                                                </div>
                                                            <?php }else{ ?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: -100px; height: 14px;"></div>
                                                                </div>
                                                                <div  style='width: 75px; margin-left: -256px; text-align:center; float:left; margin-top: 120px'>
                                                                    <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                        <center>
                                                                            <br/><br/>
                                                                            <a href='members/register.php?placement_type=m&position1=0&upline1=<?php echo $rightGenealogy['username']?>'>Sign Up</a>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            <?php }?>

                                                            <?php if(!empty($rightGenealogy1)){?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: 248px; height: 14px;"></div>
                                                                </div>
                                                                <div class='stats-node' style='width: 75px; margin-left: 94px; text-align:center; float:left; margin-top: 120px'>
                                                                    <a href='members/genealogy2.php?view=network&headUid=<?php echo $rightGenealogy1['username']?>'>
                                                                        <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                            <table cellspacing='0' cellpadding='0' style='line-height:13px;margin-left:7px;'>
                                                                                <tr><td width='135'>Username: <?php echo $rightGenealogy1['username']?></td></tr>
                                                                                <tr><td width='135'>Name: <?php echo $rightGenealogy1['nick_name']?></td></tr>
                                                                                <tr><td width='135'>Joined: <?php echo $rightGenealogy1['user_registered_date']?></td></tr>
                                                                                <tr><td width='135'>Expired Date: <?php echo $rightGenealogy1['expire_date']?></td></tr>
                                                                                <tr><td width='135'>Package: <?php

                                                                                    
                                                                                 if($rightGenealogy1['grade_id']==1)
                                                                                {
                                                                                    
                                                                                    if($rightGenealogy1['RF']==0)
                                                                                    {
                                                                                        echo  "DC".$rightGenealogy1['DC']." + PC".$rightGenealogy1['PC'];
                                                                                    }else
                                                                                    {
                                                                                        //echo  "RF".$rightGenealogy1['RF']." + DC".$rightGenealogy1['DC']." + PC".$rightGenealogy1['PC'];
                                                                                        echo "DC".$rightGenealogy1['DC']." + PC".$rightGenealogy1['PC']." + RF".$rightGenealogy1['RF'];
                                                                                    }
                                                                                     
                                                                                }else if($rightGenealogy1['grade_id']==2)
                                                                                {
                                                                                    echo "DC".$usergrade2['requiredDC']." + PC".$usergrade2['requiredPC'];
                                                                                    
                                                                                }else if($rightGenealogy1['grade_id']==3)
                                                                                {
                                                                                    echo "DC".$usergrade3['requiredDC']." + PC".$usergrade3['requiredPC'];
                                                                                }

                                                                                        //$rightGenealogy1['packageName']?></td></tr>
                                                                                <tr><td width='135'>L / R Sales: <?php echo $client->countTotalLeftUser($rightGenealogy1['userID'])?> / <?php echo $client->countTotalRightUser($rightGenealogy1['userID'])?></td></tr>
                                                                            </table>
                                                                        </div>
                                                                    </a>
                                                                    <div class='network-bottom-more-node'><a href='members/genealogy2.php?view=network&headUid=<?php echo $rightGenealogy1['username']?>'></a></div>
                                                                </div>
                                                            <?php }else {?>
                                                                <div style="width: 0px; margin-left: -77px; float:left; height: 14px;margin-top: 106px">
                                                                    <div class="stats-node-line-up stats-node-line" style="width: 2px; overflow-x: hidden; margin-left: 248px; height: 14px;"></div>
                                                                </div>
                                                                <div  style='width: 75px; margin-left: 95px; text-align:center; float:left; margin-top: 120px'>
                                                                    <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                        <center>
                                                                            <br/><br/>
                                                                            <a href='members/register.php?placement_type=m&position1=1&upline1=<?php echo $rightGenealogy['username']?>'>Sign Up</a>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            <?php }?>

                                                        <?php } else {?>
                                                            <div  style='width: 75px; margin-left: 288px; text-align:center; float:left; margin-top: 0px'>
                                                                <div style='border:2px solid grey; min-height:95px;width:150px;'>
                                                                    <center>
                                                                        <br/><br/>
                                                                        <a href='members/register.php?placement_type=m&position1=1&upline1=<?php echo $userInfo['username']?>'>Sign Up</a>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <br/>
                                            <center style='padding-left:93px;'>
                                                <a href='members/genealogy2.php?view=network#left' class='btn btn-info'>Left</a>
                                                <a href='members/genealogy2.php?view=network#right' class='btn btn-info'>Right</a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <script src="html_template/default/assets/js/jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <?php include_once "includes/footer.php";?>

    <link rel="stylesheet" href="html_template/default/assets/img/network/gentree/gentree.css" type="text/css" />
    <link rel="stylesheet" href="html_template/default/assets/img/network/legend.css" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function(){
            $('.stats-node img').hover(function(){
                    var rel = $(this).attr('rel');

                    $('#tooltip_'+rel).show();
                },
                function(){
                    var rel = $(this).attr('rel');
                    $('#tooltip_'+rel).hide();
                });
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('input[name=headUid]').keypress(function(e){
                if (e.which == 13) {
                    $('input[name=user_search]').click();
                    return false;
                }
            });

            $('input[name=user_search]').click(function(){
                $(this).attr('disabled', 'disabled');
                window.location = 'members/genealogy2.php?view=network&headUid='+$('input[name=headUid]').val();
            });

            $('select[name=did]').change(function(){
                var headUid = $(this).val();
                window.location = '?headUid='+headUid;
            });

            $('.sidebar').addClass('mini-menu');
            $('#main-content').addClass('margin-left-50');
        });
    </script>

<?php }else {
    header('location:../login.php');
}?>