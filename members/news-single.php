<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
?>
<?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
        <!-- PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <!-- STYLER -->

                    <!-- /STYLER -->
                    <!-- BREADCRUMBS -->
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="members/index.php">Home</a>
                        </li>
                        <li>News</li>
                    </ul>
                    <!-- /BREADCRUMBS -->
                    <div class="clearfix">
                        <h3 class="content-title pull-left">News</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PAGE HEADER -->


        <div class='row'>
            <div class='col-md-12'>
                <div class='box border'>
                    <div class='box-title'>
                        <h4 style='height:15px;'></h4>
                    </div>
                    <div class='box-body'>
                        <div class='tabbable header-tabs user-profile'>
                            <ul class='nav nav-tabs'>
                                <li class='active'><a href='members/news.php'>News</a></li>
                            </ul>


                            <div class='tab-content'>
                                <div class='tab-pane fade in active'>
                                    <div class='row'>
                                        <div class='col-md-12'>

                                            <div class='panel'>
                                                <div class='panel-body'>
                                                    <div class='tab-content'>
                                                        <?php
                                                            $sql="select title,news from news where newsID=:newsID and type=1 limit 1";
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':newsID',$_GET['id']);
                                                            $stmt->execute();
                                                            $news=$stmt->fetch(PDO::FETCH_ASSOC);
                                                        ?>
                                                        <?php if(!empty($news)){?>
                                                            <h4><?php echo $news['title']?></h4>
                                                            <hr/>
                                                            <div class='tab-pane fade active in'>

                                                                <?php
                                                                echo $news['news'];
                                                                ?>

                                                            </div>
                                                        <?php }else {?>
                                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                                <div class="col-md-12 profile-details">
                                                                    <br>
                                                                    <div style="width:1000px;margin-left:-100px;height:0px;">
                                                                        <a style="float:left;" name="left">&nbsp;</a>
                                                                        <a style="float:right;" name="right">&nbsp;</a>
                                                                    </div>
                                                                    <br>
                                                                    <br>
                                                                    <div class="alert alert-block alert-danger fade in">
                                                                        <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                        <p>Invalid Operation</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php }?>
                                                        <br/>
                                                        <br/>
                                                        <br/>
                                                        <h4>Other News and Events</h4>
                                                        <ul>
                                                            <?php
                                                            $sql="select newsID,title from news where type=1 order by newsID desc";
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->execute();
                                                            $news=$stmt->fetchAll(PDO::FETCH_ASSOC);
                                                            foreach ($news as $ns){
                                                                ?>
                                                                <li style="list-style: disc"><a href='members/news-single.php?id=<?php echo $ns['newsID']?>'><?php echo $ns['title']?></a></li>
                                                            <?php }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>
