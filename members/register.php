<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
header("Cache-Control: no cache");
//session_cache_limiter("private_no_expire");
if(\App\Session::get('login')==true) {
     date_default_timezone_set('Asia/Kuala_Lumpur');

    $user=new \App\user\User();
    $helper=new \App\Helper();
    
    $user->checkUserValidity(\App\Session::get('userID'));

    $client=new \App\client\Client();
    $package=new \App\package\Package();

    $packageList=$package->getAllPackages();

    $currency=$user->userCurrencies(\App\Session::get('userID'));
    $level='';
    $info=$user->getUserInfo();

    $emailHelper=new \App\EmailHelper();

    $valid=1;
     if(isset($_POST['submit_btn'])) {

         if ($user->validateInput($_POST)) {
                 
             if($user->checkExistingUser($_POST['username']) && $user->checkUserOnEcommerce($_POST['username'])){
                 
                if ($user->verifyUser((base64_encode($_POST['sec_pwd'])))) {
                
                 /**
                  * --------------------------find out parent user details----------------------
                  **/
                  
                  /**----------------check upline user-----------------**/
                  
                 if($user->uplineUserDetails($_POST['ref'])) {
                  

                 if ($res = $user->placementUserDetails($_POST['parent'])) {
                
                //  if ($res = $user->placementUserDetails($_POST['ref'])) {
                    
                    $parentID=$client->getParentID($_POST['parent']); 
                    $sponsorID=$client->getParentID($_POST['ref']); 
                    
                    
                    /**-----------check if sponsor id is with in same line --------------**/
                    
                    
                     if($client->checkUser($parentID, $sponsorID)){  
                         
                         $sponsor_id = $user->getSponsorUserID($_POST['ref']);
                           
                          /**---get info of logged in user--------------------------------------------**/
                         $loggedInUser = $user->getSponsorUserID(\App\Session::get('username'));
                         /**-------------------------------------------------------------------------**/
                     
                     
                     /**
                      * ----------------------Check if two hand is full or not-----------------------
                      **/
                     if ($user->parentUserHand($res['userID'])) {
                        
                         
                         if ($user->parentHandPositionForNewUser($_POST['hand'], $res['userID'])) {
                             /**---------------- Add new user according to their package------------------------------------- **/
                             
                             $packageInfo = $package->getCurrencyForPackage($_POST['package']);

                             if ($packageInfo['package_type'] == 1) {
                                 if ($user->checkCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {

                                     $uplineuser=$user->placementUserDetails($_POST['ref']);
                                     
                                     $valid=2;
                                     
                                     if(isset($_GET['upline1']) && $_GET['upline1']!='')
                                     {
                                         $placemment=$user->placementUserDetails($_GET['upline1']);
                                     }else
                                     {
                                        $placemment=$user->placementUserDetails($_POST['parent']);
                                     }
                                     
                                    
                                    $placementusername=$placemment['username'];
                                    $placementuserfullname=$placemment['full_name'];
                                    

                                     $uplineusername=$uplineuser['username'];
                                     $uplineuserfullname=$uplineuser['full_name'];

                                     $full_name=$_POST['full_name'];
                                     $nick_name=$_POST['nick_name'];
                                     $IC_no=$_POST['IC_no'];
                                     $wechat_ID=$_POST['wechat_ID'];
                                     $contact_no=$_POST['contact_no'];
                                     $myemail=$_POST['email'];



                                     $address=$_POST['address'];
                                     $country=$_POST['country'];
                                     $ref=$_POST['ref'];
                                     $username=$_POST['username'];
                                     $packagename=$_POST['package'];
                                     
                                     if($packageInfo['packageDC']==0)
                                     {
                                        //  $packageTitle="DC".$packageInfo['packageDC']."PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                        $packageTitle="PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                     }else if($packageInfo['packagePC']==0)
                                     {
                                          $packageTitle="DC".$packageInfo['packageDC']." + RF".$packageInfo['packageRF'];
                                     }else if($packageInfo['packageRF']==0)
                                     {
                                          $packageTitle="DC".$packageInfo['packageDC']." + PC".$packageInfo['packagePC'];
                                     }else
                                     {
                                         $packageTitle="DC".$packageInfo['packageDC']." + PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                     }
                                     

                                     $dc=$packageInfo['packageDC'];
                                     $pc=$packageInfo['packagePC'];
                                     $rf=$packageInfo['packageRF'];
                                     
                                     


                                     $parent=$_POST['parent'];
                                     $hand=$_POST['hand'];
                                     $sec_pwd=$_POST['sec_pwd'];

                                 }else
                                 {
                                     if($currency['rf']<$packageInfo['packageRF'])
                                     {
                                         $error[]="You do not have enough Registration Fee. Your balance is ".$currency['rf'].".00, Entered amount ".$packageInfo['packageRF'].".00";
                                     }
                                     if($currency['dc']<$packageInfo['packageDC'])
                                     {
                                         $error[]="You do not have enough Declaration Currency. Your balance is ".$currency['dc'].".00, Entered amount ".$packageInfo['packageDC'].".00";
                                     }
                                     if($currency['pc']<$packageInfo['packagePC'])
                                     {
                                         $error[]="You do not have enough Product Currency. Your balance is ".$currency['pc'].".00, Entered amount ".$packageInfo['packagePC'].".00";
                                     }
                                 }
                             }

                             else if ($packageInfo['package_type'] == 3) {


                                 /**--------------transaction begin---------**/

                                 if ($user->checkCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {

                                     $uplineuser=$user->placementUserDetails($_POST['ref']);
                                     
                                     if(isset($_GET['upline1']) && $_GET['upline1']!='')
                                     {
                                         $placemment=$user->placementUserDetails($_GET['upline1']);
                                     }else
                                     {
                                             $placemment=$user->placementUserDetails($_POST['parent']);
                                     }
                                        
                                    $placementusername=$placemment['username'];
                                    $placementuserfullname=$placemment['full_name'];
                                    
                                     $valid=2;
                                     $uplineusername=$uplineuser['username'];
                                     $uplineuserfullname=$uplineuser['full_name'];
                                     $full_name=$_POST['full_name'];
                                     $nick_name=$_POST['nick_name'];
                                     $IC_no=$_POST['IC_no'];
                                     $wechat_ID=$_POST['wechat_ID'];
                                     $contact_no=$_POST['contact_no'];
                                     $myemail=$_POST['email'];
                                     $address=$_POST['address'];
                                     $country=$_POST['country'];
                                     $ref=$_POST['ref'];
                                     $username=$_POST['username'];
                                     $packagename=$_POST['package'];
                                     
                                     
                                     if($packageInfo['packageDC']==0)
                                     {
                                        //  $packageTitle="DC".$packageInfo['packageDC']."PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                        $packageTitle="PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                     }else if($packageInfo['packagePC']==0)
                                     {
                                          $packageTitle="DC".$packageInfo['packageDC']." + RF".$packageInfo['packageRF'];
                                     }else if($packageInfo['packageRF']==0)
                                     {
                                          $packageTitle="DC".$packageInfo['packageDC']." + PC".$packageInfo['packagePC'];
                                     }else
                                     {
                                         $packageTitle="DC".$packageInfo['packageDC']." + PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                     }

                                     $dc=$packageInfo['packageDC'];
                                     $pc=$packageInfo['packagePC'];
                                     $rf=$packageInfo['packageRF'];

                                     $parent=$_POST['parent'];
                                     $hand=$_POST['hand'];
                                     $sec_pwd=$_POST['sec_pwd'];


                                 }else
                                 {
                                     if($currency['rf']<$packageInfo['packageRF'])
                                     {
                                         $error[]="You do not have enough Registration Fee. Your balance is ".$currency['rf'].".00, Entered amount ".$packageInfo['packageRF'].".00";
                                     }
                                     if($currency['dc']<$packageInfo['packageDC'])
                                     {
                                         $error[]="You do not have enough Declaration Currency. Your balance is ".$currency['dc'].".00, Entered amount ".$packageInfo['packageDC'].".00";
                                     }
                                     if($currency['pc']<$packageInfo['packagePC'])
                                     {
                                         $error[]="You do not have enough Product Currency. Your balance is ".$currency['pc'].".00, Entered amount ".$packageInfo['packagePC'].".00";
                                     }
                                 }
                             }

                             else if ($packageInfo['package_type'] == 11) {

                                 if ($user->checkCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {
                                     
                                     $uplineuser=$user->placementUserDetails($_POST['ref']);
                                     $valid=2;
                                    
                                    if(isset($_GET['upline1']) && $_GET['upline1']!='')
                                     {
                                         $placemment=$user->placementUserDetails($_GET['upline1']);
                                     }else
                                     {
                                             $placemment=$user->placementUserDetails($_POST['parent']);
                                     }
                                     
                                    
                                    $placementusername=$placemment['username'];
                                    $placementuserfullname=$placemment['full_name'];
                                     
                                     $uplineusername=$uplineuser['username'];
                                     $uplineuserfullname=$uplineuser['full_name'];
                                     $full_name=$_POST['full_name'];
                                     $nick_name=$_POST['nick_name'];
                                     $IC_no=$_POST['IC_no'];
                                     $wechat_ID=$_POST['wechat_ID'];
                                     $contact_no=$_POST['contact_no'];
                                     $myemail=$_POST['email'];
                                     $address=$_POST['address'];
                                     $country=$_POST['country'];
                                     $ref=$_POST['ref'];
                                     $username=$_POST['username'];
                                     $packagename=$_POST['package'];
                                     
                                     if($packageInfo['packageDC']==0)
                                     {
                                        //  $packageTitle="DC".$packageInfo['packageDC']."PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                        $packageTitle="PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                     }else if($packageInfo['packagePC']==0)
                                     {
                                          $packageTitle="DC".$packageInfo['packageDC']." + RF".$packageInfo['packageRF'];
                                     }else if($packageInfo['packageRF']==0)
                                     {
                                          $packageTitle="DC".$packageInfo['packageDC']." + PC".$packageInfo['packagePC'];
                                     }else
                                     {
                                         $packageTitle="DC".$packageInfo['packageDC']." + PC".$packageInfo['packagePC']." + RF".$packageInfo['packageRF'];
                                     }

                                     $dc=$packageInfo['packageDC'];
                                     $pc=$packageInfo['packagePC'];
                                     $rf=$packageInfo['packageRF'];

                                     $parent=$_POST['parent'];
                                     $hand=$_POST['hand'];
                                     $sec_pwd=$_POST['sec_pwd'];

                                 }else
                                 {
                                     if($currency['rf']<$packageInfo['packageRF'])
                                     {
                                         $error[]="You do not have enough Registration Fee. Your balance is ".$currency['rf'].".00, Entered amount ".$packageInfo['packageRF'].".00";
                                     }
                                     if($currency['dc']<$packageInfo['packageDC'])
                                     {
                                         $error[]="You do not have enough Declaration Currency. Your balance is ".$currency['dc'].".00, Entered amount ".$packageInfo['packageDC'].".00";
                                     }
                                     if($currency['pc']<$packageInfo['packagePC'])
                                     {
                                         $error[]="You do not have enough Product Currency. Your balance is ".$currency['pc'].".00, Entered amount ".$packageInfo['packagePC'].".00";
                                     }
                                 }
                             }

                         }else {
                          
                             $error[]="The ".($_POST['hand']==0?'A':'B')." leg of placement ID ".$_POST['parent']." has been placed. ";
                         }
                     }else {
                         $error[]="Both leg of placement ID ".$_POST['parent']." has been placed. ";
                     }
                     } else
                     {

                         $error[]="The placement Username is not within the group of the upline Username.";
                     }
                 }else
                 {
                   
                    $error[]="<p>Placement Username '".$_POST['parent']."' cannot be found.</p>";
                     
                 }
                 
                 /**--------------------------------------- upline user--------------------------------------------**/
                }
                else
                {
           
                   $error[]="<p>Upline Username '".$_POST['ref']."' cannot be found in the database.<br>The placement Username is not within the group of the upline Username.</p>";
                }
             }
             
             else
             {
                  $error[]="You have entered an invalid User Security Password.";
                  
             }
    
             }else
             {
                 $error[]="Username '".$_POST['username']."' already exist.";
             }
             
             /**---------------------**/
             
         }
     }
     




    if(isset($_POST['confirm_btn'])) {
        
     

        if ($user->verifyUser((base64_encode($_POST['sec_pwd'])))) {

            if ($user->validateInput($_POST)) {
                /**
                 * --------------------------find out parent user details----------------------
                 **/
                $sponsor_id = $user->getSponsorUserID($_POST['ref']);

                /**---get info of logged in user--------------------------------------------**/
                $loggedInUser = $user->getSponsorUserID(\App\Session::get('username'));
                /**-------------------------------------------------------------------------**/

                if ($res = $user->placementUserDetails($_POST['parent'])) {
                    /**
                     * ----------------------Check if two hand is full or not-----------------------
                     **/
                    if ($user->parentUserHand($res['userID'])) {
                        
                        if ($user->parentHandPositionForNewUser($_POST['hand'], $res['userID'])) {
                            /**---------------- Add new user according to their package------------------------------------- **/

                            $packageInfo=$package->getCurrencyForPackage($_POST['package']);
                            
                            if ($packageInfo['package_type'] == 1) {

                                try{
                                    /**--------------transaction begin---------**/
                                    \App\DBConnection::myDb()->beginTransaction();

                                    /**----------------------check if logged in user has enough currency------------------------**/
                                    if ($user->checkCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {

                                        $val=$user->randomPassword();
                                        $val1=$user->randomSecPassword();
                                        $_POST['password'] = base64_encode($val);
                                        $_POST['security_password'] = base64_encode($val1);
                                        
                                        $sec_pwd=$val1;
                                        $pswrd=$val;

                                        $user->set($_POST);
                                        if ($id = $user->addNewUser($_POST['ref'])) {
                                            $_POST['userID'] = $id;
                                            $_POST['username'] = $_POST['username'];
                                            $_POST['parentid'] = $res['userID'];
                                            $_POST['sponsorid'] = $sponsor_id['userID'];
                                            /** system root user will be the sponsor id **/
                                            /**
                                             *------- find out placement users level and increment 1 with the value for new client-------------------------------
                                             **/
                                            $_POST['client_level'] = $user->placementUserLevel($_POST['parent'])['client_level'] + 1;
                                            $_POST['packageID'] = $_POST['package'];
                                            $_POST['root_parent'] = $id;
                                            /**
                                             * -------------------------------------------------------------------------------------------------------------
                                             **/
                                            /**---------------store pp bonus 4 means product point--------------------------------**/
                                            // $user->storeParentBonus($id, $packageInfo['PP'], $_POST['client_level'], 0,$id, 4);
                                            $user->storeParentBonus($id, $packageInfo['PP'], $_POST['client_level'], 0,$id, 4);//PP bonus
                                            $user->storeParentBonus($id,  $packageInfo['MSP'], $_POST['client_level'], 0,$id, 6); //MSP bonus
                                            //5 means pp bonus
                                            $user->storePPCurrency($id, $packageInfo['PP']);
                                            $user->storeMSPCurrency($id, $packageInfo['MSP']);
                                            
                                            /**--------------------------------------------------------------**/  
                                            $currency=$user->userCurrencies($id);
                                            /**--------------------------------------------------------------**/
                                            
                                            // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                            $user->userCurrencyHistory($id,0, $packageInfo['PP'],'New registration',14,18,0, $packageInfo['PP'],"",$currency['pp'],0);
                                            $user->userCurrencyHistory($id,0, $packageInfo['MSP'],'New registration',15,19,0, $packageInfo['MSP'],"",$currency['msp'],0);

                                            /**--------------------------------------------------------------**/

                                            $client->set($_POST);
                                            if ($client->addNewClient($packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {

                                            /**---------------------decrement currency of user--------------------**/
                                            $user->decrementCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC']);
                                                
                                            /**--------------------------------------------------------------**/  
                                            $currency=$user->userCurrencies(\App\Session::get('userID'));
                                            /**--------------------------------------------------------------**/
                                            
//                                            $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packageRF'],'New account registration',3,14,$packageInfo['packageRF'],0,"",$currency['rf'],0);
                                                $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packageDC'],'New account registration',2,15,$packageInfo['packageDC'],0,"",$currency['dc'],0);
                                                $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packagePC'],'New account registration',9,16,$packageInfo['packagePC'],0,"",$currency['pc'],0);

                                                /**-------------------------------------------------------------------**/


                                                /**----------------- introductory history---------------------------------**/
                                                if($_POST['hand']==0)
                                                {
                                                    $user->storeParentBonus($sponsor_id['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], $id, 0, 3);/**----  3 means introductory bonus (EC)**/
                                                }else{
                                                    $user->storeParentBonus($sponsor_id['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], 0,$id, 3) /**----  3 means introductory bonus (EC)**/;
                                                }
                                                $user->storeElectronicCurrency($sponsor_id['userID'],$user->getBonusInfo(4)['amount']);
                                                
                                                
                                                /**--------------------------------------------------------------**/  
                                                $currency=$user->userCurrencies($sponsor_id['userID']);
                                                /**--------------------------------------------------------------**/
                                                
                                                $user->userCurrencyHistory($sponsor_id['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus ('.$_POST['username'].')',4,12,0,$user->getBonusInfo(4)['amount'],"",$currency['ec'],0);
                                                /**--------------------------------------------------------------------------------------------**/


                                                /**-------------- get parent of each user in this loop --------------------------------------------------**/
                                                foreach ($user->getUserParent($id) as $d) {
                                                    /** -------------- store client position on client_positions table -------------------------------------------------**/
//
                                                    $client->storePosition($d['parent_id'], $id, $d['hand'], $_POST['client_level']);

                                                    /** ------------------- store user bonus value------------------------------------------------- **/
                                                    $sql = "select user_id from client_positions where parent_id=:parent_id and (hand=0 or hand=1) and client_level=:client_level order by user_positionID desc limit 2";
                                                    $stmt = \App\DBConnection::myQuery($sql);
                                                    $stmt->bindValue(':parent_id', $d['parent_id']);
                                                    $stmt->bindValue(':client_level', $_POST['client_level']);
                                                    $stmt->execute();
                                                    $res1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                                                    if (count($res1) == 2) {


                                                        $sql = "select user_id,hand,client_level from client_positions where parent_id=:parent_id and client_level=:client_level order by user_positionID desc limit 2";
                                                        /** -1 means the root user's main hand so it will not be used **/

                                                        $stmt = \App\DBConnection::myQuery($sql);
                                                        $stmt->bindValue(':parent_id', $d['parent_id']);
                                                        $stmt->bindValue(':client_level', $_POST['client_level']);
                                                        $stmt->execute();
                                                        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                                                        $left_user = '';
                                                        $right_user = '';

                                                        if ($data[0]['hand'] == 0) {
                                                            $left_user = $data[0]['user_id'];
                                                        } else if ($data[0]['hand'] == 1) {
                                                            $right_user = $data[0]['user_id'];
                                                        }

                                                        if ($data[1]['hand'] == 0) {
                                                            $left_user = $data[1]['user_id'];
                                                        } else if ($data[1]['hand'] == 1) {
                                                            $right_user = $data[1]['user_id'];
                                                        }

                                                        /**------------count parent's total level bonus-----------------------------**/
                                                        if ($user->countLevelBonus($d['parent_id']) + 1 <= 5) {

                                                            $sql="select count(userID) as total from bonus where userID=:userID and bonus_date=:bonus_date and bonus_type=1";
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':userID',$d['parent_id']);
                                                            $stmt->bindValue(':bonus_date',date('Y-m-d'));
                                                            $stmt->execute();
                                                            $res=$stmt->fetch(PDO::FETCH_ASSOC);
                                                            if($res['total']==0)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 150;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        
                                                                        /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/
                                                                        
                                                                        //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }

                                                            }else if($res['total']==1)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 100;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                       $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                       /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/
                                                                        
                                                                        //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }
                                                            else if($res['total']==2)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 75;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/
                                                                        
                                                                        //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 0;
                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        
                                                                        /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/
                                                                        
//                                                                    $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }
                                                        }
//                                                else {
//                                                    $bonus_type = 2;
//                                                    /** ------------ 2 means pairing bonus -----------------**/
//                                                    $bonus_point = 25;
//                                                }
                                                        /**--------------------------------------------------------------------------**/

                                                    }
                                                    /** ------------------------------------------------------------------------------------------- **/
                                                }
                                                /** ---------------------------------------------------------------------------------------------------------**/

                                                $valid=3;
                                                $username=$_POST['username'];
                                                $myemail=$_POST['email'];
                                                $dc=$packageInfo['packageDC'];
                                                $pc=$packageInfo['packagePC'];
                                                $rf=$packageInfo['packageRF'];


                                                \App\DBConnection::myDb()->commit();

                                                /**---------------------send mail---------------------------------------**/
                                                $body.="
                                                        <tbody><tr><td>
                                                            <table cellspacing='0' border='0' id='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432email-content' cellpadding='0' width='624'>
                                                                <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td valign='top' align='center'>
                                                                                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                                                                        <tbody><tr>
                                                                                            <td valign='bottom'></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>";
                                                                                    $body.="<h1>Welcome to <span style='font-style:italic'>Mon Space</span></h1>
                                                                                    <br><br>
                                                                                    <h1>Dear <span style='font-style: italic'>Mon Space Valued Client</span>,</h1>
                                                                                    <p> Mon Space would like to welcome you as a new customer to be part of us.</p>
                                                                                    <p> You may start your simple profitable trading experience now.</p>
                                                                                    <br/>
                                                                                    <p> Below, you'll find your account information.</p>
                                                
                                                                                    <table border='0' cellpadding='2' cellspacing='0'>
                                                                                        <tbody><tr>
                                                                                            <td><b>Username</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$_POST['username']."</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Account Password</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$val."</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Security Password</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$val1."</b></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <br>
                                                                                    <p class='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432text'>
                                                                                        For any additional assistance, feel free to contact our support team via email at<br>
                                                                                        <span style='color:#FFFF00'>support@client.bmsm4all.com</span>.
                                                                                    </p>
                                                                                    <br>
                                                                                    <br>
                                                                                    <p>Sincerely,</p>
                                                                                    <p>Mon Space(M)Sdn. Bhd.</p>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td height='72'></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                ";

                                                $headers = array(
                                                    'From' => 'support@bmsm4all.com'
                                                );

                                                $emailHelper->setupEmail('mail.bmsm4all.com', 'support@bmsm4all.com', '##support123##', '587', 'tls', 'support@client.bmsm4all.com', 'Support', true);

                                                //$to, $name, $subject, $message, $header=null, $isHTML=true
                                                $emailHelper->sendMail($_POST['email'],'Support','Monspacea- Thank You for Your Registration', $body, null, true);
                                                /**--------------------------------------------------------------------------**/



                                            }
                                        } else {
                                            \App\Session::set('error',  "Username ".$_POST['username']." already exist.");
                                        }
                                    } else {
                                        \App\Session::set('error', 'You have not enough currency to add this user.');
                                    }


                                }catch(PDOException $ex)
                                {
                                    \App\DBConnection::myDb()->rollBack();
                                }
                            }

                            else if ($packageInfo['package_type'] == 3) {
                                try{

                                    /**--------------transaction begin---------**/
                                    \App\DBConnection::myDb()->beginTransaction();

                                    /**-----------------------------start of user currency check---------------------------------**/
                                    if ($user->checkCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])){
                                        /*** -------------------------------- add parent user first --------------------------------------------------------------**/

                                        $val=$user->randomPassword();
                                        $val1=$user->randomSecPassword();
                                        
                                        $sec_pwd=$val1;
                                        $pswrd=$val;
                                        
                                        $_POST['password'] = base64_encode($val);
                                        $_POST['security_password'] = base64_encode($val1);
                                        

                                        $user->set($_POST);
                                        if ($id = $user->addNewUser($_POST['ref'])) {
                                            $_POST['userID'] = $id;

                                            $_POST['username'] = $_POST['username'];
                                            $_POST['parentid'] = $res['userID'];
                                            $_POST['sponsorid'] = $sponsor_id['userID'];
                                            /** system root user will be the sponsor id **/
                                            /**
                                             *------- find out placement users level and increment 1 with the value for new client-------------------------------
                                             **/
                                             
                                            $_POST['client_level'] = $user->placementUserLevel($_POST['parent'])['client_level'] + 1;
                                            
                                            
                                            $_POST['packageID'] = $_POST['package'];
                                            $_POST['root_parent'] = $id;
                                            /**
                                             * -------------------------------------------------------------------------------------------------------------
                                             **/
                                            /**---------------store pp bonus 4 means product point--------------------------------**/
                                            $user->storeParentBonus($id, $packageInfo['PP'], $_POST['client_level'], 0,$id, 4);
                                            //5 means msp bonus
                                            $user->storePPCurrency($id,$packageInfo['PP']);
                                            // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                            
                                            $user->storeParentBonus($id, $packageInfo['MSP'], $_POST['client_level'], 0,$id,6); //6 meand MSP in bonus table
                                            
                                            $user->storeMSPCurrency($id,$packageInfo['MSP']);//3 MSP
                                            // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                            
                                            
                                            
                                            /**--------------------------------------------------------------**/  
                                            $currency=$user->userCurrencies($id);
                                            /**--------------------------------------------------------------**/
                                            
                                            
                                            $user->userCurrencyHistory($id,0,$packageInfo['PP'],'New registration',14,18,0,$packageInfo['PP'],"",$currency['pp'],0);
                                            $user->userCurrencyHistory($id,0,$packageInfo['MSP'],'New registration',15,19,0,$packageInfo['MSP'],"",$currency['msp'],0);
                                            /**--------------------------------------------------------------**/

                                            $client->set($_POST);

                                            if ($client->addNewClient($packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {

                                                /**---------------------decrement currency of user--------------------**/
                                                //$user->decrementCurrency($sponsor_id['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC']);
                                                $user->decrementCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC']);
                                                
                                                /**--------------------------------------------------------------**/  
                                                $currency=$user->userCurrencies($loggedInUser['userID']);
                                                /**--------------------------------------------------------------**/

//                                            $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packageRF'],'New account registration',3,14,$packageInfo['packageRF'],0,"",$currency['rf'],0);
                                                $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packageDC'],'New account registration',2,15,$packageInfo['packageDC'],0,"",$currency['dc'],0);
                                                $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packagePC'],'New account registration',9,16,$packageInfo['packagePC'],0,"",$currency['pc'],0);
                                                /**-------------------------------------------------------------------**/


                                                /**-----------------------------store bonus electronic currency for upline user---------------**/
                                                // $user->storeElectronicCurrency($sponsor_id['userID'],50);

                                                // $user->storeParentBonus($sponsor_id['userID'], 50, $_POST['client_level'], 0,0, 5); /** 5 means EC bonus **/

//                                            $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                // $user->userCurrencyHistory($sponsor_id['userID'],0,50,'EC Bonus',1,1,0,50,"EC Bonus");

                                                if($_POST['hand']==0)
                                                {
                                                    $user->storeParentBonus($sponsor_id['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], $id, 0, 3);/**----  3 means introductory bonus (EC)**/
                                                }else{
                                                    $user->storeParentBonus($sponsor_id['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], 0,$id, 3) /**----  3 means introductory bonus (EC)**/;
                                                }
                                                $user->storeElectronicCurrency($sponsor_id['userID'],$user->getBonusInfo(4)['amount']);
                                                
                                                /**--------------------------------------------------------------**/  
                                                $currency=$user->userCurrencies($sponsor_id['userID']);
                                                /**--------------------------------------------------------------**/
                                                
                                                $user->userCurrencyHistory($sponsor_id['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus ('.$_POST['username'].')',4,12,0,$user->getBonusInfo(4)['amount'],"IC Bonus",$currency['ec'],0);
                                                /**--------------------------------------------------------------------------------------------**/

                                                /**
                                                 * --------- store client position on client_positions table-------------------------------------------------
                                                 **/
                                                /**--------- get parent of each user in this loop------------------------------------------------------------**/
                                                //  $insert_statement="insert into table (v1,v2) values";
                                                 
                                                foreach ($user->getUserParent($id) as $d) {
                                                    // $parent=$d['parent_id'];
                                                    // $hand=$d['hand'];
                                                    // $level=$_POST['client_level'];
                                                    
                                                    // $insert_statement.="($parent,$id,$hand,$level)";
                                                    $client->storePosition($d['parent_id'], $id, $d['hand'], $_POST['client_level']);

                                                    /** ------------------- store user bonus value for package 2------------------------------------------------- **/
                                                    $sql = "select user_id from client_positions where parent_id=:parent_id and (hand=0 or hand=1) and client_level=:client_level order by user_positionID desc limit 2";
                                                    $stmt = \App\DBConnection::myQuery($sql);
                                                    $stmt->bindValue(':parent_id', $d['parent_id']);
                                                    $stmt->bindValue(':client_level', $_POST['client_level']);
                                                    $stmt->execute();
                                                    $res1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                                                    if (count($res1) == 2) {
//                                             if (($res1[0]['hand'] != 0 && $res1[1]['hand'] != 0) && ($res1[0]['hand'] != 1 && $res1[1]['hand'] != 1))
                                                        $sql = "select user_id,hand,client_level from client_positions where parent_id=:parent_id and client_level=:client_level order by user_positionID desc limit 2";
                                                        /** -1 means the root user's main hand so it will not be used **/

                                                        $stmt = \App\DBConnection::myQuery($sql);
                                                        $stmt->bindValue(':parent_id', $d['parent_id']);
                                                        $stmt->bindValue(':client_level', $_POST['client_level']);
                                                        $stmt->execute();
                                                        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                                                        $left_user = '';
                                                        $right_user = '';

                                                        if ($data[0]['hand'] == 0) {
                                                            $left_user = $data[0]['user_id'];
                                                        } else if ($data[0]['hand'] == 1) {
                                                            $right_user = $data[0]['user_id'];
                                                        }

                                                        if ($data[1]['hand'] == 0) {
                                                            $left_user = $data[1]['user_id'];
                                                        } else if ($data[1]['hand'] == 1) {
                                                            $right_user = $data[1]['user_id'];
                                                        }

                                                        /**------------count parent's total level bonus-----------------------------**/
                                                        if ($user->countLevelBonus($d['parent_id']) + 1 <= 5) {


                                                            $sql="select count(userID) as total from bonus where userID=:userID and bonus_date=:bonus_date and bonus_type=1";
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':userID',$d['parent_id']);
                                                            $stmt->bindValue(':bonus_date',date('Y-m-d'));
                                                            $stmt->execute();
                                                            $res=$stmt->fetch(PDO::FETCH_ASSOC);
                                                            if($res['total']==0)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 150;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
//                                                                    $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        
                                                                        /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/

                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        /**----------------------------------------------------------------------**/
                                                                    }
                                                                }

                                                            }else if($res['total']==1)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 100;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
//                                                                    
                                                                        /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/

                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        /**----------------------------------------------------------------------**/
                                                                        /**----------------------------------------------------------------------**/
                                                                    }
                                                                }
                                                            }else if($res['total']==2)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 75;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        
                                                                        
//                                                                    $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/

                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        /**----------------------------------------------------------------------**/
                                                                        /**----------------------------------------------------------------------**/
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 0;
                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
//                                                                    $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark

 /**--------------------------------------------------------------**/  
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        /**--------------------------------------------------------------**/

                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        /**----------------------------------------------------------------------**/
                                                                       
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                                /**
                                                 * ---------------------------------------------------------------------------------------------------------
                                                 **/
                                            }

                                            $valid=3;
                                            $username=$_POST['username'];
                                            $myemail=$_POST['email'];
                                            $dc=$packageInfo['packageDC'];
                                            $pc=$packageInfo['packagePC'];
                                            $rf=$packageInfo['packageRF'];;

                                            /**---------------------send mail---------------------------------------**/

                                            $body.="
                                                        <tbody><tr><td>
                                                            <table cellspacing='0' border='0' id='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432email-content' cellpadding='0' width='624'>
                                                                <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td valign='top' align='center'>
                                                                                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                                                                        <tbody><tr>
                                                                                            <td valign='bottom'></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>";
                                                                                    $body.="<h1>Welcome to <span style='font-style:italic'>Mon Space</span></h1>
                                                                                    <br><br>
                                                                                    <h1>Dear <span style='font-style: italic'>Mon Space Valued Client</span>,</h1>
                                                                                    <p> Mon Space would like to welcome you as a new customer to be part of us.</p>
                                                                                    <p> You may start your simple profitable trading experience now.</p>
                                                                                    <br/>
                                                                                    <p> Below, you'll find your account information.</p>
                                                
                                                                                    <table border='0' cellpadding='2' cellspacing='0'>
                                                                                        <tbody><tr>
                                                                                            <td><b>Username</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$_POST['username']."</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Account Password</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$val."</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Security Password</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$val1."</b></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <br>
                                                                                    <p class='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432text'>
                                                                                        For any additional assistance, feel free to contact our support team via email at<br>
                                                                                        <span style='color:#FFFF00'>support@client.bmsm4all.com</span>.
                                                                                    </p>
                                                                                    <br>
                                                                                    <br>
                                                                                    <p>Sincerely,</p>
                                                                                    <p>Mon Space(M)Sdn. Bhd.</p>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td height='72'></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                ";

                                                $headers = array(
                                                    'From' => 'support@bmsm4all.com'
                                                );


                                                

                                            $emailHelper->setupEmail('mail.bmsm4all.com', 'support@bmsm4all.com', '##support123##', '587', 'tls', 'support@client.bmsm4all.com', 'Support', true);

                                            //$to, $name, $subject, $message, $header=null, $isHTML=true
                                            $emailHelper->sendMail($_POST['email'],'Support','Monspacea- Thank You for Your Registration', $body, null, true);
                                            /**--------------------------------------------------------------------------**/




                                        } else {
                                            \App\Session::set('error',  "Username ".$_POST['username']." already exist.");
                                        }
                                        /**
                                         * ----------------------------------------------------------------------------------------------------------
                                         **/

                                        /**
                                         * ------------------- add user to each hand of this user----------------------------------------
                                         **/
                                        $info = $user->placementUserDetailsByID($id);
                                        $root_parent = $id;

                                        for ($i = 1; $i <= 2; $i++) {
                                            $_POST['password'] = base64_encode($val);
                                            $_POST['security_password'] = base64_encode($val1);
                                            

                                            $_POST['username'] = $info['username'] . $i;

                                            $user->set($_POST);
                                            if ($id = $user->addNewUser($_POST['ref'])) {

                                                /**-----------------------------store bonus electronic currency for upline user---------------**/
                                                // $user->storeElectronicCurrency($info['userID'],50);

                                                // $user->storeParentBonus($info['userID'], 50, 0, 0,0, 5); /** 5 means EC bonus **/
                                                // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                // $user->userCurrencyHistory($info['userID'],0,50,'EC Bonus',1,1,0,50,"EC Bonus");
                                                /**--------------------------------------------------------------------------------------------**/

                                                $_POST['userID'] = $id;
                                                if ($i % 2 == 0) {
                                                    $_POST['hand'] = 1;
                                                } else {
                                                    $_POST['hand'] = 0;
                                                }
                                                $_POST['sponsorid'] = $root_parent;

                                                /** system root user will be the sponsor id **/
                                                /**
                                                 *-- find out placement users level and increment 1 with the value for new client-------------------------------
                                                 **/
                                                $_POST['parentid'] = $info['userID'];
                                                $_POST['client_level'] = $user->placementUserLevel($info['username'])['client_level'] + 1;
                                                $_POST['packageID'] = $_POST['package'];
                                                $_POST['root_parent'] = $root_parent;
                                                /**
                                                 * -------------------------------------------------------------------------------------------------------------
                                                 **/

                                                if($_POST['hand']==0)
                                                {
                                                    $user->storeParentBonus($info['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], $id, 0, 3);/**----3 means introductory bonus (EC)**/

                                                }else{
                                                    $user->storeParentBonus($info['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], 0,$id, 3); /**----3 means introductory bonus (EC)**/
                                                }
                                                //$user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                
                                                $user->storeElectronicCurrency($info['userID'],  $user->getBonusInfo(4)['amount']);
                                                 /**--------------------------------------------------------------**/  
                                                $currency=$user->userCurrencies($info['userID']);
                                                /**--------------------------------------------------------------**/
                                                /**----------------------------------------------------------------------**/
                                                $user->userCurrencyHistory($info['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus ('.$_POST['username'].')',4,12,0,$user->getBonusInfo(4)['amount'],"",$currency['ec'],0);



                                                $client->set($_POST);
                                                if ($client->addNewClient(0, $packageInfo['packageDC'], $packageInfo['packagePC'])) {
                                                    /**
                                                     * --------- store client position on client_positions table-------------------------------------------------
                                                     **/
                                                    /**--------- get parent of each user in this loop------------------------------------------------------------**/
                                                    foreach ($user->getUserParent($id) as $d) {
                                                        
                                                        $client->storePosition($d['parent_id'], $id, $d['hand'], $_POST['client_level']);
                                                        /** ------------------- store user bonus value for package 2------------------------------------------------- **/

                                                        $sql = "select user_id from client_positions where parent_id=:parent_id and (hand=0 or hand=1) and client_level=:client_level order by user_positionID desc limit 2";
                                                        $stmt = \App\DBConnection::myQuery($sql);
                                                        $stmt->bindValue(':parent_id', $d['parent_id']);
                                                        $stmt->bindValue(':client_level', $_POST['client_level']);
                                                        $stmt->execute();
                                                        $res1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                                                        if (count($res1) == 2) {
//                                             if (($res1[0]['hand'] != 0 && $res1[1]['hand'] != 0) && ($res1[0]['hand'] != 1 && $res1[1]['hand'] != 1)) {
                                                            $sql = "select user_id,hand,client_level from client_positions where parent_id=:parent_id and client_level=:client_level order by user_positionID desc limit 2";
                                                            /** -1 means the root user's main hand so it will not be used ---------------------------------**/
                                                            $stmt = \App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':parent_id', $d['parent_id']);
                                                            $stmt->bindValue(':client_level', $_POST['client_level']);
                                                            $stmt->execute();
                                                            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                                                            $left_user = '';
                                                            $right_user = '';
                                                            if ($data[0]['hand'] == 0) {
                                                                $left_user = $data[0]['user_id'];
                                                            } else if ($data[0]['hand'] == 1) {
                                                                $right_user = $data[0]['user_id'];
                                                            }

                                                            if ($data[1]['hand'] == 0) {
                                                                $left_user = $data[1]['user_id'];
                                                            } else if ($data[1]['hand'] == 1) {
                                                                $right_user = $data[1]['user_id'];
                                                            }

//                                                    /**------------count parent's total level bonus-----------------------------**/

//                                                    /**--------------------------------------------------------------------------**/

                                                            /**------------count parent's total level bonus-----------------------------**/
                                                            if ($user->countLevelBonus($d['parent_id']) + 1 <= 5) {

                                                                $sql="select count(userID) as total from bonus where userID=:userID and bonus_date=:bonus_date and bonus_type=1";
                                                                $stmt=\App\DBConnection::myQuery($sql);
                                                                $stmt->bindValue(':userID',$d['parent_id']);
                                                                $stmt->bindValue(':bonus_date',date('Y-m-d'));
                                                                $stmt->execute();
                                                                $res=$stmt->fetch(PDO::FETCH_ASSOC);
                                                                if($res['total']==0)
                                                                {
                                                                    $bonus_type = 1;
                                                                    /** ------------ 1 means level bonus --------------------**/
                                                                    $bonus_point = 150;

                                                                    if ($left_user != 0 && $right_user != 0) {
                                                                        if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                            $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                            /**------------- store amounts in user_amounts table---------------------**/
                                                                            $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                            /**----------------------------------------------------------------------**/
                                                                            //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                            
                                                                            /**--------------------------------------------------------------**/  
                                                                                $currency=$user->userCurrencies($d['parent_id']);
                                                                                /**--------------------------------------------------------------**/
                                                                            
                                                                            $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        }
                                                                    }

                                                                }else if($res['total']==1)
                                                                {
                                                                    $bonus_type = 1;
                                                                    /** ------------ 1 means level bonus --------------------**/
                                                                    $bonus_point = 100;

                                                                    if ($left_user != 0 && $right_user != 0) {
                                                                        if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                            $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                            /**------------- store amounts in user_amounts table ---------------------**/
                                                                            $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                            /**----------------------------------------------------------------------**/
                                                                            
                                                                            /**--------------------------------------------------------------**/  
                                                                             $currency=$user->userCurrencies($d['parent_id']);
                                                                            /**--------------------------------------------------------------**/
                                                                            
                                                                            $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        }
                                                                    }
                                                                }
                                                                else if($res['total']==2)
                                                                {
                                                                    $bonus_type = 1;
                                                                    /** ------------ 1 means level bonus --------------------**/
                                                                    $bonus_point = 75;

                                                                    if ($left_user != 0 && $right_user != 0) {
                                                                        if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                            $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                            /**------------- store amounts in user_amounts table ---------------------**/
                                                                            $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                            /**----------------------------------------------------------------------**/
                                                                        
                                                                            
                                                                            /**--------------------------------------------------------------**/  
                                                                             $currency=$user->userCurrencies($d['parent_id']);
                                                                            /**--------------------------------------------------------------**/
                                                                            
                                                                            $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        }
                                                                    }
                                                                }else
                                                                {
                                                                    $bonus_type = 1;
                                                                    /** ------------ 1 means level bonus --------------------**/
                                                                    $bonus_point = 0;
                                                                    if ($left_user != 0 && $right_user != 0) {
                                                                        if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                            $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                            /**------------- store amounts in user_amounts table ---------------------**/
                                                                            $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                            /**----------------------------------------------------------------------**/
    //                                                                    
                                                                            /**--------------------------------------------------------------**/  
                                                                             $currency=$user->userCurrencies($d['parent_id']);
                                                                            /**--------------------------------------------------------------**/
                                                                            
                                                                            $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        /**-----------------------for package 2-----------------------------------**/
                                                    }
                                                    /**
                                                     * ---------------------------------------------------------------------------------------------------------
                                                     **/
                                                 
//                                                        \App\Session::set('success','User added successfully.');
//                                                        \App\Session::set('complete','1');
                                                }

                                            } else {
                                                \App\Session::set('error', 'This username has been already taken.');
                                            }
                                        }
                                    } else {
                                        \App\Session::set('error', 'You have not enough currency to add this user.');
                                    }

                                        \App\DBConnection::myDb()->commit();

                                }catch(PDOException $ex)
                                {
                                    \App\DBConnection::myDb()->rollBack();
                                }
                                /**------------------ end of user currency check ----------------------------------------**/
                            } /**-------------------------------------- end of user add for package 2--------------------------------- **/

                            else if ($packageInfo['package_type'] == 11) {

                                /**------------------------------ add parent user first ----------------------------------**/
                                /**
                                 * -------------------------------- add parent user first --------------------------------------------------------------
                                 **/
                                /**------------------------check if user can add new user for package 11-----------------------------**/
                                if ($user->checkCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {
                                    
                                    
                                    try{
                                    \App\DBConnection::myDb()->beginTransaction();
                                    
                                   
                                    
                                    $val=$user->randomPassword();
                                    $val1=$user->randomSecPassword();
                                    $_POST['password'] = base64_encode($val);
                                    $_POST['security_password'] = base64_encode($val1);
                                    $sec_pwd=$val1;
                                    $pswrd=$val;
                                    
                                    
                                    $user->set($_POST);
                                    if ($id = $user->addNewUser($_POST['parent'])) {
                                    
                                        $_POST['userID'] = $id;
                                        $_POST['username'] = $_POST['username'];
                                        $_POST['parentid'] = $res['userID'];
                                        $_POST['sponsorid'] = $sponsor_id['userID'];
                                        /** system root user will be the sponsor id **/
                                        /**
                                         *------- find out placement users level and increment 1 with the value for new client-------------------------------
                                         **/
                                        //$_POST['client_level'] = $user->placementUserLevel($_POST['parent'])['client_level'] + 1;
                                        
                                        
                                        if( $user->placementUserLevel($_POST['parent'])['client_level']==0)
                                        {
                                            $_POST['client_level']=1;
                                        }else
                                        {
                                            // $_POST['client_level'] = $user->placementUserLevel($_POST['parent'])['client_level'] ;
                                            $_POST['client_level'] = $user->placementUserLevel($_POST['parent'])['client_level']+1 ;
                                        }
                                        
                                        
                                        
                                        $_POST['packageID'] = $_POST['package'];

                                        $_POST['root_parent'] = $id;


                                        if($_POST['hand']==0)
                                        {
                                            $user->storeParentBonus($sponsor_id['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], $id, 0, 3);/**----  3 means introductory bonus (EC)**/
                                        }else{
                                            $user->storeParentBonus($sponsor_id['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], 0,$id, 3) /**----  3 means introductory bonus (EC)**/;
                                        }
                                        $user->storeElectronicCurrency($sponsor_id['userID'],$user->getBonusInfo(4)['amount']);
                                        
                                        /**--------------------------------------------------------------**/  
                                        $currency=$user->userCurrencies($sponsor_id['userID']);
                                        $user->userCurrencyHistory($sponsor_id['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus ('.$_POST['username'].')',4,12,0,$user->getBonusInfo(4)['amount'],"",$currency['ec'],0);
                                        /**--------------------------------------------------------------------------------------------**/


                                        /**
                                         * -------------------------------------------------------------------------------------------------------------
                                         **/

                                        /**---------------store pp bonus 4 means product point--------------------------------**/
                                        $user->storeParentBonus($id, $packageInfo['PP'], $_POST['client_level'], 0,$id,4);
                                        //5 means msp bonus
                                        $user->storePPCurrency($id,$packageInfo['PP']);
                                        // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                        $user->storeMSPCurrency($id,$packageInfo['MSP']);//3 MSP
                                        
                                         $user->storeParentBonus($id, $packageInfo['MSP'], $_POST['client_level'], 0,$id,6); //6 meand MSP in bonus table
                                        // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                        
                                        
                                        
                                        $currency=$user->userCurrencies($id);
                                        
                                        $user->userCurrencyHistory($id,0,$packageInfo['PP'],'New registration',14,18,0,$packageInfo['PP'],"",$currency['pp'],0);
                                        
                                        
                                        $user->userCurrencyHistory($id,0,$packageInfo['MSP'],'New registration',15,19,0,$packageInfo['MSP'],"",$currency['msp'],0);

                                        $client->set($_POST);

                                        if ($client->addNewClient($packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC'])) {

                                            /**---------------------decrement currency of user--------------------**/
                                            $user->decrementCurrency($loggedInUser['userID'], $packageInfo['packageRF'], $packageInfo['packageDC'], $packageInfo['packagePC']);
                                            /**-------------------------------------------------------------------**/
                                            
                                            $currency=$user->userCurrencies(\App\Session::get('userID'));
//                                            $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                            $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packageRF'],'New account registration',3,14,$packageInfo['packageRF'],0,"",$currency['rf'],0);
                                            $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packageDC'],'New account registration',2,15,$packageInfo['packageDC'],0,"",$currency['dc'],0);
                                            $user->userCurrencyHistory($loggedInUser['userID'],$id,$packageInfo['packagePC'],'New account registration',9,16,$packageInfo['packagePC'],0,"",$currency['pc'],0);
                                            /**-------------------------------------------------------------------**/

                                            /**
                                             * --------- store client position on client_positions table-------------------------------------------------
                                             **/
                                            /**--------- get parent of each user in this loop------------------------------------------------------------**/
                                            foreach ($user->getUserParent($id) as $d) {
                                                
                                                $client->storePosition($d['parent_id'], $id, $d['hand'], $_POST['client_level']);
                                                
                                            //     /**-----------------------start of bonus insert----------------------------------------------------**/
                                            
                                                $sql = "select user_id from client_positions where parent_id=:parent_id and (hand=0 or hand=1) and client_level=:client_level order by user_positionID desc limit 2";
                                                $stmt = \App\DBConnection::myQuery($sql);
                                                $stmt->bindValue(':parent_id', $d['parent_id']);
                                                $stmt->bindValue(':client_level', $_POST['client_level']);
                                                $stmt->execute();
                                                $res1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                                            
                                            if (count($res1) == 2) {
                                                
                                            // if (($res1[0]['hand'] != 0 && $res1[1]['hand'] != 0) && ($res1[0]['hand'] != 1 && $res1[1]['hand'] != 1) ) {
                                            
                                            // if (($res1[0]['hand'] == 0 && $res1[1]['hand'] == 1) || ($res1[0]['hand'] == 1 && $res1[1]['hand'] == 0) )
                                            
                                                    $sql = "select user_id,hand,client_level from client_positions where parent_id=:parent_id and client_level=:client_level order by user_positionID desc limit 2";
                                                    /** -1 means the root user's main hand so it will not be used **/

                                                    $stmt = \App\DBConnection::myQuery($sql);
                                                    $stmt->bindValue(':parent_id', $d['parent_id']);
                                                    $stmt->bindValue(':client_level', $_POST['client_level']);
                                                    $stmt->execute();
                                                    $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                                                    $left_user = '';
                                                    $right_user = '';

                                                    if ($data[0]['hand'] == 0) {
                                                        $left_user = $data[0]['user_id'];
                                                    } else if ($data[0]['hand'] == 1) {
                                                        $right_user = $data[0]['user_id'];
                                                    }

                                                    if ($data[1]['hand'] == 0) {
                                                        $left_user = $data[1]['user_id'];
                                                    } else if ($data[1]['hand'] == 1) {
                                                        $right_user = $data[1]['user_id'];
                                                    }

//                                                /**--------------------------------------------------------------------------**/

                                                    /**------------count parent's total level bonus-----------------------------**/
                                            if ($user->countLevelBonus($d['parent_id'])+1 <= 5) {
                                                    
                                                            $sql="select count(userID) as total from bonus where  userID=:userID and userID!=:parent and bonus_date=:bonus_date and bonus_type=1";
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':userID',$d['parent_id']);
                                                            $stmt->bindValue(':parent',$id);
                                                            $stmt->bindValue(':bonus_date',date('Y-m-d'));
                                                            $stmt->execute();
                                                            $res=$stmt->fetch(PDO::FETCH_ASSOC);

                                                           // $temp[]=$d['parent_id'];

                                                            if($res['total']==0)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 150;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                    // if ($user->checkDataExistance($d['parent_id'], $data['client_level'], $bonus_type)) {
                                                                        
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        //$user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        
                                                                        /**------------- store amounts in user_amounts table---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                    
                                                                }

                                                            }else if($res['total']==1)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 100;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    //if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }else if($res['total']==2)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 75;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    //if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                    if ($user->checkDataExistance($d['parent_id'],$_POST['client_level'], $bonus_type)) {
                                                                        
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 0;
                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    //if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                    if ($user->checkDataExistance($d['parent_id'],$_POST['client_level'], $bonus_type)) {
                                                                        
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }
                                                    }
                                            }
                                                
                                                
                                                
                                                /**-------------end of bonus insert--------------------------------------**/
                                            }
                                            /**
                                             * ---------------------------------------------------------------------------------------------------------
                                             **/

                                            $valid=3;
                                            $username=$_POST['username'];
                                            $myemail=$_POST['email'];
                                            $dc=$packageInfo['packageDC'];
                                            $pc=$packageInfo['packagePC'];
                                            $rf=$packageInfo['packageRF'];
                                            /**---------------------send mail---------------------------------------**/
                                             $body.="
                                                        <tbody><tr><td>
                                                            <table cellspacing='0' border='0' id='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432email-content' cellpadding='0' width='624'>
                                                                <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td valign='top' align='center'>
                                                                                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                                                                        <tbody><tr>
                                                                                            <td valign='bottom'></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>";
                                                                                    $body.="
                                                                                    <h1>Welcome to <span style='font-style:italic'>Mon Space</span></h1>
                                                                                    <br><br>
                                                                                    <h1>Dear <span style='font-style: italic'>Mon Space Valued Client</span>,</h1>
                                                                                    <p> Mon Space would like to welcome you as a new customer to be part of us.</p>
                                                                                    <p> You may start your simple profitable trading experience now.</p>
                                                                                    <br/>
                                                                                    <p> Below, you'll find your account information.</p>
                                                
                                                                                    <table border='0' cellpadding='2' cellspacing='0'>
                                                                                        <tbody><tr>
                                                                                            <td><b>Username</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$_POST['username']."</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Account Password</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$val."</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Security Password</b></td>
                                                                                            <td>:</td>
                                                                                            <td><b>".$val1."</b></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <br>
                                                                                    <p class='m_5582344118505344309m_-3704794746695460098m_-6310472560369449432text'>
                                                                                        For any additional assistance, feel free to contact our support team via email at<br>
                                                                                        <span style='color:#FFFF00'>support@client.bmsm4all.com</span>.
                                                                                    </p>
                                                                                    <br>
                                                                                    <br>
                                                                                    <p>Sincerely,</p>
                                                                                    <p>Mon Space(M)Sdn. Bhd.</p>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table cellspacing='0' border='0' cellpadding='0' width='100%'>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td height='72'></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                ";

                                                $headers = array(
                                                    'From' => 'support@bmsm4all.com'
                                                );

                                            $emailHelper->setupEmail('mail.bmsm4all.com', 'support@bmsm4all.com', '##support123##', '587', 'tls', 'support@client.bmsm4all.com', 'Support', true);

                                            //$to, $name, $subject, $message, $header=null, $isHTML=true
                                            $emailHelper->sendMail($_POST['email'],'Support','Monspacea- Thank You for Your Registration', $body, null, true);
                                            /**--------------------------------------------------------------------------**/
                                        }
                                    } else {
                                        \App\Session::set('error', 'This username has been already taken.');
                                    }

                                    /**
                                     * ----------------------------------------------------------------------------------------------------------
                                     **/

                                    /**----------------------------- get info of parent user------------------------------------- **/
                                    $info = $user->placementUserDetailsByID($id);
                                    $root_parent = $id;
                                    /** ----------------------------------------------------------------------------------------- **/
                                    
                                    
                                    
                                    
                                    /**---------------- loop for adding 10 users( package 3, 11 users) for this parent user-------------------------- **/
                                    for ($i = 1; $i <= 5; $i++) {

                                        // $val=$user->randomPassword();
                                        // $val1=$user->randomSecPassword();
                                        $_POST['password'] = base64_encode($val);
                                        $_POST['security_password'] = base64_encode($val1);

                                        $_POST['hand'] = $_POST['hand'];
                                        $_POST['username'] = $info['username'] . $i;
                                        $user->set($_POST);
                                        
                                        
                                        
                                        if ($id = $user->addNewUser($_POST['parent'])) {
                                            
                                            $user->storeParentBonus($info['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], 0,$id, 3);
                                            $user->storeElectronicCurrency($info['userID'],  $user->getBonusInfo(4)['amount']);
                                            /**--------------------------------------------------------------**/
                                            $currency=$user->userCurrencies($info['userID']);
                                            /**--------------------------------------------------------------**/
                                            /**----------------------------------------------------------------------**/
                                            $user->userCurrencyHistory($info['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus ('.$_POST['username'].')',4,12,0,$user->getBonusInfo(4)['amount'],"",$currency['ec'],0);


                                            $_POST['userID'] = $id;

                                            $sql = "select userID,client_level from clients where userID = (select max(userID) from clients where userID < $id)";
                                            $stmt = \App\DBConnection::myQuery($sql);
                                            $stmt->execute();
                                            $result = $stmt->fetch(PDO::FETCH_ASSOC);

                                            $_POST['parentid'] = $result['userID'];
                                            $_POST['client_level'] = $result['client_level'] + 1;
                                            //$_POST['client_level'] = $result['client_level'];

                                            $_POST['hand'] = 0;

                                            if ($i == 1) {
                                                $_POST['parentid'] = $info['userID'];
                                                $_POST['client_level'] = $user->placementUserLevel($info['username'])['client_level'] + 1;
                                                //$_POST['client_level'] = $user->placementUserLevel($info['username'])['client_level'];
                                            }

                                            $_POST['sponsorid'] = $root_parent;
                                            /** system root user will be the sponsor id **/
                                            /**
                                             *------- find out placement users level and increment 1 with the value for new client-------------------------------
                                             **/
                                            $_POST['packageID'] = $_POST['package'];

                                            $_POST['root_parent'] = $root_parent;
                                            /**
                                             * -------------------------------------------------------------------------------------------------------------
                                             **/

                                            /**----------------------------user bonus history--------------------------------------------**/
                                            //$user->storeParentBonus($info['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], $id, 0, 3);/**----  3 means introductory bonus (EC)**/
                                        
                                          //$currency=$user->userCurrencies(\App\Session::get('userID'));

                                          // $user->userCurrencyHistory($info['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus',4,12,0,$user->getBonusInfo(4)['amount'],"IC Bonus",$currency['ec']);
                                            
                                            /**--------------------------------------------------------------------------------------------**/
                                            $client->set($_POST);

                                            if ($client->addNewClient(0, $packageInfo['packageDC'], $packageInfo['packagePC'])) {
                                                /**
                                                 * --------- store client position on client_positions table-------------------------------------------------
                                                 **/
                                                /**--------- get parent of each user in this loop------------------------------------------------------------**/
                                                foreach ($user->getUserParent($id) as $d) {


                                                    $client->storePosition($d['parent_id'], $id, $d['hand'], $_POST['client_level']);
                                                    /**-----------------------start of bonus insert----------------------------------------------------**/
                                                    $sql = "select user_id from client_positions where parent_id=:parent_id and (hand=0 or hand=1) and client_level=:client_level order by user_positionID desc limit 2";
                                                    $stmt = \App\DBConnection::myQuery($sql);
                                                    $stmt->bindValue(':parent_id', $d['parent_id']);
                                                    $stmt->bindValue(':client_level', $_POST['client_level']);
                                                    $stmt->execute();
                                                    $res1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                                                    if (count($res1) == 2) {
                                                        $sql = "select user_id,hand,client_level from client_positions where parent_id=:parent_id and client_level=:client_level order by user_positionID desc limit 2";
                                                        /** -1 means the root user's main hand so it will not be used **/

                                                        $stmt = \App\DBConnection::myQuery($sql);
                                                        $stmt->bindValue(':parent_id', $d['parent_id']);
                                                        $stmt->bindValue(':client_level', $_POST['client_level']);
                                                        $stmt->execute();
                                                        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                                                        $left_user = '';
                                                        $right_user = '';

                                                        if ($data[0]['hand'] == 0) {
                                                            $left_user = $data[0]['user_id'];
                                                        } else if ($data[0]['hand'] == 1) {
                                                            $right_user = $data[0]['user_id'];
                                                        }

                                                        if ($data[1]['hand'] == 0) {
                                                            $left_user = $data[1]['user_id'];
                                                        } else if ($data[1]['hand'] == 1) {
                                                            $right_user = $data[1]['user_id'];
                                                        }

//                                                    /**--------------------------------------------------------------------------**/
                                                        if ($user->countLevelBonus($d['parent_id']) + 1 <= 5) {

                                                            $sql="select count(userID) as total from bonus where  userID=:userID and userID!=:parent and bonus_date=:bonus_date and bonus_type=1";
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':userID',$d['parent_id']);
                                                            $stmt->bindValue(':parent',$root_parent);
                                                            $stmt->bindValue(':bonus_date',date('Y-m-d'));
                                                            $stmt->execute();
                                                            $res=$stmt->fetch(PDO::FETCH_ASSOC);

                                                            $temp[]=$d['parent_id'];

                                                            if($res['total']==0)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 150;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }

                                                            }else if($res['total']==1)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 100;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        //$userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }else if($res['total']==2)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 75;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 0;
                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                    /**-------------end of bonus insert-----------------------------------------------------------------**/
                                                }
                                                /**
                                                 * -----------------------------------------------------------------------------------------------------------
                                                 **/
                                            }

                                        } else {
                                            \App\Session::set('error', 'Something went wrong user ');
                                        }

                                    }

                                    for ($i = 6; $i <= 10; $i++) {

                                        // $val=$user->randomPassword();
                                        // $val1=$user->randomSecPassword();
                                        $_POST['password'] = base64_encode($val);
                                        $_POST['security_password'] = base64_encode($val1);

                                        $_POST['hand'] = $_POST['hand'];
                                        $_POST['username'] = $info['username'] . $i;
                                        $user->set($_POST);
                                        if ($id = $user->addNewUser($_POST['parent'])) {
                                            
                                            $user->storeParentBonus($info['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], 0,$id, 3);
                                            $user->storeElectronicCurrency($info['userID'],  $user->getBonusInfo(4)['amount']);
                                            
                                            /**--------------------------------------------------------------**/
                                            $currency=$user->userCurrencies($info['userID']);
                                            /**--------------------------------------------------------------**/
                                            /**----------------------------------------------------------------------**/
                                            $user->userCurrencyHistory($info['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus ('.$_POST['username'].')',4,12,0,$user->getBonusInfo(4)['amount'],"",$currency['ec'],0);


                                            $_POST['userID'] = $id;

                                            $sql = "select userID,client_level from clients where userID = (select max(userID) from clients where userID < $id)";
                                            $stmt = \App\DBConnection::myQuery($sql);
                                            $stmt->execute();
                                            $result = $stmt->fetch(PDO::FETCH_ASSOC);

                                            $_POST['parentid'] = $result['userID'];
                                            $_POST['client_level'] = $result['client_level'] + 1;
                                            //$_POST['client_level'] = $result['client_level'];

                                            $_POST['hand'] = 1;

                                            if ($i == 6) {
                                                $_POST['parentid'] = $info['userID'];
                                                $_POST['client_level'] = $user->placementUserLevel($info['username'])['client_level'] + 1;
                                                //$_POST['client_level'] = $user->placementUserLevel($info['username'])['client_level'] ;
                                            }

                                            $_POST['sponsorid'] = $root_parent;

                                            /** system root user will be the sponsor id **/
                                            /**
                                             *------- find out placement users level and increment 1 with the value for new client-------------------------------
                                             **/
                                            $_POST['packageID'] = $_POST['package'];

                                            $_POST['root_parent'] = $root_parent;
                                            /**
                                             * -------------------------------------------------------------------------------------------------------------
                                             **/
                                            $client->set($_POST);

                                            /**----------------------------user bonus history--------------------------------------------**/
                                            //$user->storeParentBonus($info['userID'], $user->getBonusInfo(4)['amount'], $_POST['client_level'], 0,$id, 3);/**----  3 means introductory bonus (EC)**/
                                            
                                            //$user->userCurrencyHistory($info['userID'],0,$user->getBonusInfo(4)['amount'],'Introducer Bonus',4,12,0,$user->getBonusInfo(4)['amount'],"IC Bonus");
                                            
                                            /**--------------------------------------------------------------------------------------------**/

                                            if ($client->addNewClient(0, $packageInfo['packageDC'], $packageInfo['packagePC'])) {
                                                /**
                                                 * --------- store client position on client_positions table-------------------------------------------------
                                                 **/
                                                /**--------- get parent of each user in this loop------------------------------------------------------------**/
                                                foreach ($user->getUserParent($id) as $d) {
                                                    $client->storePosition($d['parent_id'], $id, $d['hand'], $_POST['client_level']);
                                                    /**-----------------------start of bonus insert----------------------------------------------------**/
                                                    $sql = "select user_id from client_positions where parent_id=:parent_id and (hand=0 or hand=1) and client_level=:client_level order by user_positionID desc limit 2";
                                                    $stmt = \App\DBConnection::myQuery($sql);
                                                    $stmt->bindValue(':parent_id', $d['parent_id']);
                                                    $stmt->bindValue(':client_level', $_POST['client_level']);
                                                    $stmt->execute();
                                                    $res1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                                                    if (count($res1) == 2) {
                                                        $sql = "select user_id,hand,client_level from client_positions where parent_id=:parent_id and client_level=:client_level order by user_positionID desc limit 2";
                                                        /** -1 means the root user's main hand so it will not be used **/

                                                        $stmt = \App\DBConnection::myQuery($sql);
                                                        $stmt->bindValue(':parent_id', $d['parent_id']);
                                                        $stmt->bindValue(':client_level', $_POST['client_level']);
                                                        $stmt->execute();
                                                        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                                                        $left_user = '';
                                                        $right_user = '';

                                                        if ($data[0]['hand'] == 0) {
                                                            $left_user = $data[0]['user_id'];
                                                        } else if ($data[0]['hand'] == 1) {
                                                            $right_user = $data[0]['user_id'];
                                                        }

                                                        if ($data[1]['hand'] == 0) {
                                                            $left_user = $data[1]['user_id'];
                                                        } else if ($data[1]['hand'] == 1) {
                                                            $right_user = $data[1]['user_id'];
                                                        }


                                                        if ($user->countLevelBonus($d['parent_id']) + 1 <= 5 ) {


                                                            $sql="select count(userID) as total from bonus where  userID=:userID and userID!=:parent and bonus_date=:bonus_date and bonus_type=1";
                                                            $stmt=\App\DBConnection::myQuery($sql);
                                                            $stmt->bindValue(':userID',$d['parent_id']);
                                                            $stmt->bindValue(':parent',$root_parent);
                                                            $stmt->bindValue(':bonus_date',date('Y-m-d'));
                                                            $stmt->execute();
                                                            $res=$stmt->fetch(PDO::FETCH_ASSOC);


                                                            if($res['total']==0)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 150;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }

                                                            }else if($res['total']==1)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 100;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }else if($res['total']==2)
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 75;

                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }else
                                                            {
                                                                $bonus_type = 1;
                                                                /** ------------ 1 means level bonus --------------------**/
                                                                $bonus_point = 0;
                                                                if ($left_user != 0 && $right_user != 0) {
                                                                    if ($user->checkDataExistance($d['parent_id'], $_POST['client_level'], $bonus_type)) {
                                                                        $user->storeParentBonus($d['parent_id'], $bonus_point, $_POST['client_level'], $left_user, $right_user, $bonus_type);
                                                                        /**------------- store amounts in user_amounts table ---------------------**/
                                                                        $user->storeElectronicCurrency($d['parent_id'], $bonus_point);
                                                                        /**----------------------------------------------------------------------**/
                                                                        $currency=$user->userCurrencies($d['parent_id']);
                                                                        $user->userCurrencyHistory($d['parent_id'],0,$bonus_point,'Leveling Bonus',13,17,0,$bonus_point,"Leveling Bonus",$currency['ec'],0);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                    /**-------------end of bonus insert-----------------------------------------------------------------**/
                                                }
                                                /**
                                                 * -----------------------------------------------------------------------------------------------------------
                                                 **/
                                            }

                                        } else {
                                            \App\Session::set('error', "Username ".$_POST['username']." already exist.");
                                        }
                                    }
                                    
                                    /**-------------------------end loop------------------------**/
                                    
                                    \App\DBConnection::myDb()->commit();
                                    }catch (PDOException $exp)
                                    {
                                        \App\DBConnection::myDb()->rollBack();
                                        \App\Session::set('error', $exp->getMessage());

                                    }

                                } else {
                                    \App\Session::set('error', 'You have not enough currency to add this user.');
                                }
                                /**----------------------end of currency checking----------------------**/
                            }

                        } else {
                            \App\Session::set('error', "The ".($_POST['hand']==0?'A':'B')." leg of placement ID ".$_POST['parent']." has been placed. ");
                        }
                    } else {
                        \App\Session::set('error', "Both leg of placement ID ".$_POST['parent']." has been placed. ");
                    }
                }else 
                {
                    \App\Session::set('error', "Username ".$_POST['username']." already exist.");
                }
            }
        }else
        {
            \App\Session::set('error', 'You have entered an invalid User Security Password.');
        }
        
         $currency=$user->userCurrencies(\App\Session::get('userID'));
    }

   


    ?>

    <?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
        <!-- PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <!-- STYLER -->

                    <!-- /STYLER -->
                    <!-- BREADCRUMBS -->
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="members/index.php">Home</a>
                        </li>

                        <li>Registration</li>

                    </ul>
                    <!-- /BREADCRUMBS -->
                    <div class="clearfix">
                        <h3 class="content-title pull-left">Registration</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PAGE HEADER -->


        <div class='page-content'>
            <div id='tab-general'>
                <div id='tab-shopping'>
                    <div class='row'>
                        <div class='col-md-3'>
                            <div class='panel'>
                                <div class='panel-body'>
                                    <div class='lifetime-sales'>
                                        <div class='ls-total text-blue text-center'><?php echo (empty($currency['dc'])?0.00:number_format($currency['dc'],2))?></div>
                                        <div class='ls-title text-blue text-center'>Declaration Currency</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <div class='panel'>
                                <div class='panel-body'>
                                    <div class='lifetime-sales'>
                                        <div class='ls-total text-blue text-center'><?php echo (empty($currency['rf'])?0.00:number_format($currency['rf'],2))?></div>
                                        <div class='ls-title text-blue text-center'>Registration Fee</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <div class='panel'>
                                <div class='panel-body'>
                                    <div class='lifetime-sales'>
                                        <div class='ls-total text-blue text-center'><?php echo (empty($currency['pc'])?0.00:number_format($currency['pc'],2))?></div>
                                        <div class='ls-title text-blue text-center'>Product Currency</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-12'>
                        <div class='box border'>
                            <div class='box-title'>
                                <h4 style='height:15px;'></h4>
                            </div>
                            <div class='box-body'>
                                <div class='tabbable header-tabs user-profile'>
                                    <ul class='nav nav-tabs'>
                                        <li ><a href='members/upgrade.php'>Account Upgrade</a></li>
                                        <li ><a href='members/renew.php'>Account Renewal</a></li>
                                        <li class='active'><a href='members/register.php'>Registration</a></li>
                                    </ul>
                                    <div class='tab-content'>
                                        <div class='tab-pane fade in active'>
                                            <div class='row registration'>
                                                
                                                <?php if(!empty($error)) {?>
                                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                        <div class="col-md-12 profile-details">
                                                            
                                                            <div class="alert alert-block alert-danger fade in">
                                                                <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                
                                                                <?php foreach($error as $err){?>
                                                                    <p><?php echo $err?></p>
                                                                <?php }?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                <?php }?>
                                                
                                                <?php if(\App\Session::get('error')) {?>
                                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                        <div class="col-md-12 profile-details">
                                                            
                                                            <div class="alert alert-block alert-danger fade in">
                                                                <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                                <p><?php echo \App\Session::get('error')?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    \App\Session::UnsetKeySession('error');
                                                }?>
                                                
                                                 <?php
                                                if(\App\Session::get('inputarray')) { ?>
                                                <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                    <div class="col-md-12 profile-details">
                                                        
                                                        <div class="alert alert-block alert-danger fade in">
                                                            <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>

                                                        <?php foreach (\App\Session::get('inputarray') as $error) {
                                                        ?>
                                                                    <p><?php echo $error ?></p>
                                                    <?php \App\Session::UnsetKeySession('inputarray');} ?>
                                                    </div>
                                                    </div>
                                                    </div>
                                                <?php }?>


                                                <?php if($valid==1){?>

                                                <div class='col-md-12'>
                                                    <div class='col-md-12'>
                                                        <form name='register_form' id='register_form' method='post' action='' enctype='multipart/form-data' class='form-horizontal form-bordered form-row-stripped'>
                                                            <input type='hidden' name='__req' value='1' />

                                                            <div class='form-body pal'>

                                                                <br/>
                                                                <h4>Particulars</h4>
                                                                <div class='form-group' id='_name'>
                                                                    <label class='col-md-3 control-label'>Full Name: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='full_name' class='form-control' value='<?php if(isset($_POST['full_name'])) { echo $_POST['full_name']; }?>'  />
                                                                    </div>
                                                                </div>


                                                                <div class='form-group' id='_nick_name'>
                                                                    <label class='col-md-3 control-label'>Nickname: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='nick_name' class='form-control' value='<?php if(isset($_POST['nick_name'])) { echo $_POST['nick_name']; }?>'  />
                                                                    </div>
                                                                </div>


                                                                <div class='form-group' id='_icno'>
                                                                    <label class='col-md-3 control-label'>IC No.: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='IC_no' class='form-control' value='<?php if(isset($_POST['IC_no'])) { echo $_POST['IC_no']; }?>'  />
                                                                    </div>
                                                                </div>


                                                                <div class='form-group' id='_wechat_id'>
                                                                    <label class='col-md-3 control-label'>Wechat ID:</label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='wechat_ID' class='form-control' value='<?php if(isset($_POST['wechat_ID'])) { echo $_POST['wechat_ID']; }?>'  />
                                                                    </div>
                                                                </div>


                                                                <div class='form-group' id='_mobileno'>
                                                                    <label class='col-md-3 control-label'>Contact Number: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='contact_no' class='form-control' value='<?php if(isset($_POST['contact_no'])) { echo $_POST['contact_no']; }?>'  />
                                                                    </div>
                                                                </div>


                                                                <div class='form-group' id='_email'>
                                                                    <label class='col-md-3 control-label'>Email: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='email' name='email' class='form-control' value='<?php if(isset($_POST['email'])) { echo $_POST['email']; }?>'  />
                                                                    </div>
                                                                </div>


                                                                <div class='form-group' id='_address'>
                                                                    <label class='col-md-3 control-label'>Address: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='address' class='form-control' value='<?php if(isset($_POST['address'])) { echo $_POST['address']; }?>'   />
                                                                    </div>
                                                                </div>


                                                                <div class='form-group' id='_country'>
                                                                    <label class='col-md-3 control-label'>Country: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <select class='form-control' name="country" ><option value="" selected="selected" >Please Select</option>
                                                                        <option value="AF" <?php if(isset($_POST['country'] ) && $_POST['country']=='AF') {echo 'selected';}?> >Afghanistan</option>
                                                                        <option value="AX" <?php if(isset($_POST['country'] ) && $_POST['country']=='AX') {echo 'selected';}?>>&#197;land Islands</option><option value="AL" >Albania</option>
                                                                        <option value="DZ" <?php if(isset($_POST['country'] ) && $_POST['country']=='DZ') {echo 'selected';}?> >Algeria</option><option value="AS" >American Samoa</option>
                                                                        <option value="AD" <?php if(isset($_POST['country'] ) && $_POST['country']=='AD') {echo 'selected';}?> >Andorra</option><option value="AO" >Angola</option>
                                                                        <option value="AI" <?php if(isset($_POST['country'] ) && $_POST['country']=='AI') {echo 'selected';}?>>Anguilla</option>
                                                                        <option value="AQ" <?php if(isset($_POST['country'] ) && $_POST['country']=='AQ') {echo 'selected';}?>>Antarctica</option>
                                                                        <option value="AG" <?php if(isset($_POST['country'] ) && $_POST['country']=='AG') {echo 'selected';}?>>Antigua and Barbuda</option>
                                                                        <option value="AR" <?php if(isset($_POST['country'] ) && $_POST['country']=='AR') {echo 'selected';}?>>Argentina</option>
                                                                        <option value="AM" <?php if(isset($_POST['country'] ) && $_POST['country']=='AM') {echo 'selected';}?>>Armenia</option>
                                                                        <option value="AW" <?php if(isset($_POST['country'] ) && $_POST['country']=='AW') {echo 'selected';}?>>Aruba</option>
                                                                        <option value="AU" <?php if(isset($_POST['country'] ) && $_POST['country']=='AU') {echo 'selected';}?>>Australia</option>
                                                                        <option value="AT" <?php if(isset($_POST['country'] ) && $_POST['country']=='AT') {echo 'selected';}?>>Austria</option>
                                                                        <option value="AZ" <?php if(isset($_POST['country'] ) && $_POST['country']=='AZ') {echo 'selected';}?>>Azerbaijan</option>
                                                                        <option value="BS" <?php if(isset($_POST['country'] ) && $_POST['country']=='BS') {echo 'selected';}?>>Bahamas</option>
                                                                        <option value="BH" <?php if(isset($_POST['country'] ) && $_POST['country']=='BH') {echo 'selected';}?>>Bahrain</option>
                                                                        <option value="BD" <?php if(isset($_POST['country'] ) && $_POST['country']=='BD') {echo 'selected';}?>>Bangladesh</option>
                                                                        <option value="BB" <?php if(isset($_POST['country'] ) && $_POST['country']=='BB') {echo 'selected';}?>>Barbados</option>
                                                                        <option value="BY" <?php if(isset($_POST['country'] ) && $_POST['country']=='BY') {echo 'selected';}?>>Belarus</option>
                                                                        <option value="BE" <?php if(isset($_POST['country'] ) && $_POST['country']=='BE') {echo 'selected';}?>>Belgium</option>
                                                                        <option value="BZ" <?php if(isset($_POST['country'] ) && $_POST['country']=='BZ') {echo 'selected';}?>>Belize</option>
                                                                        <option value="BJ" <?php if(isset($_POST['country'] ) && $_POST['country']=='BJ') {echo 'selected';}?>>Benin</option>
                                                                        <option value="BM" <?php if(isset($_POST['country'] ) && $_POST['country']=='BM') {echo 'selected';}?>>Bermuda</option>
                                                                        <option value="BT" <?php if(isset($_POST['country'] ) && $_POST['country']=='BT') {echo 'selected';}?>>Bhutan</option>
                                                                        <option value="BO" <?php if(isset($_POST['country'] ) && $_POST['country']=='BO') {echo 'selected';}?>>Bolivia</option>
                                                                        <option value="BA" <?php if(isset($_POST['country'] ) && $_POST['country']=='BA') {echo 'selected';}?>>Bosnia and Herzegovina</option>
                                                                        <option value="BW" <?php if(isset($_POST['country'] ) && $_POST['country']=='BW') {echo 'selected';}?>>Botswana</option>
                                                                        <option value="BV" <?php if(isset($_POST['country'] ) && $_POST['country']=='BV') {echo 'selected';}?>>Bouvet Island</option>
                                                                        <option value="BR" <?php if(isset($_POST['country'] ) && $_POST['country']=='BR') {echo 'selected';}?>>Brazil</option>
                                                                        <option value="IO" <?php if(isset($_POST['country'] ) && $_POST['country']=='IO') {echo 'selected';}?>>British Indian Ocean Territory</option>
                                                                        <option value="BN" <?php if(isset($_POST['country'] ) && $_POST['country']=='BN') {echo 'selected';}?>>Brunei Darussalam</option><option value="BG" >Bulgaria</option><option value="BF" >Burkina Faso</option><option value="BI" >Burundi</option><option value="KH" >Cambodia</option><option value="CM" >Cameroon</option><option value="CA" >Canada</option><option value="CV" >Cape Verde</option><option value="KY" >Cayman Islands</option><option value="CF" >Central African Republic</option><option value="TD" >Chad</option><option value="CL" >Chile</option><option value="CN" >China</option><option value="CX" >Christmas Island</option><option value="CC" >Cocos (Keeling) Islands</option><option value="CO" >Colombia</option><option value="KM" >Comoros</option><option value="CG" >Congo</option><option value="CD" >Congo, The Democratic Republic of The</option><option value="CK" >Cook Islands</option><option value="CR" >Costa Rica</option><option value="CI" >C&#244;te D'ivoire</option><option value="HR" >Croatia</option><option value="CU" >Cuba</option><option value="CY" >Cyprus</option><option value="CZ" >Czech Republic</option><option value="DK" >Denmark</option><option value="DJ" >Djibouti</option><option value="DM" >Dominica</option><option value="DO" >Dominican Republic</option><option value="EC" >Ecuador</option><option value="EG" >Egypt</option><option value="SV" >El Salvador</option><option value="GQ" >Equatorial Guinea</option><option value="ER" >Eritrea</option><option value="EE" >Estonia</option><option value="ET" >Ethiopia</option><option value="FK" >Falkland Islands (Malvinas)</option><option value="FO" >Faroe Islands</option><option value="FJ" >Fiji</option><option value="FI" >Finland</option><option value="FR" >France</option><option value="GF" >French Guiana</option><option value="PF" >French Polynesia</option><option value="TF" >French Southern Territories</option><option value="GA" >Gabon</option><option value="GM" >Gambia</option><option value="GE" >Georgia</option><option value="DE" >Germany</option><option value="GH" >Ghana</option><option value="GI" >Gibraltar</option><option value="GR" >Greece</option><option value="GL" >Greenland</option><option value="GD" >Grenada</option><option value="GP" >Guadeloupe</option><option value="GU" >Guam</option><option value="GT" >Guatemala</option><option value="GG" >Guernsey</option><option value="GN" >Guinea</option><option value="GW" >Guinea-bissau</option><option value="GY" >Guyana</option><option value="HT" >Haiti</option><option value="HM" >Heard Island and Mcdonald Islands</option><option value="VA" >Holy See (Vatican City State)</option><option value="HN" >Honduras</option><option value="HK" >Hong Kong</option><option value="HU" >Hungary</option><option value="IS" >Iceland</option><option value="IN" >India</option><option value="ID" >Indonesia</option><option value="IR" >Iran, Islamic Republic Of</option><option value="IQ" >Iraq</option><option value="IE" >Ireland</option><option value="IM" >Isle of Man</option><option value="IL" >Israel</option><option value="IT" >Italy</option><option value="JM" >Jamaica</option><option value="JP" >Japan</option><option value="JE" >Jersey</option><option value="JO" >Jordan</option><option value="KZ" >Kazakhstan</option><option value="KE" >Kenya</option><option value="KI" >Kiribati</option><option value="KP" >Korea, Democratic People's Republic Of</option><option value="KR" >Korea, Republic Of</option><option value="KW" >Kuwait</option><option value="KG" >Kyrgyzstan</option><option value="LA" >Lao People's Democratic Republic</option><option value="LV" >Latvia</option><option value="LB" >Lebanon</option><option value="LS" >Lesotho</option><option value="LR" >Liberia</option><option value="LY" >Libyan Arab Jamahiriya</option><option value="LI" >Liechtenstein</option><option value="LT" >Lithuania</option><option value="LU" >Luxembourg</option><option value="MO" >Macao</option><option value="MK" >Macedonia, The Former Yugoslav Republic Of</option><option value="MG" >Madagascar</option><option value="MW" >Malawi</option><option value="MY" >Malaysia</option><option value="MV" >Maldives</option><option value="ML" >Mali</option><option value="MT" >Malta</option><option value="MH" >Marshall Islands</option><option value="MQ" >Martinique</option><option value="MR" >Mauritania</option><option value="MU" >Mauritius</option><option value="YT" >Mayotte</option><option value="MX" >Mexico</option><option value="FM" >Micronesia, Federated States Of</option><option value="MD" >Moldova, Republic Of</option><option value="MC" >Monaco</option><option value="MN" >Mongolia</option><option value="ME" >Montenegro</option><option value="MS" >Montserrat</option><option value="MA" >Morocco</option><option value="MZ" >Mozambique</option><option value="MM" >Myanmar</option><option value="NA" >Namibia</option><option value="NR" >Nauru</option><option value="NP" >Nepal</option><option value="NL" >Netherlands</option><option value="AN" >Netherlands Antilles</option><option value="NC" >New Caledonia</option><option value="NZ" >New Zealand</option><option value="NI" >Nicaragua</option><option value="NE" >Niger</option><option value="NG" >Nigeria</option><option value="NU" >Niue</option><option value="NF" >Norfolk Island</option><option value="MP" >Northern Mariana Islands</option><option value="NO" >Norway</option><option value="OM" >Oman</option><option value="PK" >Pakistan</option><option value="PW" >Palau</option><option value="PS" >Palestinian Territory, Occupied</option><option value="PA" >Panama</option><option value="PG" >Papua New Guinea</option><option value="PY" >Paraguay</option><option value="PE" >Peru</option><option value="PH" >Philippines</option><option value="PN" >Pitcairn</option><option value="PL" >Poland</option><option value="PT" >Portugal</option><option value="PR" >Puerto Rico</option><option value="QA" >Qatar</option><option value="RE" >R&#233;union</option><option value="RO" >Romania</option><option value="RU" >Russian Federation</option><option value="RW" >Rwanda</option><option value="BL" >Saint Barth&#233;lemy</option><option value="SH" >Saint Helena</option><option value="KN" >Saint Kitts and Nevis</option><option value="LC" >Saint Lucia</option><option value="MF" >Saint Martin</option><option value="PM" >Saint Pierre and Miquelon</option><option value="VC" >Saint Vincent and The Grenadines</option><option value="WS" >Samoa</option><option value="SM" >San Marino</option><option value="ST" >Sao Tome and Principe</option><option value="SA" >Saudi Arabia</option><option value="SN" >Senegal</option><option value="RS" >Serbia</option><option value="SC" >Seychelles</option><option value="SL" >Sierra Leone</option><option value="SG" >Singapore</option><option value="SK" >Slovakia</option><option value="SI" >Slovenia</option><option value="SB" >Solomon Islands</option><option value="SO" >Somalia</option><option value="ZA" >South Africa</option><option value="GS" >South Georgia and The South Sandwich Islands</option><option value="ES" >Spain</option><option value="LK" >Sri Lanka</option><option value="SD" >Sudan</option><option value="SR" >Suriname</option><option value="SJ" >Svalbard and Jan Mayen</option><option value="SZ" >Swaziland</option><option value="SE" >Sweden</option><option value="CH" >Switzerland</option><option value="SY" >Syrian Arab Republic</option><option value="TW" >Taiwan</option><option value="TJ" >Tajikistan</option><option value="TZ" >Tanzania, United Republic Of</option><option value="TH" >Thailand</option><option value="TL" >Timor-leste</option><option value="TG" >Togo</option><option value="TK" >Tokelau</option><option value="TO" >Tonga</option><option value="TT" >Trinidad and Tobago</option><option value="TN" >Tunisia</option><option value="TR" >Turkey</option><option value="TM" >Turkmenistan</option><option value="TC" >Turks and Caicos Islands</option><option value="TV" >Tuvalu</option><option value="UG" >Uganda</option><option value="UA" >Ukraine</option><option value="AE" >United Arab Emirates</option><option value="GB" >United Kingdom</option><option value="US" >United States</option><option value="UM" >United States Minor Outlying Islands</option><option value="UY" >Uruguay</option><option value="UZ" >Uzbekistan</option><option value="VU" >Vanuatu</option><option value="VE" >Venezuela</option><option value="VN" >Viet Nam</option><option value="VG" >Virgin Islands, British</option><option value="VI" >Virgin Islands, U.s.</option><option value="WF" >Wallis and Futuna</option><option value="EH" >Western Sahara</option><option value="YE" >Yemen</option><option value="ZM" >Zambia</option><option value="ZW" >Zimbabwe</option></select>
                                                                    </div>
                                                                </div>

                                                                <br/>
                                                                <h4>Account Information</h4>

                                                                <div class='form-group' id='ref'>
                                                                    <label class='col-md-3 control-label'>Upline Username: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='ref' class='form-control uplineuser'
                                                                        value='<?php 
                                                                        if(isset($_POST['ref']) && $_POST['ref']!='')
                                                                        {
                                                                            echo $_POST['ref'];
                                                                        }else
                                                                        {
                                                                            echo $info['username'];
                                                                        }
                                                                        ?>' 
                                                                        />

                                                                        <span class='help-block'>Please provide your upline ID here, if you do not have an upline ID, please contact us.</span>
                                                                        <span id='ref_text' class='help-block'>
                                                                        <span class='bold'>Upline Username <?php echo $info['username']?>, Name: <?php echo $info['full_name']?></span>
                                                                    </span>
                                                                        <input type='button' value="Check Upline" class='btn btn-info' id='check_ref_btn' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class='form-group' id='_username'>
                                                                <label class='col-md-3 control-label'>Username: <span class='require'>*</span></label>
                                                                <div class='col-md-4'>
                                                                    <input type='text' name='username' class='form-control' value='<?php 
                                                                    if(isset($_POST['username'])) { echo $_POST['username']; }
                                                                    ?>'
                                                                    /><br />
                                                                    Please provide only a to z and A to Z for username.
                                                                </div>
                                                            </div>

                                                            <br/>
                                                            <h4>Activation Information</h4>
                                                            <div class='form-group' id='package'>
                                                                <label class='col-md-3 control-label'>Package: <span class='require'>*</span></label>
                                                                <div class='col-md-4'>
                                                                    <select class='form-control package' name="package" >
                                                                        <option value="">Please Select</option>
                                                                        <?php foreach ($packageList as $list){?>
                                                                            <option <?php if(isset($_POST['package'] ) && $_POST['package']==$list['packageID']) {echo 'selected';}?> value="<?php echo $list['packageID']?>" data-rf="<?php echo $list['packageRF']?>" data-pc="<?php echo $list['packagePC']?>" data-dc="<?php echo $list['packageDC']?>"><?php echo $list['packageName']?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class='form-group' id='placement_type'>
                                                                <label class='col-md-3 control-label'>Placement Type: <span class='require'>*</span></label>
                                                                <div class='col-md-4'>
                                                                    <input type="radio" name="placement_type" value="m" checked="checked" id='radio_placement_type_m' /> <label for='radio_placement_type_m'>Manual Placement</label>
                                                                </div>
                                                            </div>

                                                            <?php if(isset($_GET['upline1']) && $_GET['upline1']!=''){?>
                                                                <div class='form-group' id='_upline1'>
                                                                    <label class='col-md-3 control-label'>Placement Username: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='parent' class='form-control' value='<?php if(isset($_GET['upline1']) && $_GET['upline1']!=''){echo $_GET['upline1'];}?>'  readonly="readonly"/>
                                                                    </div>
                                                                </div>
                                                            <?php } else {?>
                                                                <div class='form-group' id='_upline1'>
                                                                    <label class='col-md-3 control-label'>Placement Username: <span class='require'>*</span></label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' name='parent' class='form-control' value='<?php if(isset($_POST['parent'])) { echo $_POST['parent']; }?>'  />
                                                                    </div>
                                                                </div>
                                                            <?php }?>

                                                            <div class='form-group' id='_position1'>
                                                                <label class='col-md-3 control-label'>Placement Position: <span class='require'>*</span></label>
                                                                <div class='col-md-4'>
                                                                    <input <?php if(isset($_GET['position1']) && $_GET['position1']!='' && $_GET['position1']==0) {echo 'checked';}?> type="radio" name="hand" value="0" id='radio__position1_1' checked/>
                                                                    <label for='radio__position1_1'>A</label>
                                                                    <input <?php if(isset($_GET['position1']) && $_GET['position1']!='' && $_GET['position1']==1) {echo 'checked';}?> type="radio" name="hand" value="1"  id='radio__position1_2' />
                                                                    <label for='radio__position1_2'>B</label>
                                                                </div>
                                                            </div>

                                                            <br/>
                                                            <h4>Activation Cost</h4>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Your Security Password: *</label>
                                                                <div class='col-md-4'>
                                                                    <input type='password' name='sec_pwd' class='form-control' value="">
                                                                </div>
                                                            </div>
                                                            
                                                            <?php if(isset($_POST['package']) && $_POST['package']!='') {
                                                                
                                                                $packageInfo = $package->getCurrencyForPackage($_POST['package']);
                                                            
                                                            ?>
                                                            
                                                            <div class="packagedetails ">
                                                                <?php if( $packageInfo['packageDC']!=0) {?>
                                                                    <div class='form-group dc'>
                                                                        <label class='col-md-3 control-label'>Declaration Currency to use :</label>
                                                                        <div class='col-md-4'>
                                                                            <input type='text' class='form-control dcfee' value="<?php echo $packageInfo['packageDC']?>" disabled>
                                                                        </div>
                                                                    </div>
                                                                <?php }?>
                                                                <?php if( $packageInfo['packagePC']!=0) {?>
                                                                    <div class='form-group pc'>
                                                                        <label class='col-md-3 control-label'>Product Currency to use :</label>
                                                                        <div class='col-md-4'>
                                                                            <input type='text' class='form-control pcfee' value="<?php echo $packageInfo['packagePC']?>" disabled>
                                                                        </div>
                                                                    </div>
                                                                <?php }?>
                                                                <?php if( $packageInfo['packageRF']!=0) {?>
                                                                    <div class='form-group rf'>
                                                                        <label class='col-md-3 control-label'>Registration Fee to use :</label>
                                                                        <div class='col-md-4'>
                                                                            <input type='text' class='form-control rffee' value="<?php echo $packageInfo['packageRF']?>" disabled>
                                                                        </div>
                                                                    </div>
                                                                <?php }?>
                                                            </div>
                                                            
                                                            <?php }else {?>
                                                             <div class="packagedetails hidden">
                                                                <div class='form-group dc'>
                                                                    <label class='col-md-3 control-label'>Declaration Currency to use :</label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' class='form-control dcfee' value="" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class='form-group pc'>
                                                                    <label class='col-md-3 control-label'>Product Currency to use :</label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' class='form-control pcfee' value="" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class='form-group rf'>
                                                                    <label class='col-md-3 control-label'>Registration Fee to use :</label>
                                                                    <div class='col-md-4'>
                                                                        <input type='text' class='form-control rffee' value="" disabled>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <?php }?>
                                                            
                                                            <br/>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'></label>
                                                                <div class='col-md-4'>
                                                                    <input type='submit' class='btn btn-primary' name='submit_btn' value="Preview" />
                                                                </div>
                                                            </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>

                                        <?php } else if($valid==2) {?>

                                            <div class='col-md-12'>
                                                <div class='col-md-12'>

                                                    <form name='register_form' id='register_form' method='post' action='' enctype='multipart/form-data' class='form-horizontal form-bordered form-row-stripped'>
                                                        <input type='hidden' name='__req' value='1' />
                                                        <input type='hidden' name='full_name' class='form-control' value='<?php echo $full_name?>'  required/>
                                                        <input type='hidden' name='nick_name' class='form-control' value='<?php echo $nick_name?>'  required/>
                                                        <input type='hidden' name='IC_no' class='form-control' value='<?php echo $IC_no?>'  required/>
                                                        <input type='hidden' name='wechat_ID' class='form-control' value='<?php echo $wechat_ID?>'  />
                                                        <input type='hidden' name='contact_no' class='form-control' value='<?php echo $contact_no?>' required />
                                                        <input type='hidden' name='email' class='form-control' value='<?php echo $myemail?>'  required/>
                                                        <input type='hidden' name='address' class='form-control' value='<?php echo $address?>' required />
                                                        <input type="hidden" name="country" value='<?php echo $country?>'>
                                                        <input type='hidden' name='ref'  value='<?php echo $ref?>' />
                                                        <input type='hidden' name='username'  value='<?php echo $username?>'/>
                                                        <input type='hidden' name='package'  value='<?php echo $packagename?>' />
                                                        <input type='hidden' name='parent' value='<?php 
                                                        if(isset($_GET['upline1']) && $_GET['upline1']!=''){echo $_GET['upline1'];}
                                                        else {echo $parent;}
                                                        ?>' >
                                                        <input type='hidden' name='hand'  value="<?php echo $hand?>" >
                                                        <input type='hidden' name='sec_pwd'  value="<?php echo $sec_pwd?>" required>

                                                        <div class="form-body pal">
                                                            <br><h4>Account Information</h4>
                                                            <div class="form-group" id="ref">
                                                                <label for="ref" class="col-md-3 control-label">Upline Username</label>
                                                                <div class="col-md-6">
                                                                    Username: <?php echo $uplineusername?>, Name: <?php echo $uplineuserfullname?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" id="_username">
                                                                <label for="_username" class="col-md-3 control-label">Username</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $username?>
                                                                </div>
                                                            </div><div class="form-group" id="package">
                                                                <label for="package" class="col-md-3 control-label">Package</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $packageTitle?>
                                                                </div>
                                                            </div><div class="form-group" id="placement_type">
                                                                <label for="placement_type" class="col-md-3 control-label">Placement Type</label>
                                                                <div class="col-md-6">
                                                                    Manual Placement
                                                                </div>
                                                            </div><div class="form-group" id="upline1">
                                                                <label for="upline1" class="col-md-3 control-label">Placement Username</label>
                                                                <div class="col-md-6">
                                                                    Username: <?php echo $placementusername?>, Name: <?php echo $placementuserfullname?>
                                                                </div>
                                                            </div><div class="form-group" id="position1">
                                                                <label for="position1" class="col-md-3 control-label">Placement Position</label>
                                                                <div class="col-md-6">
                                                                    <?php if($hand==0){
                                                                        echo "A";
                                                                    }else{
                                                                        echo "B";
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div><br><h4>Particulars</h4>


                                                            <div class="form-group" id="_name">
                                                                <label for="_name" class="col-md-3 control-label">Full Name</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $full_name?>
                                                                </div>
                                                            </div><div class="form-group" id="_nick_name">
                                                                <label for="_nick_name" class="col-md-3 control-label">Nickname</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $nick_name?>
                                                                </div>
                                                            </div><div class="form-group" id="_icno">
                                                                <label for="_icno" class="col-md-3 control-label">IC No.</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $IC_no?>
                                                                </div>
                                                            </div><div class="form-group" id="_wechat_id">
                                                                <label for="_wechat_id" class="col-md-3 control-label">Wechat ID</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $wechat_ID?>
                                                                </div>
                                                            </div><div class="form-group" id="_mobileno">
                                                                <label for="_mobileno" class="col-md-3 control-label">Contact Number</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $contact_no?>
                                                                </div>
                                                            </div><div class="form-group" id="_email">
                                                                <label for="_email" class="col-md-3 control-label">Email</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $myemail?>
                                                                </div>
                                                            </div><div class="form-group" id="_address">
                                                                <label for="_address" class="col-md-3 control-label">Address</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $address?>
                                                                </div>
                                                            </div><div class="form-group" id="_country">
                                                                <label for="_country" class="col-md-3 control-label">Country</label>
                                                                <div class="col-md-6">
                                                                    <?php echo $helper->code_to_country($country) //$country?>
                                                                </div>
                                                            </div>

                                                            <h4>Activation Cost</h4>

                                                            <?php if($dc!=0) {?>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Declaration Currency to use :</label>
                                                                    <div class="col-md-6">
                                                                        <?php echo $dc?>.00
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                            <?php if($pc!=0) {?>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Product Currency to use :</label>
                                                                    <div class="col-md-6">
                                                                        <?php echo $pc?>.00
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                            <?php if($rf!=0) {?>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Registration Fee to use :</label>
                                                                    <div class="col-md-6">
                                                                        <?php echo $rf?>.00
                                                                    </div>
                                                                </div>
                                                            <?php }?>

                                                            <br>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label"></label>
                                                                <div class="col-md-6">
                                                                    <!--<input type="button" name="back_btn" value="Back" class="btn btn-light-grey">-->
                                                                    <a href="javascript:void(0)" onclick="goBack()" class="btn btn-light-grey">Back</a>
                                                                    &nbsp;
                                                                    <input type="submit" name="confirm_btn" value="Confirm" class="btn btn-primary">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                            <?php }else if($valid==3){?>

<!--                                                --><?php //if(\App\Session::get('success')) {?>
                                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                        <div class="col-md-12 profile-details">
                                                            <div class="alert alert-block alert-success fade in">
                                                                <h4><i class="fa fa-heart" aria-hidden="true"></i> Successful!</h4>
                                                                <p>The registration process has been completed successfully and the account details are listed bellow</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
//                                                    \App\Session::UnsetKeySession('success');
                                                //}?>


                                            <div class='tab-content'>
                                                <div class='tab-pane fade in active'>
                                                    <div class='row registration'>
                                                        <div class='col-md-12'>

                                                            <div style="margin-top: 60px">
                                                                <form class='form-horizontal form-bordered form-row-stripped'>
                                                                    <input type='hidden' name='__req' value='1'>
                                                                    <div class='form-body'>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Username: </label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $username?>
                                                                            </div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Password </label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $pswrd?>
                                                                            </div>
                                                                        </div>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Security Password </label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $sec_pwd?>
                                                                            </div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Email </label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $myemail?>
                                                                            </div>
                                                                        </div>
                                                                        <?php if($dc!=0){?>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Declaration Currency</label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $dc?>
                                                                            </div>
                                                                        </div>
                                                                        <?php }?>
                                                                         <?php if($pc!=0){?>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Product Currency </label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $pc?>
                                                                            </div>
                                                                        </div>
                                                                        <?php }?>
                                                                        <?php if($rf!=0){?>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Registration Fee </label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $rf?>
                                                                            </div>
                                                                        </div>
                                                                        <?php }?>
                                                                        <br/>
                                                                        <p style="margin-left: 15px"> A welcome email has been sent to '<?php echo $myemail?>'.</p>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-6 control-label'></label>
                                                                            <div class='col-md-6'>
                                                                                <a href="members/genealogy2.php?view=network&headUid=<?php  if(isset($_GET['upline1']) && $_GET['upline1']!='') {echo $_GET['upline1'];} else{ echo $_POST['parent']; }  ?>"  class='btn btn-primary'>Placement</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php }?>

                                        </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once "includes/footer.php";?>

    <script src="html_template/defaults/assets/js/fn/fn_number_format.js"></script>

    <script type="text/javascript">
        
         $('.package').on('change',function () {

            if($(this).val()!='')
            {
                var rf = $('select[name=package] > option:selected').attr('data-rf');
                var dc = $('select[name=package] > option:selected').attr('data-dc');
                var pc = $('select[name=package] > option:selected').attr('data-pc');

                $('.packagedetails').removeClass('hidden');
                
                
                
                if(rf=='0')
                {
                    $('.dcfee').val(dc);
                    $('.rf').addClass('hidden');
                    $('.pcfee').val(pc);  
                }else if(dc=='0')
                {
                    $('.rffee').val(rf);
                    $('.dc').addClass('hidden');
                    $('.pcfee').val(pc); 
                }else if(pc=='0')
                {
                    $('.rffee').val(rf);
                    $('.dcfee').val(dc);
                    $('.pc').addClass('hidden');
                }else {
                    $('.rffee').val(rf);
                    $('.dcfee').val(dc);
                    $('.pcfee').val(pc);  
                    $('.pc').removeClass('hidden');
                    $('.dc').removeClass('hidden');
                    $('.rf').removeClass('hidden');
                }
               
            }else
            {
                $('.packagedetails').addClass('hidden');
            }

        })

        

        $('#check_ref_btn').on('click',function () {
            var uplineuser= $('.uplineuser').val();

            if(uplineuser!='')
            {
                $.ajax({
                    type:'post',
                    url:'ajax_files/check_upline_user.php',
                    data:{
                        'username':uplineuser
                    },
                    success:function (data) {
                        $('#ref_text').text(data);
                    }
                })
            }
        })
        
        function goBack() {
          window.history.back();
        }
    </script>

<?php }else {
    header('location:../login.php');
}?>