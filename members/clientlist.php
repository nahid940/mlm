<?php
include_once '../vendor/autoload.php';
\App\Session::init();
if(\App\Session::get('login')==true && \App\Session::get('page_approved')==false) {
    $user=new \App\user\User();
    $user->checkUserValidity(\App\Session::get('userID'));
    $client=new \App\client\Client();
    
    if(isset($_POST['user_search']))
    {
        if($_POST['headUid']!='')
        {
            $lists=$client->getSpondoredClient($_POST['headUid']);
        }else
        {
            $error=1;
        }
    }else
    {
         $lists=$client->getAllSpondoredClients(\App\Session::get('userID'));
    }
    
    
   
?>

    <?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
        <!-- PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <!-- STYLER -->

                    <!-- /STYLER -->
                    <!-- BREADCRUMBS -->
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="members/index.php">Home</a>
                        </li>
                        <li>Client List</li>
                    </ul>
                    <!-- /BREADCRUMBS -->
                    <div class="clearfix">
                        <h3 class="content-title pull-left">Client List</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PAGE HEADER -->


        <div class='row'>
            <div class='col-md-12'>
                <div class='box border'>
                    <div class='box-title'>
                        <h4 style='height:15px;'></h4>
                    </div>
                    <div class='box-body'>
                        <div class='tabbable header-tabs user-profile'>
                            <ul class='nav nav-tabs'>
                                <li class='active'><a href='members/genealogy2.php'>Client List</a></li>
                            </ul>
                        </div>
                        <?php if(isset($error)  || empty($lists)){?>
                            <div class='alert alert-block alert-danger fade in'>			
                            <h4><i class='fa fa-times'></i> There is / are some error(s), please correct them before continuing</h4>	
                            <p>There is some error(s), please correct them before continuing:<br/>
                            <br/>
                            Username '<?php echo $_POST['headUid']?>' could not be found.<br>
                            </p>					
                            </div>
                        <?php }?>
                        
                        <form class='form-horizontal' action="" method='post'>
                            <div class='form-group'>
                                <label class='col-md-2 control-label'>Username</label>
                                <div class='col-md-3'>
                                  <input class='form-control' type='text' name='headUid' value="<?php echo $_POST['headUid']?>">
                                </div>
                                <div class='col-md-3'>
                                    <input type='submit' name='user_search' value='Search' class='btn btn-info'>
                                </div>
                            </div>
                        </form>
                        
                        <table id='example-paper' class='table table-paper table-striped'>
                                <thead>
                                    <tr>
                                        <th style='text-align:center;'>No.</th>
                                        <th style='text-align:center;'>Username</th>
                                        <th style='text-align:center;'>Full Name</th>
                                        <th style='text-align:center;'>Package</th>
                                        <th style='text-align:center;'>Joined Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=0; foreach($lists as $list){ $i++?>
                                    <tr>
                                        <td style='text-align:center;' ><?php echo $i?></td>
                                        <td style='text-align:center;'><?php echo $list['username']?></td>
                                        <td style='text-align:center;'><?php echo $list['full_name']?></td>
                                        <td style='text-align:center;'><?php 
                                        // echo $list['DC']."+".$list['RF']."+".$list['PC']
                                        if($list['RF']==0)
                                        {
                                            echo  "DC".$list['DC']." + PC".$list['PC'];
                                        }else
                                        {
                                            echo  "DC".$list['DC']." + PC".$list['PC']." + RF".$list['RF'];
                                        }
                                    
                                        ?>
                                        </td>
                                        <td style='text-align:center;'><?php echo date('d/m/Y',strtotime($list['user_registered_date']))?></td>
                                    </tr> 
                                    <?php  }?>
                                </tbody>
                        </table>
                        
                        <div style="text-align:center">
                            <a  href="members/verify_login.php" class='btn btn-success'>Global Market</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <script src="html_template/default/assets/js/jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <?php include_once "includes/footer.php";?>

    <link rel="stylesheet" href="html_template/default/assets/img/network/gentree/gentree.css" type="text/css" />
    <link rel="stylesheet" href="html_template/default/assets/img/network/legend.css" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function(){
            $('.stats-node img').hover(function(){
                    var rel = $(this).attr('rel');

                    $('#tooltip_'+rel).show();
                },
                function(){
                    var rel = $(this).attr('rel');
                    $('#tooltip_'+rel).hide();
                });
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('input[name=headUid]').keypress(function(e){
                if (e.which == 13) {
                    $('input[name=user_search]').click();
                    return false;
                }
            });

            // $('input[name=user_search]').click(function(){
            //     $(this).attr('disabled', 'disabled');
            //     window.location = 'members/genealogy2.php?view=network&headUid='+$('input[name=headUid]').val();
            // });

            $('select[name=did]').change(function(){
                var headUid = $(this).val();
                window.location = '?headUid='+headUid;
            });

            // $('.sidebar').addClass('mini-menu');
            // $('#main-content').addClass('margin-left-50');
        });
    </script>

<?php }else {
    header('location:../login.php');
}?>