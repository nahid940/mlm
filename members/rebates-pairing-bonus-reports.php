<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
date_default_timezone_set('Asia/Kuala_Lumpur');
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
$data=$user->todaysPairBonusSales(\App\Session::get('userID'));
$client_level=$user->placementUserLevel(\App\Session::get('username'))['client_level'];

// if ($user->countLevelBonus(\App\Session::get('userID')) >= 1) {

    $sql = "select total_left,total_right from

        (select count(user_id) as total_left from client_positions where parent_id=:parent_id and hand=0  and action_date=:action_date and client_level>$client_level+5) as leftuser, 
        (select count(user_id) as total_right from client_positions where parent_id=:parent_id and hand=1  and action_date=:action_date and client_level>$client_level+5) as rightuser";

    $stmt = \App\DBConnection::myQuery($sql);
    $stmt->bindValue(':parent_id', \App\Session::get('userID'));
    $stmt->bindValue(':action_date', date('Y-m-d'));
    $stmt->execute();

    $res = $stmt->fetch(PDO::FETCH_ASSOC);
// }


    $sql="select count(bonusID) as totalbonus from bonus where userID=:userID and bonus_type=1";
    $stmt=\App\DBConnection::myQuery($sql);
    $stmt->bindValue(':userID',\App\Session::get('userID'));
    $stmt->execute();
    $totallevelbonus=$stmt->fetch(PDO::FETCH_ASSOC);
    
    if($totallevelbonus['totalbonus']<5)
    {

        $sql = "select total_left,total_right from

        (select count(user_id) as total_left from client_positions where parent_id=:parent_id and hand=0   and client_level>$client_level+5) as leftuser, 
        (select count(user_id) as total_right from client_positions where parent_id=:parent_id and hand=1  and client_level>$client_level+5) as rightuser";

        $stmt = \App\DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id', \App\Session::get('userID'));
        $stmt->execute();

        $left_and_right = $stmt->fetch(PDO::FETCH_ASSOC);
    }else
    {
        $sql="select left_carry,right_carry from pairing_bonus where userID=:userID and bonus_date=:bonus_date";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':bonus_date',date('Y-m-d'));
        $stmt->bindValue(':userID',\App\Session::get('userID'));
        $stmt->execute();
        $carry_amount=$stmt->fetch(PDO::FETCH_ASSOC);
        
    }


    $lists=$user->pairList(\App\Session::get('userID'));
    $pair_bonus=$user->getBonusInfo(2)['amount'];



?>
<?php include_once "includes/header.php";?>

<div id="content" class="col-lg-12">
<!-- PAGE HEADER-->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <!-- STYLER -->

            <!-- /STYLER -->
            <!-- BREADCRUMBS -->
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="members/index.php">Home</a>
                </li>

                <li>Pairing Bonus Reports</li>

            </ul>
            <!-- /BREADCRUMBS -->
            <div class="clearfix">
                <h3 class="content-title pull-left">Pairing Bonus Reports</h3>
            </div>
        </div>
    </div>
</div>
<!-- /PAGE HEADER -->


<div class='row'>
    <div class='col-md-12'>
        <div class='box border'>
            <div class='box-title'>
                <h4 style='height:15px;'></h4>
            </div>
            <div class='box-body'>
                <div class='tabbable header-tabs user-profile'>
                    <ul class='nav nav-tabs'>
                        <li class='active'><a href="members/rebates-pairing-bonus-reports.php"><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus Reports</span></a></li>
                        <li class=''><a href='members/rebates-leveling-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Leveling Bonus</span></a></li>
                        <li class=''><a href='members/rebates-pairing-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus</span></a></li>
                        <li ><a href='members/rebates-introducer.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Introducer Bonus</span></a></li>
                        <li><a href='members/rebates.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Bonus Summary</span></a></li>
                    </ul>
                    <div class='tab-content'>
                        <div class='tab-pane fade in active'>
                            <div class='row'>
                                <center>

                                    <div class='col-md-12'>
                                        <table id='example-paper' class='table table-paper table-striped table-bordered'>
                                            <thead>
                                            <tr>
                                                <th style='text-align:center' colspan='2'>To Day New Sales</th>
                                                <th style='text-align:center' colspan='2'>Unpaid Sales</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style='text-align:center;width:25%;'>Left</td>
                                                <td style='text-align:center;width:25%;'>Right</td>
                                                <td style='text-align:center;width:25%;'>Left</td>
                                                <td style='text-align:center;width:25%;'>Right</td>
                                            </tr>
                                            <tr>
                                                <td style='text-align:center'><?php echo ((empty($res['total_left']))?0:$res['total_left'])?></td>
                                                <td style='text-align:center'><?php echo (empty($res['total_right'])?0:$res['total_right'])?></td>

                                                <?php if($totallevelbonus>=5){?>
                                                    <td style='text-align:center'><?php echo (empty($carry_amount['left_carry'])?0:$carry_amount['left_carry'])?></td>
                                                    <td style='text-align:center'><?php echo (empty($carry_amount['right_carry'])?0:$carry_amount['right_carry'])?></td>
                                                <?php } else {?>

                                                    <td style='text-align:center'>

                                                        <?php
                                                        if($left_and_right['total_left']>$left_and_right['total_right'])
                                                        {
                                                            echo $left_and_right['total_left'];
                                                        }else
                                                        {
                                                            echo 0;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td style='text-align:center'>
                                                        <?php
                                                        if($left_and_right['total_right']>$left_and_right['total_left'])
                                                        {
                                                            echo $left_and_right['total_right'];
                                                        }else
                                                        {
                                                            echo 0;
                                                        }
                                                        ?>

                                                    </td>
                                                <?php }?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <br/>
                                    <div class='col-md-12'>
                                        <table id='example-paper' class='table table-paper table-striped table-bordered'>
                                            <thead>
                                            <tr>
                                                <th style='text-align:center'></th>
                                                <th style='text-align:center' colspan='3'>LEFT STATUS</th>
                                                <th style='text-align:center' colspan='3'>RIGHT STATUS</th>
                                                <th style='text-align:center' colspan='2'></th>
                                            </tr>
                                            
                                                <tr>
                                                    <th style='text-align:center'>Date</th>
                                                    <th style='text-align:center'>Accumulate</th>
                                                    <th style='text-align:center'>Payout</th>
                                                    <th style='text-align:center'>Balance</th>
                                                    <th style='text-align:center'>Accumulate</th>
                                                    <th style='text-align:center'>Payout</th>
                                                    <th style='text-align:center'>Balance</th>
                                                    <th style='text-align:center'>Electronic Currency</th>
                                                </tr>
                                                
                                            
                                            </thead>
                                            <tbody>
                                                
                                                <?php $payout; foreach($lists as $list) {
                                                
                                                
                                                $TLS=$list['total_user_left']+$list['left_carry'];
                                                $TRS=$list['total_user_right']+$list['right_carry'];
                                                
                                                if($TLS>=$TRS)
                                                {
                                                    $payout=$TRS*$pair_bonus;
                                                }else
                                                {
                                                     $payout=$TLS*$pair_bonus;
                                                }
                                                $left_accumulate=$TLS*$pair_bonus;
                                                $right_accumulate=$TRS*$pair_bonus;
                                                
                                                $left_balance=$left_accumulate-$payout;
                                                $right_balance=$right_accumulate-$payout;
                                                $ec=($payout*10)/100;
                                                
                                                
                                                ?>
                                                
                                                
                                                    <tr>
                                                        <td style='text-align:center;font-weight:bold'><?php echo date('d-m-Y',strtotime($list['bonus_date']))?></td>
                                                        <td style='text-align:center;font-weight:bold'><?php echo number_format($left_accumulate,2)?></td>
                                                        <td style='text-align:center;font-weight:bold'><?php echo number_format($payout,2)?></td>
                                                        <td style='text-align:center;font-weight:bold'><?php echo number_format($left_balance,2)?></td>
                                                        
                                                       
                                                       <!--/**--------------------------------------right----------------------------------------**/-->
                                                    
                                                        <td style='text-align:center;font-weight:bold'><?php echo number_format($right_accumulate,2)?></td>
                                                        <td style='text-align:center;font-weight:bold'><?php echo number_format($payout,2)?></td>
                                                        <td style='text-align:center;font-weight:bold'><?php echo number_format($right_balance,2)?></td>
                                                       <td style='text-align:center;font-weight:bold'><?php echo number_format($ec,2)?></td>
                                                       
                                                      
                                                    </tr>
                                                <?php }?>

                                            </tbody>
                                        </table>
                                    </div>

                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<?php include_once "includes/footer.php";?>
<?php }else {
    header('location:../login.php');
}?>