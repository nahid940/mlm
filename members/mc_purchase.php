<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$client=new \App\client\Client();
$user->checkUserValidity(\App\Session::get('userID'));
$msp=$user->userCurrencies(\App\Session::get('userID'))['msp'];

$unit=1;

if(isset($_POST['submit_btn']))
{
    if($_POST['_amount']=='')
    {
        $error[]='Amount is a required filed.';
    }
    if($_POST['_desc']=='')
    {
        $error[]='Remark is a required filed.';
    }
    if($_POST['password']=='')
    {
        $error[]='Password is a required filed.';
    }

    if(empty($error))
    {
        if($user->verifyUser(base64_encode($_POST['password'])))
        {
            if (($_POST['_amount']*$unit <= $msp && $msp >= 1))
            {
                $confirm=1;
            }else
            {
                $error[]='You have insufficient MSP.';
            }
        }
        else
        {
            $error[]='Invalid security password';
        }
    }
}


if(isset($_POST['submit_btn_confirm']))
{
    if (($_POST['amount'] <= $msp && $msp >= 1)) {
        $base_url = $_SERVER['REQUEST_URI'];
        $amount = $_POST['amount']*$unit ;
        $username=\App\Session::get('username');
        $type = 1;
        $id=\App\Session::get('userID');
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "http://ecommerce.agamisoft.net/api/transaction.php");
        curl_setopt($handle, CURLOPT_POST, 1);
        curl_setopt($handle, CURLOPT_POSTFIELDS, "username=$username&userID=$id&amount=$amount&type=$type&uri=$base_url");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $x = curl_exec($handle);
        curl_close($handle);

        if ($x == 200) {
            // $client->storeTransactionHistory(\App\Session::get('userID'), \App\Session::get('userID'), $amount, 12, 20, $_POST['remark']); //20 for msp transfer to ecommerce
            $user->userCurrencyHistory(\App\Session::get('userID'), \App\Session::get('userID'), $amount, "GPMall Transfer", 20, 1,$amount,0,$_POST['remark'],$msp-$amount,$msp-$amount);
            $client->decreaseMSPCurrencyFee($id, $amount);
            $success="Transfer successful.";
            $msp=$user->userCurrencies(\App\Session::get('userID'))['msp'];
            $comlpete=1;
        } else {
            echo "error";
        }
    }
}
?>
<?php include_once "includes/header.php";?>

 <div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>GPmall Transfer</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">GPmall Transfer</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->

    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo number_format($msp,2)?></div>
                <div>MSP</div>
            </a>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li class='active'><a href='members/mc_purchase.php'>GPmall Transfer</a></li>
                            <li ><a href='members/history_cwallet.php'>MSP History</a></li>
                            <li ><a href='members/dc_purchase.php'>Declaration Currency</a></li>
                            <li ><a href='members/ec_purchase.php'>Electronic Currency</a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <?php if(isset($error)) {?>
                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                        <div class="col-md-12 profile-details">
                                            <div class="alert alert-block alert-danger fade in">
                                                <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                <?php
                                                foreach ($error as $err)
                                                {
                                                    echo "<p>".$err."</p>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }?>
                                <?php if(isset($success)) {?>
                                    <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                        <div class="col-md-12 profile-details">
                                            <div class="alert alert-block alert-success fade in">
                                                <h4><i class="fa fa-heart" aria-hidden="true"></i>
                                                    Successful!</h4>
                                                <?php
                                                echo "<p>".$success."</p>";
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }?>
                                <div class='row'>
                                    <?php if(isset($comlpete))
                                    {?>
                                    <?php if(\App\Session::get('success')) {?>
                                        <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                <div class="alert alert-block alert-success fade in">
                                                    <h4><i class="fa fa-heart" aria-hidden="true"></i> Successful!</h4>
                                                    <p><?php echo \App\Session::get('success')?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        \App\Session::UnsetKeySession('success');
                                    }?>
                                     <div style="text-align:center">
                                        <a href="members/mc_purchase.php" class="btn btn-success">Continue</a>
                                    </div>

                                    <?php } else { ?>

                                        <?php if (isset($confirm)) { ?>
                                            <div class="col-md-12">
                                                <br>
                                                <div class='tab-content'>
                                                    <div class='tab-pane fade in active'>
                                                        <div class='row'>

                                                            <div class='col-md-12'>
                                                                <div class="alert alert-info">
                                                                    Please review your transfer information and click
                                                                    the 'Confirm' button to proceed with the transfer.
                                                                </div>
                                                                <form name='transfer_form' method='post' action=''
                                                                      class='form-horizontal form-bordered form-row-stripped'>
                                                                    <input type='hidden' name='__req' value='1'>
                                                                    <div class='form-body'>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Transfer
                                                                                Amount: <span
                                                                                        class='require'>*</span></label>
                                                                            <div class='col-md-5'>
                                                                                <?php echo $_POST['_amount']  ?>
                                                                                .00
                                                                                <input type="hidden"
                                                                                       value="<?php echo $_POST['_amount'] ?>"
                                                                                       name="amount">
                                                                            </div>
                                                                        </div>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'>Remark:
                                                                                <span class='require'>*</span></label>
                                                                            <div class='col-md-5'>
                                                                                GPMall Transfer
                                                                                (<?php echo $_POST['_desc'] ?>)
                                                                                <input type="hidden"
                                                                                       value="<?php echo $_POST['_desc'] ?>"
                                                                                       name="remark">
                                                                            </div>
                                                                        </div>
                                                                        <br/>
                                                                        <div class='form-group'>
                                                                            <label class='col-md-3 control-label'></label>
                                                                            <div class='col-md-5'>
                                                                                <a href="javascript:history.go(-1)"
                                                                                   class='btn btn-default'>Back</a>
                                                                                <input type='submit'
                                                                                       name='submit_btn_confirm'
                                                                                       value='Confirm'
                                                                                       class='btn btn-primary'/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php } else { ?>

                                            <div class='col-md-12'>
                                                <br/>
                                                <form name='withdraw_form' method='post' action=''
                                                      class='form-horizontal form-bordered form-row-stripped'>
                                                    <div class='form-body'>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'>MSP: *</label>
                                                            <div class='col-md-5' style='margin-top:0px;'>
                                                                <input type='text' name='_amount'
                                                                       autocomplete='off' class='form-control'
                                                                       value=''>
                                                            </div>
                                                        </div>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'>Remark: *</label>
                                                            <div class='col-md-5'>
                                                                <input type='text' name='_desc' autocomplete='off'
                                                                       class='form-control' value=''
                                                                       class='form-control'>
                                                            </div>
                                                        </div>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'>Security
                                                                Password:</label>
                                                            <div class='col-md-5'>
                                                                <input type='password' name='password'
                                                                       class='form-control' value=''
                                                                       class='form-control'>
                                                            </div>
                                                        </div>
                                                        <br/>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'></label>
                                                            <div class='col-md-5'>
                                                                <input type='submit' name='submit_btn' value='Submit'
                                                                       class='btn btn-primary'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <?php }

                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include_once "includes/footer.php";?>

<script type='text/javascript'>
//    jQuery.noConflict();
//    jQuery(document).ready(function(j){
//        j('form[@name=withdraw_form]').submit(function(){
//            j('input[@name=submit_btn]').attr('disabled','disabled');
//        });
//
//        j('input[name=back_btn]').click(function(){
//            q('__edit').value = 1;
//            q('__confirm').value = '';
//            j('input[name=back_btn],input[name=submit_btn]').attr('disabled','disabled');
//            document.withdraw_form.submit();
//        });
//    });

</script>

<?php }else
{
    header('location:../login.php');
}?>