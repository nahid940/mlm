<?php
include_once '../vendor/autoload.php';
\App\Session::init();

$helper=new \App\Helper();

$invest=new \App\investment\Investment();
$packages=$invest->getAllInvestmentPackages();
$package_plans=$invest->getAllInvestTypes();


$helper->checkTime();

if(\App\Session::get('login')==true) {

    $user=new \App\user\User();
    $user->checkUserValidity(\App\Session::get('userID'));
    $currency=$user->userCurrencies(\App\Session::get('userID'));
   

    //$ec=$currency['ec']+(empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')));
    $tc=$currency['tc'];
    \App\Session::UnsetKeySession('confirm');
    if(isset($_POST['submit_btn']))
    {
        if ($user->verifyUser(base64_encode($_POST['password']))) {

            $tc_amount=$invest->getInvestmentAmount($_POST['packageID'])['investment_amount'];
             
            $total_amount=$_POST['_amount']*$tc_amount;

            if($_POST['_amount']!='') {
                if (($total_amount) <= $tc) {
                    $_POST['amount'] =  $total_amount;
                    \App\Session::set('confirm', 'confirm');
                } else {
                    \App\Session::set('error', "You do not have sufficient Trade Currency.");
                }
            }
            else
            {
                \App\Session::set('error', 'Please select an amount.');
            }
        }else
        {
            \App\Session::set('error',"Invalid Security Password.");
        }
    }
    

    if(isset($_POST['submit_btn_confirm']))
    {
        $user_tc_amount=$invest->getTC()['tc'];
        $package_type=$invest->getInvestmentAmount($_POST['packageID']);
        // $investment_typeID,$investment_packageID,

        
        // $userID,$investment_typeID,$investment_packageID,$invest_amount,$initial_rate,$final_rate,$initial_date,$invest_start_date,$invest_closing_date,$invest_status,$approval
        
        try{
            \App\DBConnection::myDb()->beginTransaction();
            
            
            
            if($_POST['planID']==3) //3 for fixed deposit
        {
            $invest->newInvestment(\App\Session::get('userID'),$package_type['investment_typeID'],$_POST['packageID'], $_POST['amount'],$package_type['return_rate'],$package_type['return_rate'],date('Y-m-d'),date('Y-m-d'),date('Y-m-d', strtotime('+'.$package_type['package_duration'].'day')),1,1);
            
        }else
        {
            $invest->newInvestment(\App\Session::get('userID'),$package_type['investment_typeID'],$_POST['packageID'], $_POST['amount'],$package_type['return_rate'],0,date('Y-m-d'),date('Y-m-d'),date('Y-m-d', strtotime('+'.$package_type['package_duration'].'day')),1,1);
        }
        
        
        $remaining_tc=$user_tc_amount-$_POST['amount'];
        
        $user->decrementTC($remaining_tc, \App\Session::get('userID'));
        $user->userCurrencyHistory(\App\Session::get('userID'), 0, $_POST['amount'], 'Invest', 35, 35,$_POST['amount'],0,"",$remaining_tc,0); // 35 tc investment
        
        
        /**-----------------BC bonus--------------------**/
        $sponsor=$user->getSponsorUserIDByID(\App\Session::get('userID'));
        
        $bonus_rate=$invest->getTransferMultiplesByColumn("bc_bonus_rate")['bc_bonus_rate'];
        
        $bonus=$_POST['amount']*$bonus_rate/100;
        
        $currency=$user->userCurrencies($sponsor['sponsor_id']);
        
        $user->incrementBC($currency['bc']+$bonus,$sponsor['sponsor_id']);
        
        // storeParentBonus($id,$bonus_point,$level,$left_userID,$right_userID,$bonus_type)
        $user->storeParentBonus($sponsor['sponsor_id'], $bonus, 0, 0,0, 10); //10 for investment bc bonus
        
        $user->userInvestmentCurrencyHistory($sponsor['sponsor_id'], 0, $bonus, 'BC Bonus', 45, 45,0,$bonus,"",$currency['bc']+$bonus,0); // 45 bc bonus
        
        /**---------------------------------------------**/
        
        $currency['tc']=$remaining_tc;
        $complete=1;
        $success='Your transfer has just been completed.';
            
            
            \App\DBConnection::myDb()->commit();
        }
            
        catch(PDOException $ex)
        {
            \App\DBConnection::myDb()->rollBack();
        }
        
    }

    ?>
    <?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
        <!-- PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header">
                    <!-- STYLER -->
                    <!-- /STYLER -->
                    <!-- BREADCRUMBS -->
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="members/index.php">Home</a>
                        </li>
                        <li>Trading</li>
                        <li>Trade Policy</li>
                    </ul>
                    <!-- /BREADCRUMBS -->
                    <div class="clearfix">
                        <h3 class="content-title pull-left">Trade Policy</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /PAGE HEADER -->

        <div class='row'>
            <div class='col-md-3'>
                <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                    <div style='font-size:20px;'><?php echo ($currency['tc']==0?0.00:number_format($currency['tc'],2))?></div>
                    <div>Trade Currency</div>
                </a>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-12'>
                <div class='box border'>
                    <div class='box-title'>
                        <h4 style='height:15px;'></h4>
                    </div>
                    <div class='box-body'>
                        <div class='tabbable header-tabs user-profile'>
                            <ul class='nav nav-tabs'>
                                <li class="active"><a href='members/pc_purchase.php'>Trade Policy</a></li>
                            </ul>
                            <div class='tab-content'>
                                <div class='tab-pane fade in active'>
                                    <div class='row'>
                                        <?php if(\App\Session::get('error')) {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    <div class="alert alert-block alert-danger fade in">
                                                        <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                        <p><?php echo \App\Session::get('error')?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            \App\Session::UnsetKeySession('error');
                                        }?>

                                        <?php if( isset($complete) && $complete==1)
                                        {?>
                                            <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                                <div class="col-md-12 profile-details">
                                                    <div class="alert alert-block alert-success fade in">
                                                        <h4><i class="fa fa-heart" aria-hidden="true"></i> Successful!</h4>
                                                        <p><?php echo $success?></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="text-align:center">
                                                <a href="members/client_invest.php" class="btn btn-success">Continue</a>
                                            </div>

                                        <?php } else {?>

                                            <?php if(\App\Session::get('confirm')) {?>
                                                <!-- <div class='tab-content'>-->
                                                <!--<div class='tab-pane fade in active'>-->
                                                <!--<div class='row'>-->
                                                <div class='col-md-12'>
                                                    <div class="alert alert-info">
                                                        Please review your transfer information and click the 'Confirm' button to proceed with the transfer.
                                                    </div>
                                                    <form name='transfer_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                        <input type='hidden' name='__req' value='1'>
                                                        <div class='form-body'>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Transfer Amount: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    <?php echo $_POST['amount']?>.00
                                                                    <input type="hidden" value="<?php echo $_POST['amount']?>" name="amount">
                                                                    <input type="hidden" value="<?php echo $_POST['packageID']?>" name="packageID">
                                                                    <input type="hidden" value="<?php echo $_POST['planID']?>" name="planID">
                                                                </div>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Type: <span class='require'>*</span></label>
                                                                <div class='col-md-5'>
                                                                    Trade Policy
                                                                </div>
                                                            </div>

                                                            <br/>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'></label>
                                                                <div class='col-md-5'>
                                                                    <a href="javascript:history.go(-1)" class='btn btn-default'>Back</a>
                                                                    <input type='submit' name='submit_btn_confirm' value='Confirm' class='btn btn-primary' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- </div>-->
                                                <!--</div>-->
                                                <!--</div>-->

                                            <?php } else {?>
                                            
                                            
                                            <div class='row'>
                                                
                                                <div class='col-md-7'>
<!--                                                    <div class='alert alert-block alert-info fade in'>-->
<!--                                                        Convert Rate 1:0.8-->
<!--                                                    </div>-->
                                                    <form name='withdraw_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>
                                                        <input type='hidden' name='__req' value='1' />
                                                        <input type='hidden' name='__preview' value='1' />
                                                        <div class='form-body'>
                                                            
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Policy Type: *</label>
                                                                <div class='col-md-9'>
                                                                    <select class='form-control' name='planID' id="package_plan" required>
                                                                            <option value=''>-- select one --</option>
                                                                        <?php foreach($package_plans as $package_plan){ ?>
                                                                            <option value='<?php echo $package_plan['investment_typeID']?>'><?php echo $package_plan['investment_type_name']?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Select Policy: *</label>
                                                                <div class='col-md-9 package_div'>
                                                                    <select class='form-control' name='packageID' id="package_info" required>
                                                                            <option value=''>-- choose one --</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--<div class='form-group'>-->
                                                            <!--    <label class='col-md-3 control-label'>Amount: *</label>-->
                                                            <!--    <div class='col-md-9'>-->
                                                            <!--        <div class='row'>-->
                                                                        <!--<div class='col-md-3' style='margin-top:0px;'>-->
                                                                        <!--    <?php //echo $tc_amount?> x-->
                                                                        <!--</div>-->
                                                            <!--            <div class='col-md-12' style='margin-top:0px;'>-->
                                                            <!--                <input type='number' name='_amount' class='form-control' value='' min='1' step='1' max='20000'>-->
                                                            <!--            </div>-->
                                                            <!--        </div>-->
                                                                    <!--<p class='help-block'>Trade Currency Invest is <?php //echo $tc_rate?>  times.(E.g : 1 unit equal to <?php echo $tc_rate?> )</p>-->
                                                            <!--    </div>-->
                                                            <!--</div>-->
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Amount: *</label>
                                                                <div class="col-md-9">
                                                                    <div class="row">
                                                                        <div class="col-md-3" style="margin-top:0px;">
                                                                            <span class="amount_val">0</span> x
                                                                        </div>
                                                                        <div class="col-md-9" style="margin-top:0px;">
                                                                            <input type="number" name="_amount" class="form-control" value="" min="0" step="1">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Type: *</label>
                                                                <div class='col-md-9'>
                                                                     Trade Currency Investment
                                                                </div>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Trade Currency: *</label>
                                                                <div class='col-md-9'>
                                                                    <?php
                                                                    //$ec=$user->totalEc((\App\Session::get('userID')))+(empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')));
                                                                    echo ($currency['tc']==''?'0.00':$currency['tc'])?>
                                                                </div>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'>Security Password:</label>
                                                                <div class='col-md-9'>
                                                                    <input type='password' name='password' class='form-control' value='' >
                                                                </div>
                                                            </div>
                                                            <br/>
                                                            <div class='form-group'>
                                                                <label class='col-md-3 control-label'></label>
                                                                <div class='col-md-5'>
                                                                    <input type='submit' name='submit_btn' value='Submit' class='btn btn-primary' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                
                                                <div id='package_info_div'>
                                                    
                                                    
                                                </div>
                                            </div>
                                            <?php }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once "includes/footer.php";?>

    <script type='text/javascript'>
    
    $(document).ready(function(){
        
        // var packageID=$('#package_info').val();
        
        // if(packageID=='')
        // {
        //     packageID=10;
        // }
        
        // $.ajax({
        //         type:'post',
        //         url:'ajax_files/package_info.php',
        //         data:{
        //             'packageID':packageID
        //         },
        //         success:function(data){
        //             $('#package_info_div').html(data);
        //         }
        //     })
        
    })
    
        $(document).on('change','#package_info',function(){
            var packageID=$('#package_info').val();
            if(packageID!='')
            {
                $.ajax({
                    type:'post',
                    url:'ajax_files/package_info.php',
                    data:{
                        'packageID':packageID
                    },
                    success:function(data){
                        $('#package_info_div').html(data);
                        
                       var val= $('#packege_amount').attr('data-val');
                       
                       $('.amount_val').text(val);
                    }
                })
            }
        }) 
        
         
        $('#package_plan').on('change',function(){
            var id=$('#package_plan').val();
            if(id!='')
            {
                $.ajax({
                    type:'post',
                    url:'ajax_files/investment_packages.php',
                    data:{
                        'id':id
                    },
                    success:function(data){
                        $('.package_div').html(data);
                    }
                })
            }
        })
    

    </script>

<?php }else {
    header('location:../login.php');
}?>