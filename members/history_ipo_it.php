<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>IT IPO History</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">IT IPO History</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->

    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'>0.00</div>
                <div>IT IPO</div>
            </a>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>

            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li ><a href='members/history_ipo_agro.php'>AGRO IPO History</a></li>
                            <li class='active'><a href='members/history_ipo_it.php'>IT IPO History</a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='row'>
                                            <div class='col-md-12'>

                                                <table id='example-paper' class='table table-paper table-striped'>
                                                    <thead>
                                                    <tr>
                                                        <th style='width: 7%;text-align:center;'>No.</th>
                                                        <th style='text-align:center;'>Ref #</th>
                                                        <th style='text-align:center;'>Date</th>
                                                        <th style='width: 12%;text-align:center;'>Units</th>
                                                        <th style='text-align:center;'>Description</th>
                                                        <th style='text-align:center;'>Balance</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>
