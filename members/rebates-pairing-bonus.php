<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
$client=new \App\client\Client();

if(isset($_GET['start']) && isset($_GET['end']))
{

    $sql="select count(tableID) as total from pairing_bonus where userID=:userID and (bonus_date between :start and :enddate)";
    $stmt1=\App\DBConnection::myQuery($sql);
    $stmt1->bindValue(':userID',\App\Session::get('userID'));
    $stmt1->bindValue(':start',$_GET['start']);
    $stmt1->bindValue(':enddate',$_GET['end']);
    $stmt1->execute();
    $totalRow=$stmt1->fetch(PDO::FETCH_ASSOC);
    $perpage= $totalRow['total']/10;


    if(isset($_GET['nav'])){
        $pageNumber=$_GET['nav'];
    }else{
        $pageNumber=0;
    }


    if($pageNumber==0 || $pageNumber==1){
        $page1=0;
    }else{
        $page1=($pageNumber*10)-10;
    }



    if($_GET['start']!='' && $_GET['end']!='')
    {
        $lists=$user->dateWisePairBonusList(\App\Session::get('userID'),$_GET['start'],$_GET['end'],$page1);
    }else
    {
        \App\Session::set('error','Invalid date range.');
    }


}else
{

    $sql="select count(tableID) as total from pairing_bonus where userID=:userID";
    $stmt1=\App\DBConnection::myQuery($sql);
    $stmt1->bindValue(':userID',\App\Session::get('userID'));
    $stmt1->execute();
    $totalRow=$stmt1->fetch(PDO::FETCH_ASSOC);
    $perpage= $totalRow['total']/10;


    if(isset($_GET['nav'])){
        $pageNumber=$_GET['nav'];
    }else{
        $pageNumber=0;
    }


    if($pageNumber==0 || $pageNumber==1){
        $page1=0;
    }else{
        $page1=($pageNumber*10)-10;
    }

    $lists=$user->pairBonusList(\App\Session::get('userID'),$page1);
}

 $pair_bonus=$user->getBonusInfo(2)['amount'];

?>

<?php include_once "includes/header.php";?>

    <div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>

                    <li>Bonus Summary</li>
                    <i class='fa fa-angle-right'></i>&nbsp;&nbsp;</li><li>Pairing Bonus</li>

                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Bonus Summary</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->



    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-danger btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px;    margin-bottom: 25px;'>
                <div style='font-size:20px;'><?php echo (empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')))?>.00</div>
                <div>Pairing Bonus</div>
            </a>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li class=''><a href="members/rebates-pairing-bonus-reports.php"><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus Reports</span></a></li>
                            <li ><a href='members/rebates-leveling-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Leveling Bonus</span></a></li>
                            <li class='active'><a href='members/rebates-pairing-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus</span></a></li>
                            <li ><a href='members/rebates-introducer.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Introducer Bonus</span></a></li>
                            <li><a href='members/rebates.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Bonus Summary</span></a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <div class='col-md-12'>

                                        <div class='box border blue'>
                                            <div class='box-body' style='margin-top:10px;'>
                                                <form class='form-horizontal' action='' method='get'>
                                                    <input type='hidden' name='__req' value='1' />
                                                    <input type='hidden' name='type' value='p' />
                                                    <input type='hidden' name='nav' value='' />
                                                    <div class='form-group'>
                                                        <label class='col-md-2 control-label'>From:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='start' value=''>
                                                        </div>

                                                        <label class='col-md-1 control-label'>To:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='end' value=''>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <input type='submit' value="Generate" class='btn btn-success'/>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>


                                        <h4 style='font-weight:bold;'>Total Pairing Bonus: <?php echo (empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')))?>.00</h4>
                                        <div class='nav_link'>
<!--                                            <a href="rebates.php?__req=1&type=p&nav=1">First</a>-->
<!--                                            <a href="rebates.php?__req=1&type=p&nav=1"  style='color:red;'>1</a> <a href="rebates.php?__req=1&type=p&nav=2"  style=''>2</a> <a href="rebates.php?__req=1&type=p&nav=3"  style=''>3</a> <a href="rebates.php?__req=1&type=p&nav=4"  style=''>4</a> <a href="rebates.php?__req=1&type=p&nav=5"  style=''>5</a> <a href="rebates.php?__req=1&type=p&nav=6"  style=''>6</a> <a href="rebates.php?__req=1&type=p&nav=7"  style=''>7</a> <a href="rebates.php?__req=1&type=p&nav=8"  style=''>8</a> <a href="rebates.php?__req=1&type=p&nav=9"  style=''>9</a> <a href="rebates.php?__req=1&type=p&nav=2">Next</a> <a href="rebates.php?__req=1&type=p&nav=9">Last</a> </div><div style="clear:both;"></div>-->

                                        <?php if(!empty($lists)){?>
                                            <div class='nav_link'>
                                                <a href="members/rebates-pairing-bonus.php?__req=1&type=s&nav=1">First</a>
                                                <?php if(isset($_GET['nav']) && $_GET['nav']>1) { ?>
                                                    <a href="members/rebates-pairing-bonus.php?__req=1&type=s&nav=<?php echo ($_GET['nav']-1)?>">Previous</a>
                                                <?php }?>
                                                <?php for($x=1;$x<=ceil($perpage);$x++){?>
                                                    <a href="members/rebates-pairing-bonus.php?__req=1&type=s&nav=<?php echo $x?>"  style='color:red;'><?php echo $x?></a>
                                                <?php }?>
                                                <a href="members/rebates-pairing-bonus.php?__req=1&type=s&nav=<?php echo (isset($_GET['nav'])?$_GET['nav']+1:1)?>">Next</a>
                                                <a href="members/rebates-pairing-bonus.php?__req=1&type=s&nav=<?php echo $totalRow['total']?>">Last</a>
                                            </div>
                                        <?php }?>


                                        <table id='example-paper datatable1' class='table table-paper table-striped'>
                                            <thead><tr><th width='8%'>No.</th><th scope='col'>Type</th><th scope='col'>Description</th><th scope='col' class='center'>Status</th><th scope='col' class='center'>Amount</th><th scope='col'>Paid Date</th></tr></thead>
                                            <tbody>

                                            <?php foreach ($lists as $list){?>

                                                <?php
                                                if($list['total_user_left']<$list['total_user_right'])
                                                {
                                                    $pairamount=$list['total_user_left']*$pair_bonus;

                                                }else
                                                {
                                                    $pairamount=$list['total_user_right']*$pair_bonus;
                                                }
                                                ?>

                                                <?php

                                                // $amount=$user->previousCarryAmount($list['userID'], date('Y-m-d', strtotime($list['bonus_date'] . ' -1 day')));

                                                // if($amount['left_carry']==0)
                                                // {
                                                //     $left_carry=0;
                                                // }else
                                                // {
                                                //     $left_carry=$amount['left_carry']*$pair_bonus;
                                                // }

                                                // if($amount['right_carry']==0)
                                                // {
                                                //     $right_carry=0;
                                                // }else
                                                // {
                                                //     $right_carry=$amount['right_carry']*$pair_bonus;
                                                // }

                                                ?>
                                                <tr class='row0'>
                                                    <td>1</td>
                                                    <td>Pairing Bonus</td>
                                                    <td>Leg A (Username #<?php echo $client->leftUsersLimitedInfo($list['userID'])['username']?>) = <?php echo (($list['total_user_left']+$list['left_carry'])*$pair_bonus)?>.00<br>
                                                        Leg B (Username #<?php echo $client->rightUsersLimitedInfo($list['userID'])['username']?>) = <?php echo (($list['total_user_right']+$list['right_carry'])*$pair_bonus)?>.00
                                                        <br>Total Pair: <?php echo $pairamount?>.00 x 10% = <?php echo $pairamount*10/100?>
                                                    </td>
                                                    <td class='center'>Paid</td>
                                                    <td class='center'><?php echo $list['bonus']?>.00</td>
                                                    <td><?php echo date('d-M-Y',strtotime($list['bonus_date']))?></td>
                                                </tr>
                                            <?php } ?>
                                          </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once "includes/footer.php";?>
<?php }else {
    header('location:../login.php');
}?>