<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
?>
<?php include_once "includes/header.php";?>

<div id="content" class="col-lg-12">
<!-- PAGE HEADER-->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <!-- STYLER -->

            <!-- /STYLER -->
            <!-- BREADCRUMBS -->
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="members/index.php">Home</a>
                </li>
                <li>MSDollar</li>
                <li>MSD Trading History</li>
            </ul>
            <!-- /BREADCRUMBS -->
            <div class="clearfix">
                <h3 class="content-title pull-left">MSD Trading History</h3>
            </div>
        </div>
    </div>
</div>
<!-- /PAGE HEADER -->


<div class='row'>
    <div class='col-md-12'>
        <div class='box border'>
            <div class='box-title'>
                <h4 style='height:15px;'></h4>
            </div>
            <div class='box-body'>
                <div class='tabbable header-tabs user-profile'>
                    <ul class='nav nav-tabs'>
                        <li class='active'><a href='members/msd_trading_history.php'>MSD Trading History</a></li>
                        <li ><a href='members/msd_trading_form.php'>MSD Trading</a></li>
                    </ul>
                    <div class='tab-content'>
                        <div class='tab-pane fade in active'>
                            <div class='row'>
                                <div class='col-md-12'>
                                    <div class='row'>
                                        <div class='col-md-12'>

                                            <table id='example-paper' class='table table-paper table-striped'>
                                                <thead>
                                                <tr>
                                                    <th style='width: 6%;text-align:center;'>No.</th>
                                                    <th style='width: 10%;text-align:center;'>Ref #</th>
                                                    <th style='width: 10%;text-align:center;'>Date</th>
                                                    <th style='width: 10%;text-align:center;'>MSD Selling Price</th>
                                                    <th style='width: 9%;text-align:center;'>Amount of MSD</th>
                                                    <th style='width: 15%;text-align:center;'>MSD Wallet Address</th>
                                                    <th style='width: 20%;text-align:center;'>Status</th>
                                                    <th style='width: 20%;text-align:center;'>Remark</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>