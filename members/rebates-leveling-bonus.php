<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));

$levelbonus=$user->totalLevelBonus(\App\Session::get('userID'));


if(isset($_GET['start']) && isset($_GET['end']))
{
    if($_GET['start']!='' && $_GET['end']!='')
    {

        $sql="select count(bonusID) as total from bonus where bonus_type=1 and userID=:userID and (bonus_date between :start and :enddate)";
        $stmt1=\App\DBConnection::myQuery($sql);
        $stmt1->bindValue(':userID',\App\Session::get('userID'));
        $stmt1->bindValue(':start',$_GET['start']);
        $stmt1->bindValue(':enddate',$_GET['end']);
        $stmt1->execute();
        $totalRow=$stmt1->fetch(PDO::FETCH_ASSOC);
        $perpage= $totalRow['total']/10;


        if(isset($_GET['nav'])){
            $pageNumber=$_GET['nav'];
        }else{
            $pageNumber=0;
        }


        if($pageNumber==0 || $pageNumber==1){
            $page1=0;
        }else{
            $page1=($pageNumber*10)-10;
        }


        $allLevelBonuses=$user->dateWiseAllLevelBonus(\App\Session::get('userID'),$_GET['start'],$_GET['end'],$page1);
    }else
    {
        \App\Session::set('error','Invalid date range.');
    }
}else
{

    $sql="select count(bonusID) as total from bonus where bonus_type=1 and userID=:userID";
    $stmt1=\App\DBConnection::myQuery($sql);
    $stmt1->bindValue(':userID',\App\Session::get('userID'));
    $stmt1->execute();
    $totalRow=$stmt1->fetch(PDO::FETCH_ASSOC);
    $perpage= $totalRow['total']/10;


    if(isset($_GET['nav'])){
        $pageNumber=$_GET['nav'];
    }else{
        $pageNumber=0;
    }


    if($pageNumber==0 || $pageNumber==1){
        $page1=0;
    }else{
        $page1=($pageNumber*10)-10;
    }


    $allLevelBonuses=$user->allLevelBonus(\App\Session::get('userID'),$page1);
    
}

$parent_level=$user->getParentLevel(\App\Session::get('userID'));


?>
<?php include_once "includes/header.php";?>

<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>

                    <li>Bonus Summary</li>
                    <i class='fa fa-angle-right'></i>&nbsp;&nbsp;</li><li>Leveling Bonus</li>

                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Bonus Summary</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->


    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-danger btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px;    margin-bottom: 25px;'>
                <div style='font-size:20px;'><?php echo (empty($levelbonus)?0:$levelbonus)?>.00</div>
                <div>Leveling Bonus</div>
            </a>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li class=''><a href="members/rebates-pairing-bonus-reports.php"><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus Reports</span></a></li>
                            <li class='active'><a href='members/rebates-leveling-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Leveling Bonus</span></a></li>
                            <li class=''><a href='members/rebates-pairing-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus</span></a></li>
                            <li ><a href='members/rebates-introducer.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Introducer Bonus</span></a></li>
                            <li><a href='members/rebates.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Bonus Summary</span></a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>

                                    <?php if(\App\Session::get('error')) {?>
                                        <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                <div class="alert alert-block alert-danger fade in">
                                                    <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                    <p><?php echo \App\Session::get('error')?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        \App\Session::UnsetKeySession('error');
                                    }?>


                                    <div class='col-md-12'>
                                        <div class='box border blue'>
                                            <div class='box-body' style='margin-top:10px;'>
                                                <form class='form-horizontal' action='' method='get'>
                                                    <input type='hidden' name='__req' value='1' />
                                                    <input type='hidden' name='type' value='l' />
                                                    <input type='hidden' name='nav' value='' />
                                                    <div class='form-group'>
                                                        <label class='col-md-2 control-label'>From:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='start' value=''>
                                                        </div>
                                                        <label class='col-md-1 control-label'>To:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='end' value=''>
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <input type='submit' class='btn btn-success' value="Generate" readonly/>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <h4 style='font-weight:bold;'>Total Leveling Bonus: <?php echo (empty($levelbonus)?0:$levelbonus)?>.00</h4>


                                        <?php if(!empty($allLevelBonuses)){?>
                                            <div class='nav_link'>
                                                <a href="members/rebates-leveling-bonus.php?__req=1&type=s&nav=1">First</a>
                                                <?php if(isset($_GET['nav']) && $_GET['nav']>1) { ?>
                                                    <a href="members/rebates-leveling-bonus.php?__req=1&type=s&nav=<?php echo ($_GET['nav']-1)?>">Previous</a>
                                                <?php }?>
                                                <?php for($x=1;$x<=ceil($perpage);$x++){?>
                                                    <a href="members/rebates-leveling-bonus.php?__req=1&type=s&nav=<?php echo $x?>"  style='color:red;'><?php echo $x?></a>
                                                <?php }?>
                                                <a href="members/rebates-leveling-bonus.php?__req=1&type=s&nav=<?php echo (isset($_GET['nav'])?$_GET['nav']+1:1)?>">Next</a>
                                                <a href="members/rebates-leveling-bonus.php?__req=1&type=s&nav=<?php echo $totalRow['total']?>">Last</a>
                                            </div>
                                        <?php }?>

                                        <table id='example-paper datatable1' class='table table-paper table-striped'>
                                            <thead><tr><th width='8%'>No.</th><th scope='col'>Type</th><th scope='col'>Description</th><th scope='col' class='center'>Status</th><th scope='col' class='center'>Amount</th><th scope='col'>Paid Date</th></tr></thead>
                                            <tbody>
                                            <?php if(isset($allLevelBonuses)) {?>
                                                <?php $i=1;foreach ($allLevelBonuses as $allLevelBonus){?>
                                                    <tr class='row1'>
                                                        <td><?php echo $i?></td>
                                                        <td>Leveling Bonus</td>
                                                        <td>Leg A (Username #<?php echo $user->getUserName($allLevelBonus['left_userID'])?>) <br>Leg B (Username #<?php echo $user->getUserName($allLevelBonus['right_userID'])?>)
                                                        <br>
                                                        Level : <?php
                                                        
                                                        // if($parent_level==0)
                                                        // {
                                                        //     echo ($allLevelBonus['bonus_level']-($parent_level+1));
                                                        // }else{
                                                        //     echo ($allLevelBonus['bonus_level']-$parent_level);
                                                        // }
                                                        echo ($allLevelBonus['bonus_level']-$parent_level);
                                                        
                                                        
                                                        ?> 
                                                        
                                                        <br>
                                                        
                                                        </td>
                                                        <td class='center'>Paid</td>
                                                        <td class='center'><?php echo $allLevelBonus['bonus_point']?>.00</td>
                                                        <td><?php  echo date('d-M-Y', strtotime( $allLevelBonus['bonus_date'] ));  ?></td>
                                                    </tr>
                                                <?php  $i++; }
                                            }?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>