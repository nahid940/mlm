<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
date_default_timezone_set('Asia/Dhaka');
$user=new \App\user\User();
$user->checkUserValidity(\App\Session::get('userID'));
$info=$user->getUserInfo();
$currency=$user->userCurrencies(\App\Session::get('userID'));

if(isset($_POST['submit_btn']))
{
    $sql = "select Renew_DC_Rate from conversion_rate";
    $stmt = \App\DBConnection::myQuery($sql);
    $stmt->execute();
    $rate = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($user->verifyUser(base64_encode($_POST['password']))) {
        $sql='update users set user_status=:user_status, expire_date=:expire_date where userID=:userID';
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':user_status',1);
        $stmt->bindValue(':expire_date',date('Y-m-d', strtotime($info['expire_date'] . ' +300 day')));
        $stmt->bindValue(':userID',\App\Session::get('userID'));
        $stmt->execute();
        
       
        $user->decrementDC($currency['dc']-$rate['Renew_DC_Rate'],\App\Session::get('userID'));
        $currency=$user->userCurrencies(\App\Session::get('userID'));
        
         $user->userCurrencyHistory(\App\Session::get('userID'),0,$rate['Renew_DC_Rate'],'Account Renewal',2,5,$rate['Renew_DC_Rate'],0,'',$currency['dc'],0);
        \App\Session::set('success','Account Renewal Successful !!');
        $info=$user->getUserInfo();
    }else
    {
        \App\Session::set('error','Invalid Security Password !!');
    }
}



?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>Account Renewal</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Account Renewal</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->


    <div class='row'>
        <div class='col-md-3'>
            <div class='btn btn-info btn-icon input-block-level' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'>
                    <?php
                    //$ec=$user->totalEc((\App\Session::get('userID')))+(empty($user->totalPairBonus(\App\Session::get('userID')))?0:$user->totalPairBonus(\App\Session::get('userID')));
                    echo ($currency['ec']) ?>
                    .00</div>
                <div>Electronic Currency</div>
            </div>
        </div>
        <div class='col-md-3'>
            <div class='btn btn-info btn-icon input-block-level' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo (($currency['rf']==0)?0:$currency['rf']) ?>.00</div>
                <div>Registration Fee</div>
            </div>
        </div>
        <div class='col-md-3'>
            <div class='btn btn-info btn-icon input-block-level' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo (($currency['dc']==0)?0:$currency['dc']) ?>.00</div>
                <div>Declaration Currency</div>
            </div>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li ><a href='members/upgrade.php'>Account Upgrade</a></li>

                            <li class='active'><a href='members/renew.php'>Account Renewal</a></li>
                            <li ><a href='members/register.php'>Registration</a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <?php if(\App\Session::get('error')) {?>
                                        <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                <br>
                                                <div style="width:1000px;margin-left:-100px;height:0px;">
                                                    <a style="float:left;" name="left">&nbsp;</a>
                                                    <a style="float:right;" name="right">&nbsp;</a>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="alert alert-block alert-danger fade in">
                                                    <h4><i class="fa fa-times"></i> There is / are some error(s), please correct them before continuing</h4>
                                                    <p><?php echo \App\Session::get('error')?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        \App\Session::UnsetKeySession('error');
                                    }?>

                                    <?php if(\App\Session::get('success')) {?>
                                        <div class="row" style="overflow-x:auto;margin-left:0px;margin-right:0px">
                                            <div class="col-md-12 profile-details">
                                                <br>
                                                <div style="width:1000px;margin-left:-100px;height:0px;">
                                                    <a style="float:left;" name="left">&nbsp;</a>
                                                    <a style="float:right;" name="right">&nbsp;</a>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="alert alert-block alert-success fade in">
                                                    <p><?php echo \App\Session::get('success')?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        \App\Session::UnsetKeySession('success');
                                    }?>

                                    <div class='col-md-12'>

                                        <?php
                                            if($info['expire_date']>=date('Y-m-d'))
                                            {
                                        ?>
                                        <div class='alert alert-block alert-danger fade in'>
                                            <h4><i class='fa fa-times'></i> There is / are some error(s), please correct them before continuing</h4>
                                            <p>Not yet expired.</p>
                                        </div>
                                        <?php } else {?>

                                                <div class="alert alert-info">Your account has expired please proceed renew your account.</div>

                                                <form name='withdraw_form' method='post' action='' class='form-horizontal form-bordered form-row-stripped'>

                                                    <div class='form-body'>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'>Current Expiry Date: *</label>
                                                            <div class='col-md-5'>
                                                                <?php echo  date('d-M-Y',strtotime($info['expire_date']))?>
                                                            </div>
                                                        </div>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'>New Expired Date: *</label>
                                                            <div class='col-md-5'>
                                                                <?php echo date('d-M-Y', strtotime($info['expire_date'] . ' +300 day'))?>
                                                            </div>
                                                        </div>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'>Security Password: *</label>
                                                            <div class='col-md-5'>
                                                                <input type="password" name="password" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class='form-group'>
                                                            <label class='col-md-3 control-label'></label>
                                                            <div class='col-md-5'>
                                                                <input type='submit' name='submit_btn' value='Submit' class='btn btn-primary' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>


                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>