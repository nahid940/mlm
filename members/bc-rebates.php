<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
date_default_timezone_set('Asia/Dhaka');
$user=new \App\user\User();
$invest=new \App\investment\Investment();

$user->checkUserValidity(\App\Session::get('userID'));

$levelbonus=$user->totalLevelBonus(\App\Session::get('userID'));

$ToatlintroducerBonus=$user->totalInvestmentIntroducerBonus(\App\Session::get('userID'));

// $previousPairingEc=$user->previousPairingEc(\App\Session::get('userID'));
// $todaysPairingbonus=$user->todaysPairingEc(\App\Session::get('userID'));
// $levelBonusSummery=$user->levelingBonusSummery(\App\Session::get('userID'));

$introducerBonusSummery=$user->introducerInvestmentBonusSummery(\App\Session::get('userID'));
?>
<?php include_once "includes/header.php";?>
    <div id="content" class="col-lg-12">
    <!-- PAGE HEADER-->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header">
                <!-- STYLER -->

                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="members/index.php">Home</a>
                    </li>
                    <li>Bonus Summary</li>
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                    <h3 class="content-title pull-left">Bonus Summery</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAGE HEADER -->

    <div class='row'>
        <div class='col-md-3'>
            <a class='btn btn-danger btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php echo (empty($ToatlintroducerBonus)?0:$ToatlintroducerBonus)?>.00</div>
                <div>Introducer Bonus</div>
            </a>
        </div>
        <div class='col-md-3'>
            <a class='btn btn-blueBox btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>
                <div style='font-size:20px;'><?php 
                    $val=$user->totalInvestmentPairBonus(\App\Session::get('userID'));
                    echo (empty($val)?0:$val)
                ?>.00</div>
                <div>Pairing  Bonus</div>
            </a>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-12'>
            <div class='box border'>
                <div class='box-title'>
                    <h4 style='height:15px;'></h4>
                </div>
                <div class='box-body'>
                    <div class='tabbable header-tabs user-profile'>
                        <ul class='nav nav-tabs'>
                            <li class=''><a href="members/bc-rebates-pairing-bonus-reports.php"><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus Reports</span></a></li>
                            <li class=''><a href='members/bc-rebates-pairing-bonus.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Pairing Bonus</span></a></li>
                            <li class=''><a href='members/bc-rebates-introducer.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Introducer Bonus</span></a></li>
                            <li class='active'><a href='members/bc-rebates.php'><i class='fa fa-picture-o'></i> <span class='hidden-inline-mobile'>Bonus Summary</span></a></li>
                        </ul>
                        <div class='tab-content'>
                            <div class='tab-pane fade in active'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='box border blue'>
                                            <div class='box-body' style='margin-top:10px;'>
                                                <form class='form-horizontal' action='' method='get'>
                                                    <input type='hidden' name='__req' value='1' />
                                                    <input type='hidden' name='type' value='' />
                                                    <input type='hidden' name='nav' value='' />
                                                    <div class='form-group'>
                                                        <label class='col-md-2 control-label'>From:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='start' value='' autocomplete="off">
                                                        </div>
                                                        <label class='col-md-1 control-label'>To:</label>
                                                        <div class='col-md-3'>
                                                            <input class='form-control datepicker' type='text' name='end' value='' autocomplete="off">
                                                        </div>
                                                        <div class='col-md-3'>
                                                            <input type='submit' class='btn btn-success' value="Generate"/>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <table id='example-paper' class='table table-paper table-striped'>
                                            <thead>
                                                <tr>
                                                    <th width='30%'>Bonus</th>
                                                    <th class='center'>Last Bonus</th>
                                                    <th class='center'>No. of Times</th>
                                                    <th class='center'>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            <?php if(isset($_GET['start']) && $_GET['start']!='' && isset($_GET['end']) && $_GET['end']!=''){ ?>
                                                <?php
                                                    $introducerBonus=$user->investmentIntroducerBonusSummeryByDateRange(\App\Session::get('userID'),$_GET['start'],$_GET['end']);
                                                ?>
                                                <tr class='row0'>
                                                    <td>Introducer Bonus</td>
                                                    <?php if(!empty($introducerBonus['totalBonus'])){?>
                                                        <td class='center'>
                                                            <?php
                                                                $datetime=$user->getLastTimeInvestmentBonusRecord(\App\Session::get('userID'),45);
                                                                echo $datetime['action_date'];
                                                            ?>
                                                        </td>
                                                    <?php } else {?>
                                                        <td class='center'>
                                                            -
                                                        </td>
                                                    <?php }?>
                                                    <td class='center'><?php echo (empty($introducerBonus['totalTimes'])?0:$introducerBonus['totalTimes'])?></td>
                                                    <td class='center'><?php echo (empty($introducerBonus['totalBonus'])?0:$introducerBonus['totalBonus'])?>.00</td>
                                                </tr>

                                                <tr class='row1'>
                                                    <?php
                                                        $pairbonusSummery=$user->investmentPairingBonusSummeryOfThisDate(\App\Session::get('userID'),$_GET['start'],$_GET['end']);
                                                    ?>
                                                    <td>Pairing Bonus</td>
                                                    <td class="center">
                                                        <?php
                                                        $datetime=$user->getLastTimeInvestPairBonusRecordByDate(\App\Session::get('userID'),$_GET['start'],$_GET['end']);
                                                        echo $datetime['bonus_date'];
                                                        ?>
                                                    </td>

                                                    <td class="center">
                                                        <?php echo (empty($pairbonusSummery['totalTimes'])?0:$pairbonusSummery['totalTimes'])?>
                                                    </td>

                                                    <td class='center'>
                                                        <?php echo (empty($pairbonusSummery['totalBonus'])?'-':$pairbonusSummery['totalBonus'])?>
                                                    </td>
                                                </tr>
                                            <?php }

                                            /**----------------------------------------------------------------------------------------------------------------**/

                                            else {?>
                                                <tr class='row0'>
                                                    <td>Introducer Bonus</td>
                                                    <?php if(!empty($introducerBonusSummery['totalBonus'])){?>
                                                        <td class='center'>
                                                            <?php
                                                            $datetime=$user->getLastTimeInvestmentBonusRecord(\App\Session::get('userID'),45);
                                                            echo (empty($datetime)?'-':$datetime['action_date'])
                                                            ?>
                                                        </td>
                                                    <?php } else {?>
                                                        <td class='center'>
                                                            -
                                                        </td>
                                                    <?php }?>

                                                    <td class='center'><?php echo (empty($introducerBonusSummery['totalTimes'])?0:$introducerBonusSummery['totalTimes'])?></td>
                                                    <td class='center'><?php echo (empty($introducerBonusSummery['totalBonus'])?0:$introducerBonusSummery['totalBonus'])?>.00</td>
                                                </tr>

                                                <tr class='row1'>
                                                    <td>Pairing Bonus</td>
                                                    <td class='center'>
                                                        <?php $datetime=$user->getLastTimeInvestmentPairBonusRecord(\App\Session::get('userID'));
                                                        echo (empty($datetime)?'-':date('d/m/Y',strtotime($datetime['bonus_date']))." ".$datetime['bonus_time']);
                                                        ?>
                                                    </td>
                                                    <td class='center'>
                                                        <?php 
                                                            $val=$user->getTotalTimeOfInvestmentPairingBonus(\App\Session::get('userID'));
                                                            echo (empty($val)?0:$val);
                                                        ?>
                                                    </td>
                                                    <td class='center'>
                                                        <?php echo (empty($val)?0:$val)?>.00
                                                    </td>
                                                </tr>
                                            <?php }?>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "includes/footer.php";?>

<?php }else {
    header('location:../login.php');
}?>