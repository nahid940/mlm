<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
if(\App\Session::get('login')==true) {
$user=new \App\user\User();

$user->checkUserValidity(\App\Session::get('userID'));
$tc_currency=$user->userCurrencies(\App\Session::get('userID'))['tc'];

$histories=$user->getInvestHistory(\App\Session::get('userID'));
$helper=new \App\Helper();

?>
<?php include_once "includes/header.php";?>
<div id="content" class="col-lg-12">
<!-- PAGE HEADER-->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <!-- STYLER -->

            <!-- /STYLER -->
            <!-- BREADCRUMBS -->
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="members/index.php">Home</a>
                </li>
                <li>Trading</li>
                <li>Trading Policy</li>
            </ul>
            <!-- /BREADCRUMBS -->
            <div class="clearfix">
                <h3 class="content-title pull-left">Trading History</h3>
            </div>
        </div>
    </div>
</div>
<!-- /PAGE HEADER -->


<div class='row'>
    
    <!--<div class='col-md-3'>-->
    <!--    <a class='btn btn-info btn-icon input-block-level' href='javascript:void(0);' style='margin-top: -20px; margin-bottom: 25px; cursor: default;'>-->
            <div style='font-size:20px;'><?php //echo ($tc_currency==0?'0.00':number_format($tc_currency,2))?></div>
    <!--        <div>Trade Currency</div>-->
    <!--    </a>-->
    <!--</div>-->
    
</div>
<div class='row'>
    <div class='col-md-12'>
        <div class='box border'>
            <div class='box-title'>
                <h4 style='height:15px;'></h4>
            </div>
            <div class='box-body'>
                <div class='tabbable header-tabs user-profile'>
                    <ul class='nav nav-tabs'>
                        <li class='active'><a href='members/invest_history.php'>Trading History</a></li>
                    </ul>

                    <div class='tab-content'>
                        <div class='tab-pane fade in active'>
                            <div class='row'>
                                <div class='col-md-12'>

                                    <table id='example-paper' class='table table-paper table-striped'>
                                        <thead>
                                            <tr>
                                                <th style='width: 8%;text-align:center;'>No.</th>
                                                <th style='width: 8%;text-align:center;'>Ref # </th>
                                                <th style='text-align:center;'>Date</th>
                                                <th style='text-align:center;'>Policy Name</th>
                                                <th style='text-align:center;'>Policy Type Type</th>
                                                <th style='text-align:center;'>Amount</th>
                                                <th style='text-align:center;'>Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=0; foreach ($histories as $history){ $i++?>
                                                <tr class='row0'>
                                                    <td style='text-align:center;'><?php echo $i?></td>
                                                    <td style='text-align:center;'>#<?php echo $helper->generateReference($history['investID'],$history['investID'])?></td>
                                                    <td style='text-align:center;'><?php echo date('d-m-Y',strtotime($history['invest_initial_date']))?></td>
                                                    <td style='text-align:center;'><?php echo $history['package_name']?></td>
                                                    <td style='text-align:center;'><?php echo $history['investment_type_name']?></td>
                                                    <td style='text-align:center;'><?php echo $history['invest_amount']?></td>
                                                    <td style='text-align:center;'><?php echo $history['invest_amount']/$history['investment_amount']?></td>
                                                </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once "includes/footer.php";?>
<?php }else {
    header('location:../login.php');
}?>