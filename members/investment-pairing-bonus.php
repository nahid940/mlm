<?php
include_once '../vendor/autoload.php';
\App\Session::init();
$helper=new \App\Helper();
$helper->checkTime();
date_default_timezone_set('Asia/Kuala_Lumpur');
$user=new \App\user\User();
//$user->checkUserValidity(\App\Session::get('userID'));
// $alluser=$user->getAlluserIDAndName();

$sql="select distinct currency_history.userID from currency_history

join invests on invests.userID=currency_history.userID

where currency_type=13 and invest_typeID=1";
$stmt=\App\DBConnection::myQuery($sql);
$stmt->execute();
$alluser=$stmt->fetchAll(PDO::FETCH_ASSOC);


$pair_bonus=1000; 
$percentage=7.5;


foreach ($alluser as $usr)
{
    
//     $client_level=$user->placementUserLevel($usr['username'])['client_level'];

//     $userGrade=$user->userGrade($usr['userID']);

//     $gradeInfo=$user->gradeGradeDetails($userGrade);
    
//     $pl=$gradeInfo['pairingLimit']; //Pairing limit

        // calculate total tc invest of each user
        
        
        $sql="select total_left,total_right from
        (select count(user_id) as total_left from client_positions where parent_id=:parent_id and hand=0  and action_date=:action_date) as leftuser, 
        (select count(user_id) as total_right from client_positions where parent_id=:parent_id and hand=1  and action_date=:action_date) as rightuser";

        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':parent_id',$usr['userID']);
        //$stmt->bindValue(':action_date',date('Y-m-d'));
        $stmt->bindValue(':action_date',date('Y-m-d', strtotime(' -1 day')));
        $stmt->execute();
        $res=$stmt->fetch(PDO::FETCH_ASSOC);
        //echo $usr['userID']."".$res['total_left']."".$res['total_right'];

        $sql="select left_carry,right_carry from invest_pairing_bonus where userID=:userID and bonus_date=:bonus_date";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':bonus_date',date('Y-m-d', strtotime(' -1 day')));
        $stmt->bindValue(':userID',$usr['userID']);
        $stmt->execute();
        $num = $stmt->rowCount();
        $carry_amount=$stmt->fetch(PDO::FETCH_ASSOC);
        if($num > 0)
        {
            $plc=$carry_amount['left_carry']; //previous left carry
            $prc=$carry_amount['right_carry']; //previous right carry 
        }
        else{
            $plc=0;
            $prc=0;
        }
        
        $nls=$res['total_left'];   // new left sales
        $nrs=$res['total_right'];  // new right sales
        
        $tls=$nls+$carry_amount['left_carry']; // total left sales
        
        $trs=$nrs+$carry_amount['right_carry']; // total right sales
        
        if($tls>$trs){
		
            $rc=0;// Right carry

            if($trs>$pl)
            {   
                $nop=$pl; // Number of people
                 
                $lc=$tls-$nop; // Left carry
                
                $tls=$pl; // New Total Left sale
				
                $bonus=(($nop*$pair_bonus)*$percentage)/100;
				
                $trs=$nop;
            }
			else{
				$nop=$trs; // Number of people
				
				$lc=$tls-$trs; // Left carry
				
				$tls = $nop;
				
				$bonus=(($nop*$pair_bonus)*$percentage)/100;
			}
            
        }
		else if($trs>$tls){
            
			$lc=0;// Left carry
            
            if($tls>$pl){  
                $nop=$pl; // Number of people
                
                $rc=$trs-$nop; // Right carry
             
                $trs=$pl; // New Total right sale
                
                $bonus=(($nop*$pair_bonus)*$percentage)/100;
                
                $tls=$nop;
            }
			else{
				$nop=$tls; // Number of people
				
				$rc=$trs-$tls; // Right carry
				
				$trs = $nop;
				
				$bonus=(($nop*$pair_bonus)*$percentage)/100;
			}
        }
		else if($tls==$trs) {
        
            $rc=0; // Right carry
            
            if($tls>$pl)
            {
                 $nop=$pl; // Number of people
                 
                 $lc=$tls-$nop; // Left carry
                 
                 $tls=$pl; // New Total Left sale
                 
                 $bonus=(($nop*$pair_bonus)*$percentage)/100;
                 
                 $trs=$nop;
            }
			else{
				$nop=$tls; // Number of people
				
				$lc=$tls-$nop; // Left carry
				
				$tls = $nop;
				
				$bonus=(($nop*$pair_bonus)*$percentage)/100;
			}
        }
        
             
        $sql="select tableID from invest_pairing_bonus where bonus_date=:bonus_date and userID=:userID";
        $stmt=\App\DBConnection::myQuery($sql);
        $stmt->bindValue(':userID',$usr['userID']);
        $stmt->bindValue(':bonus_date',date('Y-m-d'));
        $stmt->execute();
        $res1=$stmt->fetch(PDO::FETCH_ASSOC);
        if(empty($res1))
        {
            $sql='insert into invest_pairing_bonus (userID,bonus_date,bonus_time,total_user_left,total_user_right,left_carry,right_carry,bonus)
        values(:userID,:bonus_date,:bonus_time,:total_user_left,:total_user_right,:left_carry,:right_carry,:bonus)';

            $stmt=\App\DBConnection::myQuery($sql);
            $stmt->bindValue(':userID',$usr['userID']);
            $stmt->bindValue(':bonus_date',date('Y-m-d'));
            $stmt->bindValue(':bonus_time',date('h:i:s A'));
            $stmt->bindValue(':total_user_left',$tls);
            $stmt->bindValue(':total_user_right',$trs);
            $stmt->bindValue(':left_carry',$lc);
            $stmt->bindValue(':right_carry',$rc);
            $stmt->bindValue(':bonus',$bonus);
            $stmt->execute();

        //     /**-----------------------------store bonus electronic currency for upline user---------------**/
        
            // $user->storeElectronicCurrency($usr['userID'],$bonus); //$id,$bonus_point,$level,$left_userID,$right_userID,$bonus_type)
            if($bonus>=1){
                $bc_currency = $user->userCurrencies($usr['userID'])['bc'];
                $user->storeParentBonus($usr['userID'], $bonus, 0, 0,0, 11); //11 invest pair bonus
                // $userID,$receiverID,$amount,$description,$currency_type,$reason,$debit,$credit,$remark,$total,$receiver_total
                $user->userInvestmentCurrencyHistory($usr['userID'],0,$bonus,'Investment Pairing Bonus',100,100,0,$bonus,"Investment Pair bonus",$ec_currency,0);
            }
            
        }
}


