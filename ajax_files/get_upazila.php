<?php
include_once '../vendor/autoload.php';

if(isset($_POST['district']))
{
    $sql="select upazila_id,name from upazilas where district_id=:district_id";
    $stmt=\App\DBConnection::myQuery($sql);
    $stmt->bindValue(':district_id',$_POST['district']);
    $stmt->execute();
    $lists=$stmt->fetchAll(PDO::FETCH_ASSOC);
}

?>
<option value="">Please Select Division</option>
<?php foreach ($lists as $upazila){?>
    <option value="<?php echo $upazila['upazila_id']?>"><?php echo $upazila['name']?></option>
<?php }?>


