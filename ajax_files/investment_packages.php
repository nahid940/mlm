<?php
include_once '../vendor/autoload.php';
$invest=new \App\investment\Investment();

if(isset($_POST['id']))
{ 
    $lists=$invest->getTypeWisePackage($_POST['id']);
}
?>
<select class='form-control' name='packageID' id="package_info" required>
    <option value=''>Select Package</option>
    <?php foreach($lists as $list){?>
        <option value="<?php echo $list['packageID']?>"><?php echo $list['package_name']?></option>
    <?php }?>
</select>