<?php
include_once '../vendor/autoload.php';
if(isset($_POST['packageID']))
{
    $sql="select package_name,investment_amount,package_duration,return_rate,investment_type_name from investment_packages
    join investment_types on investment_types.investment_typeID=investment_packages.investment_typeID
    where package_status=1 and packageID=:packageID
    ";
    $stmt=\App\DBConnection::myQuery($sql);
    $stmt->bindValue(':packageID',$_POST['packageID']);
    $stmt->execute();
    $list=$stmt->fetch(PDO::FETCH_ASSOC);
    
    
    $sql="select product_name,quantity,amount from package_details where packageID=:packageID";
    $stmt=\App\DBConnection::myQuery($sql);
    $stmt->bindValue(':packageID',$_POST['packageID']);
    $stmt->execute();
    $packages=$stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<input type='hidden' id="packege_amount" data-val="<?php echo $list['investment_amount']?>">

<div class='col-md-4' style="border:1px solid #cccccc;margin-top:14px">
    <h4>Policy Details</h4>
    <h5><b>Policy Name:</b> <?php echo $list['package_name']?></h5>
    
    <table class="table">
        <thead>
            <tr>
                <td>Product Name</td>
                <td>Quantity</td>
                <td>Amount</td>
            </tr>
        </thead>
        
        <tbody>
            <?php foreach($packages as $pck) {?>
                <tr>
                    <td><?php echo $pck['product_name']?></td>
                    <td><?php echo $pck['quantity']?></td>
                    <td><?php echo $pck['amount']?></td>
                </tr>
            <?php
                $total[]=$pck['amount'];
            }?>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Total :<?php echo array_sum($total)?></td>
                </tr>
        </tbody>
    </table>
    <table class="table">
        <tr>
            <td>Policy Type:</td>
            <td><?php echo $list['investment_type_name']?></td>
        </tr>
        <tr>
            <td>Duration:</td>
            <td><?php echo $list['package_duration']?> Days</td>
        </tr>
        <tr>
            <td> Rate:</td>
            <td><?php echo $list['return_rate']?></td>
        </tr>
    </table>
</div>
<?php }?>